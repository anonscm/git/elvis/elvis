#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
import sys
from elvis import SessionTools, AddressBookTools
from elvis.data import  Address

class ServiceAddressBook:
  def __init__(self):
    self.sessionTools = SessionTools()
    self.addressBookTools = AddressBookTools()

  @ServiceMethod
  def getAddress(self, ids ):
    # For test, to be removed
    listObj = self.addressBookTools.getAddress(ids)
    listDict = []
    for obj in listObj:
      listDict.append(obj.toDict())
    return listDict

  @ServiceMethod
  def searchAddress(self, name ):
    # For test, to be removed
    return self.addressBookTools.searchAddress(lastname = name)

if __name__ == "__main__":
  service = ServiceAddressBook()
  handleCGI(service)
