#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod
import sys

import config
from elvis import SessionTools, PlantTools, NotationTools
import elvis.data.NotationContext

class ServiceNotation:
  def __init__(self):
    self.sessionTools = SessionTools()
    self.notationTools = NotationTools()
    self.plantTools = PlantTools()

  @ServiceMethod
  def getNotationIds(self, sessionId, contextNotationId, lotId):
    #TODO Add getFilteredGroupIds
    groupsIds = self.sessionTools.getGroupIds(sessionId)
    notations = []
    ids = self.notationTools.getNotationIdsForLot(groupsIds, lotId, contextNotationId)
    notations = self.notationTools.getNotations(groupsIds, ids)

    return notations

  @ServiceMethod
  def getNotationsForVariety(self, sessionId, groupIds, contextNotationId, varId):
    fGroupsIds = self.sessionTools.getGroupIds(sessionId)
    vGroupIds = []
    for g in groupIds :
      if g in fGroupsIds :
        vGroupIds.append(g)
    notations = []
    ids = self.notationTools.getNotationIdsForVariety(vGroupIds, varId, contextNotationId)
    notations = self.notationTools.getNotations(vGroupIds, ids)

    return notations

  @ServiceMethod
  def getTypeNotationByGroup(self, sessionId, groupIds):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    typeNotations = []
    typeNotations = self.notationTools.getTypeNotationByGroup(fGroupIds)
    return typeNotations

  @ServiceMethod
  def getValuesNotationsByTypeNotationAndGroup(self, sessionId, groupIds, typeNotIds):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    valueNotations = []
    valueNotations = self.notationTools.getValuesNotationsByTypeNotationAndGroup(fGroupIds, typeNotIds)
    return valueNotations

  @ServiceMethod
  def saveNotationsForLots(self, sessionId, groupIds, notationsData, idExperiment, type):
    """
    :param notationsData: {idLot : liste de objets Notation.js}
    :param idExperiment:  idExperiment
    :param type:          "seed" or "tree"
    :type notationsData: dict
    :type idExperiment:  Integer
    :type type:          String
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    result = {"lots_found" : [], "lots_not_found" : []}

    try:
      for (idLot, notations) in notationsData.items():
        lot = None
        if (len(notations)):
          try:
            lot = self.plantTools.getLot(id = idLot, type = type)
          except Exception as e:
            result["lots_not_found"].append(idLot)
          else:
            result["lots_found"].append(idLot)
          notationIds = []
          try:
            notationIds = self.notationTools.saveNotations(fGroupIds, notations)
          except Exception as e:
            sys.stderr.write("Exception save: {0}\n".format(e))
            raise Exception("save has failed")
          try:
            self.notationTools.linkNotationsToLot(lot.getId(), notationIds)
            # self.notationTools.linkNotationsToLot(idLot, notationIds)
          except Exception as e:
            sys.stderr.write("Exception link to lot: {0}\n".format(e))
            raise Exception("link lot failed")
          try:
            self.notationTools.linkNotationsToExperiment(fGroupIds, notationIds, idExperiment)
          except Exception as e:
            sys.stderr.write("Exception link to experiment: {0}\n".format(e))
            raise Exception("link exp failed")
      if len(result["lots_not_found"]):
        raise Exception("Some lots not found")
    except Exception as e:
      sys.stderr.write("Exception: {0}\n".format(e))
      self.notationTools.rollback()
    else:
      self.notationTools.commit()
    return result

  @ServiceMethod
  def saveNotationsForVarieties(self, sessionId, groupIds, notationsData, idExperiment):
    """
    :param notationsData: {idVariety : liste de objets Notation.js}
    :param idExperiment:  idExperiment
    :type notationsData: dict
    :type idExperiment:  Integer
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    result = {"variety_found" : [], "variety_not_found" : []}
    try:
      for (idVar, notations) in notationsData.items():
        var = None
        try:
          var = self.plantTools.getVarietiesFromListId([idVar])
        except Exception as e:
          result["variety_not_found"].append(idVar)
        else:
          result["variety_found"].append(idVar)
        notationIds = []
        try:
          notationIds = self.notationTools.saveNotations(fGroupIds, notations)
        except Exception as e:
          raise Exception("save has failed")
        try:
          self.notationTools.linkNotationsToVariety(var[0].getId(), notationIds)
        except Exception as e:
          raise Exception("link lot failed")
        try:
          self.notationTools.linkNotationsToExperiment(fGroupIds, notationIds, idExperiment)
        except Exception as e:
          raise Exception("link exp failed")
      if len(result["variety_not_found"]):
        raise Exception("Some varieties not found")
    except Exception as e:
      self.notationTools.rollback()
    else:
      self.notationTools.commit()
    return result

  @ServiceMethod
  def saveNotationsForTaking(self, sessionId, groupIds, notationsData, idExperiment):
    """
    TODO pour completer son fonctionnement getTakingFromListId and linkNotationsToTaking

    :param notationsData: {idTaking : liste de objets Notation.js}
    :param idExperiment:  idExperiment
    :type notationsData: dict
    :typt idExperiment: Integer
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    result = {"taking_found" : [], "taking_not_found" : []}
    try:
      for (idTak, notations) in notationsData.items():
        var = None
        try:
          tak = self.sampleTools.getTakingFromListId([idTak])
        except Exception as e:
          result["taking_not_found"].append(idTak)
        else:
          result["taking_found"].append(idTak)
        notationIds = []
        try:
          notationIds = self.notationTools.saveNotations(fGroupIds, notations)
        except Exception as e:
          raise Exception("save has failed")
        try:
          self.notationTools.linkNotationsToTaking(tak[0].getId(), notationIds)
        except Exception as e:
          raise Exception("link lot failed")
        try:
          self.notationTools.linkNotationsToExperiment(fGroupIds, notationIds, idExperiment)
        except Exception as e:
          raise Exception("link exp failed")
      if len(result["variety_not_found"]):
        raise Exception("Some varieties not found")
    except Exception as e:
      self.notationTools.rollback()
    else:
      self.notationTools.commit()
    return result

  @ServiceMethod
  def getContext(self, sessionId, groupIds):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    list = []
    listR = []
    listContext = self.notationTools.getNotationContextList(groupIds)
    for c in listContext:
      nc = elvis.data.NotationContext()
      nc.setId(c['id'])
      nc.setName(c['name'])
      nc.setComment(c['comment'])
      nc.setUserGroupList(None)
      # Ajoute les types de notation
      listTypeNot = self.notationTools.getNotationTypesForContext (groupIds, nc.getId())
      nc.setTypeNotList(listTypeNot)
      list.append(nc)
    for nc in list:
      listR.append(nc.toDict())
    return listR

if __name__ == "__main__":
  service = ServiceNotation()
  handleCGI(service)
