#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

class ServiceTest:

  @ServiceMethod
  def ping(self):
    return "Ça fonctionne"

  @ServiceMethod
  def echo(self, data):
    return data

  @ServiceMethod
  def date(self):
    #return "new Date(Date.UTC(2011,0,1))"
    return dt.today()

if __name__=="__main__":
  service=ServiceTest()
  handleCGI(service)
