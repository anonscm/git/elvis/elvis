#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
from selfruit.VarieteTools import *

class ServiceVariete:
  """Cette classe repertorie tout les services disponibles
      Pour chaque service present il y a une fonction correspondante dans
      VarieteTools.
      Se referer donc a la documentation de VarieteTools"""

  def __init__(self):
    self.tool = VarieteTools()


    self._declareService("testDate")
#///////////////////////////Methodes verifiees/////////////////////////////////#
    self._declareService("parseLigneIntro")
    self._declareService("parseLigneNotaVerger")
    self._declareService("parseLigneNotaNivUn")
    self._declareService("getDroitLogin")
    self._declareService("getIdPays")
    self._declareService("getIdPaysByTabPays")
  #Pour le numero de prelevement suivant, faire resultat+1
    self._declareService("getIdNextPrelevement")
  #Pour le numero de groupe suivant, faire resultat+1
    self._declareService("getIdNextGroupe")
    self._declareService("getCroisementByAnneeIdCouple")
    self._declareService("getNbClones")
    self._declareService("getCloneNumVarNomIdCloneByNom")
    self._declareService("getCloneIdCloneNumByNum")
    self._declareService("getAllClone")
    self._declareService("getNomVarByIdClone")
    self._declareService("getCloneByVar")
    self._declareService("getCloneByNomVarORByNum")
    self._declareService("getCloneByNum")
  #Permet l'affichage des informations sur les varietes dispo dans une pepiniere donnee
    self._declareService("getVarByPep")
  #Renvoie les noms de toutes les pepinieres
    self._declareService("getNomPepAll")
    self._declareService("getAllParcelle")
    self._declareService("getAllPlaceByParcelle")
  #Renvoie toutes les informations concernant les pepinieres
    self._declareService("getInfoPepAll")
    self._declareService("getInfoPepByNom")
    self._declareService("getOrCreateConfidentiel")
    self._declareService("getPepinieristeParNom")
    self._declareService("getPepinieristeParNomByTab")
  #Retourne le numero du groupe, le site, le nom de la pepiniere, la parcelle, le rang,
  #l'id_clone et la desciption du groupe pour un groupe donner
    self._declareService("getGroupeById")
  #Renvoie id_parent_male, id_parent_fem, id_croisement et le nombre de pepins pour chaque croissement
    self._declareService("getCroisement")
  #Renvoie les infos("id_prelevement, nb_pepins_prel, id_croisement, annee, couple, nb_pepins) pour chaque prelevement
    self._declareService("getPrelev")
  #Renvoie les infos("id_lot, id_prelevement, gr_parent_femelle, gr_parent_male, site, parcelle, rang) pour chaque lot
    self._declareService("getLotInfo")
  #Renvoie les infos("num_lot, sum("nb_pepins) as nb_totaux, sum("nb_pepins_prel) as nb_prel,
  #sum("nb_pepins)-sum("nb_pepins_prel) as nb_frigo, nbre_valide) pour chaque lot
    self._declareService("getLotAll")
  #Renvoie les infos("num_lot,c1.numero,c2.numero,croisement.annee,croisement.nb_pepins)pour chaque lot ou pour un lot si lot=None
    self._declareService("getLotInfoByLot")
  #Renvoie le numero de lot du ou des clones passer en parametre
    self._declareService("getLotByClone")
  #Renvoie le numero du lot ou liste les numeros de lot si lot=None
    self._declareService("getNumLotByLot")
  #Renvoie toutes les informations d'un croisement,id_croisement, Femelle, Male, annee, nbPepins totaux et restant
    self._declareService("getCroisementAll")
  #Retoune tout les infos suivantes des emplacement("id_emplacement, numero, annee_premiere_pousse, porte_greffe.valeur, pepinieriste.nom, site.nom_site, parcelle, rang, position )
    self._declareService("getEmp")
  #Retoune tout les infos suivantes de l'emplacement passer en parametre("id_emplacement, numero, annee_premiere_pousse, porte_greffe.valeur,pepinieriste.nom, site.nom_site, parcelle, rang, position )
    self._declareService("getEmpBySite")
  #Meme resultat que getEmp(") mais ordoner par longueur de parcelle, parcelle, rang et id_emplacement
    self._declareService("getEmpOrdered")
  #Retourne ("nom_variete.nom, pep_reel.nom as pepiniere, site_reel.nom_site as site, annee_premiere_pousse) pour chaque hybride
    self._declareService("getHybride")
    self._declareService("getCroisement")
    self._declareService("getEssai")
    self._declareService("getNotaTavelureByLot")
    self._declareService("getEmpByRangParcPos2")
    self._declareService("getNotaOidiumeByLot")
    self._declareService("getClasseObsTavelureByLot")
    self._declareService("getCopieByTestAndByClone")
    self._declareService("getCopieAllByClone")
    self._declareService("getNumBlocByIdBloc")
    self._declareService("getCaraCondExpByEssai")
    self._declareService("getInocByCopie")
    self._declareService("getCaraRensByCopie")
    self._declareService("getNbLectByCopie")
    self._declareService("getInfoLectByLect")
    self._declareService("getIdPaysByPays")
    self._declareService("getIdPepByPep")
    self._declareService("getIdPepByPepAndVille")
    self._declareService("getIdSiteByPepAndSite")
    self._declareService("getSiteByPep")
    self._declareService("getAllSites")
  #retrourne en plus l'id du dernier tuple inserer
    self._declareService("setPep")
    self._declareService("setPepByTab")
    self._declareService("setSite")
    self._declareService("setPays")
    self._declareService("getPays")
    self._declareService("getPep")
    self._declareService("scanCreateGroupeUniByParc")
    self._declareService("scanCreateGroupeCoupleByParc")
    self._declareService("getParcelle")
    self._declareService("getParcelleBySite")
    self._declareService("getSiteByPepSite")
    self._declareService("getOrCreateSiteByTabPepSite")
#------------------------------------------------------------------------------#
#--------------FONCTIONS LIEES AUX CLONES-VARIETES-EMPLACEMENT-----------------#
#------------------------------------------------------------------------------#
    self._declareService("getStatutBio")
    self._declareService("getOrCreateStatutBio")
    self._declareService("getOrigine")
    self._declareService("getVar")
    self._declareService("getVarByNom")
    self._declareService("getType")
    self._declareService("getEmpByClone")
    self._declareService("getIdNomVarByVal")
    self._declareService("getIdStatutByVal")
    self._declareService("getIdOrigineByDesc")
    self._declareService("getIdPorteGreffeByVal")
    self._declareService("getIdNomVarieteByNom")
    self._declareService("getOrCreateIdNomVarieteByTabNom")
    self._declareService("getOrCreateNomVar")
    self._declareService("getIdCloneByNum")
  #retrourne en plus l'id du dernier tuple inserer
    self._declareService("setVar")
    self._declareService("setTaxinomie")
    self._declareService("getIdTaxinomieByGenreEspece")
    self._declareService("getTaxinomie")
    self._declareService("getTaxinomieById")
    self._declareService("setIntroductionVar")
    self._declareService("setNomVar")
    self._declareService("getMaxIdVar")
    self._declareService("getCaraByVal")
    self._declareService("setCaraByTable")
    self._declareService("getIdCloneByIdVar")
    self._declareService("getIdCloneByNomVar")
    self._declareService("getPorteGreffeAll")
  #retrourne en plus l'id du dernier tuple inserer
    self._declareService("setClone")
    self._declareService("setCloneByTab")
    self._declareService("getTestEmp")
    self._declareService("getTestIntro")
    self._declareService("setIntroduction")
    self._declareService("setExpedition")
    self._declareService("setContenuExp")
    self._declareService("getAllTypeContenu")
    self._declareService("getAllExped")
    self._declareService("getAllIntro")
    self._declareService("getExpeditionDetailById")
    self._declareService("getIntroductionDetailById")
    self._declareService("getContenuExpMaxId")
    self._declareService("getExpeditionById")
    self._declareService("getIdEmplacementNumCloneByGroupe")
    self._declareService("getIdGroupeDesByIdClone")
    self._declareService("getUniqueParcBySite")
  #------------------------------------------------------------------------------#
  #-------------------FONCTIONS LIES AUX CROISEMENTS-PRELEVEMENTS----------------#
  #------------------------------------------------------------------------------#
    self._declareService("getNotationByGrpAnSite")
    self._declareService("setCroisement")
    self._declareService("getTypeNotationById")
    self._declareService("getNotationBy")
  #retrourne en plus l'id du dernier tuple inserer
    self._declareService("setEmplacement")
    self._declareService("setEmplacementByTab")
    self._declareService("setPrelevement")
    self._declareService("setLot")
    self._declareService("setLotEmp")
    self._declareService("setEmpGrp")
    self._declareService("setClasseObs")
    self._declareService("setNotationTav")
    self._declareService("setNotationOidi")
    self._declareService("setNotationLot")
    self._declareService("setCouple")
    self._declareService("getIdGroupeByTabGroupeNbEmpDesc")
#///////////////////////Methodes impossible a verifiees////////////////////////#
    self._declareService("getExistNotation")
    self._declareService("getIdLot")
    self._declareService("getIdCouple")
  #permet l'affichage des infos pour tous clones
    self._declareService("getClones")
    self._declareService("getCloneDistinctId")
    self._declareService("getSearchClone")
    self._declareService("getNumCloneByIdCopie")
    self._declareService("getOrCreateVarNomVar")
    self._declareService("getCloneByNum")
    self._declareService("setIntroVar")
    self._declareService("getPossPrelevPep")
    self._declareService("getVarById")
#------------------------------------------------------------------------------#
#---------------FONCTIONS LIES A LA GESTION D'ECHANTILLONS---------------------#
#------------------------------------------------------------------------------#
    self._declareService("getAllTypeTissu")
    self._declareService("getIdTypeTissu")
    self._declareService("getTypeTypeTissu")
    self._declareService("getIdTypeTissuByType")
    self._declareService("getTypeTypeTissuById")
    self._declareService("setTypeTissu")
    self._declareService("getAllTypeContenant")
    self._declareService("getIdTypeContenant")
    self._declareService("getTypeTypeContenant")
    self._declareService("getIdTypeContenantByType")
    self._declareService("getTypeTypeContenantById")
    self._declareService("setTypeContenant")
    self._declareService("getFreePosByBoite")
    self._declareService("getAllContenant")
    self._declareService("getContenantById")
    self._declareService("getContenantByPosBoite")
    self._declareService("getContenantByPosBoite2")
    self._declareService("updateEtatContenant")
    self._declareService("updateCodeContenant")
    self._declareService("updateCodeContenant2")
    self._declareService("getContenantByBoite")
    self._declareService("getContenantByEmp")
    self._declareService("getContenantByEtat")
    self._declareService("setContenant")
    self._declareService("setAction")
    self._declareService("getAllPrelevement2")
    self._declareService("getPrelevement2ById")
    self._declareService("setPrelevement2")
    self._declareService("getAllTypeBoite")
    self._declareService("getIdTypeBoite")
    self._declareService("getTypeTypeBoite")
    self._declareService("getMaxPosByBoite")
    self._declareService("getIdTypeBoiteByType")
    self._declareService("getMaxLCBoiteById")
    self._declareService("setTypeBoite")
    self._declareService("getAllTypeBoite")
    self._declareService("getAllBoite")
#    self._declareService("getIdBoite")
#    self._declareService("getBoite")
    self._declareService("getIdBoiteByNom")
    self._declareService("getBoiteById")
    self._declareService("setBoite")
    self._declareService("getAllTypeAction")
    self._declareService("getIdTypeAction")
    self._declareService("getTypeTypeAction")
    self._declareService("getIdTypeActionByType")
    self._declareService("getTypeTypeActionById")
    self._declareService("setTypeAction")
    self._declareService("getAllEtatContenant")
    self._declareService("getIdEtatContenant")
    self._declareService("getTypeEtatContenant")
    self._declareService("getIdEtatContenantByType")
    self._declareService("getTypeEtatContenantById")
    self._declareService("setEtatContenant")
    self._declareService("setPosition")
    self._declareService("getAllContenantByBoite")
    self._declareService("deleteContenant")
    self._declareService("deletePosition")
#------------------------------------------------------------------------------#
#-----------------------FONCTIONS LIES A LA GESTION DES DROITS-----------------#
#------------------------------------------------------------------------------#
    self._declareService("getAllGhuser")
    self._declareService("getAllActiveGhuser")
    self._declareService("getAllGhgroup")
    self._declareService("getIdUserByLogin")
    self._declareService("getGhuserByNom")
    self._declareService("getGhgroupByUserId")
    self._declareService("getUserIdByGhgroupId")
    self._declareService("getDefaultGroupByGhuserId")
    self._declareService("getActiveGroupByGhuserId")
    self._declareService("getActiveUserByGhgroupId")
    self._declareService("setGhuser")
    self._declareService("setGhgroup")
    self._declareService("setBelongsTo")
    self._declareService("setGhuserActive")
    self._declareService("setGhgroupActive")
#------------------------------------------------------------------------------#
#------------------------FONCTIONS LIES AUX COLLECTIONS------------------------#
#------------------------------------------------------------------------------#
    self._declareService("addCloneToCol")
    self._declareService("getCollectionByNom")
    self._declareService("getAllCollection")
    self._declareService("getVarByCol")
    self._declareService("getCloneByVarAndCol")
    self._declareService("addCollection")
#------------------------------------------------------------------------------#
#------------------------FONCTIONS LIES AUX NOTATIONS--------------------------#
#------------------------------------------------------------------------------#
    self._declareService("setNotation")
    self._declareService("setNotationVariete")
    self._declareService("setLienNotationVariete")
    self._declareService("getIdNextNota")
#    self._declareService("setLienContexteNotation ")
    self._declareService("setTypeNotation")
    self._declareService("setContrainteTypeNotation")
    self._declareService("setContexteNotation")
    self._declareService("getNotaByContexte")
    self._declareService("getTypeNotationByContexte")
    self._declareService("getTypeNotaByContType")
    self._declareService("getContrainteByTypeNotation")
    self._declareService("getAllTypeNotation")
    self._declareService("getAllContrainteNotation")
    self._declareService("getAllContexte")
#------------------------------------------------------------------------------#
#-----------------------------------UPDATE-------------------------------------#
#------------------------------------------------------------------------------#
    self._declareService("upPep")
    self._declareService("upVar")
    self._declareService("upClone")
    self._declareService("upEmp")


  def _declareService(self, service_name):
    try :
      request = getattr(self.tool, service_name)
      request.__func__.IsServiceMethod = True
      setattr(self, service_name, request)
    except AttributeError, e:
      return "Error in declareService: " + e


if __name__ == "__main__":
  service = ServiceVariete()
  handleCGI(service)
