#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod
from socket import gethostname, gethostbyname
import os
import sys

import config

from elvis import SessionTools

from jsonrpc import dumps

class ServiceSession:

  def __init__(self):
    self.tools = SessionTools()

  @ServiceMethod
  def echo(self, t):
    return self.tools.echo(t)

  def getIp(self):
    ip = None
    try:
      ip = os.environ["REMOTE_ADDR"]
    except:
      pass
    if not ip:
      ip = gethostbyname(gethostname())
    return ip

  @ServiceMethod
  def sessionConnect(self, sessionId):
    mess = {}
    ip = self.getIp()
    mess = self.tools.sessionConnect(sessionId, ip)
    return mess

  @ServiceMethod
  def getSalt(self, sessionId):
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      #return self.tools.getSalt(sessionId)
      return self.tools.updateSalt(sessionId)
    return None

  @ServiceMethod
  def userLogin(self, sessionId, login, credential):
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      return self.tools.userLogin(sessionId, login, credential)
    return False

  @ServiceMethod
  def userLogout(self, sessionId):
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      return self.tools.userLogout(sessionId)
    return None

  @ServiceMethod
  def getUserInfos(self, sessionId):
    #sys.stderr.write(dumps(sessionId) + '\n')
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      return self.tools.getUserInfos(sessionId)
    return None

  @ServiceMethod
  def getUserGroups(self, sessionId):
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      return self.tools.getGroupsInfos(sessionId)
    return None

  @ServiceMethod
  def getFilteredGroupIds(self, sessionId, groupIds):
    """
    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    ip = self.getIp()
    if self.tools.sessionValidate(sessionId, ip):
      return self.tools.getFilteredGroupIds(sessionId, groupIds)
    return None


if __name__=="__main__":
  service=ServiceSession()
  handleCGI(service)
