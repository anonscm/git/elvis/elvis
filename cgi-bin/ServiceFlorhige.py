#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
from elvis.florhigeTools import *

class ServiceFlorhige:
  """Cette classe repertorie tout les services disponibles
      Pour chaque service present il y a une fonction correspondante dans
      florhigeTools.
      Se referer donc a la documentation de florhigeTools"""

  def __init__(self):
    self.tool = florhigeTools()

#///////////////////////////Methodes verifiees/////////////////////////////////#
    self._declareService("getAllSites")
    self._declareService("getAllTypeNom")
    self._declareService("getAllGenre")
    self._declareService("getAllEspece")
    self._declareService("getAllPepinieriste")
    self._declareService("createNomVariete")
    self._declareService("updateOrCreateVariete")
    self._declareService("getTaxinomie")
    self._declareService("setTaxinomie")
    self._declareService("getTypeNom")
    self._declareService("getTypeNotationVariete")
    self._declareService("getTypeNomVariete")
    self._declareService("getReferenceByValeurNotation")
    self._declareService("getReferenceByValeurBibliographie")
    self._declareService("getReferenceByValeurTaxinomie")
    self._declareService("getReferenceByValeurInformation")
    self._declareService("getVarieteById")
    self._declareService("getNomsByVariete")
    self._declareService("getNotationByVariete")
    self._declareService("getPepinieriste")
    self._declareService("getDistinctNotation")
    self._declareService("getDistinctTaxinomie")
    self._declareService("deleteNomVariete")
    self._declareService("updateNotation")
    self._declareService("getAllObtenteur")
    self._declareService("createNotation")
    self._declareService("getTypeNotation")
    self._declareService("createVarieteANotation")
    self._declareService("getNotationByIdNoType")
    self._declareService("getObtenteurByIdVar")





  def _declareService(self, service_name):
    try :
      request = getattr(self.tool, service_name)
      request.__func__.IsServiceMethod = True
      setattr(self, service_name, request)
    except AttributeError, e:
      return "Error in declareService: " + e


if __name__ == "__main__":
  service = ServiceFlorhige()
  handleCGI(service)
