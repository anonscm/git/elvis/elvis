#!/usr/bin/env python3

import sys
import config
from elvis.glamsTools_sample import glamsTools_sample
from jsonrpc import handleCGI, ServiceMethod

class glamsService:

  """
  Methods concerning the project database
  Rewritted from anandbService.py (2017-02)

  AUTHORS :
  - Sylvain GAILLARD IRHS Bioinfo team
  - Sandra PELLETIER IRHS Bioinfo team
  - Hervé Andres (M2 2017)

#  SECTIONS :
#  - Select all
#  - Create a project
#  - View details for a project
#  - Update details for a project
#  - Delete details for a project

  TODO :

  """



#--------------------------------#
#  ADD Service GlamsSample  #
#--------------------------------#
  @ServiceMethod
  def getLotGeneticsInformationsByPrelevment(self, idSample, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getLotGeneticsInformationsByPrelevment(idSample)
    return res

  @ServiceMethod
  def getInfosPlantsByPrelevment(self, idSample, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosPlantsByPrelevment(idSample, contexte)
    return res

  @ServiceMethod
  def getInfosPrelevmentByPrelevment(self, idSample, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosPrelevmentByPrelevment(idSample, contexte)
    return res

  @ServiceMethod
  def getAllPrelevmentsGroupFiltred(self, sessionId, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllPrelevmentsGroupFiltred(sessionId, contexte)
    return res

  @ServiceMethod
  def getAllLotsforPrelevFiltred(self, idGroup, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllLotsforPrelevFiltred(idGroup, contexte)
    return res

  @ServiceMethod
  def getAllLotsforPrelevFiltredByGenre(self, sessionId, tab):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllLotsforPrelevFiltredByGenre(sessionId, tab)
    return res

  @ServiceMethod
  def getNotationsLotsforPrelev(self, idlot, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getNotationsLotsforPrelev(idlot, contexte)
    return res

  @ServiceMethod
  def getNotationsByContainer(self, sessionId, tab):
    glamsTool = glamsTools_sample()
    res = glamsTool.getNotationsByContainer(sessionId, tab)
    return res

  @ServiceMethod
  def getAllTypeTissu(self, idNull, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeTissu()
    return res

  @ServiceMethod
  def getAllUser(self, idNull, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllUser()
    return res

  @ServiceMethod
  def getAllTypeContenu(self, idNull, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeContenu()
    return res

  @ServiceMethod
  def getAllTypeContenant(self, idNull, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeContenant()
    return res

  @ServiceMethod
  def getEtatContenant(self, idNull, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getEtatContenant()
    return res

  @ServiceMethod
  def getAllGroupsByLot(self, idLot, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllGroupsByLot(idLot)
    return res

  @ServiceMethod
  def createPrelev(self, tab, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.createPrelevWithTab(tab)
    return res

  @ServiceMethod
  def addUserToPrelevment(self, idUser, idPrelev):
    glamsTool = glamsTools_sample()
    res = glamsTool.addUserToPrelevment(idUser, idPrelev)
    return res

  @ServiceMethod
  def getAllUserByPrelevment(self, idPrelev, contexteNull):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllUserByPrelevment(idPrelev)
    return res

  @ServiceMethod
  def getSampleGroup(self, idGroup, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllPrelevmentsGroup()
    return res

  @ServiceMethod
  def addNotationPrelev(self, tab, sessionId):
    glamsTool = glamsTools_sample()
    res = glamsTool.addNotationPrelev(tab, sessionId)
    return res

  @ServiceMethod
  def deletePrelevementTest(self, idGroup, contexte):
    glamsTool = glamsTools_sample()
    res = glamsTool.deletePrelevementTest()
    return res

  @ServiceMethod
  def getNotationsTypeByContexte(self,sessionId, nomContexte,UserGroup):
    glamsTool = glamsTools_sample()
    res = glamsTool.getNotationsTypeByContexte(sessionId, nomContexte,UserGroup)
    return res

  @ServiceMethod
  def getContainerPlace(self,sessionId, id_container):
    glamsTool = glamsTools_sample()
    res = glamsTool.getContainerPlace(sessionId, id_container)
    return res

  @ServiceMethod
  def createIdGroupeByLots(self, sessionId,listeidLot,userGroup,name,remarque):
    glamsTool = glamsTools_sample()
    res = glamsTool.createIdGroupeByLots()
    return res

  @ServiceMethod
  def getOrCreateGroupeArbres(self,sessionId, listeLot,userGroup,name,remarque) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getOrCreateGroupeArbres()
    return res

  @ServiceMethod
  def getAllBoxTypeByContainer(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBoxTypeByContainer(sessionId, tab)
    return res

  @ServiceMethod
  def getAllBoxType(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBoxType(sessionId, tab)
    return res

  @ServiceMethod
  def getAllBox(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBox(sessionId, tab)
    return res

  @ServiceMethod
  def getAllTypeNotationAll(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeNotationAll(sessionId, tab)
    return res

  @ServiceMethod
  def createSamples(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createSamples(sessionId, tab)
    return res

  @ServiceMethod
  def createActions(self, sessionId, tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createActions(sessionId, tab)
    return res

  @ServiceMethod
  def getAlltypesLieux(self, tab, sessionId) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAlltypesLieux(sessionId)
    return res

  @ServiceMethod
  def getAlltypesLieuxO(self, tab, sessionId) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAlltypesLieuxO(sessionId)
    return res

  @ServiceMethod
  def getPrelevementContainer(self,idContainer,sessionId) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getPrelevementContainer(idContainer, sessionId)
    return res

  @ServiceMethod
  def getListContainerAction(self,idContainer,rang) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getListContainerAction(idContainer,rang)
    return res

  @ServiceMethod
  def getAllNameBoxByType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllNameBoxByType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllTypeBoxByType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeBoxByType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllValuesLieuxFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllValuesLieuxFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def getValidationCreateBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getValidationCreateBox(sessionId,tab)
    return res

  @ServiceMethod
  def setCreateBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.setCreateBox(sessionId,tab)
    return res

  @ServiceMethod
  def getInfosBoxBoxType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosBoxBoxType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllContainersByBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllContainersByBox(sessionId,tab)
    return res

  @ServiceMethod
  def getInfosBoxByType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosBoxByType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllGroupsFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllGroupsFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def setCreateContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.setCreateContainer(sessionId,tab)
    return res

  @ServiceMethod
  def getAllContentType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllContentType(sessionId,tab)
    return res

  @ServiceMethod
  def getPlaceBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getPlaceBox(sessionId,tab)
    return res

  @ServiceMethod
  def getAllContainerLevel(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllContainerLevel(sessionId,tab)
    return res

  @ServiceMethod
  def getAllValeurByNotation(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllValeurByNotation(sessionId,tab)
    return res

  @ServiceMethod
  def getAllBoxsFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBoxsFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def getAllInformationsByBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllInformationsByBox(sessionId,tab)
    return res

  @ServiceMethod
  def getAllContainersFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllContainersFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def getPlaceContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getPlaceContainer(sessionId,tab)
    return res

  @ServiceMethod
  def getAllExperiments(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllExperiments(sessionId,tab)
    return res

  @ServiceMethod
  def getAllExperimentsFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllExperimentsFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def getAllTypesBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypesBox(sessionId,tab)
    return res

  @ServiceMethod
  def getAllActionType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllActionType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllTypeContainerByBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeContainerByBox(sessionId,tab)
    return res

  @ServiceMethod
  def getAllBoxType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBoxType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllContainerType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllContainerType(sessionId,tab)
    return res

  @ServiceMethod
  def updateBoxType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.updateBoxType(sessionId,tab)
    return res

  @ServiceMethod
  def updateBox(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.updateBox(sessionId,tab)
    return res
  @ServiceMethod
  def updateContainerType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.updateContainerType(sessionId,tab)
    return res

  @ServiceMethod
  def createBoxType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createBoxType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllNomsVarieteByIdVariete(self,idVariete,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllNomsVarieteByIdVariete(idVariete,tab)
    return res

  @ServiceMethod
  def getListeVarieteByNomByGenre(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getListeVarieteByNomByGenre(sessionId,tab)
    return res

  @ServiceMethod
  def getListeAccessionByNomByGenre(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getListeAccessionByNomByGenre(sessionId,tab)
    return res

  @ServiceMethod
  def getAllBoxTypeAContainerType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllBoxTypeAContainerType(sessionId,tab)
    return res

  @ServiceMethod
  def getAllGenresBygGoup(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllGenresBygGoup(sessionId,tab)
    return res

  @ServiceMethod
  def getAllGenres(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllGenres(sessionId,tab)
    return res

  @ServiceMethod
  def getAllEspeceByGenre(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllEspeceByGenre(sessionId,tab)
    return res

  @ServiceMethod
  def getAllTypesNomVariete(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypesNomVariete(sessionId,tab)
    return res

  @ServiceMethod
  def createContainerType(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createContainerType(sessionId,tab)
    return res

  @ServiceMethod
  def updateExperiment(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.updateExperiment(sessionId,tab)
    return res

  @ServiceMethod
  def createExperiment(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createExperiment(sessionId,tab)
    return res

  @ServiceMethod
  def createProtocol(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createProtocol(sessionId,tab)
    return res

  @ServiceMethod
  def updateProtocol(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.updateProtocol(sessionId,tab)
    return res

  @ServiceMethod
  def getAllProtocolFiltred(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllProtocolFiltred(sessionId,tab)
    return res

  @ServiceMethod
  def createArbre(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createArbre(sessionId,tab)
    return res

  @ServiceMethod
  def getAllTypeNotationByContexte(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllTypeNotationByContexte(sessionId,tab)
    return res

  @ServiceMethod
  def getAllPepinieriste(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllPepinieriste(sessionId,tab)
    return res

  @ServiceMethod
  def getAllPepinieristeObtenteur(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getAllPepinieriste(sessionId,tab)
    return res

  @ServiceMethod
  def synchronyseAllPlaces(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.synchronyseAllPlaces(sessionId,tab)
    return res

  @ServiceMethod
  def setCreateBoxAndContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.setCreateBoxAndContainer(sessionId,tab)
    return res

  @ServiceMethod
  def getProtocolCultureByLot(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getProtocolCultureByLot(sessionId,tab)
    return res

  @ServiceMethod
  def createBoxs(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createBoxs(sessionId,tab)
    return res

  @ServiceMethod
  def getProtocolCultureLotByPrelevment(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getProtocolCultureLotByPrelevment(sessionId,tab)
    return res

  @ServiceMethod
  def setVarieteANotation(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.setVarieteANotation(sessionId,tab)
    return res

  @ServiceMethod
  def createAction(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.createAction(sessionId,tab)
    return res

  @ServiceMethod
  def getInfosOnAction(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosOnAction(sessionId,tab)
    return res

  @ServiceMethod
  def getInfosOnContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getInfosOnContainer(sessionId,tab)
    return res

  @ServiceMethod
  def addExperimentToContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.addExperimentToContainer(sessionId,tab)
    return res

  @ServiceMethod
  def getTreeFromExperiment(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getTreeFromExperiment(sessionId,tab)
    return res

  @ServiceMethod
  def getGraphFromContainer(self,sessionId,tab) :
    glamsTool = glamsTools_sample()
    res = glamsTool.getGraphFromContainer(sessionId,tab)
    return res


#//−−−−−−−MAIN−−−−−−−−−−
if __name__ == "__main__":
  service = glamsService()
  handleCGI(service)
