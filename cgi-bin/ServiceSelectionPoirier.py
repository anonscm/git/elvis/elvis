#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config

from elvis import SessionTools, PlantTools

from elvis.NotationTools import *
from elvis.selection_poirier import SelectionPoirierTools, CalculIndexPoirierTools

class ServiceSelectionPoirier:
  """Cette classe repertorie tout les services disponibles """

  def __init__(self):

    self.sessionTools = SessionTools()
    self.tool = SelectionPoirierTools()

    self._declareService("getTypesNotationsByContexte")
    self._declareService("getTypesNomPoirier")
    self._declareService("getAllValeursByNom")
    self._declareService("getAllValeurByNotation")
    self._declareService("getHybridBySearch")
    self._declareService("getTreeBySearch")
    self._declareService("getTypeNotation")
    self._declareService("getValeurNotationsByNumHyb")
    self._declareService("getValeurNotationsByNumLot")
    self._declareService("getAnneesNotationsByNumHyb")
    self._declareService("getAnneesNotationsByNumLot")
    self._declareService("getAllNotationsByNumHyb")
    self._declareService("getValeurNotationsByNumHybAn")
    self._declareService("getValeurNotationsByNumHybCampagne")
    self._declareService("updateNotationNumHyb")
    self._declareService("createNotationNumHyb")
    self._declareService("createNotationNumLot")
    self._declareService("updateOrCreateNotationNumHyb")
    self._declareService("getlisteParcellesPoirier")
    self._declareService("getListeArbreByParcellePoirier")
    self._declareService("insertListeNotationHybride");
    self._declareService("insertListeNotationDoubleHybride");
    self._declareService("getPairLocHybNumHyb");
    self._declareService("getAllCroisement");
    self._declareService("getAllAnCroisement");
    self._declareService("getAllParentCroisement");
    self._declareService("getAllVarieteParentCroisement");
    self._declareService("getAllCroisementByNom");
    self._declareService("getAllCroisementByAn");
    self._declareService("getAllCroisementByParent");
    self._declareService("getAllCroisementByParentF");
    self._declareService("getAllCroisementByParentM");
    self._declareService("getAllCroisementByVarieteParent");
    self._declareService("getAllCroisementByVarieteParentF");
    self._declareService("getAllCroisementByVarieteParentM");
    self._declareService("getCroisementById");
    self._declareService("getCroisementGrpFemById");
    self._declareService("getCroisementGrpMalById");
    self._declareService("getPrelevByCrois");
    self._declareService("getLotsByCrois");
    self._declareService("getNotationsByCrois");
    self._declareService("getTypeNotationsByCrois");
    self._declareService("addTypeNotationsByCrois");
    self._declareService("getNotationsLotsByCrois");
    self._declareService("getAllPlantesFromCrois");
    self._declareService("getIdGroupeByNom");
    self._declareService("getMaxNumHyb");
    self._declareService("getNumHybByNumLot");
    self._declareService("getNumIntroByNumLot");
    self._declareService("getAllContextes");
    self._declareService("getAllNotationsByContexte");
    self._declareService("getAllTypeNotation");
    self._declareService("addNotationToContexte");
    self._declareService("delAllNotationToContexte");
    self._declareService("addPrelevOnCroisment");
    self._declareService("addLotOnPrelevement");
    self._declareService("createNotationOnCroisment");
    self._declareService("createNotationOnLot");
    self._declareService("getAllLots");
    self._declareService("getOrCreateGroupeArbres");
    self._declareService("getIdCouple");
    self._declareService("insertCouple");
    self._declareService("getIdCroisement");
    self._declareService("insertCroisement");
    self._declareService("getAllContrainteNotation");
    self._declareService("getTypeByNotation");
    self._declareService("CreateOrUpdateConstraint");
    self._declareService("getAllSites");
    self._declareService("createGhGroup");

    #self._declareService("createHybride");

    self._declareService("getDateArrachageByEmplacement");
    self._declareService("updateDateArrachageByNumLot");
    self._declareService("getAllParcelles");
    self._declareService("getAllPlantesFromParcelle");
    self._declareService("getAllEmplacementByArbre");
    self._declareService("getAllLotByQuality");
    self._declareService("getAllIdLotsPourIndex");
    self._declareService("setNotationAUsergroup");
    self._declareService("getAllExperiment");
    self._declareService("addNotationToExperiment");
    self._declareService("deteleNotationNumHybCampagne");
    self._declareService("updateNotationNumHybCampagne");
    self._declareService("getAnneesDeReferenceABC");
    self._declareService("insertListeLotsPoirierElimines");

#    self._declareService("addNotationToContexte");

    self.tool = CalculIndexPoirierTools ()

    self._declareService("getIndexByNumhynAn")
    self._declareService("getIndexByNumLotAn")
    self._declareService("updateElvisPearByReferenceYear")
    self._declareService("updateElvisPearIndexSelectionByLot")

  @ServiceMethod
  def createHybride(self, sessionId, groupIds, site, parcelle, rang, arbre, numHyb, datePlantation, AnPremPous, croisement, porteGreffe, genre, espece):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    st = SessionTools();
    spt = SelectionPoirierTools()
    user = st.getUserInfos(sessionId)
    spt.createHybride(site, parcelle, rang, arbre, numHyb, datePlantation, AnPremPous, croisement, porteGreffe, genre, espece, fGroupIds, user)

  @ServiceMethod
  def getAllTreesByNamesOrNotations(self, sessionId, groupIds, name, listeNotations):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    st = SessionTools();
    spt = SelectionPoirierTools()
    pt = PlantTools()
    result=[]
    table=[]
    if name is not None :
      idl1 = pt.searchLotIdByVarietyName(fGroupIds, name) #liste des idLots
      for id in idl1 :
        result.append(id)
    if len(listeNotations) > 0 :
      idl2 = pt.searchLotIdByNotationValue(fGroupIds, listeNotations) #liste des idLots
      for id in idl2 :
        result.append(id)
    table=pt.getNamesForIdLot(result) # nom des lots de la liste des idlots
    return table

  @ServiceMethod
  def getIdContextByName(self, sessionId, groupIds,name):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    st = SessionTools();
    nt = NotationTools()
    table = nt.getNotationContextList(groupIds)
    for c in table :
      if c['name'] == name :
        return c['id']
    return None

  def _declareService(self, service_name):
    try :
      request = getattr(self.tool, service_name)
      request.__func__.IsServiceMethod = True
      setattr(self, service_name, request)
    except AttributeError as e:
      return "Error in declareService: " + str(e)


if __name__ == "__main__":
  service = ServiceSelectionPoirier()
  handleCGI(service)
