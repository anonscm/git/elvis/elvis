#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
import sys
from elvis import SessionTools, PlantTools, AddressBookTools
from elvis.data import Taxon, VarietyName, Variety, Address

class ServicePlant:
  def __init__(self):
    self.sessionTools = SessionTools()
    self.plantTools = PlantTools()
    self.addressBookTools = AddressBookTools()

  @ServiceMethod
  def getNotationIds(self, sessionId, contextNotationId, lotId):
    # TODO: Add getFilteredGroupIds
    # TODO: MOVE TO ServiceNotation
    groupsIds = self.sessionTools.getGroupIds(sessionId)
    notations = []
    ids = self.plantTools.getNotationIdsForLot(
      groupsIds,
      contextNotationId,
      lotId
      )
    notations = self.plantTools.getNotations(groupsIds, ids)
    return notations

  @ServiceMethod
  def createTreeLots(self, sessionId, groupsIds, tabLots):
    """Create tree lots

    Takes a list of dictionary of the form

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached
    :param tabLots: the list of dictionary with keys:
      name, accessionId, lotSourceId, multiplicationDate, rootstock,
    :return: a list of integer (lot ids)
    """
    # FIXME: CREATE setLot (create + update)
    # FIXME: tablots => liste de dictionnaire issue des objets lot
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    idLots = []
    # TO BE CONTINUED
    return idLots

  @ServiceMethod
  def getLotIdsFromLotNames(self, sessionId, groupIds, lotNames):
    """
    Get a all lot ids for each lot name requested (name can contain *).

    :param sessionId: the sessionId (see sessionTools)
    :param groupIds: a list of usergroup ids to which trees will be attached
    :param lotNames: a list of lot name to search
    :return: a dict with keys = lot names requested and values = list of lot ids
    """
    # FIXME: use searchLot
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    rep = {}
    for name in lotNames:
      rep[name] = pt.searchLotIdByLotName(fGroupIds, name)
    return rep

  @ServiceMethod
  def getIdsLotsByNamesOrNotationsOrPlaces(
    self,
    sessionId,
    groupIds,
    ListeNames,
    listeNotations,
    listePlaces,
    listeNamesIn,
    listeNotationsIn,
    listePlacesIn,
    listeNamesNear,
    listeNotationsAbs
    ):
    """
    Get a list of Lots which get the names
    or the notations of the list associated

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached
    :param ListeNames: list of names, dictionnary with keys:
      name_type and value_name
    :param listeNotations: list of notations, dictionnary with keys:
      id_notation_type and value_notation
    :param listePlaces: list of places, dictionnary with keys:
      id_place_type and value_place
    :param ListeNamesIn: list of names, dictionnary with keys:
      name_type and value_name, value is a list of values
    :param listeNotationsIn: list of notations, dictionnary with keys:
      id_notation_type and value_notation, value is a list of values
    :param listePlacesIn: list of places, dictionnary with keys:
      id_place_type and value_place, value is a list of values
    :param ListeNamesNear: list of names, dictionnary with keys:
      name_type and value_name, search by levenstein
    :param listeNotationsAbs: list of notations, dictionnary with keys:
      id_notation_type, search when notation is abscent
    :return: a list of integer (lot ids)
    """
    # FIXME: Eclater en plusieurs :
    # - remplacer par searchLot (pour byNames)
    # - getLotIdsByNotations (déjà présent)
    # - getLotsIdsByPlace
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    allresult = []

    if len(ListeNames) > 0:
      for name in ListeNames:
        # test le type de name
        if name['name_type'] == 'name_variety':
          #liste des idLots
          idl1 = pt.searchLotIdByVarietyName(fGroupIds, name['value_name'])
          allresult.append(idl1)
        if name['name_type'] == 'name_accession':
          #liste des idLots
          idl1 = pt.searchLotIdByAccessionName(fGroupIds, name['value_name'])
          allresult.append(idl1)
        if name['name_type'] == 'name_lot':
          #liste des idLots
          idl1 = pt.searchLotIdByLotName(fGroupIds, name['value_name'])
          allresult.append(idl1)
    if len(listeNotations) > 0:
      #liste des idLots
      idl2 = pt.searchLotIdByNotationValue(fGroupIds, listeNotations)
      allresult.append(idl2)
    if len(listeNotationsIn) > 0:
      for notationIn in listeNotationsIn:
        idl2 = []
        idNotType = notationIn['id_notation_type']
        for val in notationIn['value_notation'].split('|'):
          #liste des idLots
          idl = pt.searchLotIdByNotationValue(
            fGroupIds,
            [{u'value_notation': val, u'id_notation_type': idNotType}]
            )
          for i in idl:
            idl2.append(i)
        allresult.append(idl2)
    if len(listePlaces) > 0:
      for p in listePlaces:
        #liste des idLots
        idl3 = pt.searchLotIdByPlaceValue(fGroupIds, p)
        allresult.append(idl3)
    if len(listePlacesIn) > 0:
      for p in listePlacesIn:
        idl3 = []
        idPlaceType = p['id_place_type']
        for val in p['value_place'].split('|'):
          #liste des idLots
          idl = pt.searchLotIdByPlaceValue(
            fGroupIds,
            {u'id_place_type': idPlaceType, u'value_place': val}
            )
          for i in idl:
            idl3.append(i)
        allresult.append(idl3)
    if len(listeNamesIn) > 0:
      idl4 = []
      for name in listeNamesIn:
        # test le type de name
        if name['name_type'] == 'name_variety':
          listeNames = name['value_name']
          for n in listeNames.split('|'):
            #liste des idLots
            idl = pt.searchLotIdByVarietyName(fGroupIds, n)
            for i in idl:
              idl4.append(i)
        allresult.append(idl4)
        if name['name_type'] == 'name_accession':
          listeNames = name['value_name']
          for n in listeNames.split('|'):
            #liste des idLots
            idl = pt.searchLotIdByAccessionName(fGroupIds, n)
            for i in idl:
              idl4.append(i)
        allresult.append(idl4)
        if name['name_type'] == 'name_lot':
          listeNames = name['value_name']
          for n in listeNames.split('|'):
            #liste des idLots
            idl = pt.searchLotIdByLotName(fGroupIds, n)
            for i in idl:
              idl4.append(i)
        allresult.append(idl4)
    if len(listeNamesNear) > 0:
      for name in listeNamesNear:
        if name['name_type'] == 'name_variety':
          #liste des idLots
          idl5 = pt.searchLotIdByVarietyNameNear(fGroupIds, name['value_name'])
          allresult.append(idl5)
    if len(listeNotationsAbs) > 0:
      for p in listeNotationsAbs:
        idl6 = pt.getLotIdsWithoutNotationType(fGroupIds, p['id_notation_type'])
        allresult.append(idl6)
    #intersection des resultats
    inter01 = []
    inter2 = []
    if len(allresult) > 0:
      inter01= allresult[0]
      if len(allresult) > 1:
        inter01 = []
        for i in allresult[1]:
          if i in allresult[0]:
            inter01.append(i)
        for r in allresult:
          for i in r:
            if i in inter01:
              inter2.append(i)
          inter01 = inter2
          inter2 = []
    return inter01

  @ServiceMethod
  def getNamesLotsByNamesOrNotationsOrPlaces(
    self,
    sessionId,
    groupIds,
    ListeNames,
    listeNotations,
    listePlaces,
    listeNamesIn,
    listeNotationsIn,
    listePlacesIn,
    listeNamesNear,
    listeNotationsAbs
    ):
    """
    Associate with a id of a lot, a name, introduction name and non usuel
    or a value of variety name

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param listeNames: la list of names, dictionnary with keys:
      name_type and value_name
    :param listeNotations: la list of notations, dictionnary with keys:
      id_notation_type and value_notation
    :param listePlaces: la list of places, dictionnary with keys:
      id_place_type and value_place
    :return: a list of dictionary with keys:
      lot_name, introduction_name, variety_name
    """
    # TODO: update comments
    pt = self.plantTools
    table=[]
    result = self.getIdsLotsByNamesOrNotationsOrPlaces(
      sessionId,
      groupIds,
      ListeNames,
      listeNotations,
      listePlaces,
      listeNamesIn,
      listeNotationsIn,
      listePlacesIn,
      listeNamesNear,
      listeNotationsAbs
      )
    # nom des lots de la liste des idlots
    if (self.sessionTools.getUserInfos(sessionId)):
      table = pt.getNamesForIdLot(result)
      return table

  @ServiceMethod
  def getNamesForIdLot(self, sessionId, idLot):
    """
    Get all differents names (lot, accession variety) used by a group

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param typeName: type of name: name_variety, name_accession, name_lot
    :return: a list of name
    """
    # TODO: UPDATE COMMENTS
    # FIXME: ne retourne pas seulement des noms comme attendu
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      table=[]
      table = pt.getNamesForIdLot(idLot)
      return table

  @ServiceMethod
  def getAllNamesByGroup(self, sessionId, groupIds, typeName, filterText):
    """
    Get all differents names (lot, accession variety) used by a group

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param typeName: type of name: name_variety, name_accession, name_lot
    :return: a list of name
    """
    # TODO: UPDATE COMMENTS
    # FIXME: remplacer par searchLot, searchVarietie, searchAccession (à créer)
    pt = self.plantTools
    nameList = []
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    if typeName == 'name_variety':
      nameList = pt.getNamesVarietyForGroup(fGroupIds, filterText)
    if typeName == 'name_accession':
      nameList = pt.getNamesAccessionForGroup(fGroupIds, filterText)
    if typeName == 'name_lot':
      nameList = pt.getNamesLotForGroup(fGroupIds, filterText)
    return nameList

  @ServiceMethod
  def getAllPlacesByIdLot(self, sessionId, groupIds, idLot):
    """
    Get all differents places used by a lot

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idLot: id of the plant.lot
    :return: a list of dictionary with keys:
      plantation_date, removing_date, type, value
    """
    # TODO: TO DELETE, REPLACED BY getAllPlacesByIdLots
    pt = self.plantTools
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    placeList = pt.getAllPlacesByIdLot(fGroupIds, idLot)
    return placeList

  @ServiceMethod
  def getAllPlacesByIdLots(self, sessionId, groupIds, idLots):
    """
    Get all differents places used by a list of lot

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idLots: list of id of the plant.lot
    :return: a list of dictionary with keys:
      plantation_date, removing_date, type, value
    """
    pt = self.plantTools
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    placeList = pt.getAllPlacesByIdLots(fGroupIds, idLots)
    return placeList

  @ServiceMethod
  def getAllInformationsByIdLot(self, sessionId, groupIds, idLot):
    """
    Get all differents informations for lot

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch lot will be attached
    :param idLots: list of id of the plant.lot
    :return: a dictionary with keys: id, name, id_accession, id_lot_source,
      name_lot_source , multiplication_date, harvesting_date, quantity,
      first_shoot_year, rootstock, death_date
    """
    # TODO: REMPLACER PAR getLot
    pt = self.plantTools
    data = {}
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    tree = pt.getAllInformationsTreeByIdLot(fGroupIds,idLot)
    seed = pt.getAllInformationsSeedByIdLot(fGroupIds,idLot)
    if len(tree) > 0:
      data = tree
      data['harvesting_date'] = None
      data['quantity'] = None
    else:
      if len(seed) > 0:
        data = seed
        data['first_shoot_year'] = None
        data['rootstock'] = None
        data['death_date'] = None
      else:
        data['id'] = None
        data['name'] = None
        data['id_accession'] = None
        data['id_lot_source'] = None
        data['name_lot_source'] = None
        data['multiplication_date'] = None
        data['first_shoot_year'] = None
        data['rootstock'] = None
        data['death_date'] = None
        data['harvesting_date'] = None
        data['quantity'] = None
    return data

  @ServiceMethod
  def getAllPlaceNamesByGroup(self, sessionId, groupIds):
    """
    Get all differents places names used by a usergroup

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :return: a list of dictionary with keys: name, id
    """
    # FIXME: delete tools and replace by several services (objet lot)
    pt = self.plantTools
    nameList = []
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    nameList = pt.getAllPlaceNamesByGroup(fGroupIds)
    return nameList

  @ServiceMethod
  def getValuesPlacesByTypeAndGroup(self, sessionId, groupIds, typePlaceIds):
    # TODO: ADD COMMENTS
    # placeType
    # FIXME: Fusionner avec la précédente
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    values = []
    values = pt.getValuesPlacesByTypeAndGroup(fGroupIds, typePlaceIds)
    return values

  @ServiceMethod
  def getAllNamesByVariete(self, sessionId, idVariete):
    """
    Get all variety names for a variety id

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idAccession: the id of the accession
    :return: a list of idLots
    """
    # FIXME: remplacer par getVarieties + methodes complémentaires
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      values = []
      values = pt.getAllNamesByVariete(idVariete)
      return values

  @ServiceMethod
  def getGeneticBackgroundList(self, sessionId, taxonomyId):
    """
    Get the id list of variety for a taxonomy id
    which are considered as a genetic background reference

    :param taxonomyId: the taxonomy id
    :return: ids and values for each Genetic Background variety
    :type taxonomyId: int
    :rtype: a list of dictionary with keys: id, name

    .. sectionauthor:: Sandra Pelletier
    .. warnings:: CONF DATA TO MANAGE ('mutant', 'nom usuel')
    """
    # FIXME: date conf to manage
    # FIXME: return list elvis.data.Variety
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      values = []
      values = pt.getGeneticBackgroundList(taxonomyId, 'mutant', 'nom usuel')
      return values

  @ServiceMethod
  def getVarietyIdsByGeneticBackgroundIds(
    self,
    sessionId,
    geneticBackgroundIds,
    taxonomyId,
    varietyRelationType = "mutant"
    ):
    """
    Get list of variety ids for an id of genetic background

    :param geneticBackgroundIds: the id list of parent ids
    :param taxonomyId: the id of the current taxonomy
    :param varietyRelationType: the relation between variety (default: "mutant")
    :return: a list of variety ids
    :type geneticBackgroundIds: list of int
    :type taxonomyId: int
    :type varietyRelationType: string
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      return self.plantTools.getVarietyIdsByGeneticBackgroundIds(
        geneticBackgroundIds,
        taxonomyId
        )

  @ServiceMethod
  def getVarietiesByGeneticBackgroundIds(
    self,
    sessionId,
    usergroupIds,
    geneticBackgroundIds,
    taxonomyId,
    varietyRelationType = "mutant"
    ):
    """
    Get list of variety object for ids of variety has genetic background

    :param sessionId: the sessionId for which the user info are requested
    :param usergroupIds: the list of usergroups ids
    :param geneticBackgroundIds: the id list of parent ids
    :param taxonomyId: the id of the current taxonomy
    :param varietyRelationType: the relation between variety (default: "mutant")
    :return: a list of elvis.data.Variety objects
    :type sessionId: string
    :type usergroupIds: list of int
    :type geneticBackgroundIds: list of int
    :type taxonomyId: int
    :type varietyRelationType: string
    :rtype: list of dict

    .. sectionauthor:: Sandra Pelletier
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
      rep = self.plantTools.getVarietyIdsByGeneticBackgroundIds(
        geneticBackgroundIds,
        taxonomyId
        )
      varList = []
      for var in rep:
        varObj = self.plantTools.getVariety(var)
        acc = self.plantTools.getAccessionsByVarietyId(var, fGroupIds, 1)
        varObj.setAccessionList(acc)
        varList.append(varObj.toDict())
      return varList

  @ServiceMethod
  def getInformationsByIdAccession(self, sessionId, groupIds, idAccession):
    # TODO: REMPLACER PAR getAccession + autres objets
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    values = []
    values = pt.getInformationsByIdAccession(fGroupIds, idAccession)
    return values

  @ServiceMethod
  def getAllIdLotsTreeByIdAccession(self, sessionId, groupIds, idAccession):
    """
    Get all differents lots under accession

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idAccession: the id of the accession
    :return: a list of idLots
    """
    # TODO: REMPLACER PAR getAccession
    # avec depth = 1
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    values = []
    values = pt.getAllIdLotsTreeByIdAccession(fGroupIds, idAccession)
    return values

  @ServiceMethod
  def getAllIdLotsSeedByIdAccession(self, sessionId, groupIds, idAccession):
    """
    Get all differents lots under accession

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idAccession: the id of the accession
    :return: a list of idLots
    """
    # TODO: REMPLACER PAR getAccession avec depth = 1
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    values = []
    values = pt.getAllIdLotsSeedByIdAccession(fGroupIds, idAccession)
    return values

  @ServiceMethod
  def getConformityVarietyList(self, sessionId, groupIds, varietyList):
    """
    Author: Fabrice Dupuis juin 2019

    Get conformity of list of variety

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param varietyList: the list of variety
    :return: message of conformity and errors and the varietyList completed
    """
    # TODO: Pas de vérification de session
    # puisque pas de réutilisation de fGroupIds dans la suite du code
    # TODO: UTILISER getUserInfos
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    errorMsg = ""
    infosMsg = ""
    # 1. conformity taxonomy
    taxonAll = [] # liste des differents taxons
    taxonList = [] # liste des objets taxon.py
    # Parcours la liste des varietes
    for k in varietyList:
      taxon = Taxon(None, k['genus'], k['specie'], None)
      taxonList.append(taxon)
      genus = k['genus']
      specie = k['specie']
      gs = str(genus + ' ' + specie)
      if gs not in taxonAll:
        taxonAll.append(gs)
        taxon = Taxon(None, k['genus'], k['specie'], None)
        taxonList.append(taxon)
    # verification des taxons, ajoute Id si genre et espèces renseignés
    taxonList = pt.completeTaxonomyList(taxonList)
    for t in taxonList:
      if t.getId() is None:
        errorMsg = errorMsg + 'taxon inconnu : ' + t.getGenus()
        errorMsg = errorMsg + ' ' + t.getSpecie() + "\n"
      #complete varietyList
      for l in varietyList:
        if t.getGenus() == l['genus'] and t.getSpecie() == l['specie']:
          l['taxonomyId'] = t.getId()

    # 2 conformity varietyNameType
    for k in varietyList:
      listNameVar = [] #liste des objects VarietyName
      #parcours la liste des noms variete
      for v in k['listVarietyName']:
        nameVar = VarietyName(
          value = v['value'],
          date = v['date'],
          type = v['type']
          )
        listNameVar.append(nameVar)
        # get liste de tous les types de noms
        listTypeName = pt.getVarietyNameType(None)

      # ajoute idNameType
      for n in listNameVar:
        for v in listTypeName:
          if n.getType() == v['value']:
            n.setVarietyNameTypeId(v['id'])
            break
        for n in listNameVar:
            if n.getVarietyNameTypeId() is None:
              errorMsg = errorMsg +'type nom inconnu : '+ str(n.getType()) + "\n"
              errorMsg = errorMsg +'type nom inconnu : '+ v['value'] + "\n"
      # enregistre idTypeName
      varietyNameList = []
      for n in listNameVar:
        varietyNameList.append(n.toDict())
        k['listVarietyName'] = varietyNameList
    # 3 conformity variety
    listVar = []
    for k in varietyList:
      # parcours la liste des noms de varietes
      listNameVar = [] #liste des objects VarietyName
      for v in k['listVarietyName'] :
        nameVar = VarietyName(
          None,
          v['value'],
          v['date'],
          v['type'],
          v['varietyNameTypeId'],
          None
          )
        listNameVar.append(nameVar)
      # passer la liste des variete de dictionnaire en objet variete.py
      variety = Variety(
        None,
        k['breeder'],
        None,
        k['comment'],
        k['taxonomyId'],
        None,
        k['genus'],
        k['specie'],
        k['editor'],
        None,
        listNameVar
        )
      listVar.append(variety)
    # 3.1 verifie unicite du Breeder
    for var in listVar:
      if var.getBreeder():
        idBreed = self.addressBookTools.searchAddress (lastname = var.getBreeder())
        if len(idBreed) == 0:
          errorMsg = errorMsg +" Obtenteur inconnu : " + str(var.getBreeder()) + "\n"
        if len(idBreed) > 1:
          errorMsg = errorMsg +" Obtenteur ambigu : " + str(var.getBreeder()) + "\n"
        if len(idBreed) == 1:
          var.setBreederId(idBreed[0])
    # 3.2 verifie unicite de l'éditeur
    for var in listVar:
      if var.getEditor():
        idBreed = self.addressBookTools.searchAddress (lastname = var.getEditor())
        if len(idBreed) == 0:
          errorMsg = errorMsg +" Obtenteur inconnu : " + str(var.getEditor()) + "\n"
        if len(idBreed) > 1:
          errorMsg = errorMsg +" Obtenteur ambigu : " + str(var.getEditor()) + "\n"
        if len(idBreed) == 1:
          var.setEditorId(idBreed[0])

    # 3.3 searchVariete
    for var in listVar:
      listeNoms = {}
      for nomvar in var.getVarietyNameList():
        listeNoms[nomvar.getType()] = nomvar.getValue()
      listIdVar = self.plantTools.searchVariety(
        idTaxonomy = var.getTaxonomyId(),
        listNames = listeNoms
      )
      if len(listIdVar) == 0:
        infosMsg = infosMsg + "variété inconnue : "+ str(listeNoms) + "\n"
      if len(listIdVar) > 1:
        errorMsg = errorMsg + "variété ambigüe : "+ str(listeNoms) + "\n"
      if len(listIdVar) == 1:
        var.setId(listIdVar[0])

    # passer la liste des variete de objet variete.py en dictionnaire
    varietyList = []
    for var in listVar:
      varietyList.append(var.toDict())
    return [errorMsg, infosMsg, varietyList]

  @ServiceMethod
  def getConformityAccessionList(self, sessionId, groupIds, accessionList):
    # TO BE CONTINUED
    # TODO: UTILISER getUserInfos
    """
    Get conformity of list of accession

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param accessionList: the list of accession
    :return: message of conformity and errors and the accessionList completed
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    errorMsg = ""
    infosMsg = ""
    return [errorMsg, infosMsg, accessionList]

  @ServiceMethod
  def getConformityLotList(self, sessionId, groupIds, lotList):
    # TO BE CONTINUED
    # TODO: UTILISER getUserInfos
    """
    Get conformity of list of lots

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param lotList: the list of lot
    :return: message of conformity and errors and the lotList completed
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    pt = self.plantTools
    errorMsg = ""
    infosMsg = ""
    return [errorMsg, infosMsg, lotList]

  @ServiceMethod
  def getVarietyNameType(self, sessionId, taxonomyId):
    """
    Get the list of variety name for a taxon list (optional)

    :param taxonomyId: None or a list of taxon id
    :return: a list of couple id, value of variety name
    :type id: list of int
    :rtype: list of dict with keys : id, value

    .. sectionauthor:: Sandra Pelletier
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      values = []
      values = pt.getVarietyNameType(taxonomyId)
      return values

  @ServiceMethod
  def getVarieteByLot(self, sessionId, lotIds):
    # FIXME: CHANGE NAME: getVarietyTaxonomyByLotId
    # FIXME: CHANGE lotIds BY lotId
    # TODO: ADD COMMENTS
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      values = []
      values = pt.getVarieteByLot(lotIds)
      return values

  @ServiceMethod
  def getTaxonomiesFromListId(
    self,
    sessionId,
    taxonomyListId,
    taxonomyTypeListId
    ):
    """
    Get all informations about taxonomy
    :param taxonomyListId: the list of id taxonomy
    :param taxonomyTypeListId: the list of id type taxonomy
    :return: a list of objects taxon.py
    """
    # FIXME: getTaxonomies
    if (self.sessionTools.getUserInfos(sessionId)):
      pt = self.plantTools
      taxonList = pt.getTaxonomiesFromListId(taxonomyListId, taxonomyTypeListId)
      result = []
      for t in taxonList:
        result.append(t.toDict())
      return result

  @ServiceMethod
  def saveVarieties(self, sessionId, groupIds, tabVariety):
    """Create or update varieties
    Author : Fabrice Dupuis. Juin 2019
    Save a list of dictionnary variety

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached
    :param tabVariety: the list of dictionary with keys:

    :return: a list of integer (variety ids)
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    idVar = []
    # TO BE CONTINUED
    # si id variety : update variety
    # si pas id variety : create variety
    #  utilise aussi addVarietyName
    for var in tabVariety:
      sys.stderr.write(str("var : ") +" "+str(var) + "\n")
      if var['id'] == None:
        pass
    return idVar

  @ServiceMethod
  def saveLots(self, sessionId, groupsIds, tabLot):
    """Create or update lots

    Takes a list of object lots

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached
    :param tabLot: the list of dictionary with keys:

    :return: a list of integer (lots ids)
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    idLots = []
    # TO BE CONTINUED
    # si id lot : update lot
    # si pas id lot : create lot
    #  utilise ussi addVarietyName
    return idLots

  @ServiceMethod
  def getVarietyNames(self, sessionId, id):
    """
    Get list of varietyName objects from an variety id list

    :param id: variety id
    :return: elvis.data.VarietyName (json object)
    :type id: int
    :rtype: dict

    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      listDict = []
      listObj = self.plantTools.getVarietyNames(id)
      for obj in listObj:
        listDict.append(obj.toDict())
      return listDict

  @ServiceMethod
  def getVariety(
    self,
    sessionId,
    varietyId,
    usergroupIds = None,
    depth = 0
    ):
    """
    Get a variety object with accession and lot (optional) from variety id

    :param sessionId: the sessionId for which the user info are requested
    :param varietyId: the variety id
    :param usergroupIds: the list of usergroups ids
    :param depth:
      0: only variety
      1: with accession
      2: with accession and associated lots
    :return: elvis.data.Variety (json object)
    :type sessionId: string
    :type varietyId: int
    :type usergroupIds: list of int
    :type depth: int
    :rtype: dict

    .. sectionauthor:: Sandra Pelletier
    """
    # Vérification des bornes de depth
    depth = 0 if depth < 0 else depth
    depth = 2 if depth > 2 else depth
    # Vérification session
    if (self.sessionTools.getUserInfos(sessionId)):
      # Création de l'objet
      varObj = self.plantTools.getVariety(varietyId)
      # Si profondeur demandée
      if depth > 0:
        # depth descend d'un niveau pour le lot
        depth -= 1
        # Vérification des usergroupes
        fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
        # Liste des accessions pour cette variété, avec depth pour lots
        rep = self.plantTools.getAccessionsByVarietyId(varietyId, fGroupIds, depth)
        if rep:
          # Mise à jour de la liste des accession de l'objet variete
          varObj.setAccessionList(rep)
      # Retourne l'objet en dictionnaire
      return varObj.toDict()

  @ServiceMethod
  def getAccessionsByVarietyId(
    self,
    sessionId,
    varietyId,
    usergroupIds = None,
    depth = 0
    ):
    """
    Get List of accessions objects (with lot, optional) for a variety id

    :param sessionId: the sessionId for which the user info are requested
    :param varietyId: the variety id
    :param usergroupIds: the list of usergroups ids
    :param depth:
      0: only accessions
      1: with associated lots
    :return: list of elvis.data.Accession (json object)
    :type sessionId: string
    :type varietyId: int
    :type usergroupIds: list of int
    :type depth: int
    :rtype: list of dict

    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    depth = 0 if depth < 0 else depth
    depth = 1 if depth > 1 else depth
    if depth > 0:
      fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
      rep = self.plantTools.getAccessionsByVarietyId(varietyId, fGroupIds, depth)
    elif (self.sessionTools.getUserInfos(sessionId)):
      rep = self.plantTools.getAccessionsByVarietyId(varietyId, [], depth)
    accList = []
    for acc in rep:
      accList.append(acc.toDict())
    return accList

  @ServiceMethod
  def getLotsByAccessionId(self, sessionId, accessionId, usergroupIds):
    """
    Get List of lots objects for an accession id

    :param sessionId: the sessionId for which the user info are requested
    :param accessionId: the accession id
    :param usergroupIds: the list of usergroup ids
    :return: List of elvis.data.Lot object
    :type sessionId: string
    :type accessionId: int
    :type usergroupIds: list of int
    :rtype: list of dict

    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
    rep = self.plantTools.getLotsByAccessionId(accessionId, fGroupIds)
    lotList = []
    for lot in rep:
      lotList.append(lot.toDict())
    return lotList

  @ServiceMethod
  def getLotType(self, sessionId):
    """
    Get the type of lot type

    :return: list lotType
    :rtype: list of string

    .. sectionauthor:: Sandra Pelletier
    .. warning:: FOR TEST, TO BE REMOVED
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      return self.plantTools.getLotType()

  @ServiceMethod
  def getAccession(
    self,
    sessionId,
    accessionId,
    usergroupIds = None,
    depth = 0
    ):
    """
    Get accessions objects (with lot, optional) from accession id

    :param sessionId: the sessionId for which the user info are requested
    :param accessionId: the accession id
    :param usergroupIds: the list of usergroups ids
    :param depth:
      0: only accessions
      1: with associated lots
    :return: elvis.data.Accession (json object)
    :type sessionId: string
    :type accessionId: int
    :type usergroupIds: list of int
    :type depth: int
    :rtype: dict

    .. sectionauthor:: Sandra Pelletier
    """
    # Vérification des bornes de depth
    depth = 0 if depth < 0 else depth
    depth = 1 if depth > 1 else depth
    # Vérification session
    if (self.sessionTools.getUserInfos(sessionId)):
      # Création de l'objet
      accObj = self.plantTools.getAccession(accessionId)
      # Si profondeur demandée
      if depth > 0:
        # Vérification des usergroupes
        fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
        # Liste des lots pour cette accession
        rep = self.plantTools.getLotsByAccessionId(accessionId, fGroupIds)
        if rep:
          # Mise à jour de la liste des lots de l'objet accession
          accObj.setLotList(rep)
      # Retourne l'objet en dictionnaire
      return accObj.toDict()

  @ServiceMethod
  def getLotRigthsByUserGroupIds(
    self,
    sessionId,
    lotIds,
    usergroupIds
    ):
    """
    Get List of lots ids with rigths fonction of usergroups

    :param sessionId: the sessionId for which the user info are requested
    :param lotIds: list of lot ids
    :param usergroupIds: the list of usergroup ids
    :return: List of elvis.data.Lot object
    :type sessionId: string
    :type accessionId: list of int
    :type usergroupIds: list of int
    :rtype: list of dict (keys : id, write)

    .. sectionauthor:: Sandra Pelletier
    """
    if (self.sessionTools.getUserInfos(sessionId)):
      return self.plantTools.getLotRigthsByUserGroupIds(lotIds, usergroupIds)

  @ServiceMethod
  def getLot(
    self,
    sessionId,
    lotId,
    usergroupIds,
    type = None
    ):
    """
    Get lot object from a lot id

    :param sessionId: the sessionId for which the user info are requested
    :param lotId: the lot id to get
    :param usergroupIds: the list of usergroups ids
    :param type: one element of tree, seed or lot
    :return: an elvis.data.Lot objects
    :type sessionId: string
    :type lotId: int
    :type usergroupIds: list of int
    :type type: string
    :rtype: dict

    .. sectionauthor:: Sandra Pelletier
    """
    # Vérification session
    if (self.sessionTools.getUserInfos(sessionId)):
      # Vérification des droits de lecture
      rep = self.plantTools.getLotRigthsByUserGroupIds([lotId], usergroupIds)[0]
      if rep != []:
        # Création de l'objet
        lotType = self.plantTools.getLotType()
        if (type != None):
          lotObj = self.plantTools.getLot(rep['id'], type)
          return lotObj.toDict()
        else:
          for type in lotType:
            lotObj = None
            try:
              lotObj = self.plantTools.getLot(rep['id'], type['name'])
            except:
              pass
            # return rep['id']
            if (lotObj != None):
              # Retourne l'objet en dictionnaire
              return lotObj.toDict()

  @ServiceMethod
  def searchLotIds(
    self,
    sessionId,
    usergroupIds,
    name = None,
    accessionId = None,
    lotSourceId = None,
    place = None
    ):
    """Search lots name, accessionId, lotSourceId and/or place

    :param sessionId: the sessionId for which the user info are requested
    :param usergroupIds: the list of usergroups ids
    :param name: the lot name to search. This can search for partial
                 information using the '*' character
    :param placeValues: the notations used to filter the results
    :return: a list of lot ids
    :type groupsIds: list of integer
    :type name: string
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    .. warnings:: TO VERIFY PLACE (no dataTest)
    """
    # TODO: TO VERIFY PLACE (no dataTest)
    # TODO: code review
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, usergroupIds)
    ids = []
    idList = []
    idList_name = []
    idList_accession = []
    idList_source = []
    idList_place = []
    if (self.sessionTools.getUserInfos(sessionId)):
      if (name != None):
        idList_name = self.plantTools.searchLotIdByLotName(fGroupIds, name)
        idList.append(idList_name)
      if (accessionId != None):
        rep =  self.plantTools.getLotIdsByAccessionId(accessionId, fGroupIds)
        idList_accession = [(d['id']) for d in rep]
        idList.append(idList_accession)
      if (lotSourceId != None):
        rep = self.plantTools.getLotIdsbyLotSourceId(lotSourceId, fGroupIds)
        idList_source = [(d['id']) for d in rep]
        idList.append(idList_source)
      if (place != None):
        idList_place =  self.plantTools.searchLotIdByPlaceValue(fGroupIds, place)
        idList.append(idList_place)
    if len(idList) > 1:
      tmp = idList[0]
      for i in range(1, len(idList)):
        tmp = [id for id in tmp if id in idList[i]]
        ids = tmp
      # ids = [id for id in idList_name if id in idList_accession]
      return ids
    elif len(idList) == 1:
      return idList[0]
    return idList

  @ServiceMethod
  def searchLot(
    self,
    sessionId,
    usergroupIds,
    name = None,
    accessionId = None,
    lotSourceId = None,
    place = None
    ):
    """Search lots name, accessionId, lotSourceId and/or place

    :param sessionId: the sessionId for which the user info are requested
    :param usergroupIds: the list of usergroups ids
    :param name: the lot name to search. This can search for partial
                 information using the '*' character
    :param placeValues: the notations used to filter the results
    :return: a list of elvis.data.Lot objects
    :type groupsIds: list of integer
    :type name: string
    :rtype: list of dict

    .. sectionauthor:: Sandra Pelletier
    .. warnings:: TO VERIFY PLACE (no dataTest)
    """
    # TODO: TO VERIFY PLACE (no dataTest)
    # TODO: code review
    rep = []
    rep = self.searchLotIds(
      sessionId,
      usergroupIds,
      name,
      accessionId,
      lotSourceId,
      place,
      )
    ids = []
    for id in rep:
      ids.append(self.getLot(sessionId, id, usergroupIds))
    return ids

  @ServiceMethod
  def searchAccessionIds(
    self,
    sessionId,
    introductionName = None,
    varietyId = None,
    introductionCloneId = None,
    provider = None,
    providerId = None,
    collectionSiteId = None
    ):
    """Search accessions by introductionName, varietyId, introductionCloneId,
    provider, providerId and/or collectionSiteId

    :param sessionId: the sessionId for which the user info are requested
    :param introductionName: the name to search. This can search for partial
                 information using the '*' character
    :param varietyId: id of the variety
    :param introductionCloneId: id of the introduction clone
    :param provider: lastname of the provider
    :param providerId: id of the provider
    :param collectionSiteId: id of the site
    :return: a list of elvis.data.Accession objects
    :type introductionName: string
    :type varietyId: int
    :type introductionCloneId: int
    :type provider: string
    :type providerId: int
    :type collectionSiteId: int
    :rtype: list of dict

    .. sectionauthor:: Sandra Pelletier
    .. warnings:: TO VERIFY introduction_clone (no data in db irhs-001)
    .. warnings:: TO VERIFY collectionSiteId (no data in db irhs-001)
    """
    # TODO: TO VERIFY PLACE (no dataTest)
    # TODO: code review
    ids = []
    idList = []
    idList_name = []
    idList_variety = []
    idList_clone = []
    idList_provider = []
    idList_provId = []
    idList_site = []
    if (self.sessionTools.getUserInfos(sessionId)):
      if (introductionName != None):
        idList_name = self.plantTools.searchAccessionIdsByIntroductionName(
          introductionName)
        idList.append(idList_name)
      if (varietyId != None):
        rep =  self.plantTools.getAccessionIdsByVarietyId(varietyId)
        idList_variety = [(d['id']) for d in rep]
        idList.append(idList_variety)
      if (introductionCloneId != None):
        rep = self.plantTools.searchAccessionIdsByIntroductionCloneId(
          introductionCloneId)
        idList_clone = [(d['id']) for d in rep]
        idList.append(idList_clone)
      if (provider != None):
        idList_provider =  self.plantTools.searchAccessionIdsByProvider(
          provider)
        idList.append(idList_provider)
      if (providerId != None):
        idList_provId =  self.plantTools.searchAccessionIdsByProviderId(
          providerId)
        idList.append(idList_provId)
      if (collectionSiteId != None):
        idList_site =  self.plantTools.searchAccessionIdsByCollectionSiteId(
          collectionSiteId)
        idList.append(idList_site)
    if len(idList) > 1:
      tmp = idList[0]
      for i in range(1, len(idList)):
        tmp = [id for id in tmp if id in idList[i]]
        ids = tmp
      return ids
    elif len(idList) == 1:
      return idList[0]
    return idList

  @ServiceMethod
  def searchAccession(
    self,
    sessionId,
    introductionName = None,
    varietyId = None,
    introductionCloneId = None,
    provider = None,
    providerId = None,
    collectionSiteId = None
    ):
    """Search accessions by introductionName, varietyId, introductionCloneId,
    provider, providerId and/or collectionSiteId

    :param sessionId: the sessionId for which the user info are requested
    :param name: the lot name to search. This can search for partial
                 information using the '*' character
    :param placeValues: the notations used to filter the results
    :return: a list of lot ids
    :type groupsIds: list of integer
    :type name: string
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    .. warnings:: TO VERIFY introduction_clone (no data in db irhs-001)
    .. warnings:: TO VERIFY collectionSiteId (no data in db irhs-001)
    """
    rep = self.searchAccessionIds(
      sessionId,
      introductionName,
      varietyId,
      introductionCloneId,
      provider,
      providerId,
      collectionSiteId
      )
    ids = []
    for id in rep:
      ids.append(self.getAccession(sessionId, id))
    return ids

if __name__ == "__main__":
  service = ServicePlant()
  handleCGI(service)
