#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
from elvis import SessionTools
from elvis.ProjectTools import *

import elvis.data.Project

class projectService:
  def __init__(self):
    self.sessionTools = SessionTools()
    self.projectTools = ProjectTools()

  @ServiceMethod
  def getNameProject(self, sessionId, groupIds):
    """
    Get list of experiment names

    Author: Lysiane Hauguel 2019-07

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: a list of string and integer (name, id)
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    nameProject = []
    nameProject = self.projectTools.getNameProject('project')
    return nameProject

  @ServiceMethod
  def getFinancingTypesName(self, sessionId, groupIds):
    """
    Get list of financing names

    Author: Lysiane Hauguel 2019-07

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: a list of string and integer (name, id)
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    financingType = []
    financingType = self.projectTools.getFinancingTypesName()
    return financingType

  @ServiceMethod
  def getAxisTypesName(self, sessionId, groupIds):
    """
    Get list of axis name

    Author: Lysiane Hauguel 2019-07

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: a list of string and integer (name, id)
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    axis = []
    axis = self.projectTools.getAxisTypesName()
    return axis

  @ServiceMethod
  def getAllInfoProject(self, sessionId, groupIds, nameProject, userId):
    """
    Get a project object from a user id and project name

    Author: Lysiane Hauguel 2019-07

    :param nameProject: a project name.
    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: a project.py object
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    project = []
    project = self.projectTools.getAllInfoProject(nameProject, userId)
    return project

  @ServiceMethod
  def getUserWritable(self, sessionId, groupIds, idProject):
    """
    Get the information user writable on a project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: boolean if user is writable or not
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    writable = []
    writable = self.projectTools.getUserWritable(idProject, groupIds)
    return writable

  @ServiceMethod
  def getChildProject(self, sessionId, groupIds, idProject):
    """
    Get child project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: list with experiment name his parent id and child id
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    child = []
    child = self.projectTools.getChildProject(idProject)
    return child

  @ServiceMethod
  def getTakingByProject(self, sessionId, groupIds, idProject):
    """
    Get taking by project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: list with experiment name his parent id and child id
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    taking = []
    taking = self.projectTools.getTakingByProject(idProject)
    return taking

  @ServiceMethod
  def getActionByTaking(self, sessionId, groupIds, nameTaking):
    """
    Get action by taking

    Author: Lysiane Hauguel 2019-07

    :param nameTaking: a taking name.
    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to which trees will be attached

    :return: list with ids and names of taking and his action
    """
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    action = []
    action = self.projectTools.getActionByTaking(nameTaking)
    return action

  @ServiceMethod
  def getExperimentsFromProjectId(self, sessionId, groupIds, projectId):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    projChildren = self.projectTools.getChildrenExperimentIds(projectId)
    exp = self.projectTools.getExperiments(fGroupIds, projChildren)
    return exp

if __name__=="__main__":
  service=projectService()
  handleCGI(service)
