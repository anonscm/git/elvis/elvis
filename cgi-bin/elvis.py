#!/usr/bin/env python3

from werkzeug.wrappers import Request, Response
from jsonrpc import WsgiServiceHandler
import config
import sys

def application(environ, start_response):
  print(sys.path)
  environ['PATH_INFO'] = environ['PATH_INFO'].replace(config.Config.WEB_PREFIX, '')
  request = Request(environ)
  text = WsgiServiceHandler(request).handleRequest()
  response = Response(text, mimetype='json/application')
  return response(environ, start_response)

if __name__ == '__main__':
  from werkzeug.serving import run_simple
  run_simple('localhost', 8081, application, use_reloader=True)
