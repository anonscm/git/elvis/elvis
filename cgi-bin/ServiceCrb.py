#!/usr/bin/env python3

from jsonrpc import handleCGI, ServiceMethod

import config
from elvis import SessionTools
from elvis import CrbTools

class ServiceCrb:
  """Cette classe repertorie tout les services disponibles
      Pour chaque service present il y a une fonction correspondante dans
      CrbTools.
      Se referer donc a la documentation de CrbTools"""

  def __init__(self):
    self.sessionTools = SessionTools()
    self.tool = CrbTools()

#///////////////////////////Methodes verifiees/////////////////////////////////
#//-----Users--------
    self._declareService("getUser")
    self._declareService("getUserById")
    self._declareService("updateUser")
    self._declareService("getGroupsByUser")
    self._declareService("getHostByName")

#//-----GhGroup--------
    self._declareService("setGhGroup")
    self._declareService("getGhGroupById")
#//-----Sites--------
    self._declareService("getAllSites")
    self._declareService("getSite")
    self._declareService("getSiteById")
    self._declareService("createSite")
    self._declareService("updateSite")
    self._declareService("createSiteWithCoord")
    self._declareService("getSiteByLot")

#//-----Pays--------
    self._declareService("getAllPays")
    self._declareService("getPays")
    self._declareService("getPaysById")


#//-----Lieux--------
    self._declareService("getAllTypeLieu")
    self._declareService("getTypeLieu")
    self._declareService("getTypeLieuById")
    self._declareService("createTypeLieu")
    self._declareService("updateTypeLieu")

#//-----Noms variété--------
    self._declareService("getAllNomVariete")
    self._declareService("getAllNomsByTypeNom")
    self._declareService("deleteNomVariete")
    self._declareService("addNomsVariete")
    self._declareService("addNomsClone")
    self._declareService("addNomsLot")
    self._declareService("getNomsByVariete")
    self._declareService("getNomVarieteById")
    self._declareService("getNomVarieteByClone")
    self._declareService("createNomVariete")
    self._declareService("updateNomVariete")
    self._declareService("deleteNomVariete")

#//-----Types noms variété-----
    self._declareService("getAllTypeNom")
    self._declareService("getTypeNom")
    self._declareService("getTypeNomById")
    self._declareService("createTypeNom")
    self._declareService("updateTypeNom")
    self._declareService("getAllNomVarieteByTypeNom")
    self._declareService("getNomByTypeNomAndVariete")

#//-----Taxinomie--------
    self._declareService("getAllGenre")
    self._declareService("getAllEspece")
    self._declareService("getAllTaxinomie")
    self._declareService("getTaxinomieByGenreAndEspece")
    self._declareService("getTaxinomieById")
    self._declareService("createTaxinomie")
    self._declareService("updateTaxinomie")
    self._declareService("getTaxinomieByVariete")
    self._declareService("getAllEspeceByGenre")

#//-----Pepinieriste--------
    self._declareService("getAllPepinieriste")
    self._declareService("getPepinieristeByNom")
    self._declareService("getPepinieristeById")
    self._declareService("createPepinieriste")
    self._declareService("updatePepinieriste")
    self._declareService("getAllPepinieristePays")

#//-----Collection--------
    self._declareService("getAllCollection")
    self._declareService("getCollection")
    self._declareService("getCollectionById")
    self._declareService("createCollection")
    self._declareService("updateCollection")
    self._declareService("addCloneToCollection")
    self._declareService("removeCloneFromCollection")
    self._declareService("getCollectionsByClone")
    self._declareService("deleteCollectionClone")


#//-----Variété--------
    self._declareService("createVariete")
    self._declareService("updateVariete")
    self._declareService("setTaxinomie")

#//-----Requêtes Variété--------
    self._declareService("getVarieteByNom")
    self._declareService("getAllVariete")
    self._declareService("getVarieteById")
    self._declareService("getVarieteByClone")
    self._declareService("getNomVarieteByClone")
    self._declareService("getVarieteByNom")
    self._declareService("getVarieteByNomClone")
    self._declareService("getVarieteByObtenteur")
    self._declareService("getVarieteByEditeur")
    self._declareService("getVarieteByGenre")
    self._declareService("getVarieteByEspece")
    self._declareService("getVarieteByNomVariete")
    self._declareService("getVarieteByNomAndTypeNom")
    self._declareService("getVarieteByNomAndTypeNomWithoutUserGroup")
    self._declareService("getVarieteByNumeroClone")
    self._declareService("getVarieteByDateIntroduction")
    self._declareService("getVarieteByCollection")
    self._declareService("getVarieteByNotation")
    self._declareService("getVarieteByLieu")
    self._declareService("getVarieteBySite")
    self._declareService("getVarieteByFournisseur")

#//-----Clone--------
    self._declareService("getAllNumeroClone")

    self._declareService("getIntroductionCloneById")
    self._declareService("createIntroductionClone")
    self._declareService("updateIntroductionClone")

    self._declareService("createClone")
    self._declareService("updateClone")

    self._declareService("createIntroduction")
    self._declareService("updateIntroduction")

    self._declareService("createMaterielVegetal")
    self._declareService("updateMaterielVegetal")



#//-----Requêtes clone---------
    self._declareService("getAllDateIntroduction")
    self._declareService("getAllClone")
    self._declareService("getCloneById")
    self._declareService("getCloneByIdVariete")
    self._declareService("getCloneByNumero")
    self._declareService("getCloneByNumeroWithoutGroups")
    self._declareService("getCloneByNom")
    self._declareService("getCloneByNumero2")
    self._declareService("getCloneByNumero3")
    self._declareService("getCloneByNomVariete")
    self._declareService("getCloneByObtenteur")
    self._declareService("getCloneByGenre")
    self._declareService("getCloneByEspece")
    self._declareService("getCloneByEditeur")
    self._declareService("getCloneByDateIntroduction")
    self._declareService("getCloneByFournisseur")
    self._declareService("getCloneByCollection")
    self._declareService("getIdCloneByNumero")
    self._declareService("getCloneByLot")
    self._declareService("getCloneByNotation")
    self._declareService("getCloneByLieu")
    self._declareService("getCloneBySite")
    self._declareService("getCloneByNomAndTypeNom")

#//------Arbres-----------------
    self._declareService("createArbre")
    self._declareService("updateArbre")
    self._declareService("getArbre")

#//------Graines-----------------
    self._declareService("createGraines")
    self._declareService("updateGraines")
    self._declareService("getGraines")

#//-------Requêtes sur les lot----------
    self._declareService("getAllLot")
    self._declareService("getLotByGroupe")
    self._declareService("getLotById")
    self._declareService("getIdLotByNumeroArbre")
    self._declareService("getIdLotByNumeroGraines")
    self._declareService("getLotByNumero")
    self._declareService("getLotByNomVariete")
    self._declareService("getLotByNumeroClone")
    self._declareService("getLotByNomClone")
    self._declareService("getLotByQuantite")
    self._declareService("getLotBySite")
    self._declareService("getLotByEquilibre")
    self._declareService("getLotByLieu")
    self._declareService("getLotByRangEvaluation")
    self._declareService("getLotByAnneePremierePousse")
    self._declareService("getLotByClone")
    self._declareService("getArbreByClone")
    self._declareService("getGrainesByClone")
    self._declareService("getLotByNotation")
    self._declareService("getLotByNomAndTypeNom")
    self._declareService("getLotByCollection")
    self._declareService("getLotByGenre")
    self._declareService("getLotByEspece")
    self._declareService("getLotByEditeur")
    self._declareService("getLotByDateIntroduction")
    self._declareService("getLotByFournisseur")
    self._declareService("getLotByObtenteur")

#//------Emplacement-----------------
    self._declareService("getEmplacementByLot")
    self._declareService("getEmplacement")
    self._declareService("createEmplacement")
    self._declareService("updateEmplacement")

#-------Lieu-----------------------
    self._declareService("deleteLieuByEmplacement")
    self._declareService("createLieu")
    self._declareService("updateLieu")
    self._declareService("deleteLieu")
    self._declareService("getLieu")
    self._declareService("getLieuxByEmplacement")
    self._declareService("getLieuByLot")
    self._declareService("getAllLieuByTypeLieu")

#-------Type notation------------------
    self._declareService("getAllTypeNotation")
    self._declareService("getTypeNotation")
    self._declareService("createTypeNotation")
    self._declareService("updateTypeNotation")
    self._declareService("getTypeByTypeNotation")
    self._declareService("getTypeNotationById")

#-------Groupes------------------
    self._declareService("getNextIdGroupe")
    self._declareService("incrementGroup")
    self._declareService("createGroupe")
    self._declareService("getGroupeByLot")
    self._declareService("getGroupeByIdLot")
    self._declareService("getGroupeByClone")

#-------GroupesANotation------------------
    self._declareService("setGroupeANotation")

#------Notations------------------
    self._declareService("createNotation")
    self._declareService("updateNotation")
    self._declareService("deleteNotation")
    self._declareService("getNotationsByLot")
    self._declareService("getNotationById")
    self._declareService("getAllValeurByNotation")

#------Multiplication------------------
    self._declareService("createMultiplication")
    self._declareService("updateMultiplication")
    self._declareService("getMultiplicationByLotDesc")
    self._declareService("getMultiplicationById")
    self._declareService("deleteMultiplicationByLotDesc");

#------Compteurs numméros clones------------------
# Commenter le 13 juin 2019 (décision collégiale)
# TODO : Si pas de problème côté utilisateurs,
# supprimer la table plant.cpt_numero_clone de la database
    # self._declareService("getNextValueCpt")
    # self._declareService("incrementCptRosa")
    # self._declareService("incrementCptMalus")
    # self._declareService("incrementCptDaucus")
    # self._declareService("updateNumeroCloneValue")

  @ServiceMethod
  def getAllVarieteCount(self, sessionId, groupIds):
    fGroupIds = self.sessionTools.getFilteredGroupIds(sessionId, groupIds)
    vn = self.tool.getAllVariete(groupIds)
    return len(vn)

  def _declareService(self, service_name):
    try :
      request = getattr(self.tool, service_name)
      request.__func__.IsServiceMethod = True
      setattr(self, service_name, request)
    except AttributeError as e:
      return "Error in declareService: " + str(e)

if __name__ == "__main__":
  service = ServiceCrb()
  handleCGI(service)
