#!/usr/bin/env python3

import sys
from jsonrpc import handleCGI, ServiceMethod

import config
from elvis import terminology

class terminologyService:

  """
  Methods concerning the terminology database schema

  AUTHORS :
  - Julie BOURBEILLON IRHS Bioinfo team


  SECTIONS :
  - Terminology management
  - Context management
  - Concept management
  - Term Management
  - Relation Management
  - Concept Graph Management
  - Language management
  - Translation management
  - User Interfaces
  """

#--------------------------#
#  TERMINOLOGY MANAGEMENT  #
#--------------------------#

  @ServiceMethod
  def getTerminologyCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getTerminologyCount()
    return res

  @ServiceMethod
  def getTerminologyById(self, terminology_id):
    terminologyTool = terminology()
    res = terminologyTool.getTerminologyById(terminology_id)
    return res

  @ServiceMethod
  def getTerminologyByName(self, terminology_name):
    terminologyTool = terminology()
    res = terminologyTool.getTerminologyByName(terminology_name)
    return res

  @ServiceMethod
  def getAllTerminology(self, status):
    terminologyTool = terminology()
    res = terminologyTool.getAllTerminology(status)
    return res

  @ServiceMethod
  def addTerminology(self, name, description):
    terminologyTool = terminology()
    res = terminologyTool.addTerminology(name, description)
    return res

  @ServiceMethod
  def checkTerminologyExists(self, name):
    terminologyTool = terminology()
    res = terminologyTool.checkTerminologyExists(name)
    return res

  @ServiceMethod
  def linkTerminologyToContext(self, id_terminology, id_context):
    terminologyTool = terminology()
    res = terminologyTool.linkTerminologyToContext(id_terminology, id_context)
    return res

  @ServiceMethod
  def setTerminologyDetails(self, name, description, obsolete, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.setTerminologyDetails(name, description, obsolete, id_terminology)
    return res

  @ServiceMethod
  def setTerminologyContexts(self, id_terminology, contexts):
    terminologyTool = terminology()
    res = terminologyTool.setTerminologyContexts(id_terminology, contexts)
    return res

#----------------------#
#  CONTEXT MANAGEMENT  #
#----------------------#

  @ServiceMethod
  def getContextCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getContextCount()
    return res

  @ServiceMethod
  def getContextById(self, id_context):
    terminologyTool = terminology()
    res = terminologyTool.getContextById(id_context)
    return res

  @ServiceMethod
  def getContextByName(self, context_name):
    terminologyTool = terminology()
    res = terminologyTool.getContextByName(context_name)
    return res

  @ServiceMethod
  def getAllContext(self):
    terminologyTool = terminology()
    res = terminologyTool.getAllContext()
    return res

  @ServiceMethod
  def getContextByTerminology(self, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.getContextByTerminology(id_terminology)
    return res

  @ServiceMethod
  def getContextByTerm(self, id_term):
    terminologyTool = terminology()
    res = terminologyTool.getContextByTerm(id_term)
    return res

  @ServiceMethod
  def addContext(self, name, description):
    terminologyTool = terminology()
    res = terminologyTool.addContext(name, description)
    return res

  @ServiceMethod
  def setContextDetails(self, name, description, id_context):
    terminologyTool = terminology()
    res = terminologyTool.setContextDetails(name, description, id_context)
    return res

  @ServiceMethod
  def checkContextExists(self, name):
    terminologyTool = terminology()
    res = terminologyTool.checkContextExists(name)
    return res

#----------------------#
#  CONCEPT MANAGEMENT  #
#----------------------#

  @ServiceMethod
  def getConceptCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getConceptCount()
    return res

  @ServiceMethod
  def getConceptById(self, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.getConceptById(id_concept)
    return res

  @ServiceMethod
  def getConceptByLabel(self, concept_label):
    terminologyTool = terminology()
    res = terminologyTool.getConceptByLabel(concept_label)
    return res

  @ServiceMethod
  def getConceptByTerminology(self, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.getConceptByTerminology(id_terminology)
    return res

  @ServiceMethod
  def getAllConcept(self):
    terminologyTool = terminology()
    res = terminologyTool.getAllConcept()
    return res

  @ServiceMethod
  def checkConceptExists(self, label, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.checkConceptExists(label, id_terminology)
    return res

  @ServiceMethod
  def addConcept(self, label, description, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.addConcept(label, description, id_terminology)
    return res

  @ServiceMethod
  def setConceptDetails(self, label, description, obsolete, id_terminology, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.setConceptDetails(label, description, obsolete, id_terminology, id_concept)
    return res

#-------------------#
#  TERM MANAGEMENT  #
#-------------------#

  @ServiceMethod
  def getTermCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getTermCount()
    return res

  @ServiceMethod
  def getAllTerm(self):
    terminologyTool = terminology()
    res = terminologyTool.getAllTerm()
    return res

  @ServiceMethod
  def getTermById(self, id_term):
    terminologyTool = terminology()
    res = terminologyTool.getTermById(id_term)
    return res

  @ServiceMethod
  def getTermByConcept(self, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.getTermByConcept(id_concept)
    return res

  @ServiceMethod
  def checkTermExists(self, label, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.checkTermExists(label, id_concept)
    return res

  @ServiceMethod
  def addTerm(self, label, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.addTerm(label, id_concept)
    return res

  @ServiceMethod
  def setTermDetails(self, label, obsolete, id_concept, id_term):
    terminologyTool = terminology()
    res = terminologyTool.setTermDetails(label, obsolete, id_concept, id_term)
    return res

  @ServiceMethod
  def setTermContexts(self, id_term, contexts):
    terminologyTool = terminology()
    res = terminologyTool.setTermContexts(id_term, contexts)
    return res

#-----------------------#
#  RELATION MANAGEMENT  #
#-----------------------#

  @ServiceMethod
  def getRelationCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getRelationCount()
    return res

  @ServiceMethod
  def getAllRelation(self):
    terminologyTool = terminology()
    res = terminologyTool.getAllRelation()
    return res

  @ServiceMethod
  def getRelationById(self, relation_id):
    terminologyTool = terminology()
    res = terminologyTool.getRelationById(relation_id)
    return res

  @ServiceMethod
  def checkRelationExists(self, name):
    terminologyTool = terminology()
    res = terminologyTool.checkRelationExists(name)
    return res

  @ServiceMethod
  def addRelation(self, name, description):
    terminologyTool = terminology()
    res = terminologyTool.addRelation(name, description)
    return res

  @ServiceMethod
  def setRelationDetails(self, name, description, obsolete, id_relationship):
    terminologyTool = terminology()
    res = terminologyTool.setRelationDetails(name, description, obsolete, id_relationship)
    return res

#----------------------------#
#  CONCEPT GRAPH MANAGEMENT  #
#----------------------------#

  @ServiceMethod
  def getConceptGraphCount(self):
    terminologyTool = terminology()
    res = terminologyTool.getConceptGraphCount()
    return res

  @ServiceMethod
  def getConceptGraphByTerminology(self, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.getConceptGraphByTerminology(id_terminology)
    return res

  @ServiceMethod
  def checkTripleExists(self, id_left_concept, id_relationship, id_right_concept):
    terminologyTool = terminology()
    res = terminologyTool.checkTripleExists(id_left_concept, id_relationship, id_right_concept)
    return res

  @ServiceMethod
  def setTriple(self, id_left_concept, id_relationship, id_right_concept, id_left_concept_old, id_relationship_old, id_right_concept_old):
    terminologyTool = terminology()
    res = terminologyTool.setTriple(id_left_concept, id_relationship, id_right_concept, id_left_concept_old, id_relationship_old, id_right_concept_old)
    return res

  @ServiceMethod
  def addTriple(self, id_left_concept, id_relationship, id_right_concept):
    terminologyTool = terminology()
    res = terminologyTool.addTriple(id_left_concept, id_relationship, id_right_concept)
    return res

#-----------------------#
#  LANGUAGE MANAGEMENT  #
#-----------------------#

  @ServiceMethod
  def getAllLanguage(self):
    terminologyTool = terminology()
    res = terminologyTool.getAllLanguage()
    return res

  @ServiceMethod
  def getLanguageById(self, language_id):
    terminologyTool = terminology()
    res = terminologyTool.getLanguageById(language_id)
    return res

  @ServiceMethod
  def checkLanguageExists(self, code):
    terminologyTool = terminology()
    res = terminologyTool.checkLanguageExists(code)
    return res

  @ServiceMethod
  def addLanguage(self, code, name):
    terminologyTool = terminology()
    res = terminologyTool.addLanguage(code, name)
    return res

  @ServiceMethod
  def setLanguageDetails(self, code, name, id_language):
    terminologyTool = terminology()
    res = terminologyTool.setLanguageDetails(code, name, id_language)
    return res

#--------------------------#
#  TRANSLATION MANAGEMENT  #
#--------------------------#

  @ServiceMethod
  def getTranslationByTerminology(self, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.getTranslationByTerminology(id_terminology)
    return res

  @ServiceMethod
  def getTranslationByTerm(self, id_term):
    terminologyTool = terminology()
    res = terminologyTool.getTranslationByTerm(id_term)
    return res

  @ServiceMethod
  def addTerminologyTranslation(self, label, id_language, id_terminology):
    terminologyTool = terminology()
    res = terminologyTool.addTerminologyTranslation(label, id_language, id_terminology)
    return res

  @ServiceMethod
  def setTranslationDetails(self, label, id_language, id_translation):
    terminologyTool = terminology()
    res = terminologyTool.setTranslationDetails(label, id_language, id_translation)
    return res

  @ServiceMethod
  def deleteTerminologyTranslation(self, id_translation):
    terminologyTool = terminology()
    res = terminologyTool.deleteTerminologyTranslation(id_translation)
    return res

#-------------------#
#  USER INTERFACES  #
#-------------------#
# Queries design for external user interfaces which are exploiting the terminology database schema

  @ServiceMethod
  def getTermsList(self, id_terminology, id_context):
    terminologyTool = terminology()
    res = terminologyTool.getTermsList(id_terminology, id_context)
    return res

  @ServiceMethod
  def getTermsByTerminologyName(self, terminology_name, id_context):
    terminologyTool = terminology()
    res = terminologyTool.getTermsByTerminologyName(terminology_name, id_context)
    return res

  @ServiceMethod
  def getConceptNeighbours(self, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.getConceptNeighbours(id_concept, id_context)
    return res

  @ServiceMethod
  def getTermsByConceptContext(self, id_concept, id_context):
    terminologyTool = terminology()
    res = terminologyTool.getTermsByConceptContext(id_concept, id_context)
    return res

#-------------------#
#  USER INTERFACES  #
#-------------------#
# Queries design for Hervé user interface
  @ServiceMethod
  def getAllConceptSample(self, sessionId, idNull):
    terminologyTool = terminology()
    res = terminologyTool.getAllConcept()
    return res

  @ServiceMethod
  def getTermByConceptSample(self, sessionId, id_concept):
    terminologyTool = terminology()
    res = terminologyTool.getTermByConcept(id_concept)
    return res

#//−−−−−−−MAIN−−−−−−−−−−
if __name__ == "__main__":
  service = terminologyService()
  handleCGI(service)
