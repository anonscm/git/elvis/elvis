#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, "/home/sgaillard/public_html/elvis/lib/")
#import ..cgi-bin.config
from json import loads, dumps

from elvis import NotationTools as NT
from elvis import PlantTools as PT

def main():
  fGroupIds = [1020]
  experimentName = '2009'
  nto = NT.NotationTools()
  pto = PT.PlantTools()
  #nt = nto.getNotationType(name = "éliminé")
  nt = nto.getNotationType(id = 150)
  # print("Id   : {}".format(nt.getId()))
  # print("Name : {}".format(nt.getName()))
  # print("Type : {}".format(nt.getType()))

  notations = loads('[{"id":null,"value":"2","date":"20/03/2019","comment":null,"siteId":null,"type":"éliminé","observer":"Sylvain Gaillard"}]')
  print("loaded notations")
  notationIds = nto.saveNotations(fGroupIds, notations)
  print("saved notation")
  lot = pto.getLot(id = 32301)
  print("got lot")
  nto.linkNotationsToLot(lot.getId(), notationIds)
  print("linked notations to lot")
  nto.linkNotationsToExperiment(fGroupIds, notationIds, experimentName)
  print("linked notations to experiment")

if __name__ == '__main__':
  main()
