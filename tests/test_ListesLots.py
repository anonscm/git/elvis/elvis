#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, "/home/sgaillard/public_html/elvis/lib/")
#import ..cgi-bin.config
from json import loads, dumps

from elvis import NotationTools as NT
from elvis import PlantTools as PT
from elvis.selection_poirier import SelectionPoirierTools as SPT

def main():
  fGroupIds = [1009]
  experimentName = '2018'
  # nto = NT.NotationTools()
  # pto = PT.PlantTools()
  # ntElim = nto.getNotationType(name = "éliminé")
  # ntFlo = nto.getNotationType(name = "floraison")
  # notation = [
    # {'id_notation_type': int(ntElim.getId()), 'value_notation': '0'},
    # {'id_notation_type': int(ntFlo.getId()), 'value_notation': '>2'}
  # ]
  # nt = nto.getNotationType(id = 150)
  # print("Id   : {}".format(nt.getId()))
  # print("Name : {}".format(nt.getName()))
  # print("Type : {}".format(nt.getType()))
#
  # placeType = pto.getLocationType(name = "parcelle")
  # placeValue = {"id_place_type" : placeType.getId(), "value_place" : "P5"}
  # listParcelle = set(pto.searchLotIdByPlaceValue(fGroupIds, placeValue))
  # listElim = set(pto.searchLotIdByNotationValue(fGroupIds, notation))
  # listPnotE = listParcelle - listElim
  # listPnotE = listParcelle & listElim
  # print(dumps(len(listPnotE)))

  spt = SPT.SelectionPoirierTools()
  listTrees = spt.getlisteArbreByParcellePoirier("P5", 2018, 2016, "Floraison", 2, 2, 2, fGroupIds)
  print(dumps(sorted(listTrees)))

if __name__ == '__main__':
  main()
