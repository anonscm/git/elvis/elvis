#!/bin/bash
# Lance les tests unitaires predefinis pour chaque methode
# et pour chaque service précisés en arguments si besoin
#
# Auteure : Sandra Pelletier
#
# A lancer dans le dossier de tests
# elvis_tester installé sur /usr/local/bin
# Les noms de fichier de test sont service.methode.json
# Ex pour getLot de servicePlant : plant.getLot.json
#
# Ex sur toutes les méthodes :
#   ./serviceTest.sh
# Ex sur seulement les méthodes de plant et project :
#   ./serviceTest.sh plant project

arg=( )
if [ $# -lt 1 ]
then
  arg="*.*.json"
else
  for a in $@
  do
    arg=( ${arg[@]} $a".*.json" )
  done
fi
ok=0
er=0
for file in ${arg[@]}
do
  if [ -f $file ]
  then
    echo "== testing $file =="
    res1=$(elvis_tester $file)
    res2=$(echo $res1 | grep -c '"error": null')
    if [ $res2 != 1 ]
    then
      ((er++))
      echo
      elvis_tester $file
      echo ; echo "=========================================" ; echo
    else
      ((ok++))
    fi
  fi
done
echo
echo $ok 'test(s) ok'
echo $er 'error(s)'
echo 'serviceTest done'
