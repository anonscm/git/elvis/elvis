-- Enregistrement de données générales
--   * usergroup unité
--   * link users-unité

INSERT INTO users.usergroup (
  name,
  description,
  active
)
VALUES (
  'IRHS',
  'Ensemble des users de appartenant à IRHS',
  TRUE
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
SELECT u.id, g.id
  FROM users."user" AS u,
  users.usergroup AS g
    WHERE g.name = 'IRHS'
;
