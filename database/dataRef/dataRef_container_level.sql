INSERT INTO sample.container_level (
  level
)
VALUES (
  "full"
);

INSERT INTO sample.container_level (
  level
)
VALUES (
  "empty"
);

INSERT INTO sample.container_level (
  level
)
VALUES (
  "started"
);

INSERT INTO sample.container_level (
  level
)
VALUES (
  "few"
);
