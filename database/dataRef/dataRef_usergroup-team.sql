-- Création du usergroup
-- Déclaration type équipe

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'Arch-E',
  'Biologie Intégrative de l’Architecture et Environnement',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'Arch-E'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'GDO',
  'Génétique et Diversité des plantes Ornementales',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'GDO'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'QualiPom',
  'Génétique, Ecophysiologie et Modélisation des Pomoïdeae',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'QualiPom'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'ResPom',
  'Résistance du pommier et du poirier aux bioagresseurs',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'ResPom'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'EcoFun',
  'Ecologie évolutive chez les champignons',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'EcoFun'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'QuaRVeg',
  'Qualité et résistance aux bioagresseurs des espèces légumières',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'QuaRVeg'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'EmerSys',
  'Emergence, systématique et écologie des bactéries phytopathogènes',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'EmerSys'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'FungiSem',
  'Pathologies fongiques des semences',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'FungiSem'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'Conserto',
  'Conservation et tolérance aux stress des semences',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'Conserto'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'SMS',
  'Seedling, Metabolism and Stress',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'SMS'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'BiDefI',
  'Bioinformatics for plant Defense Investigations',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'BiDefI'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup(
  name,
  description,
  active
)
VALUES (
  'EpiCenter',
  'EpiCenter',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'EpiCenter'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);
