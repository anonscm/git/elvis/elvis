INSERT INTO users.usergroup_type(
  name,
  description
)
VALUES (
  'team',
  'Groupe de type équipe'
);

INSERT INTO users.usergroup_type(
  name,
  description
)
VALUES (
  'project',
  'Groupe défini par la liste des contributeurs pour une expérience de type project'
);
