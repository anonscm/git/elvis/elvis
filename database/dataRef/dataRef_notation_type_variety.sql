-- notation_type
-- lien_context_notation

-- Statut biologique
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Statut biologique',
  'list'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Statut biologique'
  )
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Statut biologique'
  ),
  'non OGM',
  0
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Statut biologique'
  ),
  'OGM',
  1
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Statut biologique'
  ),
  'non quarantaine',
  2
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Statut biologique'
  ),
  'quarantaine',
  3
);

-- Génotype
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Génotype',
  'list'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Génotype'
  )
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Génotype'
  ),
  'Wild type',
  0
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Génotype'
  ),
  'Mutant',
  1
);

-- Résistance et sensibilité
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'résistance',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'résistance'
  )
);

INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'sensibilité',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'sensibilité'
  )
);

-- Gène muté
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Gène muté',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Gène muté'
  )
);

-- Phénotype
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'phenotype',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'phenotype'
  )
);

-- Référence bibliographique
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Référence bibliographique',
  'document'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Référence bibliographique'
  )
);

-- Origine
INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'origine géographique'
  )
);

-- Informations complémentaires
INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'autre information'
  )
);

-- Agent mutagène
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Agent mutagène',
  'list'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'T-DNA',
  0
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'EMS',
  1
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'Rayons UV',
  2
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'Rayons X',
  3
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'Rayons gamma',
  4
);

INSERT INTO notation.notation_list_values (
  id_type_notation,
  value,
  code
)
VALUES (
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  ),
  'CRISPR/Cas9',
  5
);
