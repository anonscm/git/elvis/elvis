INSERT INTO sample.action_type (
  type
)
VALUES (
  'DNA extraction'
);

INSERT INTO sample.action_type (
  type
)
VALUES (
  'RNA extraction'
);

INSERT INTO sample.action_type (
  type
)
VALUES (
  'Protein extraction'
);

INSERT INTO sample.action_type (
  type
)
VALUES (
  'PCR'
);

INSERT INTO sample.action_type (
  type
)
VALUES (
  'Cy3 direct labelling'
);

INSERT INTO sample.action_type (
  type
)
VALUES (
  'Cy5 direct labelling'
);
