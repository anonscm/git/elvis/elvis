-- notation_type
-- lien_context_notation

-- N° demande INEM
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'N° demande INEM',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description lots de graines'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'N° demande INEM'
  )
);

-- Salle de culture
INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Salle de culture',
  'texte'
);

INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description lots de graines'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'Salle de culture'
  )
);
-- Informations complémentaires
INSERT INTO notation.lien_contexte_notation (
  id_context,
  id_notation_type
)
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description lots de graines'
  ),
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE notation_type.name = 'autre information'
  )
);
