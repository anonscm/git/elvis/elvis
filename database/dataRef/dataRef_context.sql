-- context
-- context_has_usergroup

-- variety
INSERT INTO notation.context (
  name,
  comment
  )
VALUES (
  'Description plantes',
  'types de notations générales liées à la description des plantes'
);

INSERT INTO notation.context_has_usergroup (
  id_context,
  id_usergroup
  )
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description plantes'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'IRHS'
  )
);

-- lots
INSERT INTO notation.context (
  name,
  comment
  )
VALUES (
  'Description lots de graines',
  'types de notations générales liées à la description des lots de plantes'
);

INSERT INTO notation.context_has_usergroup (
  id_context,
  id_usergroup
  )
VALUES (
  (SELECT id
    FROM notation.context
    WHERE context.name = 'Description lots de graines'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'IRHS'
  )
);
