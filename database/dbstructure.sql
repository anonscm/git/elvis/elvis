--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.15
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: elvis; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE elvis WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';


\connect elvis

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: address_book; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA address_book;


--
-- Name: analytics; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA analytics;


--
-- Name: bibliography; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA bibliography;


--
-- Name: collection; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA collection;


--
-- Name: documentation; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA documentation;


--
-- Name: experiment; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA experiment;


--
-- Name: import_export; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA import_export;


--
-- Name: location; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA location;


--
-- Name: notation; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA notation;


--
-- Name: plant; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA plant;


--
-- Name: plant_cross; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA plant_cross;


--
-- Name: sample; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA sample;


--
-- Name: terminology; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA terminology;


--
-- Name: users; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA users;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: address_book; Owner: -
--

CREATE TABLE address_book.address (
    id integer NOT NULL,
    lastname character varying(200),
    address1 character varying(100),
    address2 character varying(100),
    city character varying(50),
    state character varying(50),
    postal_code character varying(50),
    id_country integer,
    email character(50),
    phone character varying(20),
    firstname text
);


--
-- Name: country; Type: TABLE; Schema: address_book; Owner: -
--

CREATE TABLE address_book.country (
    id integer NOT NULL,
    name character varying(50)
);


--
-- Name: pays_id_pays_seq; Type: SEQUENCE; Schema: address_book; Owner: -
--

CREATE SEQUENCE address_book.pays_id_pays_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pays_id_pays_seq; Type: SEQUENCE OWNED BY; Schema: address_book; Owner: -
--

ALTER SEQUENCE address_book.pays_id_pays_seq OWNED BY address_book.country.id;


--
-- Name: pepinieriste_id_pepinieriste_seq; Type: SEQUENCE; Schema: address_book; Owner: -
--

CREATE SEQUENCE address_book.pepinieriste_id_pepinieriste_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pepinieriste_id_pepinieriste_seq; Type: SEQUENCE OWNED BY; Schema: address_book; Owner: -
--

ALTER SEQUENCE address_book.pepinieriste_id_pepinieriste_seq OWNED BY address_book.address.id;


--
-- Name: site; Type: TABLE; Schema: address_book; Owner: -
--

CREATE TABLE address_book.site (
    id_site integer NOT NULL,
    id_address integer,
    name character varying(50),
    latitude text,
    longitude text,
    elevation text
);


--
-- Name: site_id_site_seq; Type: SEQUENCE; Schema: address_book; Owner: -
--

CREATE SEQUENCE address_book.site_id_site_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: site_id_site_seq; Type: SEQUENCE OWNED BY; Schema: address_book; Owner: -
--

ALTER SEQUENCE address_book.site_id_site_seq OWNED BY address_book.site.id_site;


--
-- Name: comparison; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.comparison (
    id integer NOT NULL,
    filename character varying[] NOT NULL,
    id_groupe integer NOT NULL,
    id_ctrl integer NOT NULL,
    id_ttmt integer NOT NULL
);


--
-- Name: comparison_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.comparison_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comparison_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.comparison_id_seq OWNED BY analytics.comparison.id;


--
-- Name: export; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.export (
    id integer NOT NULL,
    id_rscript integer NOT NULL,
    name character varying[] NOT NULL
);


--
-- Name: export_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.export_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: export_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.export_id_seq OWNED BY analytics.export.id;


--
-- Name: intensity; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.intensity (
    id integer NOT NULL,
    id_sens integer NOT NULL,
    control boolean NOT NULL,
    int_min real NOT NULL,
    int_mean real NOT NULL,
    int_med real NOT NULL,
    int_max real NOT NULL,
    bkg real NOT NULL,
    bkg_mean real NOT NULL,
    bkg_dev real NOT NULL
);


--
-- Name: intensity_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.intensity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: intensity_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.intensity_id_seq OWNED BY analytics.intensity.id;


--
-- Name: rscript; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.rscript (
    id integer NOT NULL,
    date date NOT NULL,
    source character varying[] NOT NULL,
    id_groupe integer NOT NULL,
    id_design integer NOT NULL,
    direct boolean NOT NULL,
    ctrl_name character varying[] NOT NULL,
    ttmt_name character varying[] NOT NULL
);


--
-- Name: rscript_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.rscript_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rscript_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.rscript_id_seq OWNED BY analytics.rscript.id;


--
-- Name: rscripts; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.rscripts (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    source character varying(32) NOT NULL,
    value text NOT NULL
);


--
-- Name: rscripts_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.rscripts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rscripts_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.rscripts_id_seq OWNED BY analytics.rscripts.id;


--
-- Name: sens; Type: TABLE; Schema: analytics; Owner: -
--

CREATE TABLE analytics.sens (
    id integer NOT NULL,
    id_export integer NOT NULL,
    sens boolean NOT NULL,
    var real NOT NULL,
    remove integer NOT NULL,
    pval_nb integer NOT NULL,
    pval_min real,
    bh_nb integer NOT NULL,
    bh_min real,
    int_sup_05 integer NOT NULL,
    int_sup_1 integer NOT NULL,
    total integer NOT NULL
);


--
-- Name: sens_id_seq; Type: SEQUENCE; Schema: analytics; Owner: -
--

CREATE SEQUENCE analytics.sens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sens_id_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: -
--

ALTER SEQUENCE analytics.sens_id_seq OWNED BY analytics.sens.id;


--
-- Name: reference; Type: TABLE; Schema: bibliography; Owner: -
--

CREATE TABLE bibliography.reference (
    id integer NOT NULL,
    id_variety integer NOT NULL,
    author character varying,
    title character varying,
    date date,
    page character varying,
    plank character varying,
    chapter character varying,
    variety_number character varying,
    tome character varying,
    livret character varying,
    illustration character varying
);


--
-- Name: reference_id_reference_seq; Type: SEQUENCE; Schema: bibliography; Owner: -
--

CREATE SEQUENCE bibliography.reference_id_reference_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reference_id_reference_seq; Type: SEQUENCE OWNED BY; Schema: bibliography; Owner: -
--

ALTER SEQUENCE bibliography.reference_id_reference_seq OWNED BY bibliography.reference.id;


--
-- Name: collection; Type: TABLE; Schema: collection; Owner: -
--

CREATE TABLE collection.collection (
    id_collection integer NOT NULL,
    name text
);


--
-- Name: collection_has_accession; Type: TABLE; Schema: collection; Owner: -
--

CREATE TABLE collection.collection_has_accession (
    id_collection integer NOT NULL,
    id_clone integer NOT NULL
);


--
-- Name: collection_id_collection_seq; Type: SEQUENCE; Schema: collection; Owner: -
--

CREATE SEQUENCE collection.collection_id_collection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: collection_id_collection_seq; Type: SEQUENCE OWNED BY; Schema: collection; Owner: -
--

ALTER SEQUENCE collection.collection_id_collection_seq OWNED BY collection.collection.id_collection;


--
-- Name: protocol; Type: TABLE; Schema: documentation; Owner: -
--

CREATE TABLE documentation.protocol (
    id integer NOT NULL,
    name character varying(256) NOT NULL,
    adress character varying(256),
    summary text NOT NULL,
    reference_laboratory_handbook character varying(256) NOT NULL,
    reference_alfresco character varying(256)
);


--
-- Name: protocol_id_seq; Type: SEQUENCE; Schema: documentation; Owner: -
--

CREATE SEQUENCE documentation.protocol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: protocol_id_seq; Type: SEQUENCE OWNED BY; Schema: documentation; Owner: -
--

ALTER SEQUENCE documentation.protocol_id_seq OWNED BY documentation.protocol.id;


--
-- Name: axis; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.axis (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


--
-- Name: axis_id_seq; Type: SEQUENCE; Schema: experiment; Owner: -
--

CREATE SEQUENCE experiment.axis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: axis_id_seq; Type: SEQUENCE OWNED BY; Schema: experiment; Owner: -
--

ALTER SEQUENCE experiment.axis_id_seq OWNED BY experiment.axis.id;


--
-- Name: experiment_id_seq; Type: SEQUENCE; Schema: experiment; Owner: -
--

CREATE SEQUENCE experiment.experiment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: experiment; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment (
    id integer DEFAULT nextval('experiment.experiment_id_seq'::regclass) NOT NULL,
    name character varying(256),
    description text,
    creation_date date,
    title text,
    active boolean DEFAULT true,
    date_start date,
    date_end date,
    code character varying(256),
    id_user integer,
    id_experiment_type integer
);


--
-- Name: experiment_has_axis; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_axis (
    id_experiment integer NOT NULL,
    id_axis integer NOT NULL
);


--
-- Name: experiment_has_clone; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_clone (
    id_experiment integer NOT NULL,
    id_clone integer NOT NULL
);


--
-- Name: TABLE experiment_has_clone; Type: COMMENT; Schema: experiment; Owner: -
--

COMMENT ON TABLE experiment.experiment_has_clone IS 'Group';


--
-- Name: experiment_has_container; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_container (
    id_experiment integer NOT NULL,
    id_container integer NOT NULL
);


--
-- Name: TABLE experiment_has_container; Type: COMMENT; Schema: experiment; Owner: -
--

COMMENT ON TABLE experiment.experiment_has_container IS 'Serie';


--
-- Name: experiment_has_experiment; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_experiment (
    id_experiment_child integer NOT NULL,
    id_experiment integer NOT NULL
);


--
-- Name: experiment_has_financing; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_financing (
    id_experiment integer NOT NULL,
    id_financing integer NOT NULL
);


--
-- Name: experiment_has_group; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_group (
    id_group integer NOT NULL,
    id_experiment integer NOT NULL
);


--
-- Name: experiment_has_notation; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_notation (
    id_experiment integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: TABLE experiment_has_notation; Type: COMMENT; Schema: experiment; Owner: -
--

COMMENT ON TABLE experiment.experiment_has_notation IS 'Campaign';


--
-- Name: experiment_has_status; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_status (
    id_experiment integer NOT NULL,
    id_status integer NOT NULL,
    date date
);


--
-- Name: experiment_has_user; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_user (
    id_experiment integer NOT NULL,
    id_user integer NOT NULL
);


--
-- Name: experiment_has_usergroup; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_has_usergroup (
    id_experiment integer NOT NULL,
    id_usergroup integer NOT NULL,
    writable boolean NOT NULL
);


--
-- Name: experiment_type; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.experiment_type (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


--
-- Name: experiment_type_id_seq; Type: SEQUENCE; Schema: experiment; Owner: -
--

CREATE SEQUENCE experiment.experiment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: experiment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: experiment; Owner: -
--

ALTER SEQUENCE experiment.experiment_type_id_seq OWNED BY experiment.experiment_type.id;


--
-- Name: financing; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.financing (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


--
-- Name: financing_id_seq; Type: SEQUENCE; Schema: experiment; Owner: -
--

CREATE SEQUENCE experiment.financing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: financing_id_seq; Type: SEQUENCE OWNED BY; Schema: experiment; Owner: -
--

ALTER SEQUENCE experiment.financing_id_seq OWNED BY experiment.financing.id;


--
-- Name: status; Type: TABLE; Schema: experiment; Owner: -
--

CREATE TABLE experiment.status (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


--
-- Name: status_type_id_seq; Type: SEQUENCE; Schema: experiment; Owner: -
--

CREATE SEQUENCE experiment.status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_type_id_seq; Type: SEQUENCE OWNED BY; Schema: experiment; Owner: -
--

ALTER SEQUENCE experiment.status_type_id_seq OWNED BY experiment.status.id;


--
-- Name: introduction; Type: TABLE; Schema: import_export; Owner: -
--

CREATE TABLE import_export.introduction (
    id_intro integer NOT NULL,
    groupe integer,
    date_intro date,
    fournisseur integer
);


--
-- Name: introduction_clone; Type: TABLE; Schema: import_export; Owner: -
--

CREATE TABLE import_export.introduction_clone (
    id integer NOT NULL,
    etat_sanitaire character varying(50),
    mode_introduction character varying(50),
    numero_passeport_phytosanitaire character varying(150),
    lieu_greffage character(50)
);


--
-- Name: introduction_id_intro_seq; Type: SEQUENCE; Schema: import_export; Owner: -
--

CREATE SEQUENCE import_export.introduction_id_intro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: introduction_id_intro_seq; Type: SEQUENCE OWNED BY; Schema: import_export; Owner: -
--

ALTER SEQUENCE import_export.introduction_id_intro_seq OWNED BY import_export.introduction.id_intro;


--
-- Name: introduction_var_id_seq; Type: SEQUENCE; Schema: import_export; Owner: -
--

CREATE SEQUENCE import_export.introduction_var_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: introduction_var_id_seq; Type: SEQUENCE OWNED BY; Schema: import_export; Owner: -
--

ALTER SEQUENCE import_export.introduction_var_id_seq OWNED BY import_export.introduction_clone.id;


--
-- Name: place; Type: TABLE; Schema: location; Owner: -
--

CREATE TABLE location.place (
    id_lieu integer NOT NULL,
    id_plant_place integer,
    id_place_type integer NOT NULL,
    value character varying(64)
);


--
-- Name: lieu_id_lieu_seq; Type: SEQUENCE; Schema: location; Owner: -
--

CREATE SEQUENCE location.lieu_id_lieu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lieu_id_lieu_seq; Type: SEQUENCE OWNED BY; Schema: location; Owner: -
--

ALTER SEQUENCE location.lieu_id_lieu_seq OWNED BY location.place.id_lieu;


--
-- Name: place_type; Type: TABLE; Schema: location; Owner: -
--

CREATE TABLE location.place_type (
    id integer NOT NULL,
    value character varying(64) NOT NULL
);


--
-- Name: place_type_has_terminology; Type: TABLE; Schema: location; Owner: -
--

CREATE TABLE location.place_type_has_terminology (
    id_type_lieu integer NOT NULL,
    id_concept integer NOT NULL,
    id_context integer NOT NULL
);


--
-- Name: type_lieu_id_seq; Type: SEQUENCE; Schema: location; Owner: -
--

CREATE SEQUENCE location.type_lieu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_lieu_id_seq; Type: SEQUENCE OWNED BY; Schema: location; Owner: -
--

ALTER SEQUENCE location.type_lieu_id_seq OWNED BY location.place_type.id;


--
-- Name: action_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.action_has_notation (
    id_action integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: container_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.container_has_notation (
    id_container integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: contexte_notation_id; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.contexte_notation_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: context; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.context (
    id integer DEFAULT nextval('notation.contexte_notation_id'::regclass) NOT NULL,
    name text NOT NULL,
    id_usergroup integer,
    comment text
);


--
-- Name: context_has_usergroup; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.context_has_usergroup (
    id_context integer NOT NULL,
    id_usergroup integer NOT NULL
);


--
-- Name: cross_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.cross_has_notation (
    id_cross integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: cross_seed_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.cross_seed_has_notation (
    id_lot_pepins integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: lien_contexte_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.lien_contexte_notation (
    id_context integer NOT NULL,
    id_notation_type integer NOT NULL,
    rank integer
);


--
-- Name: lot_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.lot_has_notation (
    id_lot integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation (
    id_notation integer NOT NULL,
    date date,
    comment text,
    id_site integer,
    id_type_notation integer DEFAULT 1 NOT NULL,
    observer text
);


--
-- Name: notation_date; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_date (
    value date NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_document; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_document (
    value text NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_double; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_double (
    value double precision NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_has_protocol; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_has_protocol (
    id_notation integer NOT NULL,
    id_protocol integer NOT NULL
);


--
-- Name: notation_has_user; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_has_user (
    id_user integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: notation_has_usergroup; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_has_usergroup (
    id_notation integer NOT NULL,
    id_usergroup integer NOT NULL,
    write boolean
);


--
-- Name: notation_id_notation_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.notation_id_notation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notation_id_notation_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.notation_id_notation_seq OWNED BY notation.notation.id_notation;


--
-- Name: notation_integer; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_integer (
    value integer NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_list; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_list (
    value integer NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_list_values; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_list_values (
    id integer NOT NULL,
    id_type_notation integer NOT NULL,
    value text NOT NULL,
    code integer NOT NULL
);


--
-- Name: notation_list_values_id_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.notation_list_values_id_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: notation_list_values_id_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.notation_list_values_id_seq OWNED BY notation.notation_list_values.id;


--
-- Name: notation_texte; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_texte (
    value text NOT NULL
)
INHERITS (notation.notation);


--
-- Name: notation_type; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_type (
    id_type_notation integer NOT NULL,
    name text,
    type text
);


--
-- Name: notation_type_constraint; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.notation_type_constraint (
    id integer NOT NULL,
    code character varying(2),
    value text,
    id_type_notation integer DEFAULT 1
);


--
-- Name: taking_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.taking_has_notation (
    id_taking integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: type_notation_id_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.type_notation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_notation_id_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.type_notation_id_seq OWNED BY notation.notation_type.id_type_notation;


--
-- Name: type_notation_id_type_notation_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.type_notation_id_type_notation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_notation_id_type_notation_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.type_notation_id_type_notation_seq OWNED BY notation.notation_type_constraint.id;


--
-- Name: utilisateur; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.utilisateur (
    id_utilisateur integer NOT NULL,
    nom character(1),
    prenom character(1),
    initiales character(1)
);


--
-- Name: utilisateur_a_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.utilisateur_a_notation (
    id_notation integer NOT NULL,
    id_utilisateur integer NOT NULL
);


--
-- Name: utilisateur_id_utilisateur_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.utilisateur_id_utilisateur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: utilisateur_id_utilisateur_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.utilisateur_id_utilisateur_seq OWNED BY notation.utilisateur.id_utilisateur;


--
-- Name: variety_has_notation; Type: TABLE; Schema: notation; Owner: -
--

CREATE TABLE notation.variety_has_notation (
    id_notation integer NOT NULL,
    id_variete integer NOT NULL
);


--
-- Name: variete_a_notation_id_notation_seq; Type: SEQUENCE; Schema: notation; Owner: -
--

CREATE SEQUENCE notation.variete_a_notation_id_notation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variete_a_notation_id_notation_seq; Type: SEQUENCE OWNED BY; Schema: notation; Owner: -
--

ALTER SEQUENCE notation.variete_a_notation_id_notation_seq OWNED BY notation.variety_has_notation.id_notation;


--
-- Name: accession; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.accession (
    id integer NOT NULL,
    introduction_name character varying(256),
    id_variety integer,
    comment text,
    introduction_clone integer,
    provider integer,
    introduction_date date,
    collection_date date,
    collection_site integer
);


--
-- Name: clone_id_clone_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.clone_id_clone_seq
    START WITH 59539
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clone_id_clone_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.clone_id_clone_seq OWNED BY plant.accession.id;


--
-- Name: cpt_numero_clone; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.cpt_numero_clone (
    cpt_rosa integer,
    cpt_malus integer,
    cpt_daucus integer,
    char_rosa character(1),
    char_malus character(1),
    char_daucus character(1)
);


--
-- Name: place; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.place (
    id integer NOT NULL,
    id_site integer,
    plantation_date date,
    removing_date date,
    id_lot integer
);


--
-- Name: emplacement_id_emplacement_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.emplacement_id_emplacement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: emplacement_id_emplacement_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.emplacement_id_emplacement_seq OWNED BY plant.place.id;


--
-- Name: group_has_lot; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.group_has_lot (
    id_group integer NOT NULL,
    id_lot integer NOT NULL
);


--
-- Name: groupe_id_groupe_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.groupe_id_groupe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groupe_id_groupe_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.groupe_id_groupe_seq OWNED BY plant.group_has_lot.id_group;


--
-- Name: group; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant."group" (
    id_group integer DEFAULT nextval('plant.groupe_id_groupe_seq'::regclass) NOT NULL,
    name character varying(128),
    description text
);


--
-- Name: group_has_notation; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.group_has_notation (
    id_group integer NOT NULL,
    id_notation integer NOT NULL
);


--
-- Name: group_has_usergroup; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.group_has_usergroup (
    id_group integer NOT NULL,
    id_usergroup integer NOT NULL,
    write boolean
);


--
-- Name: lot; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.lot (
    id_lot integer NOT NULL,
    name text,
    id_accession integer,
    id_lot_source integer,
    multiplication_date date,
    creation_date date,
    destruction_date date,
    id_lot_type integer
);


--
-- Name: lot_has_protocol; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.lot_has_protocol (
    id_lot integer NOT NULL,
    id_protocol integer NOT NULL
);


--
-- Name: lot_has_user; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.lot_has_user (
    id_lot integer NOT NULL,
    id_user integer NOT NULL
);


--
-- Name: lot_has_usergroup; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.lot_has_usergroup (
    id_lot integer NOT NULL,
    id_usergroup integer NOT NULL,
    write boolean DEFAULT true NOT NULL
);


--
-- Name: lot_id_lot_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.lot_id_lot_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lot_id_lot_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.lot_id_lot_seq OWNED BY plant.lot.id_lot;


--
-- Name: lot_type; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.lot_type (
    id integer NOT NULL,
    name text
);


--
-- Name: lot_type_id_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.lot_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lot_type_id_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.lot_type_id_seq OWNED BY plant.lot_type.id;


--
-- Name: variety_name; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.variety_name (
    id_nom_var integer NOT NULL,
    id_variety integer,
    date date,
    id_variety_name_type integer,
    value character varying(150)
);


--
-- Name: nom_variete_id_nom_var_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.nom_variete_id_nom_var_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nom_variete_id_nom_var_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.nom_variete_id_nom_var_seq OWNED BY plant.variety_name.id_nom_var;


--
-- Name: seed; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.seed (
    harvesting_date date,
    quantity text
)
INHERITS (plant.lot);


--
-- Name: taxonomy; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.taxonomy (
    id integer NOT NULL,
    genus character varying NOT NULL,
    specie character varying,
    id_taxon_type integer
);


--
-- Name: taxinomie_id_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.taxinomie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taxinomie_id_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.taxinomie_id_seq OWNED BY plant.taxonomy.id;


--
-- Name: taxon_has_variety_name_type; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.taxon_has_variety_name_type (
    id_taxonomy integer NOT NULL,
    id_variety_name_type integer NOT NULL
);


--
-- Name: taxon_type; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.taxon_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL
);


--
-- Name: taxon_type_id_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.taxon_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taxon_type_id_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.taxon_type_id_seq OWNED BY plant.taxon_type.id;


--
-- Name: tree; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.tree (
    first_shoot_year integer,
    rootstock character varying(256),
    death_date date
)
INHERITS (plant.lot);


--
-- Name: variety_name_type; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.variety_name_type (
    id_type_nom integer NOT NULL,
    value character varying(50)
);


--
-- Name: type_nom_variete_id_nom_var_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.type_nom_variete_id_nom_var_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_nom_variete_id_nom_var_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.type_nom_variete_id_nom_var_seq OWNED BY plant.variety_name_type.id_type_nom;


--
-- Name: variety; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.variety (
    id_variete integer NOT NULL,
    breeder integer,
    comment character varying(300),
    id_taxonomy integer,
    editor integer
);


--
-- Name: variete_id_variete_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.variete_id_variete_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variete_id_variete_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.variete_id_variete_seq OWNED BY plant.variety.id_variete;


--
-- Name: variety_relation; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.variety_relation (
    id_parent integer NOT NULL,
    id_child integer NOT NULL,
    id_type integer NOT NULL
);


--
-- Name: variety_relation_type; Type: TABLE; Schema: plant; Owner: -
--

CREATE TABLE plant.variety_relation_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL
);


--
-- Name: variety_relation_type_id_seq; Type: SEQUENCE; Schema: plant; Owner: -
--

CREATE SEQUENCE plant.variety_relation_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variety_relation_type_id_seq; Type: SEQUENCE OWNED BY; Schema: plant; Owner: -
--

ALTER SEQUENCE plant.variety_relation_type_id_seq OWNED BY plant.variety_relation_type.id;


--
-- Name: vue_arbre; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_arbre AS
 SELECT tree.id_lot AS id,
    tree.id_accession AS clone,
    tree.first_shoot_year AS annee_premiere_pousse
   FROM plant.tree;


--
-- Name: vue_clone; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_clone AS
 SELECT accession.id,
    accession.introduction_name AS numero,
    accession.comment AS remarque,
    accession.id_variety AS variete,
    ( SELECT taxonomy.genus AS genre
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS genre,
    ( SELECT taxonomy.specie AS espece
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS espece,
    ( SELECT address.lastname AS nom
           FROM address_book.address
          WHERE (address.id IN ( SELECT introduction.fournisseur
                   FROM import_export.introduction
                  WHERE (introduction.groupe IN ( SELECT groupe.id_group AS id_groupe
                           FROM plant.group_has_lot groupe
                          WHERE (groupe.id_lot IN ( SELECT lot.id_lot
                                   FROM plant.lot
                                  WHERE (lot.id_accession = accession.id)))))))) AS fournisseur
   FROM plant.accession;


--
-- Name: vue_graines; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_graines AS
 SELECT seed.id_lot AS id,
    seed.name AS numero_lot,
    seed.id_accession AS clone,
    seed.quantity AS quantite
   FROM plant.seed;


--
-- Name: vue_lieu; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_lieu AS
 SELECT place.id_lieu,
    place.id_plant_place AS emplacement,
    place_type.value AS type,
    place.value AS valeur
   FROM (location.place
     LEFT JOIN location.place_type ON ((place_type.id = place.id_place_type)));


--
-- Name: vue_lot; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_lot AS
 SELECT tree.id_lot AS id,
    'arbre'::text AS type,
    tree.name AS numero_lot,
    accession.introduction_name AS accession,
    accession.id AS id_clone,
    ( SELECT taxonomy.genus
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS genre,
    ( SELECT taxonomy.specie
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS espece,
    ( SELECT variety.id_variete
           FROM plant.variety
          WHERE (variety.id_variete IN ( SELECT clone_1.id_variety AS variete
                   FROM plant.accession clone_1
                  WHERE (clone_1.id = tree.id_accession)))) AS id_variete
   FROM (plant.tree
     LEFT JOIN plant.accession ON ((tree.id_accession = accession.id)))
UNION
 SELECT seed.id_lot AS id,
    'graines'::text AS type,
    seed.name AS numero_lot,
    accession.introduction_name AS accession,
    accession.id AS id_clone,
    ( SELECT taxonomy.genus
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS genre,
    ( SELECT taxonomy.specie
           FROM plant.taxonomy
          WHERE (taxonomy.id IN ( SELECT variety.id_taxonomy
                   FROM plant.variety
                  WHERE (variety.id_variete = accession.id_variety)))) AS espece,
    ( SELECT variety.id_variete
           FROM plant.variety
          WHERE (variety.id_variete IN ( SELECT clone_1.id_variety AS variete
                   FROM plant.accession clone_1
                  WHERE (clone_1.id = seed.id_accession)))) AS id_variete
   FROM (plant.seed
     LEFT JOIN plant.accession ON ((seed.id_accession = accession.id)));


--
-- Name: vue_nom_variete; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_nom_variete AS
 SELECT variety_name.date AS date_var,
    variety_name.id_nom_var,
    variety_name.id_variety AS id_variete,
    variety_name.value AS nom,
    variety_name_type.value AS type_nom
   FROM (plant.variety_name
     LEFT JOIN plant.variety_name_type ON ((variety_name.id_variety_name_type = variety_name_type.id_type_nom)));


--
-- Name: vue_variete; Type: VIEW; Schema: plant; Owner: -
--

CREATE VIEW plant.vue_variete AS
 SELECT variety.id_variete AS id,
    variety.comment AS remarque,
    taxonomy.genus AS genre,
    taxonomy.specie AS espece,
    ( SELECT address.lastname AS nom
           FROM address_book.address
          WHERE (variety.breeder = address.id)) AS obtenteur,
    ( SELECT address.lastname AS nom
           FROM address_book.address
          WHERE (variety.editor = address.id)) AS editeur
   FROM (plant.variety
     LEFT JOIN plant.taxonomy ON ((variety.id_taxonomy = taxonomy.id)))
  ORDER BY variety.id_variete;


--
-- Name: couple; Type: TABLE; Schema: plant_cross; Owner: -
--

CREATE TABLE plant_cross.couple (
    id integer NOT NULL,
    id_group_female integer,
    id_group_male integer
);


--
-- Name: couple_id_couple_seq; Type: SEQUENCE; Schema: plant_cross; Owner: -
--

CREATE SEQUENCE plant_cross.couple_id_couple_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: couple_id_couple_seq; Type: SEQUENCE OWNED BY; Schema: plant_cross; Owner: -
--

ALTER SEQUENCE plant_cross.couple_id_couple_seq OWNED BY plant_cross.couple.id;


--
-- Name: plant_cross; Type: TABLE; Schema: plant_cross; Owner: -
--

CREATE TABLE plant_cross.plant_cross (
    id integer NOT NULL,
    year integer,
    id_couple integer,
    name character varying(128)
);


--
-- Name: croisement_id_croisement_seq; Type: SEQUENCE; Schema: plant_cross; Owner: -
--

CREATE SEQUENCE plant_cross.croisement_id_croisement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: croisement_id_croisement_seq; Type: SEQUENCE OWNED BY; Schema: plant_cross; Owner: -
--

ALTER SEQUENCE plant_cross.croisement_id_croisement_seq OWNED BY plant_cross.plant_cross.id;


--
-- Name: lot_id_lot_seq; Type: SEQUENCE; Schema: plant_cross; Owner: -
--

CREATE SEQUENCE plant_cross.lot_id_lot_seq
    START WITH 122
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cross_seed; Type: TABLE; Schema: plant_cross; Owner: -
--

CREATE TABLE plant_cross.cross_seed (
    id integer DEFAULT nextval('plant_cross.lot_id_lot_seq'::regclass) NOT NULL,
    name character varying(20),
    id_taking integer
);


--
-- Name: multiplication; Type: TABLE; Schema: plant_cross; Owner: -
--

CREATE TABLE plant_cross.multiplication (
    id integer NOT NULL,
    grp_ascendant integer,
    grp_descendant integer,
    conforme boolean,
    multiplicateur integer,
    remarque text,
    nb_porte_graines integer,
    quantite_initiale double precision
);


--
-- Name: multiplication_id_multiplication_seq; Type: SEQUENCE; Schema: plant_cross; Owner: -
--

CREATE SEQUENCE plant_cross.multiplication_id_multiplication_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: multiplication_id_multiplication_seq; Type: SEQUENCE OWNED BY; Schema: plant_cross; Owner: -
--

ALTER SEQUENCE plant_cross.multiplication_id_multiplication_seq OWNED BY plant_cross.multiplication.id;


--
-- Name: taking; Type: TABLE; Schema: plant_cross; Owner: -
--

CREATE TABLE plant_cross.taking (
    id integer NOT NULL,
    id_plant_cross integer,
    seed_number integer,
    date date,
    name character varying(128)
);


--
-- Name: prelevement_id_prelevement_seq; Type: SEQUENCE; Schema: plant_cross; Owner: -
--

CREATE SEQUENCE plant_cross.prelevement_id_prelevement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prelevement_id_prelevement_seq; Type: SEQUENCE OWNED BY; Schema: plant_cross; Owner: -
--

ALTER SEQUENCE plant_cross.prelevement_id_prelevement_seq OWNED BY plant_cross.taking.id;


--
-- Name: action; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.action (
    id integer NOT NULL,
    id_action_type integer NOT NULL,
    id_user integer,
    date date
);


--
-- Name: action_a_container_origin; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.action_a_container_origin (
    id_action integer NOT NULL,
    id_container integer NOT NULL
);


--
-- Name: action_a_protocol; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.action_a_protocol (
    id_action integer NOT NULL,
    id_protocol integer NOT NULL
);


--
-- Name: action_id_action_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.action_id_action_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: action_id_action_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.action_id_action_seq OWNED BY sample.action.id;


--
-- Name: action_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.action_type (
    id integer NOT NULL,
    type character varying(50)
);


--
-- Name: box; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.box (
    id integer NOT NULL,
    name character varying(64),
    id_usergroup integer NOT NULL,
    id_box_type integer NOT NULL
);


--
-- Name: boite_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.boite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: boite_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.boite_id_seq OWNED BY sample.box.id;


--
-- Name: box_has_place; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.box_has_place (
    id_box integer NOT NULL,
    id_place integer NOT NULL,
    date_on date,
    date_off date
);


--
-- Name: box_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.box_type (
    id integer NOT NULL,
    name character varying(30),
    row_number integer,
    column_number integer
);


--
-- Name: box_type_a_container_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.box_type_a_container_type (
    id_box_type integer NOT NULL,
    id_container_type integer NOT NULL
);


--
-- Name: container; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.container (
    id integer NOT NULL,
    code character varying(256),
    valid boolean DEFAULT true,
    usable boolean DEFAULT true,
    id_container_type integer,
    id_container_level integer,
    comment text,
    id_content_type integer,
    id_action integer
);


--
-- Name: container_has_usergroup; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.container_has_usergroup (
    id_container integer,
    id_usergroup integer,
    writable boolean
);


--
-- Name: container_level; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.container_level (
    id integer NOT NULL,
    level character varying(64)
);


--
-- Name: container_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.container_type (
    id integer NOT NULL,
    type character varying(64)
);


--
-- Name: contenant_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.contenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contenant_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.contenant_id_seq OWNED BY sample.container.id;


--
-- Name: content_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.content_type (
    id integer NOT NULL,
    type character varying(64)
);


--
-- Name: etat_contenant_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.etat_contenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etat_contenant_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.etat_contenant_id_seq OWNED BY sample.container_level.id;


--
-- Name: location; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.location (
    id_box integer NOT NULL,
    id_container integer NOT NULL,
    "column" character varying(20),
    "row" character varying(20),
    date_on date,
    date_off date
);


--
-- Name: position_id; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.position_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taking; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.taking (
    id integer NOT NULL,
    id_plant_group integer,
    date date,
    id_tissue_type integer,
    id_container integer
);


--
-- Name: TABLE taking; Type: COMMENT; Schema: sample; Owner: -
--

COMMENT ON TABLE sample.taking IS 'Paquet gestion d''echantillon';


--
-- Name: prelevement2_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.prelevement2_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prelevement2_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.prelevement2_id_seq OWNED BY sample.taking.id;


--
-- Name: tissue_type; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.tissue_type (
    id integer NOT NULL,
    type character varying(64)
);


--
-- Name: type_action_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.type_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_action_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.type_action_id_seq OWNED BY sample.action_type.id;


--
-- Name: type_boite_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.type_boite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_boite_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.type_boite_id_seq OWNED BY sample.box_type.id;


--
-- Name: type_contenant_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.type_contenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_contenant_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.type_contenant_id_seq OWNED BY sample.container_type.id;


--
-- Name: type_contenu_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.type_contenu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_contenu_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.type_contenu_id_seq OWNED BY sample.content_type.id;


--
-- Name: type_tissu_id_seq; Type: SEQUENCE; Schema: sample; Owner: -
--

CREATE SEQUENCE sample.type_tissu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_tissu_id_seq; Type: SEQUENCE OWNED BY; Schema: sample; Owner: -
--

ALTER SEQUENCE sample.type_tissu_id_seq OWNED BY sample.tissue_type.id;


--
-- Name: user_a_taking; Type: TABLE; Schema: sample; Owner: -
--

CREATE TABLE sample.user_a_taking (
    id_user integer NOT NULL,
    id_taking integer NOT NULL
);


--
-- Name: concept; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.concept (
    id integer NOT NULL,
    label character varying(256) NOT NULL,
    description text,
    id_terminology integer NOT NULL,
    obsolete timestamp without time zone
);


--
-- Name: concept_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.concept_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: concept_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.concept_id_seq OWNED BY terminology.concept.id;


--
-- Name: concepts_graph; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.concepts_graph (
    id_left_concept integer NOT NULL,
    id_right_concept integer NOT NULL,
    id_relationship integer NOT NULL,
    obsolete timestamp without time zone
);


--
-- Name: context; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.context (
    id integer NOT NULL,
    name character varying(256) NOT NULL,
    description text
);


--
-- Name: context_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.context_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: context_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.context_id_seq OWNED BY terminology.context.id;


--
-- Name: i18n; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.i18n (
    id integer NOT NULL,
    label character varying(256) NOT NULL,
    id_language integer NOT NULL
);


--
-- Name: i18n_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.i18n_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: i18n_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.i18n_id_seq OWNED BY terminology.i18n.id;


--
-- Name: language; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.language (
    id integer NOT NULL,
    code character varying(16) NOT NULL,
    name character varying(256)
);


--
-- Name: language_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: language_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.language_id_seq OWNED BY terminology.language.id;


--
-- Name: relationship; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.relationship (
    id integer NOT NULL,
    name character varying(256) NOT NULL,
    description text,
    obsolete timestamp without time zone
);


--
-- Name: relationship_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.relationship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.relationship_id_seq OWNED BY terminology.relationship.id;


--
-- Name: term; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.term (
    id integer NOT NULL,
    label character varying(256) NOT NULL,
    id_concept integer,
    obsolete timestamp without time zone
);


--
-- Name: term_context; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.term_context (
    id_term integer NOT NULL,
    id_context integer NOT NULL
);


--
-- Name: term_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.term_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: term_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.term_id_seq OWNED BY terminology.term.id;


--
-- Name: term_translation; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.term_translation (
    id_term integer NOT NULL,
    id_translation integer NOT NULL
);


--
-- Name: terminology; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.terminology (
    id integer NOT NULL,
    name character varying(256) NOT NULL,
    description text,
    obsolete timestamp without time zone
);


--
-- Name: terminology_context; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.terminology_context (
    id_context integer NOT NULL,
    id_terminology integer NOT NULL
);


--
-- Name: terminology_id_seq; Type: SEQUENCE; Schema: terminology; Owner: -
--

CREATE SEQUENCE terminology.terminology_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: terminology_id_seq; Type: SEQUENCE OWNED BY; Schema: terminology; Owner: -
--

ALTER SEQUENCE terminology.terminology_id_seq OWNED BY terminology.terminology.id;


--
-- Name: terminology_translation; Type: TABLE; Schema: terminology; Owner: -
--

CREATE TABLE terminology.terminology_translation (
    id_terminology integer NOT NULL,
    id_translation integer NOT NULL
);


--
-- Name: belongs_to_group; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users.belongs_to_group (
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: usergroup; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users.usergroup (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    active boolean DEFAULT true
);


--
-- Name: ghgroup_id_seq; Type: SEQUENCE; Schema: users; Owner: -
--

CREATE SEQUENCE users.ghgroup_id_seq
    START WITH 1001
    INCREMENT BY 1
    MINVALUE 1001
    NO MAXVALUE
    CACHE 1;


--
-- Name: ghgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: users; Owner: -
--

ALTER SEQUENCE users.ghgroup_id_seq OWNED BY users.usergroup.id;


--
-- Name: user; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users."user" (
    id integer NOT NULL,
    firstname text,
    lastname text,
    login text NOT NULL,
    password text NOT NULL,
    email text,
    active boolean DEFAULT true,
    phone character varying(20),
    preferences text,
    id_address integer
);


--
-- Name: ghuser_id_seq; Type: SEQUENCE; Schema: users; Owner: -
--

CREATE SEQUENCE users.ghuser_id_seq
    START WITH 1001
    INCREMENT BY 1
    MINVALUE 1001
    NO MAXVALUE
    CACHE 1;


--
-- Name: ghuser_id_seq; Type: SEQUENCE OWNED BY; Schema: users; Owner: -
--

ALTER SEQUENCE users.ghuser_id_seq OWNED BY users."user".id;


--
-- Name: usergroup_type; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users.usergroup_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description text
);


--
-- Name: usergroup_type_id_type_seq; Type: SEQUENCE; Schema: users; Owner: -
--

CREATE SEQUENCE users.usergroup_type_id_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: usergroup_type_id_type_seq; Type: SEQUENCE OWNED BY; Schema: users; Owner: -
--

ALTER SEQUENCE users.usergroup_type_id_type_seq OWNED BY users.usergroup_type.id;


--
-- Name: usergroup_usergroup_type; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users.usergroup_usergroup_type (
    usergroup_id integer NOT NULL,
    usergroup_type_id integer NOT NULL
);


--
-- Name: web_session; Type: TABLE; Schema: users; Owner: -
--

CREATE TABLE users.web_session (
    id character varying(36) NOT NULL,
    user_id integer,
    ip text NOT NULL,
    salt character varying(32),
    application_id character varying(36),
    last_action timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: address id; Type: DEFAULT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.address ALTER COLUMN id SET DEFAULT nextval('address_book.pepinieriste_id_pepinieriste_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.country ALTER COLUMN id SET DEFAULT nextval('address_book.pays_id_pays_seq'::regclass);


--
-- Name: site id_site; Type: DEFAULT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.site ALTER COLUMN id_site SET DEFAULT nextval('address_book.site_id_site_seq'::regclass);


--
-- Name: comparison id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.comparison ALTER COLUMN id SET DEFAULT nextval('analytics.comparison_id_seq'::regclass);


--
-- Name: export id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.export ALTER COLUMN id SET DEFAULT nextval('analytics.export_id_seq'::regclass);


--
-- Name: intensity id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.intensity ALTER COLUMN id SET DEFAULT nextval('analytics.intensity_id_seq'::regclass);


--
-- Name: rscript id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.rscript ALTER COLUMN id SET DEFAULT nextval('analytics.rscript_id_seq'::regclass);


--
-- Name: rscripts id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.rscripts ALTER COLUMN id SET DEFAULT nextval('analytics.rscripts_id_seq'::regclass);


--
-- Name: sens id; Type: DEFAULT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.sens ALTER COLUMN id SET DEFAULT nextval('analytics.sens_id_seq'::regclass);


--
-- Name: reference id; Type: DEFAULT; Schema: bibliography; Owner: -
--

ALTER TABLE ONLY bibliography.reference ALTER COLUMN id SET DEFAULT nextval('bibliography.reference_id_reference_seq'::regclass);


--
-- Name: collection id_collection; Type: DEFAULT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection ALTER COLUMN id_collection SET DEFAULT nextval('collection.collection_id_collection_seq'::regclass);


--
-- Name: protocol id; Type: DEFAULT; Schema: documentation; Owner: -
--

ALTER TABLE ONLY documentation.protocol ALTER COLUMN id SET DEFAULT nextval('documentation.protocol_id_seq'::regclass);


--
-- Name: axis id; Type: DEFAULT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.axis ALTER COLUMN id SET DEFAULT nextval('experiment.axis_id_seq'::regclass);


--
-- Name: experiment_type id; Type: DEFAULT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_type ALTER COLUMN id SET DEFAULT nextval('experiment.experiment_type_id_seq'::regclass);


--
-- Name: financing id; Type: DEFAULT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.financing ALTER COLUMN id SET DEFAULT nextval('experiment.financing_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.status ALTER COLUMN id SET DEFAULT nextval('experiment.status_type_id_seq'::regclass);


--
-- Name: introduction id_intro; Type: DEFAULT; Schema: import_export; Owner: -
--

ALTER TABLE ONLY import_export.introduction ALTER COLUMN id_intro SET DEFAULT nextval('import_export.introduction_id_intro_seq'::regclass);


--
-- Name: introduction_clone id; Type: DEFAULT; Schema: import_export; Owner: -
--

ALTER TABLE ONLY import_export.introduction_clone ALTER COLUMN id SET DEFAULT nextval('import_export.introduction_var_id_seq'::regclass);


--
-- Name: place id_lieu; Type: DEFAULT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place ALTER COLUMN id_lieu SET DEFAULT nextval('location.lieu_id_lieu_seq'::regclass);


--
-- Name: place_type id; Type: DEFAULT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type ALTER COLUMN id SET DEFAULT nextval('location.type_lieu_id_seq'::regclass);


--
-- Name: notation id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_date id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_date ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_date id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_date ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_document id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_document ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_document id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_document ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_double id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_double ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_double id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_double ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_integer id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_integer ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_integer id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_integer ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_list id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_list id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_list_values id; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list_values ALTER COLUMN id SET DEFAULT nextval('notation.notation_list_values_id_seq'::regclass);


--
-- Name: notation_texte id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_texte ALTER COLUMN id_notation SET DEFAULT nextval('notation.notation_id_notation_seq'::regclass);


--
-- Name: notation_texte id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_texte ALTER COLUMN id_type_notation SET DEFAULT 1;


--
-- Name: notation_type id_type_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type ALTER COLUMN id_type_notation SET DEFAULT nextval('notation.type_notation_id_seq'::regclass);


--
-- Name: notation_type_constraint id; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type_constraint ALTER COLUMN id SET DEFAULT nextval('notation.type_notation_id_type_notation_seq'::regclass);


--
-- Name: utilisateur id_utilisateur; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.utilisateur ALTER COLUMN id_utilisateur SET DEFAULT nextval('notation.utilisateur_id_utilisateur_seq'::regclass);


--
-- Name: variety_has_notation id_notation; Type: DEFAULT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.variety_has_notation ALTER COLUMN id_notation SET DEFAULT nextval('notation.variete_a_notation_id_notation_seq'::regclass);


--
-- Name: accession id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession ALTER COLUMN id SET DEFAULT nextval('plant.clone_id_clone_seq'::regclass);


--
-- Name: lot id_lot; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot ALTER COLUMN id_lot SET DEFAULT nextval('plant.lot_id_lot_seq'::regclass);


--
-- Name: lot_type id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_type ALTER COLUMN id SET DEFAULT nextval('plant.lot_type_id_seq'::regclass);


--
-- Name: place id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.place ALTER COLUMN id SET DEFAULT nextval('plant.emplacement_id_emplacement_seq'::regclass);


--
-- Name: seed id_lot; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.seed ALTER COLUMN id_lot SET DEFAULT nextval('plant.lot_id_lot_seq'::regclass);


--
-- Name: taxon_type id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_type ALTER COLUMN id SET DEFAULT nextval('plant.taxon_type_id_seq'::regclass);


--
-- Name: taxonomy id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxonomy ALTER COLUMN id SET DEFAULT nextval('plant.taxinomie_id_seq'::regclass);


--
-- Name: tree id_lot; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.tree ALTER COLUMN id_lot SET DEFAULT nextval('plant.lot_id_lot_seq'::regclass);


--
-- Name: variety id_variete; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety ALTER COLUMN id_variete SET DEFAULT nextval('plant.variete_id_variete_seq'::regclass);


--
-- Name: variety_name id_nom_var; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name ALTER COLUMN id_nom_var SET DEFAULT nextval('plant.nom_variete_id_nom_var_seq'::regclass);


--
-- Name: variety_name_type id_type_nom; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name_type ALTER COLUMN id_type_nom SET DEFAULT nextval('plant.type_nom_variete_id_nom_var_seq'::regclass);


--
-- Name: variety_relation_type id; Type: DEFAULT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation_type ALTER COLUMN id SET DEFAULT nextval('plant.variety_relation_type_id_seq'::regclass);


--
-- Name: couple id; Type: DEFAULT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.couple ALTER COLUMN id SET DEFAULT nextval('plant_cross.couple_id_couple_seq'::regclass);


--
-- Name: multiplication id; Type: DEFAULT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.multiplication ALTER COLUMN id SET DEFAULT nextval('plant_cross.multiplication_id_multiplication_seq'::regclass);


--
-- Name: plant_cross id; Type: DEFAULT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.plant_cross ALTER COLUMN id SET DEFAULT nextval('plant_cross.croisement_id_croisement_seq'::regclass);


--
-- Name: taking id; Type: DEFAULT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.taking ALTER COLUMN id SET DEFAULT nextval('plant_cross.prelevement_id_prelevement_seq'::regclass);


--
-- Name: action id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action ALTER COLUMN id SET DEFAULT nextval('sample.action_id_action_seq'::regclass);


--
-- Name: action_type id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_type ALTER COLUMN id SET DEFAULT nextval('sample.type_action_id_seq'::regclass);


--
-- Name: box id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box ALTER COLUMN id SET DEFAULT nextval('sample.boite_id_seq'::regclass);


--
-- Name: box_type id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type ALTER COLUMN id SET DEFAULT nextval('sample.type_boite_id_seq'::regclass);


--
-- Name: container id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container ALTER COLUMN id SET DEFAULT nextval('sample.contenant_id_seq'::regclass);


--
-- Name: container_level id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_level ALTER COLUMN id SET DEFAULT nextval('sample.etat_contenant_id_seq'::regclass);


--
-- Name: container_type id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_type ALTER COLUMN id SET DEFAULT nextval('sample.type_contenant_id_seq'::regclass);


--
-- Name: content_type id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.content_type ALTER COLUMN id SET DEFAULT nextval('sample.type_contenu_id_seq'::regclass);


--
-- Name: taking id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.taking ALTER COLUMN id SET DEFAULT nextval('sample.prelevement2_id_seq'::regclass);


--
-- Name: tissue_type id; Type: DEFAULT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.tissue_type ALTER COLUMN id SET DEFAULT nextval('sample.type_tissu_id_seq'::regclass);


--
-- Name: concept id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concept ALTER COLUMN id SET DEFAULT nextval('terminology.concept_id_seq'::regclass);


--
-- Name: context id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.context ALTER COLUMN id SET DEFAULT nextval('terminology.context_id_seq'::regclass);


--
-- Name: i18n id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.i18n ALTER COLUMN id SET DEFAULT nextval('terminology.i18n_id_seq'::regclass);


--
-- Name: language id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.language ALTER COLUMN id SET DEFAULT nextval('terminology.language_id_seq'::regclass);


--
-- Name: relationship id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.relationship ALTER COLUMN id SET DEFAULT nextval('terminology.relationship_id_seq'::regclass);


--
-- Name: term id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term ALTER COLUMN id SET DEFAULT nextval('terminology.term_id_seq'::regclass);


--
-- Name: terminology id; Type: DEFAULT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology ALTER COLUMN id SET DEFAULT nextval('terminology.terminology_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: users; Owner: -
--

ALTER TABLE ONLY users."user" ALTER COLUMN id SET DEFAULT nextval('users.ghuser_id_seq'::regclass);


--
-- Name: usergroup id; Type: DEFAULT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup ALTER COLUMN id SET DEFAULT nextval('users.ghgroup_id_seq'::regclass);


--
-- Name: usergroup_type id; Type: DEFAULT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup_type ALTER COLUMN id SET DEFAULT nextval('users.usergroup_type_id_type_seq'::regclass);


--
-- Name: country pkey_pays; Type: CONSTRAINT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.country
    ADD CONSTRAINT pkey_pays PRIMARY KEY (id);


--
-- Name: address pkey_pep; Type: CONSTRAINT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.address
    ADD CONSTRAINT pkey_pep PRIMARY KEY (id);


--
-- Name: site pkey_site; Type: CONSTRAINT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.site
    ADD CONSTRAINT pkey_site PRIMARY KEY (id_site);


--
-- Name: comparison comparison_pkey; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.comparison
    ADD CONSTRAINT comparison_pkey PRIMARY KEY (id);


--
-- Name: export export_pkey; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.export
    ADD CONSTRAINT export_pkey PRIMARY KEY (id);


--
-- Name: intensity intensity_pkey; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.intensity
    ADD CONSTRAINT intensity_pkey PRIMARY KEY (id);


--
-- Name: rscripts pk_id_rscripts; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.rscripts
    ADD CONSTRAINT pk_id_rscripts PRIMARY KEY (id);


--
-- Name: rscript rscript_pkey; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.rscript
    ADD CONSTRAINT rscript_pkey PRIMARY KEY (id);


--
-- Name: sens sens_pkey; Type: CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.sens
    ADD CONSTRAINT sens_pkey PRIMARY KEY (id);


--
-- Name: reference pk_id_reference; Type: CONSTRAINT; Schema: bibliography; Owner: -
--

ALTER TABLE ONLY bibliography.reference
    ADD CONSTRAINT pk_id_reference PRIMARY KEY (id);


--
-- Name: collection_has_accession PK_col_clone; Type: CONSTRAINT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection_has_accession
    ADD CONSTRAINT "PK_col_clone" PRIMARY KEY (id_collection, id_clone);


--
-- Name: collection Pk_id_col; Type: CONSTRAINT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection
    ADD CONSTRAINT "Pk_id_col" PRIMARY KEY (id_collection);


--
-- Name: collection Unique_nom_col; Type: CONSTRAINT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection
    ADD CONSTRAINT "Unique_nom_col" UNIQUE (name);


--
-- Name: protocol pk_protocol; Type: CONSTRAINT; Schema: documentation; Owner: -
--

ALTER TABLE ONLY documentation.protocol
    ADD CONSTRAINT pk_protocol PRIMARY KEY (id);


--
-- Name: experiment PK_id_experiment; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment
    ADD CONSTRAINT "PK_id_experiment" PRIMARY KEY (id);


--
-- Name: experiment_has_clone Unique_id_experiment_id_clone; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_clone
    ADD CONSTRAINT "Unique_id_experiment_id_clone" UNIQUE (id_experiment, id_clone);


--
-- Name: experiment_has_container Unique_id_experiment_id_container; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_container
    ADD CONSTRAINT "Unique_id_experiment_id_container" UNIQUE (id_experiment, id_container);


--
-- Name: experiment_has_experiment Unique_id_experiment_id_experiment_child; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_experiment
    ADD CONSTRAINT "Unique_id_experiment_id_experiment_child" UNIQUE (id_experiment, id_experiment_child);


--
-- Name: experiment_has_group Unique_id_experiment_id_groupe; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_group
    ADD CONSTRAINT "Unique_id_experiment_id_groupe" UNIQUE (id_experiment, id_group);


--
-- Name: experiment_has_notation Unique_id_experiment_id_notation; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_notation
    ADD CONSTRAINT "Unique_id_experiment_id_notation" UNIQUE (id_experiment, id_notation);


--
-- Name: experiment_has_usergroup Unique_id_experiment_id_usergroup; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_usergroup
    ADD CONSTRAINT "Unique_id_experiment_id_usergroup" UNIQUE (id_experiment, id_usergroup);


--
-- Name: experiment_has_axis pk_experiment_has_axis; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_axis
    ADD CONSTRAINT pk_experiment_has_axis PRIMARY KEY (id_experiment, id_axis);


--
-- Name: experiment_has_financing pk_experiment_has_financing; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_financing
    ADD CONSTRAINT pk_experiment_has_financing PRIMARY KEY (id_experiment, id_financing);


--
-- Name: experiment_has_status pk_experiment_has_status; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_status
    ADD CONSTRAINT pk_experiment_has_status PRIMARY KEY (id_experiment, id_status);


--
-- Name: experiment_has_user pk_experiment_has_user; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_user
    ADD CONSTRAINT pk_experiment_has_user PRIMARY KEY (id_experiment, id_user);


--
-- Name: axis pk_id_axis; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.axis
    ADD CONSTRAINT pk_id_axis PRIMARY KEY (id);


--
-- Name: experiment_type pk_id_experiment_type; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_type
    ADD CONSTRAINT pk_id_experiment_type PRIMARY KEY (id);


--
-- Name: financing pk_id_financing; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.financing
    ADD CONSTRAINT pk_id_financing PRIMARY KEY (id);


--
-- Name: status pk_id_status; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.status
    ADD CONSTRAINT pk_id_status PRIMARY KEY (id);


--
-- Name: axis un_name_axis; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.axis
    ADD CONSTRAINT un_name_axis UNIQUE (name);


--
-- Name: experiment_type un_name_experiment_type; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_type
    ADD CONSTRAINT un_name_experiment_type UNIQUE (name);


--
-- Name: financing un_name_financing; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.financing
    ADD CONSTRAINT un_name_financing UNIQUE (name);


--
-- Name: status un_name_status; Type: CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.status
    ADD CONSTRAINT un_name_status UNIQUE (name);


--
-- Name: introduction_clone PK_intro_var; Type: CONSTRAINT; Schema: import_export; Owner: -
--

ALTER TABLE ONLY import_export.introduction_clone
    ADD CONSTRAINT "PK_intro_var" PRIMARY KEY (id);


--
-- Name: introduction pkey_intro; Type: CONSTRAINT; Schema: import_export; Owner: -
--

ALTER TABLE ONLY import_export.introduction
    ADD CONSTRAINT pkey_intro PRIMARY KEY (id_intro);


--
-- Name: place_type_has_terminology pk_place_type_has_terminology; Type: CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type_has_terminology
    ADD CONSTRAINT pk_place_type_has_terminology PRIMARY KEY (id_type_lieu, id_concept, id_context);


--
-- Name: place pkey_lieu; Type: CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place
    ADD CONSTRAINT pkey_lieu PRIMARY KEY (id_lieu);


--
-- Name: place_type pkey_type_lieu; Type: CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type
    ADD CONSTRAINT pkey_type_lieu PRIMARY KEY (id);


--
-- Name: place_type type_lieu_unique; Type: CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type
    ADD CONSTRAINT type_lieu_unique UNIQUE (value);


--
-- Name: context PK_contexte_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.context
    ADD CONSTRAINT "PK_contexte_notation" PRIMARY KEY (id);


--
-- Name: notation PK_id_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation
    ADD CONSTRAINT "PK_id_notation" PRIMARY KEY (id_notation);


--
-- Name: lien_contexte_notation PK_lien_contexte_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.lien_contexte_notation
    ADD CONSTRAINT "PK_lien_contexte_notation" PRIMARY KEY (id_context, id_notation_type);


--
-- Name: notation_list_values PK_notation_list_value; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list_values
    ADD CONSTRAINT "PK_notation_list_value" PRIMARY KEY (id);


--
-- Name: notation_type PK_type_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type
    ADD CONSTRAINT "PK_type_notation" PRIMARY KEY (id_type_notation);


--
-- Name: variety_has_notation PK_variete_a_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.variety_has_notation
    ADD CONSTRAINT "PK_variete_a_notation" PRIMARY KEY (id_notation, id_variete);


--
-- Name: utilisateur Pk_id_utilisateur; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.utilisateur
    ADD CONSTRAINT "Pk_id_utilisateur" PRIMARY KEY (id_utilisateur);


--
-- Name: utilisateur_a_notation Pk_utilisateur_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.utilisateur_a_notation
    ADD CONSTRAINT "Pk_utilisateur_notation" PRIMARY KEY (id_notation, id_utilisateur);


--
-- Name: notation_list_values code_id_type_notation_unique; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list_values
    ADD CONSTRAINT code_id_type_notation_unique UNIQUE (code, id_type_notation);


--
-- Name: notation_type nom_unique; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type
    ADD CONSTRAINT nom_unique UNIQUE (name);


--
-- Name: action_has_notation pk_action_a_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.action_has_notation
    ADD CONSTRAINT pk_action_a_notation PRIMARY KEY (id_action, id_notation);


--
-- Name: container_has_notation pk_container_a_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.container_has_notation
    ADD CONSTRAINT pk_container_a_notation PRIMARY KEY (id_container, id_notation);


--
-- Name: context_has_usergroup pk_context_has_usergroup; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.context_has_usergroup
    ADD CONSTRAINT pk_context_has_usergroup PRIMARY KEY (id_context, id_usergroup);


--
-- Name: cross_has_notation pk_cross_has_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.cross_has_notation
    ADD CONSTRAINT pk_cross_has_notation PRIMARY KEY (id_cross, id_notation);


--
-- Name: cross_seed_has_notation pk_cross_seed_has_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.cross_seed_has_notation
    ADD CONSTRAINT pk_cross_seed_has_notation PRIMARY KEY (id_lot_pepins, id_notation);


--
-- Name: lot_has_notation pk_lot_a_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.lot_has_notation
    ADD CONSTRAINT pk_lot_a_notation PRIMARY KEY (id_lot, id_notation);


--
-- Name: notation_has_protocol pk_notation_a_protocol; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_protocol
    ADD CONSTRAINT pk_notation_a_protocol PRIMARY KEY (id_notation, id_protocol);


--
-- Name: notation_date pk_notation_date; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_date
    ADD CONSTRAINT pk_notation_date PRIMARY KEY (id_notation);


--
-- Name: notation_document pk_notation_document; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_document
    ADD CONSTRAINT pk_notation_document PRIMARY KEY (id_notation);


--
-- Name: notation_double pk_notation_double; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_double
    ADD CONSTRAINT pk_notation_double PRIMARY KEY (id_notation);


--
-- Name: notation_has_user pk_notation_has_user; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_user
    ADD CONSTRAINT pk_notation_has_user PRIMARY KEY (id_user, id_notation);


--
-- Name: notation_has_usergroup pk_notation_has_usergroup; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_usergroup
    ADD CONSTRAINT pk_notation_has_usergroup PRIMARY KEY (id_notation, id_usergroup);


--
-- Name: notation_integer pk_notation_integer; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_integer
    ADD CONSTRAINT pk_notation_integer PRIMARY KEY (id_notation);


--
-- Name: notation_list pk_notation_list; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list
    ADD CONSTRAINT pk_notation_list PRIMARY KEY (id_notation);


--
-- Name: notation_texte pk_notation_texte; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_texte
    ADD CONSTRAINT pk_notation_texte PRIMARY KEY (id_notation);


--
-- Name: taking_has_notation pk_taking_has_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.taking_has_notation
    ADD CONSTRAINT pk_taking_has_notation PRIMARY KEY (id_taking, id_notation);


--
-- Name: notation_type_constraint pkey_type_notation; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type_constraint
    ADD CONSTRAINT pkey_type_notation PRIMARY KEY (id);


--
-- Name: notation_list_values value_id_type_notation_unique; Type: CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list_values
    ADD CONSTRAINT value_id_type_notation_unique UNIQUE (value, id_type_notation);


--
-- Name: group_has_notation PK_groupe_a_notation; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_notation
    ADD CONSTRAINT "PK_groupe_a_notation" PRIMARY KEY (id_group, id_notation);


--
-- Name: taxonomy PK_id_taxinomie; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxonomy
    ADD CONSTRAINT "PK_id_taxinomie" PRIMARY KEY (id);


--
-- Name: taxon_type PK_id_taxon_type; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_type
    ADD CONSTRAINT "PK_id_taxon_type" PRIMARY KEY (id);


--
-- Name: variety_relation_type PK_id_variety_relation_type; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation_type
    ADD CONSTRAINT "PK_id_variety_relation_type" PRIMARY KEY (id);


--
-- Name: variety_relation PK_variety_relation; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation
    ADD CONSTRAINT "PK_variety_relation" PRIMARY KEY (id_parent, id_child, id_type);


--
-- Name: taxonomy genre_espece_unique; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxonomy
    ADD CONSTRAINT genre_espece_unique UNIQUE (genus, specie);


--
-- Name: group_has_notation id_notation_unique; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_notation
    ADD CONSTRAINT id_notation_unique UNIQUE (id_notation);


--
-- Name: lot_has_protocol lot_a_protocol_pk; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_protocol
    ADD CONSTRAINT lot_a_protocol_pk PRIMARY KEY (id_lot, id_protocol);


--
-- Name: lot_has_user lot_has_user_pk; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_user
    ADD CONSTRAINT lot_has_user_pk PRIMARY KEY (id_lot, id_user);


--
-- Name: lot_has_usergroup lot_has_usergroup_pk; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_usergroup
    ADD CONSTRAINT lot_has_usergroup_pk PRIMARY KEY (id_lot, id_usergroup);


--
-- Name: variety_relation_type name_relation_type_unique; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation_type
    ADD CONSTRAINT name_relation_type_unique UNIQUE (name);


--
-- Name: taxon_type name_unique; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_type
    ADD CONSTRAINT name_unique UNIQUE (name);


--
-- Name: tree pk_arbre; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.tree
    ADD CONSTRAINT pk_arbre PRIMARY KEY (id_lot);


--
-- Name: seed pk_graines; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.seed
    ADD CONSTRAINT pk_graines PRIMARY KEY (id_lot);


--
-- Name: group pk_groupe; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant."group"
    ADD CONSTRAINT pk_groupe PRIMARY KEY (id_group);


--
-- Name: group_has_lot pk_groupe_a_lot; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_lot
    ADD CONSTRAINT pk_groupe_a_lot PRIMARY KEY (id_group, id_lot);


--
-- Name: group_has_usergroup pk_groupe_a_usergroup; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_usergroup
    ADD CONSTRAINT pk_groupe_a_usergroup PRIMARY KEY (id_group, id_usergroup);


--
-- Name: lot pk_id_lot; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot
    ADD CONSTRAINT pk_id_lot PRIMARY KEY (id_lot);


--
-- Name: lot_type pk_id_lot_type; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_type
    ADD CONSTRAINT pk_id_lot_type PRIMARY KEY (id);


--
-- Name: variety_name pk_id_nom_var; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name
    ADD CONSTRAINT pk_id_nom_var PRIMARY KEY (id_nom_var);


--
-- Name: taxon_has_variety_name_type pk_taxon_has_variety_name_type; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_has_variety_name_type
    ADD CONSTRAINT pk_taxon_has_variety_name_type PRIMARY KEY (id_taxonomy, id_variety_name_type);


--
-- Name: accession pkey_clone; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession
    ADD CONSTRAINT pkey_clone PRIMARY KEY (id);


--
-- Name: place pkey_emplacement; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.place
    ADD CONSTRAINT pkey_emplacement PRIMARY KEY (id);


--
-- Name: variety_name_type pkey_nom_variete; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name_type
    ADD CONSTRAINT pkey_nom_variete PRIMARY KEY (id_type_nom);


--
-- Name: variety pkey_var; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety
    ADD CONSTRAINT pkey_var PRIMARY KEY (id_variete);


--
-- Name: variety_name_type unique_valeur; Type: CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name_type
    ADD CONSTRAINT unique_valeur UNIQUE (value);


--
-- Name: couple couple_unique; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.couple
    ADD CONSTRAINT couple_unique UNIQUE (id_group_female, id_group_male);


--
-- Name: plant_cross crois_unique; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.plant_cross
    ADD CONSTRAINT crois_unique UNIQUE (year, id_couple);


--
-- Name: multiplication pk_id_multiplication; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.multiplication
    ADD CONSTRAINT pk_id_multiplication PRIMARY KEY (id);


--
-- Name: plant_cross pkey_annee_croisement; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.plant_cross
    ADD CONSTRAINT pkey_annee_croisement PRIMARY KEY (id);


--
-- Name: couple pkey_couple; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.couple
    ADD CONSTRAINT pkey_couple PRIMARY KEY (id);


--
-- Name: cross_seed pkey_lot; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.cross_seed
    ADD CONSTRAINT pkey_lot PRIMARY KEY (id);


--
-- Name: taking pkey_prelevement; Type: CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.taking
    ADD CONSTRAINT pkey_prelevement PRIMARY KEY (id);


--
-- Name: action_a_protocol PK_action_a_protocol; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_protocol
    ADD CONSTRAINT "PK_action_a_protocol" PRIMARY KEY (id_action, id_protocol);


--
-- Name: box_type_a_container_type PK_box_type_a_container_type; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type_a_container_type
    ADD CONSTRAINT "PK_box_type_a_container_type" PRIMARY KEY (id_box_type, id_container_type);


--
-- Name: action PK_id_action; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action
    ADD CONSTRAINT "PK_id_action" PRIMARY KEY (id);


--
-- Name: box PK_id_boite; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box
    ADD CONSTRAINT "PK_id_boite" PRIMARY KEY (id);


--
-- Name: container PK_id_contenant; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container
    ADD CONSTRAINT "PK_id_contenant" PRIMARY KEY (id);


--
-- Name: container_level PK_id_etat_contenant; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_level
    ADD CONSTRAINT "PK_id_etat_contenant" PRIMARY KEY (id);


--
-- Name: taking PK_id_prelevement; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.taking
    ADD CONSTRAINT "PK_id_prelevement" PRIMARY KEY (id);


--
-- Name: action_type PK_id_type_action; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_type
    ADD CONSTRAINT "PK_id_type_action" PRIMARY KEY (id);


--
-- Name: container_type PK_id_type_contenant; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_type
    ADD CONSTRAINT "PK_id_type_contenant" PRIMARY KEY (id);


--
-- Name: content_type PK_id_type_contenu; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.content_type
    ADD CONSTRAINT "PK_id_type_contenu" PRIMARY KEY (id);


--
-- Name: tissue_type PK_id_type_tissu; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.tissue_type
    ADD CONSTRAINT "PK_id_type_tissu" PRIMARY KEY (id);


--
-- Name: box_type PK_type_boite; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type
    ADD CONSTRAINT "PK_type_boite" PRIMARY KEY (id);


--
-- Name: user_a_taking PK_user_a_taking; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.user_a_taking
    ADD CONSTRAINT "PK_user_a_taking" PRIMARY KEY (id_user, id_taking);


--
-- Name: container_level UNIQUE_etat_contenant; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_level
    ADD CONSTRAINT "UNIQUE_etat_contenant" UNIQUE (level);


--
-- Name: user_a_taking UNIQUE_id_user_id_taking; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.user_a_taking
    ADD CONSTRAINT "UNIQUE_id_user_id_taking" UNIQUE (id_user, id_taking);


--
-- Name: box UNIQUE_nom_id_groupe; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box
    ADD CONSTRAINT "UNIQUE_nom_id_groupe" UNIQUE (name, id_usergroup);


--
-- Name: box_type UNIQUE_nom_type_boite; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type
    ADD CONSTRAINT "UNIQUE_nom_type_boite" UNIQUE (name);


--
-- Name: action_type UNIQUE_type_action; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_type
    ADD CONSTRAINT "UNIQUE_type_action" UNIQUE (type);


--
-- Name: container_type UNIQUE_type_contenant; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_type
    ADD CONSTRAINT "UNIQUE_type_contenant" UNIQUE (type);


--
-- Name: content_type UNIQUE_type_contenu; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.content_type
    ADD CONSTRAINT "UNIQUE_type_contenu" UNIQUE (type);


--
-- Name: tissue_type UNIQUE_type_tissu; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.tissue_type
    ADD CONSTRAINT "UNIQUE_type_tissu" UNIQUE (type);


--
-- Name: action_a_container_origin pk_action_a_container_origin; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_container_origin
    ADD CONSTRAINT pk_action_a_container_origin PRIMARY KEY (id_action, id_container);


--
-- Name: box_has_place pk_id_place_id_box; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_has_place
    ADD CONSTRAINT pk_id_place_id_box PRIMARY KEY (id_place, id_box);


--
-- Name: container_has_usergroup unique_id_container_id_usergroup; Type: CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_has_usergroup
    ADD CONSTRAINT unique_id_container_id_usergroup UNIQUE (id_container, id_usergroup);


--
-- Name: language code_un; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.language
    ADD CONSTRAINT code_un UNIQUE (code);


--
-- Name: concept concept_label_un; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concept
    ADD CONSTRAINT concept_label_un UNIQUE (label);


--
-- Name: concept concept_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concept
    ADD CONSTRAINT concept_pk PRIMARY KEY (id);


--
-- Name: concepts_graph concepts_graph_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concepts_graph
    ADD CONSTRAINT concepts_graph_pk PRIMARY KEY (id_left_concept, id_right_concept, id_relationship);


--
-- Name: context context_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.context
    ADD CONSTRAINT context_pk PRIMARY KEY (id);


--
-- Name: i18n i18n_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.i18n
    ADD CONSTRAINT i18n_pk PRIMARY KEY (id);


--
-- Name: language language_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.language
    ADD CONSTRAINT language_pk PRIMARY KEY (id);


--
-- Name: context name_un; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.context
    ADD CONSTRAINT name_un UNIQUE (name);


--
-- Name: relationship relationship_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.relationship
    ADD CONSTRAINT relationship_pk PRIMARY KEY (id);


--
-- Name: relationship relname_un; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.relationship
    ADD CONSTRAINT relname_un UNIQUE (name);


--
-- Name: term_context term_context_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_context
    ADD CONSTRAINT term_context_pk PRIMARY KEY (id_term, id_context);


--
-- Name: term term_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term
    ADD CONSTRAINT term_pk PRIMARY KEY (id);


--
-- Name: term_translation term_translation_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_translation
    ADD CONSTRAINT term_translation_pk PRIMARY KEY (id_term, id_translation);


--
-- Name: terminology_context terminology_context_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_context
    ADD CONSTRAINT terminology_context_pk PRIMARY KEY (id_context, id_terminology);


--
-- Name: terminology terminology_name_un; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology
    ADD CONSTRAINT terminology_name_un UNIQUE (name);


--
-- Name: terminology terminology_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology
    ADD CONSTRAINT terminology_pk PRIMARY KEY (id);


--
-- Name: terminology_translation terminology_translation_pk; Type: CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_translation
    ADD CONSTRAINT terminology_translation_pk PRIMARY KEY (id_terminology, id_translation);


--
-- Name: user UN_login; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users."user"
    ADD CONSTRAINT "UN_login" UNIQUE (login);


--
-- Name: belongs_to_group belongs_to_active_pkey; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.belongs_to_group
    ADD CONSTRAINT belongs_to_active_pkey PRIMARY KEY (user_id, group_id);


--
-- Name: usergroup ghgroup_name_key; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup
    ADD CONSTRAINT ghgroup_name_key UNIQUE (name);


--
-- Name: usergroup ghgroup_pkey; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup
    ADD CONSTRAINT ghgroup_pkey PRIMARY KEY (id);


--
-- Name: user ghuser_pkey; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users."user"
    ADD CONSTRAINT ghuser_pkey PRIMARY KEY (id);


--
-- Name: usergroup_type pk_usergroup_type; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup_type
    ADD CONSTRAINT pk_usergroup_type PRIMARY KEY (id);


--
-- Name: usergroup_usergroup_type usergroup_usergroup_type_pk; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup_usergroup_type
    ADD CONSTRAINT usergroup_usergroup_type_pk PRIMARY KEY (usergroup_id, usergroup_type_id);


--
-- Name: web_session web_session_pkey; Type: CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.web_session
    ADD CONSTRAINT web_session_pkey PRIMARY KEY (id);


--
-- Name: IX_id_notation; Type: INDEX; Schema: experiment; Owner: -
--

CREATE INDEX "IX_id_notation" ON experiment.experiment_has_notation USING btree (id_notation);


--
-- Name: IX_name; Type: INDEX; Schema: experiment; Owner: -
--

CREATE INDEX "IX_name" ON experiment.experiment USING btree (name varchar_ops);


--
-- Name: lieu_emplacement_index; Type: INDEX; Schema: location; Owner: -
--

CREATE INDEX lieu_emplacement_index ON location.place USING btree (id_plant_place);


--
-- Name: lieu_type_lieu_index; Type: INDEX; Schema: location; Owner: -
--

CREATE INDEX lieu_type_lieu_index ON location.place USING btree (id_place_type);


--
-- Name: IX_type_notation; Type: INDEX; Schema: notation; Owner: -
--

CREATE INDEX "IX_type_notation" ON notation.notation USING btree (id_type_notation);


--
-- Name: emplacement_site_index; Type: INDEX; Schema: plant; Owner: -
--

CREATE INDEX emplacement_site_index ON plant.place USING btree (id_site);


--
-- Name: nom_variete_index; Type: INDEX; Schema: plant; Owner: -
--

CREATE INDEX nom_variete_index ON plant.variety_name USING btree (value);


--
-- Name: address pepinieriste_pays_fk; Type: FK CONSTRAINT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.address
    ADD CONSTRAINT pepinieriste_pays_fk FOREIGN KEY (id_country) REFERENCES address_book.country(id) ON DELETE SET NULL;


--
-- Name: site site_pepiniere_fk; Type: FK CONSTRAINT; Schema: address_book; Owner: -
--

ALTER TABLE ONLY address_book.site
    ADD CONSTRAINT site_pepiniere_fk FOREIGN KEY (id_address) REFERENCES address_book.address(id) ON DELETE SET NULL;


--
-- Name: comparison comparison_id_ctrl_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.comparison
    ADD CONSTRAINT comparison_id_ctrl_fkey FOREIGN KEY (id_ctrl) REFERENCES sample.container(id);


--
-- Name: comparison comparison_id_groupe_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.comparison
    ADD CONSTRAINT comparison_id_groupe_fkey FOREIGN KEY (id_groupe) REFERENCES plant."group"(id_group);


--
-- Name: comparison comparison_id_ttmt_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.comparison
    ADD CONSTRAINT comparison_id_ttmt_fkey FOREIGN KEY (id_ttmt) REFERENCES sample.container(id);


--
-- Name: export export_id_rscript_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.export
    ADD CONSTRAINT export_id_rscript_fkey FOREIGN KEY (id_rscript) REFERENCES analytics.rscript(id);


--
-- Name: intensity intensity_id_sens_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.intensity
    ADD CONSTRAINT intensity_id_sens_fkey FOREIGN KEY (id_sens) REFERENCES analytics.sens(id);


--
-- Name: rscript rscript_id_groupe_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.rscript
    ADD CONSTRAINT rscript_id_groupe_fkey FOREIGN KEY (id_groupe) REFERENCES plant."group"(id_group);


--
-- Name: sens sens_id_export_fkey; Type: FK CONSTRAINT; Schema: analytics; Owner: -
--

ALTER TABLE ONLY analytics.sens
    ADD CONSTRAINT sens_id_export_fkey FOREIGN KEY (id_export) REFERENCES analytics.export(id);


--
-- Name: reference fk_id_var; Type: FK CONSTRAINT; Schema: bibliography; Owner: -
--

ALTER TABLE ONLY bibliography.reference
    ADD CONSTRAINT fk_id_var FOREIGN KEY (id_variety) REFERENCES plant.variety(id_variete);


--
-- Name: collection_has_accession FK_id_clone; Type: FK CONSTRAINT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection_has_accession
    ADD CONSTRAINT "FK_id_clone" FOREIGN KEY (id_clone) REFERENCES plant.accession(id);


--
-- Name: collection_has_accession FK_id_collection; Type: FK CONSTRAINT; Schema: collection; Owner: -
--

ALTER TABLE ONLY collection.collection_has_accession
    ADD CONSTRAINT "FK_id_collection" FOREIGN KEY (id_collection) REFERENCES collection.collection(id_collection);


--
-- Name: experiment_has_clone FK_id_clone; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_clone
    ADD CONSTRAINT "FK_id_clone" FOREIGN KEY (id_clone) REFERENCES plant.accession(id);


--
-- Name: experiment_has_container FK_id_container; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_container
    ADD CONSTRAINT "FK_id_container" FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: experiment_has_usergroup FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_usergroup
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_notation FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_notation
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_container FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_container
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_clone FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_clone
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_group FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_group
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_experiment FK_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_experiment
    ADD CONSTRAINT "FK_id_experiment" FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_experiment FK_id_experiment_child; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_experiment
    ADD CONSTRAINT "FK_id_experiment_child" FOREIGN KEY (id_experiment_child) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_group FK_id_groupe; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_group
    ADD CONSTRAINT "FK_id_groupe" FOREIGN KEY (id_group) REFERENCES plant."group"(id_group);


--
-- Name: experiment_has_usergroup FK_id_usergroup; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_usergroup
    ADD CONSTRAINT "FK_id_usergroup" FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: experiment_has_axis fk_id_axis; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_axis
    ADD CONSTRAINT fk_id_axis FOREIGN KEY (id_axis) REFERENCES experiment.axis(id);


--
-- Name: experiment_has_user fk_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_user
    ADD CONSTRAINT fk_id_experiment FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_status fk_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_status
    ADD CONSTRAINT fk_id_experiment FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_axis fk_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_axis
    ADD CONSTRAINT fk_id_experiment FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment_has_financing fk_id_experiment; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_financing
    ADD CONSTRAINT fk_id_experiment FOREIGN KEY (id_experiment) REFERENCES experiment.experiment(id);


--
-- Name: experiment fk_id_experiment_type; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment
    ADD CONSTRAINT fk_id_experiment_type FOREIGN KEY (id_experiment_type) REFERENCES experiment.experiment_type(id);


--
-- Name: experiment_has_financing fk_id_financing; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_financing
    ADD CONSTRAINT fk_id_financing FOREIGN KEY (id_financing) REFERENCES experiment.financing(id);


--
-- Name: experiment_has_status fk_id_status; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_status
    ADD CONSTRAINT fk_id_status FOREIGN KEY (id_status) REFERENCES experiment.status(id);


--
-- Name: experiment_has_user fk_id_user; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment_has_user
    ADD CONSTRAINT fk_id_user FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: experiment fk_id_user; Type: FK CONSTRAINT; Schema: experiment; Owner: -
--

ALTER TABLE ONLY experiment.experiment
    ADD CONSTRAINT fk_id_user FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: introduction fk_intro_fourni; Type: FK CONSTRAINT; Schema: import_export; Owner: -
--

ALTER TABLE ONLY import_export.introduction
    ADD CONSTRAINT fk_intro_fourni FOREIGN KEY (fournisseur) REFERENCES address_book.address(id);


--
-- Name: place FK_emplacement; Type: FK CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place
    ADD CONSTRAINT "FK_emplacement" FOREIGN KEY (id_plant_place) REFERENCES plant.place(id);


--
-- Name: place FK_type; Type: FK CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place
    ADD CONSTRAINT "FK_type" FOREIGN KEY (id_place_type) REFERENCES location.place_type(id);


--
-- Name: place_type_has_terminology fk_id_concept; Type: FK CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type_has_terminology
    ADD CONSTRAINT fk_id_concept FOREIGN KEY (id_concept) REFERENCES terminology.concept(id);


--
-- Name: place_type_has_terminology fk_id_context; Type: FK CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type_has_terminology
    ADD CONSTRAINT fk_id_context FOREIGN KEY (id_context) REFERENCES terminology.context(id);


--
-- Name: place_type_has_terminology fk_id_type_lieu; Type: FK CONSTRAINT; Schema: location; Owner: -
--

ALTER TABLE ONLY location.place_type_has_terminology
    ADD CONSTRAINT fk_id_type_lieu FOREIGN KEY (id_type_lieu) REFERENCES location.place_type(id);


--
-- Name: lien_contexte_notation FK_contexte_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.lien_contexte_notation
    ADD CONSTRAINT "FK_contexte_notation" FOREIGN KEY (id_context) REFERENCES notation.context(id);


--
-- Name: utilisateur_a_notation FK_id_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.utilisateur_a_notation
    ADD CONSTRAINT "FK_id_notation" FOREIGN KEY (id_notation) REFERENCES notation.notation(id_notation);


--
-- Name: utilisateur_a_notation FK_id_utilisateur; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.utilisateur_a_notation
    ADD CONSTRAINT "FK_id_utilisateur" FOREIGN KEY (id_utilisateur) REFERENCES notation.utilisateur(id_utilisateur);


--
-- Name: variety_has_notation FK_id_variete; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.variety_has_notation
    ADD CONSTRAINT "FK_id_variete" FOREIGN KEY (id_variete) REFERENCES plant.variety(id_variete);


--
-- Name: lien_contexte_notation FK_type_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.lien_contexte_notation
    ADD CONSTRAINT "FK_type_notation" FOREIGN KEY (id_notation_type) REFERENCES notation.notation_type(id_type_notation);


--
-- Name: notation_type_constraint FK_type_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_type_constraint
    ADD CONSTRAINT "FK_type_notation" FOREIGN KEY (id_type_notation) REFERENCES notation.notation_type(id_type_notation);


--
-- Name: notation FK_type_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation
    ADD CONSTRAINT "FK_type_notation" FOREIGN KEY (id_type_notation) REFERENCES notation.notation_type(id_type_notation);


--
-- Name: context contexte_notation_id_usergroup_fk; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.context
    ADD CONSTRAINT contexte_notation_id_usergroup_fk FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: cross_has_notation fk_croisement; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.cross_has_notation
    ADD CONSTRAINT fk_croisement FOREIGN KEY (id_cross) REFERENCES plant_cross.plant_cross(id);


--
-- Name: action_has_notation fk_id_action_action; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.action_has_notation
    ADD CONSTRAINT fk_id_action_action FOREIGN KEY (id_action) REFERENCES sample.action(id);


--
-- Name: container_has_notation fk_id_container_container; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.container_has_notation
    ADD CONSTRAINT fk_id_container_container FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: context_has_usergroup fk_id_context; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.context_has_usergroup
    ADD CONSTRAINT fk_id_context FOREIGN KEY (id_context) REFERENCES notation.context(id);


--
-- Name: taking_has_notation fk_id_prelevement_prelevement; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.taking_has_notation
    ADD CONSTRAINT fk_id_prelevement_prelevement FOREIGN KEY (id_taking) REFERENCES sample.taking(id);


--
-- Name: notation fk_id_site; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation
    ADD CONSTRAINT fk_id_site FOREIGN KEY (id_site) REFERENCES address_book.site(id_site);


--
-- Name: notation_list_values fk_id_type_notation; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_list_values
    ADD CONSTRAINT fk_id_type_notation FOREIGN KEY (id_type_notation) REFERENCES notation.notation_type(id_type_notation);


--
-- Name: notation_has_user fk_id_user; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_user
    ADD CONSTRAINT fk_id_user FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: notation_has_usergroup fk_id_usergroup; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_usergroup
    ADD CONSTRAINT fk_id_usergroup FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: context_has_usergroup fk_id_usergroup; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.context_has_usergroup
    ADD CONSTRAINT fk_id_usergroup FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: cross_seed_has_notation fk_lot_pepins; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.cross_seed_has_notation
    ADD CONSTRAINT fk_lot_pepins FOREIGN KEY (id_lot_pepins) REFERENCES plant_cross.cross_seed(id);


--
-- Name: notation_has_protocol fk_protocol; Type: FK CONSTRAINT; Schema: notation; Owner: -
--

ALTER TABLE ONLY notation.notation_has_protocol
    ADD CONSTRAINT fk_protocol FOREIGN KEY (id_protocol) REFERENCES documentation.protocol(id);


--
-- Name: variety FK_id_taxinomie; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety
    ADD CONSTRAINT "FK_id_taxinomie" FOREIGN KEY (id_taxonomy) REFERENCES plant.taxonomy(id);


--
-- Name: accession clone_introduction_var_fkey; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession
    ADD CONSTRAINT clone_introduction_var_fkey FOREIGN KEY (introduction_clone) REFERENCES import_export.introduction_clone(id);


--
-- Name: accession clone_var_fk; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession
    ADD CONSTRAINT clone_var_fk FOREIGN KEY (id_variety) REFERENCES plant.variety(id_variete) ON DELETE SET NULL;


--
-- Name: lot fk_clone; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot
    ADD CONSTRAINT fk_clone FOREIGN KEY (id_accession) REFERENCES plant.accession(id);


--
-- Name: variety fk_editeur; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety
    ADD CONSTRAINT fk_editeur FOREIGN KEY (editor) REFERENCES address_book.address(id);


--
-- Name: place fk_emplacement_site; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.place
    ADD CONSTRAINT fk_emplacement_site FOREIGN KEY (id_site) REFERENCES address_book.site(id_site);


--
-- Name: accession fk_fournisseur; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession
    ADD CONSTRAINT fk_fournisseur FOREIGN KEY (provider) REFERENCES address_book.address(id);


--
-- Name: variety_relation fk_id_child; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation
    ADD CONSTRAINT fk_id_child FOREIGN KEY (id_child) REFERENCES plant.variety(id_variete);


--
-- Name: group_has_usergroup fk_id_ghgroup; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_usergroup
    ADD CONSTRAINT fk_id_ghgroup FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: group_has_lot fk_id_groupe; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_lot
    ADD CONSTRAINT fk_id_groupe FOREIGN KEY (id_group) REFERENCES plant."group"(id_group);


--
-- Name: group_has_notation fk_id_groupe; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_notation
    ADD CONSTRAINT fk_id_groupe FOREIGN KEY (id_group) REFERENCES plant."group"(id_group);


--
-- Name: group_has_usergroup fk_id_groupe; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.group_has_usergroup
    ADD CONSTRAINT fk_id_groupe FOREIGN KEY (id_group) REFERENCES plant."group"(id_group);


--
-- Name: variety_relation fk_id_parent; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation
    ADD CONSTRAINT fk_id_parent FOREIGN KEY (id_parent) REFERENCES plant.variety(id_variete);


--
-- Name: taxon_has_variety_name_type fk_id_taxonomy; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_has_variety_name_type
    ADD CONSTRAINT fk_id_taxonomy FOREIGN KEY (id_taxonomy) REFERENCES plant.taxonomy(id);


--
-- Name: variety_relation fk_id_type; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_relation
    ADD CONSTRAINT fk_id_type FOREIGN KEY (id_type) REFERENCES plant.variety_relation_type(id);


--
-- Name: taxon_has_variety_name_type fk_id_variety_name_type; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxon_has_variety_name_type
    ADD CONSTRAINT fk_id_variety_name_type FOREIGN KEY (id_variety_name_type) REFERENCES plant.variety_name_type(id_type_nom);


--
-- Name: lot fk_lot; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot
    ADD CONSTRAINT fk_lot FOREIGN KEY (id_lot_source) REFERENCES plant.lot(id_lot);


--
-- Name: variety fk_obtenteur; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety
    ADD CONSTRAINT fk_obtenteur FOREIGN KEY (breeder) REFERENCES address_book.address(id);


--
-- Name: lot_has_protocol fk_protocol; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_protocol
    ADD CONSTRAINT fk_protocol FOREIGN KEY (id_protocol) REFERENCES documentation.protocol(id);


--
-- Name: taxonomy fk_taxon_type; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.taxonomy
    ADD CONSTRAINT fk_taxon_type FOREIGN KEY (id_taxon_type) REFERENCES plant.taxon_type(id);


--
-- Name: variety_name fk_type_nom; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.variety_name
    ADD CONSTRAINT fk_type_nom FOREIGN KEY (id_variety_name_type) REFERENCES plant.variety_name_type(id_type_nom);


--
-- Name: lot id_type_fkey; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot
    ADD CONSTRAINT id_type_fkey FOREIGN KEY (id_lot_type) REFERENCES plant.lot_type(id);


--
-- Name: lot_has_user lot_has_user_user_fk; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_user
    ADD CONSTRAINT lot_has_user_user_fk FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: lot_has_usergroup lot_has_usergroup_usergroup_fk; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.lot_has_usergroup
    ADD CONSTRAINT lot_has_usergroup_usergroup_fk FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: accession site_fk; Type: FK CONSTRAINT; Schema: plant; Owner: -
--

ALTER TABLE ONLY plant.accession
    ADD CONSTRAINT site_fk FOREIGN KEY (collection_site) REFERENCES address_book.site(id_site);


--
-- Name: plant_cross croisement_couple_fk; Type: FK CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.plant_cross
    ADD CONSTRAINT croisement_couple_fk FOREIGN KEY (id_couple) REFERENCES plant_cross.couple(id) ON DELETE SET NULL;


--
-- Name: multiplication fk_multiplicateur; Type: FK CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.multiplication
    ADD CONSTRAINT fk_multiplicateur FOREIGN KEY (multiplicateur) REFERENCES address_book.address(id);


--
-- Name: taking prelevement_croisement_fk; Type: FK CONSTRAINT; Schema: plant_cross; Owner: -
--

ALTER TABLE ONLY plant_cross.taking
    ADD CONSTRAINT prelevement_croisement_fk FOREIGN KEY (id_plant_cross) REFERENCES plant_cross.plant_cross(id) ON DELETE SET NULL;


--
-- Name: container FK_etat_contenant; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container
    ADD CONSTRAINT "FK_etat_contenant" FOREIGN KEY (id_container_level) REFERENCES sample.container_level(id);


--
-- Name: container FK_id_action; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container
    ADD CONSTRAINT "FK_id_action" FOREIGN KEY (id_action) REFERENCES sample.action(id);


--
-- Name: location FK_id_boite; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.location
    ADD CONSTRAINT "FK_id_boite" FOREIGN KEY (id_box) REFERENCES sample.box(id);


--
-- Name: taking FK_id_contenant; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.taking
    ADD CONSTRAINT "FK_id_contenant" FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: location FK_id_contenant; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.location
    ADD CONSTRAINT "FK_id_contenant" FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: user_a_taking FK_id_taking; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.user_a_taking
    ADD CONSTRAINT "FK_id_taking" FOREIGN KEY (id_taking) REFERENCES sample.taking(id);


--
-- Name: action FK_id_type_action; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action
    ADD CONSTRAINT "FK_id_type_action" FOREIGN KEY (id_action_type) REFERENCES sample.action_type(id);


--
-- Name: container FK_id_type_contenant; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container
    ADD CONSTRAINT "FK_id_type_contenant" FOREIGN KEY (id_container_type) REFERENCES sample.container_type(id);


--
-- Name: container FK_id_type_contenu; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container
    ADD CONSTRAINT "FK_id_type_contenu" FOREIGN KEY (id_content_type) REFERENCES sample.content_type(id);


--
-- Name: user_a_taking FK_id_user; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.user_a_taking
    ADD CONSTRAINT "FK_id_user" FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: box FK_type_boite; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box
    ADD CONSTRAINT "FK_type_boite" FOREIGN KEY (id_box_type) REFERENCES sample.box_type(id);


--
-- Name: taking Fk_id_type_tissu; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.taking
    ADD CONSTRAINT "Fk_id_type_tissu" FOREIGN KEY (id_tissue_type) REFERENCES sample.tissue_type(id);


--
-- Name: action_a_protocol fk_action; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_protocol
    ADD CONSTRAINT fk_action FOREIGN KEY (id_action) REFERENCES sample.action(id);


--
-- Name: action_a_container_origin fk_id_action; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_container_origin
    ADD CONSTRAINT fk_id_action FOREIGN KEY (id_action) REFERENCES sample.action(id);


--
-- Name: box_has_place fk_id_box; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_has_place
    ADD CONSTRAINT fk_id_box FOREIGN KEY (id_box) REFERENCES sample.box(id);


--
-- Name: box_type_a_container_type fk_id_box_type; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type_a_container_type
    ADD CONSTRAINT fk_id_box_type FOREIGN KEY (id_box_type) REFERENCES sample.box_type(id);


--
-- Name: action_a_container_origin fk_id_container; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_container_origin
    ADD CONSTRAINT fk_id_container FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: container_has_usergroup fk_id_container; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_has_usergroup
    ADD CONSTRAINT fk_id_container FOREIGN KEY (id_container) REFERENCES sample.container(id);


--
-- Name: box_type_a_container_type fk_id_container_type; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_type_a_container_type
    ADD CONSTRAINT fk_id_container_type FOREIGN KEY (id_container_type) REFERENCES sample.container_type(id);


--
-- Name: box_has_place fk_id_lieu; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box_has_place
    ADD CONSTRAINT fk_id_lieu FOREIGN KEY (id_place) REFERENCES location.place(id_lieu);


--
-- Name: box fk_id_usergroup; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.box
    ADD CONSTRAINT fk_id_usergroup FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: container_has_usergroup fk_id_usergroup; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.container_has_usergroup
    ADD CONSTRAINT fk_id_usergroup FOREIGN KEY (id_usergroup) REFERENCES users.usergroup(id);


--
-- Name: action_a_protocol fk_protocol; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action_a_protocol
    ADD CONSTRAINT fk_protocol FOREIGN KEY (id_protocol) REFERENCES documentation.protocol(id);


--
-- Name: action fk_user; Type: FK CONSTRAINT; Schema: sample; Owner: -
--

ALTER TABLE ONLY sample.action
    ADD CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES users."user"(id);


--
-- Name: term id_concept_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term
    ADD CONSTRAINT id_concept_fk FOREIGN KEY (id_concept) REFERENCES terminology.concept(id);


--
-- Name: term_context id_context_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_context
    ADD CONSTRAINT id_context_fk FOREIGN KEY (id_context) REFERENCES terminology.context(id);


--
-- Name: terminology_context id_context_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_context
    ADD CONSTRAINT id_context_fk FOREIGN KEY (id_context) REFERENCES terminology.context(id);


--
-- Name: i18n id_language_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.i18n
    ADD CONSTRAINT id_language_fk FOREIGN KEY (id_language) REFERENCES terminology.language(id);


--
-- Name: concepts_graph id_left_concept_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concepts_graph
    ADD CONSTRAINT id_left_concept_fk FOREIGN KEY (id_left_concept) REFERENCES terminology.concept(id);


--
-- Name: concepts_graph id_relationship_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concepts_graph
    ADD CONSTRAINT id_relationship_fk FOREIGN KEY (id_relationship) REFERENCES terminology.relationship(id);


--
-- Name: concepts_graph id_right_concept_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concepts_graph
    ADD CONSTRAINT id_right_concept_fk FOREIGN KEY (id_right_concept) REFERENCES terminology.concept(id);


--
-- Name: term_context id_term_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_context
    ADD CONSTRAINT id_term_fk FOREIGN KEY (id_term) REFERENCES terminology.term(id);


--
-- Name: term_translation id_term_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_translation
    ADD CONSTRAINT id_term_fk FOREIGN KEY (id_term) REFERENCES terminology.term(id);


--
-- Name: terminology_context id_terminology_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_context
    ADD CONSTRAINT id_terminology_fk FOREIGN KEY (id_terminology) REFERENCES terminology.terminology(id);


--
-- Name: terminology_translation id_terminology_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_translation
    ADD CONSTRAINT id_terminology_fk FOREIGN KEY (id_terminology) REFERENCES terminology.terminology(id);


--
-- Name: term_translation id_translation_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.term_translation
    ADD CONSTRAINT id_translation_fk FOREIGN KEY (id_translation) REFERENCES terminology.i18n(id);


--
-- Name: terminology_translation id_translation_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.terminology_translation
    ADD CONSTRAINT id_translation_fk FOREIGN KEY (id_translation) REFERENCES terminology.i18n(id);


--
-- Name: concept terminology_id_fk; Type: FK CONSTRAINT; Schema: terminology; Owner: -
--

ALTER TABLE ONLY terminology.concept
    ADD CONSTRAINT terminology_id_fk FOREIGN KEY (id_terminology) REFERENCES terminology.terminology(id);


--
-- Name: user address_address_book_fkey; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users."user"
    ADD CONSTRAINT address_address_book_fkey FOREIGN KEY (id_address) REFERENCES address_book.address(id);


--
-- Name: belongs_to_group belongs_to_active_group_id_fkey; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.belongs_to_group
    ADD CONSTRAINT belongs_to_active_group_id_fkey FOREIGN KEY (group_id) REFERENCES users.usergroup(id) ON DELETE RESTRICT;


--
-- Name: belongs_to_group belongs_to_active_user_id_fkey; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.belongs_to_group
    ADD CONSTRAINT belongs_to_active_user_id_fkey FOREIGN KEY (user_id) REFERENCES users."user"(id) ON DELETE RESTRICT;


--
-- Name: usergroup_usergroup_type usergroup_id_fk; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup_usergroup_type
    ADD CONSTRAINT usergroup_id_fk FOREIGN KEY (usergroup_id) REFERENCES users.usergroup(id);


--
-- Name: usergroup_usergroup_type usergroup_type_id_fk; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.usergroup_usergroup_type
    ADD CONSTRAINT usergroup_type_id_fk FOREIGN KEY (usergroup_type_id) REFERENCES users.usergroup_type(id);


--
-- Name: web_session web_session_user_id_fkey; Type: FK CONSTRAINT; Schema: users; Owner: -
--

ALTER TABLE ONLY users.web_session
    ADD CONSTRAINT web_session_user_id_fkey FOREIGN KEY (user_id) REFERENCES users."user"(id) ON DELETE RESTRICT;


--
-- Name: SCHEMA address_book; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA address_book FROM PUBLIC;
REVOKE ALL ON SCHEMA address_book FROM dev_team;
GRANT ALL ON SCHEMA address_book TO dev_team;
GRANT USAGE ON SCHEMA address_book TO PUBLIC;


--
-- Name: SCHEMA analytics; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA analytics FROM PUBLIC;
REVOKE ALL ON SCHEMA analytics FROM dev_team;
GRANT ALL ON SCHEMA analytics TO dev_team;


--
-- Name: SCHEMA bibliography; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA bibliography FROM PUBLIC;
REVOKE ALL ON SCHEMA bibliography FROM dev_team;
GRANT ALL ON SCHEMA bibliography TO dev_team;
GRANT USAGE ON SCHEMA bibliography TO PUBLIC;


--
-- Name: SCHEMA collection; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA collection FROM PUBLIC;
REVOKE ALL ON SCHEMA collection FROM dev_team;
GRANT ALL ON SCHEMA collection TO dev_team;
GRANT USAGE ON SCHEMA collection TO PUBLIC;


--
-- Name: SCHEMA experiment; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA experiment FROM PUBLIC;
REVOKE ALL ON SCHEMA experiment FROM dev_team;
GRANT ALL ON SCHEMA experiment TO dev_team;


--
-- Name: SCHEMA import_export; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA import_export FROM PUBLIC;
REVOKE ALL ON SCHEMA import_export FROM dev_team;
GRANT ALL ON SCHEMA import_export TO dev_team;
GRANT USAGE ON SCHEMA import_export TO PUBLIC;


--
-- Name: SCHEMA location; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA location FROM PUBLIC;
REVOKE ALL ON SCHEMA location FROM dev_team;
GRANT ALL ON SCHEMA location TO dev_team;
GRANT USAGE ON SCHEMA location TO PUBLIC;


--
-- Name: SCHEMA notation; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA notation FROM PUBLIC;
REVOKE ALL ON SCHEMA notation FROM dev_team;
GRANT ALL ON SCHEMA notation TO dev_team;
GRANT USAGE ON SCHEMA notation TO PUBLIC;


--
-- Name: SCHEMA plant; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA plant FROM PUBLIC;
REVOKE ALL ON SCHEMA plant FROM dev_team;
GRANT ALL ON SCHEMA plant TO dev_team;
GRANT USAGE ON SCHEMA plant TO PUBLIC;


--
-- Name: SCHEMA plant_cross; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA plant_cross FROM PUBLIC;
REVOKE ALL ON SCHEMA plant_cross FROM dev_team;
GRANT ALL ON SCHEMA plant_cross TO dev_team;
GRANT USAGE ON SCHEMA plant_cross TO PUBLIC;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO dev_team;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: SCHEMA sample; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA sample FROM PUBLIC;
REVOKE ALL ON SCHEMA sample FROM dev_team;
GRANT ALL ON SCHEMA sample TO dev_team;
GRANT USAGE ON SCHEMA sample TO PUBLIC;


--
-- Name: SCHEMA terminology; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA terminology FROM PUBLIC;
REVOKE ALL ON SCHEMA terminology FROM dev_team;
GRANT ALL ON SCHEMA terminology TO dev_team;


--
-- Name: SCHEMA users; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA users FROM PUBLIC;
REVOKE ALL ON SCHEMA users FROM dev_team;
GRANT ALL ON SCHEMA users TO dev_team;
GRANT USAGE ON SCHEMA users TO PUBLIC;


--
-- Name: TABLE address; Type: ACL; Schema: address_book; Owner: -
--

REVOKE ALL ON TABLE address_book.address FROM PUBLIC;
REVOKE ALL ON TABLE address_book.address FROM dev_team;
GRANT ALL ON TABLE address_book.address TO dev_team;
GRANT SELECT ON TABLE address_book.address TO PUBLIC;


--
-- Name: TABLE country; Type: ACL; Schema: address_book; Owner: -
--

REVOKE ALL ON TABLE address_book.country FROM PUBLIC;
REVOKE ALL ON TABLE address_book.country FROM dev_team;
GRANT ALL ON TABLE address_book.country TO dev_team;
GRANT SELECT ON TABLE address_book.country TO PUBLIC;


--
-- Name: TABLE site; Type: ACL; Schema: address_book; Owner: -
--

REVOKE ALL ON TABLE address_book.site FROM PUBLIC;
REVOKE ALL ON TABLE address_book.site FROM dev_team;
GRANT ALL ON TABLE address_book.site TO dev_team;
GRANT SELECT ON TABLE address_book.site TO PUBLIC;


--
-- Name: TABLE comparison; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.comparison FROM PUBLIC;
REVOKE ALL ON TABLE analytics.comparison FROM dev_team;
GRANT ALL ON TABLE analytics.comparison TO dev_team;
GRANT SELECT ON TABLE analytics.comparison TO PUBLIC;


--
-- Name: TABLE export; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.export FROM PUBLIC;
REVOKE ALL ON TABLE analytics.export FROM dev_team;
GRANT ALL ON TABLE analytics.export TO dev_team;
GRANT SELECT ON TABLE analytics.export TO PUBLIC;


--
-- Name: TABLE intensity; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.intensity FROM PUBLIC;
REVOKE ALL ON TABLE analytics.intensity FROM dev_team;
GRANT ALL ON TABLE analytics.intensity TO dev_team;
GRANT SELECT ON TABLE analytics.intensity TO PUBLIC;


--
-- Name: TABLE rscript; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.rscript FROM PUBLIC;
REVOKE ALL ON TABLE analytics.rscript FROM dev_team;
GRANT ALL ON TABLE analytics.rscript TO dev_team;
GRANT SELECT ON TABLE analytics.rscript TO PUBLIC;


--
-- Name: TABLE rscripts; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.rscripts FROM PUBLIC;
REVOKE ALL ON TABLE analytics.rscripts FROM dev_team;
GRANT ALL ON TABLE analytics.rscripts TO dev_team;
GRANT SELECT ON TABLE analytics.rscripts TO PUBLIC;


--
-- Name: TABLE sens; Type: ACL; Schema: analytics; Owner: -
--

REVOKE ALL ON TABLE analytics.sens FROM PUBLIC;
REVOKE ALL ON TABLE analytics.sens FROM dev_team;
GRANT ALL ON TABLE analytics.sens TO dev_team;
GRANT SELECT ON TABLE analytics.sens TO PUBLIC;


--
-- Name: TABLE reference; Type: ACL; Schema: bibliography; Owner: -
--

REVOKE ALL ON TABLE bibliography.reference FROM PUBLIC;
REVOKE ALL ON TABLE bibliography.reference FROM dev_team;
GRANT ALL ON TABLE bibliography.reference TO dev_team;
GRANT SELECT ON TABLE bibliography.reference TO PUBLIC;


--
-- Name: TABLE collection; Type: ACL; Schema: collection; Owner: -
--

REVOKE ALL ON TABLE collection.collection FROM PUBLIC;
REVOKE ALL ON TABLE collection.collection FROM dev_team;
GRANT ALL ON TABLE collection.collection TO dev_team;
GRANT SELECT ON TABLE collection.collection TO PUBLIC;


--
-- Name: TABLE collection_has_accession; Type: ACL; Schema: collection; Owner: -
--

REVOKE ALL ON TABLE collection.collection_has_accession FROM PUBLIC;
REVOKE ALL ON TABLE collection.collection_has_accession FROM dev_team;
GRANT ALL ON TABLE collection.collection_has_accession TO dev_team;
GRANT SELECT ON TABLE collection.collection_has_accession TO PUBLIC;


--
-- Name: TABLE protocol; Type: ACL; Schema: documentation; Owner: -
--

REVOKE ALL ON TABLE documentation.protocol FROM PUBLIC;
REVOKE ALL ON TABLE documentation.protocol FROM dev_team;
GRANT ALL ON TABLE documentation.protocol TO dev_team;


--
-- Name: TABLE experiment; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment FROM dev_team;
GRANT ALL ON TABLE experiment.experiment TO dev_team;
GRANT SELECT ON TABLE experiment.experiment TO PUBLIC;


--
-- Name: TABLE experiment_has_axis; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_axis FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_axis FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_axis TO dev_team;


--
-- Name: TABLE experiment_has_clone; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_clone FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_clone FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_clone TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_clone TO PUBLIC;


--
-- Name: TABLE experiment_has_container; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_container FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_container FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_container TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_container TO PUBLIC;


--
-- Name: TABLE experiment_has_experiment; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_experiment FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_experiment FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_experiment TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_experiment TO PUBLIC;


--
-- Name: TABLE experiment_has_financing; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_financing FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_financing FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_financing TO dev_team;


--
-- Name: TABLE experiment_has_group; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_group FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_group FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_group TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_group TO PUBLIC;


--
-- Name: TABLE experiment_has_notation; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_notation FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_notation TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_notation TO PUBLIC;


--
-- Name: TABLE experiment_has_status; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_status FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_status FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_status TO dev_team;


--
-- Name: TABLE experiment_has_user; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_user FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_user FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_user TO dev_team;


--
-- Name: TABLE experiment_has_usergroup; Type: ACL; Schema: experiment; Owner: -
--

REVOKE ALL ON TABLE experiment.experiment_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE experiment.experiment_has_usergroup FROM dev_team;
GRANT ALL ON TABLE experiment.experiment_has_usergroup TO dev_team;
GRANT SELECT ON TABLE experiment.experiment_has_usergroup TO PUBLIC;


--
-- Name: TABLE introduction; Type: ACL; Schema: import_export; Owner: -
--

REVOKE ALL ON TABLE import_export.introduction FROM PUBLIC;
REVOKE ALL ON TABLE import_export.introduction FROM dev_team;
GRANT ALL ON TABLE import_export.introduction TO dev_team;
GRANT SELECT ON TABLE import_export.introduction TO PUBLIC;


--
-- Name: TABLE introduction_clone; Type: ACL; Schema: import_export; Owner: -
--

REVOKE ALL ON TABLE import_export.introduction_clone FROM PUBLIC;
REVOKE ALL ON TABLE import_export.introduction_clone FROM dev_team;
GRANT ALL ON TABLE import_export.introduction_clone TO dev_team;
GRANT SELECT ON TABLE import_export.introduction_clone TO PUBLIC;


--
-- Name: TABLE place; Type: ACL; Schema: location; Owner: -
--

REVOKE ALL ON TABLE location.place FROM PUBLIC;
REVOKE ALL ON TABLE location.place FROM dev_team;
GRANT ALL ON TABLE location.place TO dev_team;
GRANT SELECT ON TABLE location.place TO PUBLIC;


--
-- Name: TABLE place_type; Type: ACL; Schema: location; Owner: -
--

REVOKE ALL ON TABLE location.place_type FROM PUBLIC;
REVOKE ALL ON TABLE location.place_type FROM dev_team;
GRANT ALL ON TABLE location.place_type TO dev_team;
GRANT SELECT ON TABLE location.place_type TO PUBLIC;


--
-- Name: TABLE place_type_has_terminology; Type: ACL; Schema: location; Owner: -
--

REVOKE ALL ON TABLE location.place_type_has_terminology FROM PUBLIC;
REVOKE ALL ON TABLE location.place_type_has_terminology FROM dev_team;
GRANT ALL ON TABLE location.place_type_has_terminology TO dev_team;
GRANT SELECT ON TABLE location.place_type_has_terminology TO PUBLIC;


--
-- Name: TABLE action_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.action_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.action_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.action_has_notation TO dev_team;


--
-- Name: TABLE container_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.container_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.container_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.container_has_notation TO dev_team;


--
-- Name: TABLE context; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.context FROM PUBLIC;
REVOKE ALL ON TABLE notation.context FROM dev_team;
GRANT ALL ON TABLE notation.context TO dev_team;
GRANT SELECT ON TABLE notation.context TO PUBLIC;


--
-- Name: TABLE context_has_usergroup; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.context_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE notation.context_has_usergroup FROM dev_team;
GRANT ALL ON TABLE notation.context_has_usergroup TO dev_team;


--
-- Name: TABLE cross_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.cross_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.cross_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.cross_has_notation TO dev_team;


--
-- Name: TABLE cross_seed_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.cross_seed_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.cross_seed_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.cross_seed_has_notation TO dev_team;
GRANT SELECT ON TABLE notation.cross_seed_has_notation TO PUBLIC;


--
-- Name: TABLE lien_contexte_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.lien_contexte_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.lien_contexte_notation FROM dev_team;
GRANT ALL ON TABLE notation.lien_contexte_notation TO dev_team;
GRANT SELECT ON TABLE notation.lien_contexte_notation TO PUBLIC;


--
-- Name: TABLE lot_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.lot_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.lot_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.lot_has_notation TO dev_team;
GRANT SELECT ON TABLE notation.lot_has_notation TO PUBLIC;


--
-- Name: TABLE notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation FROM dev_team;
GRANT ALL ON TABLE notation.notation TO dev_team;
GRANT SELECT ON TABLE notation.notation TO PUBLIC;


--
-- Name: TABLE notation_date; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_date FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_date FROM dev_team;
GRANT ALL ON TABLE notation.notation_date TO dev_team;
GRANT SELECT ON TABLE notation.notation_date TO PUBLIC;


--
-- Name: TABLE notation_document; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_document FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_document FROM dev_team;
GRANT ALL ON TABLE notation.notation_document TO dev_team;


--
-- Name: TABLE notation_double; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_double FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_double FROM dev_team;
GRANT ALL ON TABLE notation.notation_double TO dev_team;
GRANT SELECT ON TABLE notation.notation_double TO PUBLIC;


--
-- Name: TABLE notation_has_protocol; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_has_protocol FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_has_protocol FROM dev_team;
GRANT ALL ON TABLE notation.notation_has_protocol TO dev_team;
GRANT SELECT ON TABLE notation.notation_has_protocol TO PUBLIC;


--
-- Name: TABLE notation_has_user; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_has_user FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_has_user FROM dev_team;
GRANT ALL ON TABLE notation.notation_has_user TO dev_team;


--
-- Name: TABLE notation_has_usergroup; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_has_usergroup FROM dev_team;
GRANT ALL ON TABLE notation.notation_has_usergroup TO dev_team;


--
-- Name: TABLE notation_integer; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_integer FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_integer FROM dev_team;
GRANT ALL ON TABLE notation.notation_integer TO dev_team;
GRANT SELECT ON TABLE notation.notation_integer TO PUBLIC;


--
-- Name: TABLE notation_list; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_list FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_list FROM dev_team;
GRANT ALL ON TABLE notation.notation_list TO dev_team;
GRANT SELECT ON TABLE notation.notation_list TO PUBLIC;


--
-- Name: TABLE notation_list_values; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_list_values FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_list_values FROM dev_team;
GRANT ALL ON TABLE notation.notation_list_values TO dev_team;
GRANT SELECT ON TABLE notation.notation_list_values TO PUBLIC;


--
-- Name: TABLE notation_texte; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_texte FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_texte FROM dev_team;
GRANT ALL ON TABLE notation.notation_texte TO dev_team;
GRANT SELECT ON TABLE notation.notation_texte TO PUBLIC;


--
-- Name: TABLE notation_type; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_type FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_type FROM dev_team;
GRANT ALL ON TABLE notation.notation_type TO dev_team;
GRANT SELECT ON TABLE notation.notation_type TO PUBLIC;


--
-- Name: TABLE notation_type_constraint; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.notation_type_constraint FROM PUBLIC;
REVOKE ALL ON TABLE notation.notation_type_constraint FROM dev_team;
GRANT ALL ON TABLE notation.notation_type_constraint TO dev_team;
GRANT SELECT ON TABLE notation.notation_type_constraint TO PUBLIC;


--
-- Name: TABLE taking_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.taking_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.taking_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.taking_has_notation TO dev_team;
GRANT SELECT ON TABLE notation.taking_has_notation TO PUBLIC;


--
-- Name: TABLE utilisateur; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.utilisateur FROM PUBLIC;
REVOKE ALL ON TABLE notation.utilisateur FROM dev_team;
GRANT ALL ON TABLE notation.utilisateur TO dev_team;
GRANT SELECT ON TABLE notation.utilisateur TO PUBLIC;


--
-- Name: TABLE utilisateur_a_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.utilisateur_a_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.utilisateur_a_notation FROM dev_team;
GRANT ALL ON TABLE notation.utilisateur_a_notation TO dev_team;
GRANT SELECT ON TABLE notation.utilisateur_a_notation TO PUBLIC;


--
-- Name: TABLE variety_has_notation; Type: ACL; Schema: notation; Owner: -
--

REVOKE ALL ON TABLE notation.variety_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE notation.variety_has_notation FROM dev_team;
GRANT ALL ON TABLE notation.variety_has_notation TO dev_team;
GRANT SELECT ON TABLE notation.variety_has_notation TO PUBLIC;


--
-- Name: TABLE accession; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.accession FROM PUBLIC;
REVOKE ALL ON TABLE plant.accession FROM dev_team;
GRANT ALL ON TABLE plant.accession TO dev_team;
GRANT SELECT ON TABLE plant.accession TO PUBLIC;


--
-- Name: TABLE cpt_numero_clone; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.cpt_numero_clone FROM PUBLIC;
REVOKE ALL ON TABLE plant.cpt_numero_clone FROM dev_team;
GRANT ALL ON TABLE plant.cpt_numero_clone TO dev_team;
GRANT SELECT ON TABLE plant.cpt_numero_clone TO PUBLIC;


--
-- Name: TABLE place; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.place FROM PUBLIC;
REVOKE ALL ON TABLE plant.place FROM dev_team;
GRANT ALL ON TABLE plant.place TO dev_team;
GRANT SELECT ON TABLE plant.place TO PUBLIC;


--
-- Name: TABLE group_has_lot; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.group_has_lot FROM PUBLIC;
REVOKE ALL ON TABLE plant.group_has_lot FROM dev_team;
GRANT ALL ON TABLE plant.group_has_lot TO dev_team;
GRANT SELECT ON TABLE plant.group_has_lot TO PUBLIC;


--
-- Name: TABLE "group"; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant."group" FROM PUBLIC;
REVOKE ALL ON TABLE plant."group" FROM dev_team;
GRANT ALL ON TABLE plant."group" TO dev_team;
GRANT SELECT ON TABLE plant."group" TO PUBLIC;


--
-- Name: TABLE group_has_notation; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.group_has_notation FROM PUBLIC;
REVOKE ALL ON TABLE plant.group_has_notation FROM dev_team;
GRANT ALL ON TABLE plant.group_has_notation TO dev_team;
GRANT SELECT ON TABLE plant.group_has_notation TO PUBLIC;


--
-- Name: TABLE group_has_usergroup; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.group_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE plant.group_has_usergroup FROM dev_team;
GRANT ALL ON TABLE plant.group_has_usergroup TO dev_team;
GRANT SELECT ON TABLE plant.group_has_usergroup TO PUBLIC;


--
-- Name: TABLE lot; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.lot FROM PUBLIC;
REVOKE ALL ON TABLE plant.lot FROM dev_team;
GRANT ALL ON TABLE plant.lot TO dev_team;
GRANT SELECT ON TABLE plant.lot TO PUBLIC;


--
-- Name: TABLE lot_has_protocol; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.lot_has_protocol FROM PUBLIC;
REVOKE ALL ON TABLE plant.lot_has_protocol FROM dev_team;
GRANT ALL ON TABLE plant.lot_has_protocol TO dev_team;
GRANT SELECT ON TABLE plant.lot_has_protocol TO PUBLIC;


--
-- Name: TABLE lot_has_user; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.lot_has_user FROM PUBLIC;
REVOKE ALL ON TABLE plant.lot_has_user FROM dev_team;
GRANT ALL ON TABLE plant.lot_has_user TO dev_team;
GRANT SELECT ON TABLE plant.lot_has_user TO PUBLIC;


--
-- Name: TABLE lot_has_usergroup; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.lot_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE plant.lot_has_usergroup FROM dev_team;
GRANT ALL ON TABLE plant.lot_has_usergroup TO dev_team;
GRANT SELECT ON TABLE plant.lot_has_usergroup TO PUBLIC;


--
-- Name: TABLE lot_type; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.lot_type FROM PUBLIC;
REVOKE ALL ON TABLE plant.lot_type FROM dev_team;
GRANT ALL ON TABLE plant.lot_type TO dev_team;
GRANT SELECT ON TABLE plant.lot_type TO PUBLIC;


--
-- Name: TABLE variety_name; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.variety_name FROM PUBLIC;
REVOKE ALL ON TABLE plant.variety_name FROM dev_team;
GRANT ALL ON TABLE plant.variety_name TO dev_team;
GRANT SELECT ON TABLE plant.variety_name TO PUBLIC;


--
-- Name: TABLE seed; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.seed FROM PUBLIC;
REVOKE ALL ON TABLE plant.seed FROM dev_team;
GRANT ALL ON TABLE plant.seed TO dev_team;
GRANT SELECT ON TABLE plant.seed TO PUBLIC;


--
-- Name: TABLE taxonomy; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.taxonomy FROM PUBLIC;
REVOKE ALL ON TABLE plant.taxonomy FROM dev_team;
GRANT ALL ON TABLE plant.taxonomy TO dev_team;
GRANT SELECT ON TABLE plant.taxonomy TO PUBLIC;


--
-- Name: TABLE taxon_type; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.taxon_type FROM PUBLIC;
REVOKE ALL ON TABLE plant.taxon_type FROM dev_team;
GRANT ALL ON TABLE plant.taxon_type TO dev_team;
GRANT SELECT ON TABLE plant.taxon_type TO PUBLIC;


--
-- Name: TABLE tree; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.tree FROM PUBLIC;
REVOKE ALL ON TABLE plant.tree FROM dev_team;
GRANT ALL ON TABLE plant.tree TO dev_team;
GRANT SELECT ON TABLE plant.tree TO PUBLIC;


--
-- Name: TABLE variety_name_type; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.variety_name_type FROM PUBLIC;
REVOKE ALL ON TABLE plant.variety_name_type FROM dev_team;
GRANT ALL ON TABLE plant.variety_name_type TO dev_team;
GRANT SELECT ON TABLE plant.variety_name_type TO PUBLIC;


--
-- Name: TABLE variety; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.variety FROM PUBLIC;
REVOKE ALL ON TABLE plant.variety FROM dev_team;
GRANT ALL ON TABLE plant.variety TO dev_team;
GRANT SELECT ON TABLE plant.variety TO PUBLIC;


--
-- Name: TABLE variety_relation; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.variety_relation FROM PUBLIC;
REVOKE ALL ON TABLE plant.variety_relation FROM dev_team;
GRANT ALL ON TABLE plant.variety_relation TO dev_team;
GRANT SELECT ON TABLE plant.variety_relation TO PUBLIC;


--
-- Name: TABLE variety_relation_type; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.variety_relation_type FROM PUBLIC;
REVOKE ALL ON TABLE plant.variety_relation_type FROM dev_team;
GRANT ALL ON TABLE plant.variety_relation_type TO dev_team;
GRANT SELECT ON TABLE plant.variety_relation_type TO PUBLIC;


--
-- Name: TABLE vue_arbre; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_arbre FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_arbre FROM fdupuis;
GRANT ALL ON TABLE plant.vue_arbre TO fdupuis;
GRANT ALL ON TABLE plant.vue_arbre TO dev_team;
GRANT SELECT ON TABLE plant.vue_arbre TO PUBLIC;


--
-- Name: TABLE vue_clone; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_clone FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_clone FROM fdupuis;
GRANT ALL ON TABLE plant.vue_clone TO fdupuis;
GRANT ALL ON TABLE plant.vue_clone TO dev_team;
GRANT SELECT ON TABLE plant.vue_clone TO PUBLIC;


--
-- Name: TABLE vue_graines; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_graines FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_graines FROM dev_team;
GRANT ALL ON TABLE plant.vue_graines TO dev_team;
GRANT SELECT ON TABLE plant.vue_graines TO PUBLIC;


--
-- Name: TABLE vue_lieu; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_lieu FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_lieu FROM fdupuis;
GRANT ALL ON TABLE plant.vue_lieu TO fdupuis;
GRANT ALL ON TABLE plant.vue_lieu TO dev_team;
GRANT SELECT ON TABLE plant.vue_lieu TO PUBLIC;


--
-- Name: TABLE vue_lot; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_lot FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_lot FROM fdupuis;
GRANT ALL ON TABLE plant.vue_lot TO fdupuis;
GRANT ALL ON TABLE plant.vue_lot TO dev_team;
GRANT SELECT ON TABLE plant.vue_lot TO PUBLIC;


--
-- Name: TABLE vue_nom_variete; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_nom_variete FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_nom_variete FROM fdupuis;
GRANT ALL ON TABLE plant.vue_nom_variete TO fdupuis;
GRANT ALL ON TABLE plant.vue_nom_variete TO dev_team;
GRANT SELECT ON TABLE plant.vue_nom_variete TO PUBLIC;


--
-- Name: TABLE vue_variete; Type: ACL; Schema: plant; Owner: -
--

REVOKE ALL ON TABLE plant.vue_variete FROM PUBLIC;
REVOKE ALL ON TABLE plant.vue_variete FROM fdupuis;
GRANT ALL ON TABLE plant.vue_variete TO fdupuis;
GRANT ALL ON TABLE plant.vue_variete TO dev_team;
GRANT SELECT ON TABLE plant.vue_variete TO PUBLIC;


--
-- Name: TABLE couple; Type: ACL; Schema: plant_cross; Owner: -
--

REVOKE ALL ON TABLE plant_cross.couple FROM PUBLIC;
REVOKE ALL ON TABLE plant_cross.couple FROM dev_team;
GRANT ALL ON TABLE plant_cross.couple TO dev_team;
GRANT SELECT ON TABLE plant_cross.couple TO PUBLIC;


--
-- Name: TABLE plant_cross; Type: ACL; Schema: plant_cross; Owner: -
--

REVOKE ALL ON TABLE plant_cross.plant_cross FROM PUBLIC;
REVOKE ALL ON TABLE plant_cross.plant_cross FROM dev_team;
GRANT ALL ON TABLE plant_cross.plant_cross TO dev_team;
GRANT SELECT ON TABLE plant_cross.plant_cross TO PUBLIC;


--
-- Name: TABLE cross_seed; Type: ACL; Schema: plant_cross; Owner: -
--

REVOKE ALL ON TABLE plant_cross.cross_seed FROM PUBLIC;
REVOKE ALL ON TABLE plant_cross.cross_seed FROM dev_team;
GRANT ALL ON TABLE plant_cross.cross_seed TO dev_team;
GRANT SELECT ON TABLE plant_cross.cross_seed TO PUBLIC;


--
-- Name: TABLE multiplication; Type: ACL; Schema: plant_cross; Owner: -
--

REVOKE ALL ON TABLE plant_cross.multiplication FROM PUBLIC;
REVOKE ALL ON TABLE plant_cross.multiplication FROM dev_team;
GRANT ALL ON TABLE plant_cross.multiplication TO dev_team;
GRANT SELECT ON TABLE plant_cross.multiplication TO PUBLIC;


--
-- Name: TABLE taking; Type: ACL; Schema: plant_cross; Owner: -
--

REVOKE ALL ON TABLE plant_cross.taking FROM PUBLIC;
REVOKE ALL ON TABLE plant_cross.taking FROM dev_team;
GRANT ALL ON TABLE plant_cross.taking TO dev_team;
GRANT SELECT ON TABLE plant_cross.taking TO PUBLIC;


--
-- Name: TABLE action; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.action FROM PUBLIC;
REVOKE ALL ON TABLE sample.action FROM dev_team;
GRANT ALL ON TABLE sample.action TO dev_team;
GRANT SELECT ON TABLE sample.action TO PUBLIC;


--
-- Name: TABLE action_a_container_origin; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.action_a_container_origin FROM PUBLIC;
REVOKE ALL ON TABLE sample.action_a_container_origin FROM dev_team;
GRANT ALL ON TABLE sample.action_a_container_origin TO dev_team;
GRANT SELECT ON TABLE sample.action_a_container_origin TO PUBLIC;


--
-- Name: TABLE action_a_protocol; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.action_a_protocol FROM PUBLIC;
REVOKE ALL ON TABLE sample.action_a_protocol FROM dev_team;
GRANT ALL ON TABLE sample.action_a_protocol TO dev_team;
GRANT SELECT ON TABLE sample.action_a_protocol TO PUBLIC;


--
-- Name: TABLE action_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.action_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.action_type FROM dev_team;
GRANT ALL ON TABLE sample.action_type TO dev_team;
GRANT SELECT ON TABLE sample.action_type TO PUBLIC;


--
-- Name: TABLE box; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.box FROM PUBLIC;
REVOKE ALL ON TABLE sample.box FROM dev_team;
GRANT ALL ON TABLE sample.box TO dev_team;
GRANT SELECT ON TABLE sample.box TO PUBLIC;


--
-- Name: TABLE box_has_place; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.box_has_place FROM PUBLIC;
REVOKE ALL ON TABLE sample.box_has_place FROM dev_team;
GRANT ALL ON TABLE sample.box_has_place TO dev_team;
GRANT SELECT ON TABLE sample.box_has_place TO PUBLIC;


--
-- Name: TABLE box_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.box_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.box_type FROM dev_team;
GRANT ALL ON TABLE sample.box_type TO dev_team;
GRANT SELECT ON TABLE sample.box_type TO PUBLIC;


--
-- Name: TABLE box_type_a_container_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.box_type_a_container_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.box_type_a_container_type FROM dev_team;
GRANT ALL ON TABLE sample.box_type_a_container_type TO dev_team;
GRANT SELECT ON TABLE sample.box_type_a_container_type TO PUBLIC;


--
-- Name: TABLE container; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.container FROM PUBLIC;
REVOKE ALL ON TABLE sample.container FROM dev_team;
GRANT ALL ON TABLE sample.container TO dev_team;
GRANT SELECT ON TABLE sample.container TO PUBLIC;


--
-- Name: TABLE container_has_usergroup; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.container_has_usergroup FROM PUBLIC;
REVOKE ALL ON TABLE sample.container_has_usergroup FROM dev_team;
GRANT ALL ON TABLE sample.container_has_usergroup TO dev_team;


--
-- Name: TABLE container_level; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.container_level FROM PUBLIC;
REVOKE ALL ON TABLE sample.container_level FROM dev_team;
GRANT ALL ON TABLE sample.container_level TO dev_team;
GRANT SELECT ON TABLE sample.container_level TO PUBLIC;


--
-- Name: TABLE container_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.container_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.container_type FROM dev_team;
GRANT ALL ON TABLE sample.container_type TO dev_team;
GRANT SELECT ON TABLE sample.container_type TO PUBLIC;


--
-- Name: TABLE content_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.content_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.content_type FROM dev_team;
GRANT ALL ON TABLE sample.content_type TO dev_team;


--
-- Name: TABLE location; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.location FROM PUBLIC;
REVOKE ALL ON TABLE sample.location FROM dev_team;
GRANT ALL ON TABLE sample.location TO dev_team;
GRANT SELECT ON TABLE sample.location TO PUBLIC;


--
-- Name: TABLE taking; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.taking FROM PUBLIC;
REVOKE ALL ON TABLE sample.taking FROM dev_team;
GRANT ALL ON TABLE sample.taking TO dev_team;
GRANT SELECT ON TABLE sample.taking TO PUBLIC;


--
-- Name: TABLE tissue_type; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.tissue_type FROM PUBLIC;
REVOKE ALL ON TABLE sample.tissue_type FROM dev_team;
GRANT ALL ON TABLE sample.tissue_type TO dev_team;
GRANT SELECT ON TABLE sample.tissue_type TO PUBLIC;


--
-- Name: TABLE user_a_taking; Type: ACL; Schema: sample; Owner: -
--

REVOKE ALL ON TABLE sample.user_a_taking FROM PUBLIC;
REVOKE ALL ON TABLE sample.user_a_taking FROM dev_team;
GRANT ALL ON TABLE sample.user_a_taking TO dev_team;


--
-- Name: TABLE concept; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.concept FROM PUBLIC;
REVOKE ALL ON TABLE terminology.concept FROM dev_team;
GRANT ALL ON TABLE terminology.concept TO dev_team;
GRANT SELECT ON TABLE terminology.concept TO PUBLIC;


--
-- Name: TABLE concepts_graph; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.concepts_graph FROM PUBLIC;
REVOKE ALL ON TABLE terminology.concepts_graph FROM dev_team;
GRANT ALL ON TABLE terminology.concepts_graph TO dev_team;
GRANT SELECT ON TABLE terminology.concepts_graph TO PUBLIC;


--
-- Name: TABLE context; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.context FROM PUBLIC;
REVOKE ALL ON TABLE terminology.context FROM dev_team;
GRANT ALL ON TABLE terminology.context TO dev_team;
GRANT SELECT ON TABLE terminology.context TO PUBLIC;


--
-- Name: TABLE i18n; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.i18n FROM PUBLIC;
REVOKE ALL ON TABLE terminology.i18n FROM dev_team;
GRANT ALL ON TABLE terminology.i18n TO dev_team;
GRANT SELECT ON TABLE terminology.i18n TO PUBLIC;


--
-- Name: TABLE language; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.language FROM PUBLIC;
REVOKE ALL ON TABLE terminology.language FROM dev_team;
GRANT ALL ON TABLE terminology.language TO dev_team;
GRANT SELECT ON TABLE terminology.language TO PUBLIC;


--
-- Name: TABLE relationship; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.relationship FROM PUBLIC;
REVOKE ALL ON TABLE terminology.relationship FROM dev_team;
GRANT ALL ON TABLE terminology.relationship TO dev_team;
GRANT SELECT ON TABLE terminology.relationship TO PUBLIC;


--
-- Name: TABLE term; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.term FROM PUBLIC;
REVOKE ALL ON TABLE terminology.term FROM dev_team;
GRANT ALL ON TABLE terminology.term TO dev_team;
GRANT SELECT ON TABLE terminology.term TO PUBLIC;


--
-- Name: TABLE term_context; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.term_context FROM PUBLIC;
REVOKE ALL ON TABLE terminology.term_context FROM dev_team;
GRANT ALL ON TABLE terminology.term_context TO dev_team;
GRANT SELECT ON TABLE terminology.term_context TO PUBLIC;


--
-- Name: TABLE term_translation; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.term_translation FROM PUBLIC;
REVOKE ALL ON TABLE terminology.term_translation FROM dev_team;
GRANT ALL ON TABLE terminology.term_translation TO dev_team;
GRANT SELECT ON TABLE terminology.term_translation TO PUBLIC;


--
-- Name: TABLE terminology; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.terminology FROM PUBLIC;
REVOKE ALL ON TABLE terminology.terminology FROM dev_team;
GRANT ALL ON TABLE terminology.terminology TO dev_team;
GRANT SELECT ON TABLE terminology.terminology TO PUBLIC;


--
-- Name: TABLE terminology_context; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.terminology_context FROM PUBLIC;
REVOKE ALL ON TABLE terminology.terminology_context FROM dev_team;
GRANT ALL ON TABLE terminology.terminology_context TO dev_team;
GRANT SELECT ON TABLE terminology.terminology_context TO PUBLIC;


--
-- Name: TABLE terminology_translation; Type: ACL; Schema: terminology; Owner: -
--

REVOKE ALL ON TABLE terminology.terminology_translation FROM PUBLIC;
REVOKE ALL ON TABLE terminology.terminology_translation FROM dev_team;
GRANT ALL ON TABLE terminology.terminology_translation TO dev_team;
GRANT SELECT ON TABLE terminology.terminology_translation TO PUBLIC;


--
-- Name: TABLE belongs_to_group; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users.belongs_to_group FROM PUBLIC;
REVOKE ALL ON TABLE users.belongs_to_group FROM dev_team;
GRANT ALL ON TABLE users.belongs_to_group TO dev_team;
GRANT SELECT ON TABLE users.belongs_to_group TO PUBLIC;


--
-- Name: TABLE usergroup; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users.usergroup FROM PUBLIC;
REVOKE ALL ON TABLE users.usergroup FROM dev_team;
GRANT ALL ON TABLE users.usergroup TO dev_team;
GRANT SELECT ON TABLE users.usergroup TO PUBLIC;


--
-- Name: TABLE "user"; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users."user" FROM PUBLIC;
REVOKE ALL ON TABLE users."user" FROM dev_team;
GRANT ALL ON TABLE users."user" TO dev_team;
GRANT SELECT ON TABLE users."user" TO PUBLIC;


--
-- Name: TABLE usergroup_type; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users.usergroup_type FROM PUBLIC;
REVOKE ALL ON TABLE users.usergroup_type FROM dev_team;
GRANT ALL ON TABLE users.usergroup_type TO dev_team;
GRANT SELECT ON TABLE users.usergroup_type TO PUBLIC;


--
-- Name: TABLE usergroup_usergroup_type; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users.usergroup_usergroup_type FROM PUBLIC;
REVOKE ALL ON TABLE users.usergroup_usergroup_type FROM dev_team;
GRANT ALL ON TABLE users.usergroup_usergroup_type TO dev_team;
GRANT SELECT ON TABLE users.usergroup_usergroup_type TO PUBLIC;


--
-- Name: TABLE web_session; Type: ACL; Schema: users; Owner: -
--

REVOKE ALL ON TABLE users.web_session FROM PUBLIC;
REVOKE ALL ON TABLE users.web_session FROM dev_team;
GRANT ALL ON TABLE users.web_session TO dev_team;
GRANT SELECT ON TABLE users.web_session TO PUBLIC;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT SELECT ON TABLES  TO PUBLIC;


--
-- PostgreSQL database dump complete
--

