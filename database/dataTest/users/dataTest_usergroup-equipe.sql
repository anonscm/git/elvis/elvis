INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '501',
  'eqTest1',
  'Equipe Test composée de user3, user4 et user5',
  TRUE
);

INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '502',
  'eqTest2',
  'Equipe Test composée de user1 et user2',
  TRUE
);

INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '503',
  'eqTest3',
  'Equipe Test composée de user3, user6',
  TRUE
);

INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '504',
  'eqTest4',
  'Equipe Test composée de user7 et user8',
  TRUE
);
