INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'eqTest1'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'eqTest2'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'eqTest3'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'eqTest4'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'team'
  )
);
