INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'TEC'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'project'
  )
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'TRepBio'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'project'
  )
);
