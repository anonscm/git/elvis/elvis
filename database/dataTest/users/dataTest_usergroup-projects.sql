INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '511',
  'TEC',
  'Projet test avec échantillons communs',
  TRUE
);

INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '512',
  'TRepBio',
  'Projet test avec RepBio',
  TRUE
);
