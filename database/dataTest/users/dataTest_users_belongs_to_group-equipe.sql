INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user1'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest2'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user2'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest2'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user3'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest1'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user3'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user4'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest1'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user5'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest1'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user7'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest4'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user8'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest4'
  )
);
