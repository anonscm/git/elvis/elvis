INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user1'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TEC'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user2'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TEC'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user3'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TEC'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user4'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TEC'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user4'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TRepBio'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user5'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TRepBio'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TRepBio'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user7'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TRepBio'
  )
);
