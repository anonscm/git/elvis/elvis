INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  501,
  'Fanny',
  'Testeuse',
  'user1',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  502,
  'Pierre',
  'Testeur',
  'user2',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  503,
  'Claudie',
  'Testeuse',
  'user3',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  504,
  'Seb',
  'Testeur',
  'user4',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  505,
  'Cerise',
  'Testeuse',
  'user5',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  506,
  'Fred',
  'Testeur',
  'user6',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  507,
  'Charlie',
  'Testeuse',
  'user7',
  'pwd',
  TRUE
);

INSERT INTO users.'user'(
  id,
  firstname,
  lastname,
  login,
  password,
  active
)
VALUES (
  508,
  'Dave',
  'Testeur',
  'user8',
  'pwd',
  TRUE
);
