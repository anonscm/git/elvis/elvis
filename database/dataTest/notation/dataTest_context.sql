INSERT INTO notation.context(
    name,
    id_usergroup,
    comment
    )
  VALUES (
    'context de notation test pour eqTest1',
    501,
    'contexte de test'
  );

INSERT INTO notation.context_has_usergroup(
  id_context,
  id_usergroup
)
VALUES(
  ( SELECT id
    FROM notation.context
    WHERE name = 'context de notation test pour eqTest1'
  ),
  501
);
