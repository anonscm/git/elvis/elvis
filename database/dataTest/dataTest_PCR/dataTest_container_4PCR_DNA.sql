INSERT INTO sample.container (
  id,
  code,
  id_container_type,
  id_container_level,
  id_content_type,
  id_action
)
VALUES (
  502,
  'Col0',
  (SELECT id
    FROM sample.container_type
    WHERE container_type.type = 'Microtube 1.5 ml'
  ),
  (SELECT id
    FROM sample.container_level
    WHERE container_level.level = 'full'
  ),
  (SELECT id
    FROM sample.content_type
    WHERE content_type.type = 'DNA'
  ),
  500
);

INSERT INTO sample.container (
  id,
  code,
  id_container_type,
  id_container_level,
  id_content_type,
  id_action
)
VALUES (
  503,
  'bak1',
  (SELECT id
    FROM sample.container_type
    WHERE container_type.type = 'Microtube 1.5 ml'
  ),
  (SELECT id
    FROM sample.container_level
    WHERE container_level.level = 'full'
  ),
  (SELECT id
    FROM sample.content_type
    WHERE content_type.type = 'DNA'
  ),
  501
);

-- insert container_has_usergroup
INSERT INTO sample.container_has_usergroup (
  id_container,
  id_usergroup,
  writable
)
VALUES (
  502,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);

INSERT INTO sample.container_has_usergroup (
  id_container,
  id_usergroup,
  writable
)
VALUES (
  503,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);
