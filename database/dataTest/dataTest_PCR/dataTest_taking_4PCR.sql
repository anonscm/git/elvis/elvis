INSERT INTO sample.taking (
  id,
  id_plant_group,
  date,
  id_tissue_type,
  id_container
)
VALUES (
  500,
  500,
  '2019-01-15',
  (SELECT id
    FROM sample.tissue_type
    WHERE tissue_type.type = 'Whole plant'
  ),
  500
);

INSERT INTO sample.taking (
  id,
  id_plant_group,
  date,
  id_tissue_type,
  id_container
)
VALUES (
  501,
  501,
  '2019-01-15',
  (SELECT id
    FROM sample.tissue_type
    WHERE tissue_type.type = 'Whole plant'
  ),
  501
);

INSERT INTO sample.user_a_taking (
  id_user,
  id_taking
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  500
);

INSERT INTO sample.user_a_taking (
  id_user,
  id_taking
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  501
);
