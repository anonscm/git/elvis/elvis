-- insert tree
-- insert lot_has_user
-- insert lot_has_usergroup

-- insert tree
INSERT INTO plant.tree(
  id_lot,
  name,
  id_accession,
  id_lot_source,
  multiplication_date,
  death_date
)
VALUES (
  600,
  'Col0',
  506,
  507,
  '2019-01-05',
  '2019-01-28'
);

INSERT INTO plant.tree(
  id_lot,
  name,
  id_accession,
  id_lot_source,
  multiplication_date,
  death_date
)
VALUES (
  601,
  'bak1-4',
  505,
  506,
  '2019-01-05',
  '2019-01-28'
);

-- insert lot_has_user
INSERT INTO plant.lot_has_user (
  id_lot,
  id_user
)
VALUES (
  600,
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  )
);

INSERT INTO plant.lot_has_user (
  id_lot,
  id_user
)
VALUES (
  601,
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  )
);

-- insert lot_has_usergroup
INSERT INTO plant.lot_has_usergroup (
  id_lot,
  id_usergroup,
  write
)
VALUES (
  600,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);

INSERT INTO plant.lot_has_usergroup (
  id_lot,
  id_usergroup,
  write
)
VALUES (
  601,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);
