INSERT INTO sample.action (
  id,
  id_action_type,
  id_user,
  date
)
VALUES (
  500,
  (SELECT id
    FROM sample.action_type
    WHERE action_type.type = 'DNA extraction'
  ),
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  '2019-01-15'
);

INSERT INTO sample.action (
  id,
  id_action_type,
  id_user,
  date
)
VALUES (
  501,
  (SELECT id
    FROM sample.action_type
    WHERE action_type.type = 'DNA extraction'
  ),
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  '2019-01-15'
);

INSERT INTO sample.action_a_container_origin (
  id_action,
  id_container
)
VALUES (
  500,
  500
);

INSERT INTO sample.action_a_container_origin (
  id_action,
  id_container
)
VALUES (
  501,
  501
);
