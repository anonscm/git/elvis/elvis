INSERT INTO sample.action (
  id,
  id_action_type,
  id_user,
  date
)
VALUES (
  502,
  (SELECT id
    FROM sample.action_type
    WHERE action_type.type = 'PCR'
  ),
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  '2019-01-15'
);

INSERT INTO sample.action (
  id,
  id_action_type,
  id_user,
  date
)
VALUES (
  503,
  (SELECT id
    FROM sample.action_type
    WHERE action_type.type = 'PCR'
  ),
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  '2019-01-15'
);

INSERT INTO sample.action_a_container_origin (
  id_action,
  id_container
)
VALUES (
  502,
  502
);

INSERT INTO sample.action_a_container_origin (
  id_action,
  id_container
)
VALUES (
  503,
  503
);
