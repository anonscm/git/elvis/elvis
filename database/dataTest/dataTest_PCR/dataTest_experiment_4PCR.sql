-- PROJECT
-- insert experiment
--    experiment_has_axis
--    experiment_has_financing
--    experiment_has_status
--    experiment_has_experiment
-- USERGROUP
-- insert usergroup
--    usergroup_usergroup_type
--    belongs_to_group
--    experiment_has_usergroup 4 project
--    experiment_has_usergroup 4 team(s)'s user
-- PLANT
-- insert
--    experiment_has_group
--    experiment_has_clone
-- NOTATION
-- insert notation_texte
--    notation_has_user
--    notation_has_usergroup
--    experiment_has_notation
-- CONTAINER
-- insert
--    experiment_has_container

INSERT INTO experiment.experiment(
  id,
  name,
  description,
  creation_date,
  title,
  active,
  date_start,
  date_end,
  id_user,
  id_experiment_type
)
VALUES (
  502,
  'dataTest_PCR',
  'Projet simple d''une expérience sur deux lots de graines avec extraction d''ADN et PCR',
  NOW(),
  'PCR sur Col0 et bak1',
  'TRUE',
  '2019-01-05',
  '2019-01-28',
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  (SELECT id
    FROM experiment.experiment_type
    WHERE experiment_type.name = 'project'
  )
);

INSERT INTO experiment.experiment_has_axis (
  id_experiment,
  id_axis
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM experiment.axis
    WHERE axis.name = 'Axis 2 - Fruits & Vegetables'
  )
);

INSERT INTO experiment.experiment_has_financing (
  id_experiment,
  id_financing
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM experiment.financing
    WHERE financing.name = 'INRA-BAP'
  )
);

INSERT INTO experiment.experiment_has_status (
  id_experiment,
  id_status
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM experiment.status
    WHERE status.name = 'private'
  )
);

INSERT INTO experiment.experiment_has_experiment (
  id_experiment_child,
  id_experiment
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  )
)

-- USERGROUP
INSERT INTO users.usergroup(
  id,
  name,
  description,
  active
)
VALUES (
  '513',
  'dataTest_PCR',
  'Projet test PCR',
  TRUE
);

INSERT INTO users.usergroup_usergroup_type (
  usergroup_id,
  usergroup_type_id
)
VALUES (
  (SELECT id
    FROM users.usergroup
    WHERE users.usergroup.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM users.usergroup_type
    WHERE users.usergroup_type.name = 'project'
  )
);

INSERT INTO users.belongs_to_group (
  user_id,
  group_id
)
VALUES (
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user6'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'dataTest_PCR'
  )
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'dataTest_PCR'
  ),
  TRUE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'dataTest_PCR'
  ),
  (SELECT
    usergroup.id
  FROM
    users."user",
    users.usergroup,
    users.usergroup_type,
    users.usergroup_usergroup_type,
    users.belongs_to_group
  WHERE
    usergroup.id = belongs_to_group.group_id AND
    usergroup_usergroup_type.usergroup_id = usergroup.id AND
    usergroup_usergroup_type.usergroup_type_id = usergroup_type.id AND
    belongs_to_group.user_id = "user".id AND
    "user".id = 506 AND
    usergroup_type.name = 'team'
  ),
  FALSE
);

-- PLANT
INSERT INTO experiment.experiment_has_group (
  id_group,
  id_experiment
)
VALUES (
  500,
  502
);

INSERT INTO experiment.experiment_has_group (
  id_group,
  id_experiment
)
VALUES (
  501,
  502
);

INSERT INTO experiment.experiment_has_clone (
  id_experiment,
  id_clone -- id_accession
)
VALUES (
  502,
  505
);

INSERT INTO experiment.experiment_has_clone (
  id_experiment,
  id_clone -- id_accession
)
VALUES (
  502,
  506
);

-- NOTATIONS
INSERT INTO notation.notation_texte (
  id_notation,
  value,
  id_type_notation
)
VALUES (
  500,
  'Manip faite avec le stagiaire M.Martin',
  156 -- name = remarque
);

INSERT INTO notation.notation_has_user (
  id_user,
  id_notation
)
VALUES (
  506, -- login = user6
  500
);

INSERT INTO notation.notation_has_usergroup (
  id_usergroup,
  id_notation
)
VALUES (
  513,  -- name = dataTest_PCR
  500
);

INSERT INTO experiment.experiment_has_notation (
  id_experiment,
  id_notation
)
VALUES (
  502,
  500
);

-- CONTAINER
INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  500
);

INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  501
);

INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  502
);

INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  503
);

INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  504
);

INSERT INTO experiment.experiment_has_container (
  id_experiment,
  id_container
)
VALUES (
  502,
  505
);
