-- insert container
-- insert container_has_usergroup

-- insert container
INSERT INTO sample.container (
  id,
  code,
  id_container_type,
  id_container_level,
  id_content_type
)
VALUES (
  500,
  'Col0',
  (SELECT id
    FROM sample.container_type
    WHERE container_type.type = 'Microtube 1.5 ml'
  ),
  (SELECT id
    FROM sample.container_level
    WHERE container_level.level = 'empty'
  ),
  (SELECT id
    FROM sample.content_type
    WHERE content_type.type = 'fresh tissue'
  )
);

INSERT INTO sample.container (
  id,
  code,
  id_container_type,
  id_container_level,
  id_content_type
)
VALUES (
  501,
  'bak1',
  (SELECT id
    FROM sample.container_type
    WHERE container_type.type = 'Microtube 1.5 ml'
  ),
  (SELECT id
    FROM sample.container_level
    WHERE container_level.level = 'empty'
  ),
  (SELECT id
    FROM sample.content_type
    WHERE content_type.type = 'fresh tissue'
  )
);

-- insert container_has_usergroup
INSERT INTO sample.container_has_usergroup (
  id_container,
  id_usergroup,
  writable
)
VALUES (
  500,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);

INSERT INTO sample.container_has_usergroup (
  id_container,
  id_usergroup,
  writable
)
VALUES (
  501,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);
