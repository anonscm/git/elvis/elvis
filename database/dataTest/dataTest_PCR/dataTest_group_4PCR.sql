-- insert group
-- insert group_has_lot
-- insert group_has_usergroup

-- insert group
INSERT INTO plant.group (
  id_group,
  name
)
VALUES (
  500,
  'Col0 (bak1-4)'
);

INSERT INTO plant.group (
  id_group,
  name
)
VALUES (
  501,
  'bak1-4'
);

-- insert group_has_lot
INSERT INTO plant.group_has_lot (
  id_group,
  id_lot
)
VALUES (
  500,
  600
);

INSERT INTO plant.group_has_lot (
  id_group,
  id_lot
)
VALUES (
  501,
  601
);

-- insert group_has_usergroup
INSERT INTO plant.group_has_usergroup (
  id_group,
  id_usergroup,
  write
)
VALUES (
  500,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);

INSERT INTO plant.group_has_usergroup (
  id_group,
  id_usergroup,
  write
)
VALUES (
  501,
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE
);
