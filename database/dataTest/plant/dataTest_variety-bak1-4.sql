notation.notation_texte -- variety
-- variety_name
-- variety_has_notation

-- variety
INSERT INTO plant.variety (
  id_variete,
  id_taxonomy
)
VALUES (
  601,
  (SELECT id
    FROM plant.taxonomy
    WHERE taxonomy.genus = 'Arabidopsis'
    AND taxonomy.specie = 'thaliana'
  )
);

-- Fond génétique
INSERT INTO plant.variety_relation (
  id_parent,
  id_child,
  id_type
)
VALUES (
  (SELECT id_variety FROM plant.variety_name
    WHERE value = 'Col0'
    AND id_variety_name_type = (
      SELECT id_type_nom FROM plant.variety_name_type
      WHERE value = 'nom usuel'
    )
  ),
  601,
  (SELECT id
    FROM plant.variety_relation_type
    WHERE name = 'mutant'
  )
);

-- variety_name
INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  601,
  'bak1-4',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'nom usuel'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  601,
  'bak1-4',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Nom mutant'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  601,
  'SALK_116202',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'numéro SALK'
  )
);

-- Notation (gène muté)
INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'bak1-4',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Gène muté'
  )
);

-- variety_has_notation
INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  601,
  (SELECT id_notation
    FROM notation.notation_texte
    WHERE value = 'bak1-4' -- Gène muté
  )
);

-- INSERT INTO notation.variety_has_notation (
--   id_variete,
--   id_notation
-- )
-- VALUES (
--   601,
--   (SELECT id_notation
--     FROM notation.notation_list
--     WHERE
--       (SELECT code
--         FROM notation.notation_list_values
--         WHERE value = 'T-DNA') -- Agent mutagène
--   )
-- );

-- INSERT INTO notation.variety_has_notation (
--   id_variete,
--   id_notation
-- )
-- VALUES (
--   601,
--   (SELECT id_notation
--     FROM notation.notation_texte
--     WHERE value = 'Mutant' -- Génotype
--   )
-- );
