INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  500,
  500,
  ''
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  501,
  501,
  ''
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  502,
  502,
  ''
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  503,
  600,
  'issu de Versailles'
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  504,
  501,
  'WT associé à fir'
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  505,
  601,
  ''
);

INSERT INTO plant.accession (
  id,
  id_variety,
  comment
)
VALUES (
  506,
  500,
  'WT associé à bak1-4'
);
