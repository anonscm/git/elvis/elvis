INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  harvesting_date,
  quantity
)
VALUES (
  500,
  'Col0_A',
  500,
  '2018-06-01',
  'full'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  harvesting_date,
  quantity
)
VALUES (
  501,
  'Ws',
  501,
  '2018-06-01',
  'full'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  harvesting_date,
  quantity
)
VALUES (
  502,
  'Ler (sachet 3)',
  502,
  '2014-06-01',
  'few'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  harvesting_date,
  quantity
)
VALUES (
  503,
  'fir',
  503,
  '2007-01-01',
  'empty'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  id_lot_source,
  harvesting_date,
  quantity
)
VALUES (
  504,
  'proscoop12',
  503,
  503,
  '2018-06-01',
  'full'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  quantity
)
VALUES (
  505,
  'Ws (fir)',
  504,
  'few'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  harvesting_date,
  quantity
)
VALUES (
  506,
  'bak1-4',
  505,
  '2018-06-01',
  'full'
);

INSERT INTO plant.seed(
  id_lot,
  name,
  id_accession,
  quantity
)
VALUES (
  507,
  'Col0 (bak1-4)',
  506,
  'few'
);
