INSERT INTO plant.taxonomy (
  genus,
  specie,
  cultivated
)
VALUES (
  'Arabidopsis',
  'thaliana',
  FALSE
);

INSERT INTO plant.taxonomy (
  genus,
  specie,
  cultivated
)
VALUES (
  'Medicago',
  'truncatula',
  FALSE
);
