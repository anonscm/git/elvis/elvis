-- variety
-- variety_name
-- variety_has_notation

-- variety
INSERT INTO plant.variety (
  id_variete,
  id_taxonomy
)
VALUES (
  502,
  (SELECT id
    FROM plant.taxonomy
    WHERE taxonomy.genus = 'Arabidopsis'
    AND taxonomy.specie = 'thaliana'
  )
);

-- variety_name
INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Ler',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'nom usuel'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Landsberg erecta',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Ecotype'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Ler',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Ecotype'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Landsberg erecta',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Accession'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Ler',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Accession'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Landsberg erecta',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Lignée'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  502,
  'Ler',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Lignée'
  )
);

-- variety_has_notation
INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  502,
  (SELECT id_notation
    FROM notation.notation
    WHERE comment = 'Wild type'
  )
);

INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  502,
  (SELECT id_notation
    FROM notation.notation
    WHERE comment = 'Ler'
  )
);
