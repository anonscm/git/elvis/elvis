INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Génotype',
  'texte'
);

INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Fond génétique',
  'texte'
);

INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Gène muté',
  'texte'
);

INSERT INTO notation.notation_type (
  name,
  type
)
VALUES (
  'Agent mutagène',
  'texte'
);
