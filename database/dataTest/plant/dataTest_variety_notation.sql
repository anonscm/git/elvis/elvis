INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Wild type',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Génotype'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Mutant',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Génotype'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Col0',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Fond génétique'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Ler',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Fond génétique'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Ws',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Fond génétique'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'T-DNA',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'EMS',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Rayons UV',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Rayons X',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'Rayons gamma',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);

INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'CRISPR/Cas9',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Agent mutagène'
  )
);
