-- variety
-- variety_name
-- variety_has_notation

-- variety
INSERT INTO plant.variety (
  id_variete,
  id_taxonomy
)
VALUES (
  600,
  (SELECT id
    FROM plant.taxonomy
    WHERE taxonomy.genus = 'Arabidopsis'
    AND taxonomy.specie = 'thaliana'
  )
);

-- Fond génétique
INSERT INTO plant.variety_relation (
  id_parent,
  id_child,
  id_type
)
VALUES (
  (SELECT id_variety FROM plant.variety_name
    WHERE value = 'Ws'
    AND id_variety_name_type = (
      SELECT id_type_nom FROM plant.variety_name_type
      WHERE value = 'nom usuel'
    )
  ),
  600,
  (SELECT id
    FROM plant.variety_relation_type
    WHERE name = 'mutant'
  )
);

-- variety_name
INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  600,
  'proscoop12',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'nom usuel'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  600,
  'proscoop12',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Nom mutant'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  600,
  'fireman',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Nom mutant'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  600,
  'fir',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'Nom mutant'
  )
);

INSERT INTO plant.variety_name (
  id_variety,
  value,
  id_variety_name_type
)
VALUES (
  600,
  'FLAG_394H10',
  (SELECT id_type_nom
    FROM plant.variety_name_type
    WHERE variety_name_type.value = 'numéro versailles'
  )
);

-- Notation (gène muté)
INSERT INTO notation.notation_texte (
  value,
  id_type_notation
)
VALUES (
  'AT4G44585',
  (SELECT id_type_notation
    FROM notation.notation_type
    WHERE name = 'Gène muté'
  )
);

-- variety_has_notation
INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  600,
  (SELECT id_notation
    FROM notation.notation_texte
    WHERE value = 'AT4G44585' -- Gène muté
  )
);

INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  600,
  (SELECT id_notation
    FROM notation.notation
    WHERE comment = 'T-DNA' -- Agent mutagène
  )
);

INSERT INTO notation.variety_has_notation (
  id_variete,
  id_notation
)
VALUES (
  600,
  (SELECT id_notation
    FROM notation.notation
    WHERE comment = 'Mutant' -- Génotype
  )
);
