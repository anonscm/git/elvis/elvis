INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest1'
  ),
  TRUE,
  FALSE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest2'
  ),
  TRUE,
  FALSE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE,
  FALSE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest1'
  ),
  TRUE,
  FALSE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest3'
  ),
  TRUE,
  FALSE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'eqTest4'
  ),
  TRUE,
  FALSE
);
