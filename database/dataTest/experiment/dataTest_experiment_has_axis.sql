INSERT INTO experiment.experiment_has_axis (
  id_experiment,
  id_axis
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM experiment.axis
    WHERE axis.name = 'Axis 3 - Seeds'
  )
);

INSERT INTO experiment.experiment_has_axis (
  id_experiment,
  id_axis
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM experiment.axis
    WHERE axis.name = 'Axis 2 - Fruits & Vegetables'
  )
);
