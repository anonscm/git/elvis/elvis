INSERT INTO experiment.experiment(
  id,
  name,
  description,
  creation_date,
  title,
  active,
  date_start,
  date_end,
  id_user,
  id_experiment_type
)
VALUES (
  500,
  'TEC',
  'Extraction de molécule ARN + Prot puis expérience de microarray et expérience de protéomique',
  NOW(),
  'Projet de test avec échantillons communs pour deux expériences',
  'TRUE',
  '2017-10-01',
  '2019-10-01',
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user1'
  ),
  (SELECT id
    FROM experiment.experiment_type
    WHERE experiment_type.name = 'project'
  )
);

INSERT INTO experiment.experiment(
  id,
  name,
  description,
  creation_date,
  title,
  active,
  id_user,
  id_experiment_type
)
VALUES (
  501,
  'TRepBio',
  'Infection bactérienne puis prélèvement de feuille pour comptage batérien',
  NOW(),
  'Projet de test avec Répétition des noms d’échantillons',
  'TRUE',
  (SELECT id
    FROM users."user"
    WHERE "user".login = 'user4'
  ),
  (SELECT id
    FROM experiment.experiment_type
    WHERE experiment_type.name = 'project'
  )
);
