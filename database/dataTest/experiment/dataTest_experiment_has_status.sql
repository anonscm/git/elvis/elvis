INSERT INTO experiment.experiment_has_status (
  id_experiment,
  id_status
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM experiment.status
    WHERE status.name = 'private'
  )
);

INSERT INTO experiment.experiment_has_status (
  id_experiment,
  id_status
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM experiment.status
    WHERE status.name = 'private'
  )
);
