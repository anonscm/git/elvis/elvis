INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TEC'
  ),
  TRUE,
  TRUE
);

INSERT INTO experiment.experiment_has_usergroup (
  id_experiment,
  id_usergroup,
  readable,
  writable
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM users.usergroup
    WHERE usergroup.name = 'TRepBio'
  ),
  TRUE,
  TRUE
);
