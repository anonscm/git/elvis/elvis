INSERT INTO experiment.experiment_has_financing (
  id_experiment,
  id_financing
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TEC'
  ),
  (SELECT id
    FROM experiment.financing
    WHERE financing.name = 'UA'
  )
);

INSERT INTO experiment.experiment_has_financing (
  id_experiment,
  id_financing
)
VALUES (
  (SELECT id
    FROM experiment.experiment
    WHERE experiment.name = 'TRepBio'
  ),
  (SELECT id
    FROM experiment.financing
    WHERE financing.name = 'RFI'
  )
);
