INSERT INTO experiment.axis(
  name
)
VALUES (
  'Axis 1 - Roses & Ornamentals'
);

INSERT INTO experiment.axis(
  name
)
VALUES (
  'Axis 2 - Fruits & Vegetables'
);

INSERT INTO experiment.axis(
  name
)
VALUES (
  'Axis 3 - Seeds'
);
