-- variety
-- variety_name

-- variety
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (510, 453);
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (511, 453);
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (512, 453);
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (513, 453);
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (514, 453);
INSERT INTO plant.variety (id_variete, id_taxonomy)
  VALUES (515, 453);

-- variety_name
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (510, 'genBkg_WT1', 63);
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (511, 'genBkg_mut1', 63);
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (512, 'genBkg_mut2', 63);
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (513, 'genBkg_mut3', 63);
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (514, 'genBkg_mut12', 63);
INSERT INTO plant.variety_name (id_variety, value, id_variety_name_type)
  VALUES (515, 'genBkg_mut123', 63);

-- relation
INSERT INTO plant.variety_relation(id_parent, id_child, id_type)
  VALUES (510, 511, 1);
INSERT INTO plant.variety_relation(id_parent, id_child, id_type)
  VALUES (510, 512, 1);
INSERT INTO plant.variety_relation(id_parent, id_child, id_type)
  VALUES (510, 513, 1);
INSERT INTO plant.variety_relation(id_parent, id_child, id_type)
  VALUES (511, 514, 1);
INSERT INTO plant.variety_relation(id_parent, id_child, id_type)
  VALUES (514, 515, 1);

-- accession
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(511, 'genBkg_WT1acc1', 510);
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(512, 'genBkg_WT1acc2', 510);
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(513, 'genBkg_WT1acc3', 510);
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(514, 'genBkg_mut1acc1', 511);
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(515, 'genBkg_mut2acc1', 512);
INSERT INTO plant.accession(id, introduction_name, id_variety)
  VALUES(516, 'genBkg_mut12acc1', 514);

-- seed lot
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (511, 'genBkg_WT1acc1lot1', 511);
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (512, 'genBkg_WT1acc1lot2', 511);
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (513, 'genBkg_WT1acc2lot1', 512);
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (514, 'genBkg_mut1acc1lot1', 514);
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (515, 'genBkg_mut1acc1lot2', 514);
INSERT INTO plant.seed(id_lot, name, id_accession)
  VALUES (516, 'genBkg_mut2acc2lot1', 515);

-- lot has user (user1)
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (511, 501);
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (512, 501);
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (513, 501);
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (514, 501);
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (515, 501);
INSERT INTO plant.lot_has_user(id_lot, id_user)
  VALUES (516, 501);

-- lot has usergroup (eqTest2)
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (511, 502);
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (512, 502);
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (513, 502);
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (514, 502);
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (515, 502);
INSERT INTO plant.lot_has_usergroup(id_lot, id_usergroup)
  VALUES (516, 502);
