INSERT INTO sample.content_type (
  type
)
VALUES (
  'fresh tissue'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'DNA'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'RNA'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'cDNA'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'aRNA'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'Cy3 direct labelling'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'Cy5 direct labelling'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'Cy3 indirect labelling'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'Cy5 indirect labelling'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'protein extract'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'RNA and protein extract'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'powdered tissue'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'lipid extract'
);

INSERT INTO sample.content_type (
  type
)
VALUES (
  'PCR product'
);
