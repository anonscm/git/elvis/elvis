# -*- coding: utf-8 -*-

import sys
import random
import hashlib
import datetime
from types import *
from elvis.DbConnection import DbConnection
import psycopg2.extras

class SessionTools(DbConnection):
  """This class manage a session against the ELVIS database

  This class provides methods to manage a session and authenticate
  users and remote applications within ELVIS.
  """

  def __init__(self):
    super(SessionTools, self).__init__()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    # The pepper is a static value used to space login and password when
    # generating the credentials
    self.__pepper = "1a1d69845070285b386cb50dddfe2446"
    #sys.stderr.write("I'm the local copy of SessionTools\n")

  def debug(self, msg):
    """Pring a message in server logs

    msg: the message to print
    """
    sys.stderr.write(msg + "\n")

  def echo(self, t):
    return t

  def generateSalt(self):
    """Generate a salting value

    The salting value retruned is based on current datetime and a string
    of 16 hex values hashed with md5.

    :returns: the salting value
    :rtype: string
    """

    ts = str(datetime.datetime.now())
    tab = "0123456789abcdef"
    rdm = ""
    for i in range(15):
      rdm += tab[random.randint(0,15)]
    return hashlib.md5(str(ts + rdm).encode('utf-8')).hexdigest()

  def userAuthenticate(self, login, credential, salt):
    """Authenticates a user given login and credential

    :param login: the value of the login
    :param credential: the value of the credential as stored in the database (credential is sha512(password))
    :type login: string
    :type credential: string
    :returns: the user id
    :rtype: int
    """

    sql = "SELECT id, password FROM users.user WHERE login = %s"
    self.cur.execute(sql, [login])
    res = self.cur.fetchone()
    userId = None
    if res:
      pwsig = hashlib.sha512(str(salt + res["password"]).encode('utf-8')).hexdigest()
      if pwsig == credential:
        userId = res["id"]
    return userId

  def sessionValidate(self, sessionId, ip):
    """Validate a session

    Check if the sessionId match the IP address used to generate it.

    :param sessionId: the sessionId to check
    :param ip: the IP address to check
    :type sessionId: string
    :type ip: string
    :returns: True if sessionId match the IP, False if not
    :rtype: bool
    """

    sql = "SELECT COUNT(id) FROM users.web_session WHERE id = %s and ip = %s;"
    self.cur.execute(sql, [sessionId, ip])
    res = self.cur.fetchone()
    rep = False
    if res['count'] == 1:
      rep = True
    return rep

  def getSalt(self, sessionId):
    """Get the salt stored for that sessionId

    :param sessionId: the sessionId for which the salt value must be returned
    :type sessionId: string
    :returns: the salt value associated to the sessionId
    :rtype: string
    """

    sql = "SELECT salt FROM users.web_session WHERE id = %s"
    self.cur.execute(sql, [sessionId])
    salt = self.cur.fetchone()['salt']
    return salt

  def updateSalt(self, sessionId):
    """Generate a new salt for that sessionId

    Generate, store and return a new salt value for a given sessionId.

    :param sessionId: the sessionId for which the salt value must be generated
    :type sessionId: string
    :returns: the new salt value associated to the sessionId
    :rtype: string
    """

    sql = "UPDATE users.web_session SET salt = %s WHERE id = %s;"
    salt = self.generateSalt()
    self.cur.execute(sql, [salt, sessionId])
    self.conn.commit() # TODO : set exception handling
    return salt

  def sessionConnect(self, sessionId, ip):
    """Register a sessionId with an IP

    This is the first action needed to authenticate an application against
    ELVIS. The application needs to send its own sessionId in the form of a
    UUID and register it along with the IP of the computer on which the
    application runs.

    :param sessionId: the sessionId to register
    :param ip: the IP for which the sessionId must be registered
    :type sessionId: string
    :type ip: string
    :returns: a dictionary containing sessionId, IP, salt and user_id if a user
    is authenticated for that session or False if IP doesn't match sessionId
    :rtype: dict|bool
    """

    # Check if session exists
    sql = "SELECT id, ip, salt, user_id FROM users.web_session WHERE id = %s;"
    self.cur.execute(sql, [sessionId])
    res = self.cur.fetchone()
    # If session doesn't exist, create one
    if not res:
      sql = "INSERT INTO users.web_session (id, ip, salt) VALUES (%s, %s, %s) RETURNING id, ip, salt, user_id;"
      salt = self.generateSalt()
      self.cur.execute(sql, [sessionId, ip, salt])
      res = self.cur.fetchone()
      self.conn.commit()
    # If session exists but ip doesn't match
    if res['ip'] != ip:
      return False
    # Update timestamp for last_action
    sql = "UPDATE users.web_session SET last_action = now() where id = %s;"
    self.cur.execute(sql, [sessionId])
    self.conn.commit()
    res["pepper"] = self.__pepper
    return res

  def userLogout(self, sessionId):
    """Effectively logout a user

    This method logout a user from a sessionId.

    :param sessionId: the sessionId from which detach any user
    :type sessionId: string
    :returns: True if the session has no more user attached to it.
    :rtype: bool
    """

    sql = "UPDATE users.web_session SET user_id = NULL, last_action = now() WHERE id = %s;"
    self.cur.execute(sql, [sessionId])
    self.conn.commit() # TODO : set exception handling
    return True

  def userLogin(self, sessionId, login, credential):
    """Athenticate and attach a user to a sessionId

    :param login: the user login
    :param credential: the hashing string of the concatenation of password and
    the salt value
    :type login: string
    :type credential: string
    :returns: the user id if the authentication is OK or None if it fails
    :rtype: int, None
    """

    user_id = self.userAuthenticate(login, credential, self.getSalt(sessionId))
    if user_id:
      sql = "UPDATE users.web_session SET user_id = %s, last_action = now() WHERE id = %s;"
      self.cur.execute(sql, [user_id, sessionId]);
      self.conn.commit() # TODO : set exception handling
      return user_id
    return False

  def getUserInfos(self, sessionId):
    """Get information about the user linked to the sessionId

    :param sessionId: the sessionId for which the user info are requested
    :type sessionId: string
    :returns: a dictionary containing user id, first name, last name, login,
    e-mail and phone
    :rtype: dict, None
    """
    sql = "SELECT u.id, u.firstname, u.lastname, u.login, u.email, u.phone FROM users.web_session ws, users.user u WHERE ws.id = %s AND ws.user_id = u.id;"
    self.cur.execute(sql, [sessionId])
    user = None
    res = self.cur.fetchone() # TODO : what if no user attached to that sessionId
    if (res):
      user = res
    return user

  def setPasswordForUser(self, login, password):
    """Set the password associated with a login

    :param login: the login for which the password must be set
    :param password: the password to set (will be encrypted before storage)
    :type login: string
    :type password: string
    """
    sql = "UPDATE users.user SET password = %s WHERE login = %s;"
    pwsig = hashlib.sha512(login + self.__pepper + password).hexdigest()
    self.cur.execute(sql, [pwsig, login])
    self.conn.commit() # TODO : set exception handling

  def getSessionsOlderThan(self, sec):
    """Get sessions older than a number of seconds from now

    :param sec: the number of second from now from which searching sessions
    :type sec: integer
    :rtype: dict
    """
    # TODO: use that method to set a method to clean the web_session table
    sec = int(sec)
    sql = "SELECT * FROM users.web_session WHERE EXTRACT(EPOCH FROM NOW()) - EXTRACT(EPOCH FROM last_action) > %s"
    self.cur.execute(sql, [sec])
    res = self.cur.fetchall()
    return res

  def getGroupsInfos(self, sessionId, active = True):
    """Get information about the groups linked to the currently logged in user

    :param sessionId: the sessionId for which the user info are requested
    :param active: if True returns only active groups, if False returns all
                   groups
    :type sessionId: string
    :type active: boolean
    :rtype: dict
    """
    user = self.getUserInfos(sessionId)
    groups = None;
    if (user):
      userId = user["id"]
      sql = "SELECT ug.id, ug.name, ug.description, ug.active FROM users.usergroup ug, users.belongs_to_group bg WHERE ug.id = bg.group_id AND bg.user_id = %s"
      self.cur.execute(sql, [userId])
      groups = self.cur.fetchall()
    return groups

  def getGroupIds(self, sessionId, active = True):
    """Get the list of group ids linked to the currently logged in user

    :param sessionId: the sessionId for which the user info are requested
    :param active: if True returns only active groups, if False returns all
                   groups
    :type sessionId: string
    :type active: boolean
    :rtype: array
    """
    groups = self.getGroupsInfos(sessionId, active)
    groupIds = []
    if groups:
      for g in groups:
        groupIds.append(g['id'])
    return groupIds

  def getFilteredGroupIds(self, sessionId, groupIds):
    """Check the list of sumited groupIds against the list groups for the current user

    Filter a list of groupIds and return only valid groupIds for the user.

    :param sessionId: the sessionId for which the user info are requested
    :param groupIds: the list to check
    :return: a array of validated groupIds
    :type sessionId: string
    :type groupIds: array of int
    :rtype: array of int
    """
    groups = self.getGroupIds(sessionId, True)
    nGroupIds = []
    for g in groupIds:
      if g in groups:
        nGroupIds.append(g)
    return nGroupIds
