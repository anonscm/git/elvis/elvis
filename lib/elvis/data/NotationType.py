# -*- coding: utf-8 -*-

class NotationType():
  def __init__(self, id = None, name = None, type = None):
    self._id = id
    self._name = name
    self._type = type

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getName(self):
    return self._name
  def setName(self, name):
    self._name = name
    return self

  def getType(self):
    return self._type
  def setType(self, type):
    self._type = type
    return self
