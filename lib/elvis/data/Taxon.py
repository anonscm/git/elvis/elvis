# -*- coding: utf-8 -*-

class Taxon():
  def __init__(self, id = None, genus = None, specie = None, type = None, idType= None):
    self._id = id
    self._genus = genus
    self._specie = specie
    self._type = type
    self._idType = idType

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getGenus(self):
    return self._genus
  def setGenus(self, genus):
    self._genus = genus
    return self

  def getSpecie(self):
    return self._specie
  def setSpecie(self, specie):
    self._specie = specie
    return self

  def getType(self):
    return self._type
  def setType(self, type):
    self._type = type
    return self

  def getIdType(self):
    return self._idType
  def setIdType(self, idType):
    self._idType = idType
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['type'] = self.getType()
    dict['idType'] = self.getIdType()
    dict['genus'] = self.getGenus()
    dict['specie'] = self.getSpecie()
    return dict
