# -*- coding: utf-8 -*-

class NotationContext():
  def __init__(self, id = None, name = None, comment = None, userGroupList = [], typeNotList = []):
    self._id = id
    self._name = name
    self._comment = comment
    self._userGroupList = userGroupList
    self._typeNotList = typeNotList

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getName(self):
    return self._name
  def setName(self, name):
    self._name = name
    return self

  def getComment(self):
    return self._comment
  def setComment(self, comment):
    self._comment = comment
    return self

  def getUserGroupList(self):
    return self._userGroupList
  def setUserGroupList(self, userGroupList):
    self._userGroupList = userGroupList
    return self

  def getTypeNotList(self):
    return self._typeNotList
  def setTypeNotList(self, typeNotList):
    self._typeNotList = typeNotList
    return self

  def toDict (self):
    dict = {}
    dict['id'] = self.getId()
    dict['name'] = self.getName()
    dict['comment'] = self.getComment()
    dict['userGroupList'] = self.getUserGroupList()
    dict['typeNotList'] = self.getTypeNotList()
    return dict
