# -*- coding: utf-8 -*-

class Project():
  def __init__(self, id = None, name = None, parentProjectId = None, title = None, description = None, dateStart = None, dateEnd = None, nameStatus = None, axis = None, financing = None):
    self._id = id
    self._name = name
    self._parentProjectId = parentProjectId
    self._title = title
    self._description = description
    self._dateStart = dateStart
    self._dateEnd = dateEnd
    self._nameStatus = nameStatus
    self._axis = axis
    self._financing = financing

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getName(self):
    return self._name
  def setName(self, name):
    self._name = name
    return self

  def getParentProjectId(self):
    return self._parentProjectId
  def setParentProjectId(self, parentProjectId):
    self._parentProjectId = parentProjectId
    return self

  def getTitle(self):
    return self._title
  def setTitle(self, title):
    self._title = title
    return self

  def getDescription(self):
    return self._description
  def setParentProjectId(self, description):
    self._description = description
    return self

  def getDateStart(self):
    return self._dateStart
  def setDateStart(self, dateStart):
    self._dateStart = dateStart
    return self

  def getDateEnd(self):
    return self._dateEnd
  def setDateEnd(self, dateEnd):
    self._dateEnd = dateEnd
    return self

  def getNameStatus(self):
    return self._nameStatus
  def setNameStatus(self, nameStatus):
    self._nameStatus = nameStatus
    return self

  def getAxis(self):
    return self._axis
  def setAxis(self, axis):
    self._axis = axis
    return self

  def getFinancing(self):
    return self._financing
  def setFinancing(self, financing):
    self._financing = financing
    return self

  def toJson(self):
    return {"__class__" : "Project",
    "id": self._id,
    "name": self._name,
    "parentProjectId": self._parentProjectId,
    "title": self._title,
    "description": self._description,
    "dateStart": self._dateStart,
    "dateEnd": self._dateEnd,
    "nameStatus": self._nameStatus,
    "axis": self._axis,
    "financing": self._financing}