# -*- coding: utf-8 -*-

# modification juin 2019 FD
# modification des noms de variables pour aligner
# avec les variables utilisées dans la classe en javascript

class VarietyName():
  def __init__(self,
    id = None,
    value = None,
    date = None,
    type = None,
    varietyNameTypeId = None,
    varietyId = None
    ):
    self._id = id
    self._value = value
    self._type = type
    self._varietyNameTypeId = varietyNameTypeId
    self._date = date
    self._varietyId = varietyId

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getValue(self):
    return self._value
  def setValue(self, value):
    self._value = value
    return self

  def getType(self):
    return self._type
  def setType(self, type):
    self._type = type
    return self

  def getVarietyNameTypeId(self):
    return self._varietyNameTypeId
  def setVarietyNameTypeId(self, varietyNameTypeId):
    self._varietyNameTypeId = varietyNameTypeId
    return self

  def getDate(self):
    return self._date
  def setDate(self, date):
    self._date = date
    return self

  def getVarietyId(self):
    return self._varietyId
  def setVarietyId(self, varietyId):
    self._varietyId = varietyId
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['value'] = self.getValue()
    dict['date'] = self.getDate()
    dict['type'] = self.getType()
    dict['varietyNameTypeId'] = self.getVarietyNameTypeId()
    dict['varietyId'] = self.getVarietyId()
    return dict
