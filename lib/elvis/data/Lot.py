# -*- coding: utf-8 -*-

class Lot():
  def __init__(self,
    id = None,
    type = None,
    name = None,
    accessionId = None,
    lotSourceId = None,
    multiplicationDate = None,
    harvestingDate = None,
    creationDate = None,
    destructionDate = None,
    quantity = None,
    place = None
    ):
    self._id = id
    self._type = type
    self._name = name
    self._accessionId = accessionId
    self._lotSourceId = lotSourceId
    self._multiplicationDate = multiplicationDate
    self._harvestingDate = harvestingDate
    self._creationDate = creationDate
    self._destructionDate = destructionDate
    self._quantity = quantity
    self._place = place

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getType(self):
    return self._type
  def setType(self, type):
    self._type = type
    return self

  def getName(self):
    return self._name
  def setName(self, name):
    self._name = name
    return self

  def getAccessionId(self):
    return self._accessionId
  def setAccessionId(self, accessionId):
    self._accessionId = accessionId
    return self

  def getLotSourceId(self):
    return self._lotSourceId
  def setLotSourceId(self, lotSourceId):
    self._lotSourceId = lotSourceId
    return self

  def getMultiplicationDate(self):
    return self._multiplicationDate
  def setMultiplicationDate(self, multiplicationDate):
    self._multiplicationDate = multiplicationDate
    return self

  def getHarvestingDate(self):
    return self._harvestingDate
  def setHarvestingDate(self, harvestingDate):
    self._harvestingDate = harvestingDate
    return self

  def getCreationDate(self):
    return self._creationDate
  def setCreationDate(self, creationDate):
    self._creationDate = creationDate
    return self

  def getDestructionDate(self):
    return self._destructionDate
  def setDestructionDate(self, destructionDate):
    self._destructionDate = destructionDate
    return self

  def getQuantity(self):
    return self._quantity
  def setQuantity(self, quantity):
    self._quantity = quantity
    return self

  def getPlace(self):
    return self._place
  def setPlace(self, place):
    self._place = place
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['type'] = self.getType()
    dict['name'] = self.getName()
    dict['accessionId'] = self.getAccessionId()
    dict['lotSourceId'] = self.getLotSourceId()
    dict['multiplicationDate'] = self.getMultiplicationDate()
    dict['harvestingDate'] = self.getHarvestingDate()
    dict['creationDate'] = self.getCreationDate()
    dict['destructionDate'] = self.getDestructionDate()
    dict['quantity'] = self.getQuantity()
    dict['place'] = self.getPlace()
    return dict
