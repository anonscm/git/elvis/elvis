# -*- coding: utf-8 -*-

class Variety():
  def __init__(self,
      id = None,
      breeder = None,
      breederId = None,
      comment = None,
      taxonomyId = None,
      taxonType = None,
      genus = None,
      specie = None,
      editor = None,
      editorId = None,
      varietyNameList = [],
      accessionList = []
      ):

    self._id = id
    self._breeder = breeder
    self._breederId = breederId
    self._comment = comment
    self._genus = genus
    self._taxonomyId = taxonomyId
    self._taxonType = taxonType
    self._specie = specie
    self._editor = editor
    self._editorId = editorId
    self._varietyNameList = varietyNameList
    self._accessionList = accessionList

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getBreeder(self):
    return self._breeder
  def setBreeder(self, breeder):
    self._breeder = breeder
    return self

  def getBreederId(self):
    return self._breederId
  def setBreederId(self, breederId):
    self._breederId = breederId
    return self

  def getComment(self):
    return self._comment
  def setComment(self, comment):
    self._comment = comment
    return self

  def getTaxonomyId(self):
    return self._taxonomyId
  def setTaxonomyId(self, taxonomyId):
    self._taxonomyId = taxonomyId
    return self

  def getTaxonType(self):
    return self._taxonType
  def setTaxonType(self, taxonType):
    self._taxonType = taxonType
    return self

  def getGenus(self):
    return self._genus
  def setGenus(self, genus):
    self._genus = genus
    return self

  def getSpecie(self):
    return self._specie
  def setSpecie(self, specie):
    self._specie = specie
    return self

  def getEditor(self):
    return self._editor
  def setEditor(self, editor):
    self._editor = editor
    return self

  def getEditorId(self):
    return self._editorId
  def setEditorId(self, editorId):
    self._editorId = editorId
    return self

  def getVarietyNameList(self):
    return self._varietyNameList
  def setVarietyNameList(self, varietyNameList):
    self._varietyNameList = varietyNameList
    return self

  def getAccessionList(self):
    return self._accessionList
  def setAccessionList(self, accessionList):
    self._accessionList = accessionList
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['breeder'] = self.getBreeder()
    dict['breederId'] = self.getBreederId()
    dict['comment'] = self.getComment()
    dict['taxonomyId'] = self.getTaxonomyId()
    dict['taxonType'] = self.getTaxonType()
    dict['genus'] = self.getGenus()
    dict['specie'] = self.getSpecie()
    dict['editor'] = self.getEditor()
    dict['editorId'] = self.getEditorId()
    dict['varietyNameList'] = []
    for vn in self.getVarietyNameList():
      dict['varietyNameList'].append(vn.toDict())
    dict['accessionList'] = []
    for acc in self.getAccessionList():
      dict['accessionList'].append(acc.toDict())
    return dict
