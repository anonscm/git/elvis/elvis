# -*- coding: utf-8 -*-

class Address():
  def __init__(self,
    id = None,
    firstname = None,
    lastname = None,
    address1 = None,
    address2 = None,
    city = None,
    state = None,
    postalCode = None,
    idCountry = None,
    country = None,
    email = None,
    phone = None
    ):
    self._id = id
    self._firstname = firstname
    self._lastname = lastname
    self._address1 = address1
    self._address2 = address2
    self._city = city
    self._state = state
    self._postalCode = postalCode
    self._idCountry = idCountry
    self._country = country
    self._email = email
    self._phone = phone

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getFirstname(self):
    return self._firstname
  def setFirstname(self, firstname):
    self._firstname = firstname
    return self

  def getLastname(self):
    return self._lastname
  def setLastame(self, lastname):
    self._lastname = lastname
    return self

  def getAddress1(self):
    return self._address1
  def setAddress1(self, address1):
    self._address1 = address1
    return self

  def getAddress2(self):
    return self._address2
  def setAddress2(self, address2):
    self._address2 = address2
    return self

  def getCity(self):
    return self._city
  def setCity(self, city):
    self._city = city
    return self

  def getState(self):
    return self._state
  def setState(self, state):
    self._state = state
    return self

  def getPostalCode(self):
    return self._postalCode
  def setPostalCode(self, postalCode):
    self._postalCode = postalCode
    return self

  def getIdCountry(self):
    return self._idCountry
  def setIdCountry(self, idCountry):
    self._idCountry = idCountry
    return self

  def getCountry(self):
    return self._country
  def setCountry(self, country):
    self._country = country
    return self

  def getEmail(self):
    return self._email
  def setEmail(self, email):
    self._email = email
    return self

  def getPhone(self):
    return self._phone
  def setPhone(self, phone):
    self._phone = phone
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['fistname'] = self.getFirstname()
    dict['lastname'] = self.getLastname()
    dict['address1'] = self.getAddress1()
    dict['address2'] = self.getAddress2()
    dict['city'] = self.getCity()
    dict['state'] = self.getState()
    dict['postalCode'] = self.getPostalCode()
    dict['idCountry'] = self.getIdCountry()
    dict['country'] = self.getCountry()
    dict['email'] = self.getEmail()
    dict['phone'] = self.getPhone()
    return dict
