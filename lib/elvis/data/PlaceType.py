# -*- coding: utf-8 -*-

class PlaceType():
  def __init__(self, id = None, name = None):
    self._id = id
    self._name = name

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getName(self):
    return self._name
  def setName(self, name):
    self._name = name
    return self
