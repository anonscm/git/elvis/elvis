# -*- coding: utf-8 -*-
from elvis.data.NotationType import NotationType

class ExtendedNotationType(NotationType):
  def __init__(self, id = None, name = None, type = None, constraint = None, list = []):
    super().__init__(id, name, type)
    self._constraint = constraint
    self._list = list

  def getList(self):
    return self._list
  def setList(self, list):
    self._list = list
    return self

  def getConstraint(self):
    return self._constraint
  def setConstraint(self, constraint):
    self._constraint = constraint
    return self

  def toDict (self):
    dict = {}
    dict['id'] = self.getId()
    dict['name'] = self.getName()
    dict['type'] = self.getType()
    dict['constraint'] = self.getConstraint()
    dict['list'] = self.getList()
    return dict
