# -*- coding: utf-8 -*-

class Accession():
  def __init__(self,
    id = None,
    introductionName = None,
    varietyId = None,
    comment = None,
    introductionCloneId = None,
    provider = None,
    providerId = None,
    introductionDate = None,
    collectionDate = None,
    collectionSiteId = None,
    lotList = []
    ):
    self._id = id
    self._introductionName = introductionName
    self._varietyId = varietyId
    self._comment = comment
    self._introductionCloneId = introductionCloneId
    self._provider = provider
    self._providerId = providerId
    self._introductionDate = introductionDate
    self._collectionDate = collectionDate
    self._collectionSiteId = collectionSiteId
    self._lotList = lotList

  def getId(self):
    return self._id
  def setId(self, id):
    self._id = id
    return self

  def getIntroductionName(self):
    return self._introductionName
  def setIntroductionName(self, introductionName):
    self._introductionName = introductionName
    return self

  def getVarietyId(self):
    return self._varietyId
  def setVarietyId(self, varietyId):
    self._varietyId = varietyId
    return self

  def getComment(self):
    return self._comment
  def setComment(self, comment):
    self._comment = comment
    return self

  def getIntroductionCloneId(self):
    return self._introductionCloneId
  def setIntroductionCloneId(self, introductionCloneId):
    self._introductionCloneId = introductionCloneId
    return self

  def getProvider(self):
    return self._provider
  def setProvider(self, provider):
    self._provider = provider
    return self

  def getProviderId(self):
    return self._providerId
  def setProviderId(self, providerId):
    self._providerId = providerId
    return self

  def getIntroductionDate(self):
    return self._introductionDate
  def setIntroductionDate(self, introductionDate):
    self._introductionDate = introductionDate
    return self

  def getCollectionDate(self):
    return self._collectionDate
  def setCollectionDate(self, collectionDate):
    self._collectionDate = collectionDate
    return self

  def getCollectionSiteId(self):
    return self._collectionSiteId
  def setCollectionSiteId(self, collectionSiteId):
    self._collectionSiteId = collectionSiteId
    return self

  def getLotList(self):
    return self._lotList
  def setLotList(self, lotList):
    self._lotList = lotList
    return self

  def toDict(self):
    dict = {}
    dict['id'] = self.getId()
    dict['introductionName'] = self.getIntroductionName()
    dict['varietyId'] = self.getVarietyId()
    dict['comment'] = self.getComment()
    dict['introductionCloneId'] = self.getIntroductionCloneId()
    dict['provider'] = self.getProvider()
    dict['providerId'] = self.getProviderId()
    dict['introductionDate'] = self.getIntroductionDate()
    dict['collectionDate'] = self.getCollectionDate()
    dict['collectionSiteId'] = self.getCollectionSiteId()
    dict['lotList'] = []
    for lot in self.getLotList():
      dict['lotList'].append(lot.toDict())
    return dict
