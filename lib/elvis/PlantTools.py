# -*- coding: utf-8 -*-

from elvis.DbConnection import DbConnection
import psycopg2.extras
from elvis.SessionTools import SessionTools
import sys
import json

import elvis.data.Accession
import elvis.data.Lot
import elvis.data.PlaceType
import elvis.data.Variety
import elvis.data.VarietyName
import elvis.data.Taxon

#    sys.stderr.write("test config: " + str(name)+" "+str(groupsIds)+ "\n")
class PlantTools(DbConnection):
  def __init__(self):
    super(PlantTools, self).__init__()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.__sessionTools = SessionTools()
    self.__lotTypes = ['seed', 'tree']
    self.__textualNotationTypes = ['texte', 'document']
    self.__orderedNotationTypes = ['integer', 'double', 'date']
    self.__notationTypes = self.__textualNotationTypes + self.__orderedNotationTypes

  def searchLotIdByVarietyName(self, groupsIds, name):
    """Search lots given a variety name

    :param groupsIds: the Ids of the user groups to use as filter
    :param name: the variety name to search. This can search for partial
                 information using the '*' character
    :return: a list of integer (lot ids)

    :type groupsIds: list of integer
    :type name: string
    :rtype: list
    """
    name = name.replace('=', '==').replace('%', '=%').replace('_', '=_')
    name = name.replace('*', '%')
    res = []
    for t in self.__lotTypes:
      req = """
        SELECT
          DISTINCT a.id_lot
        FROM
          plant.{0} a,
          plant.accession c,
          plant.variety v,
          plant.variety_name nv,
          plant.lot_has_usergroup ug
        WHERE
          nv.value ILIKE %s ESCAPE '='
          AND nv.id_variety = v.id_variete
          AND c.id_variety = v.id_variete
          AND a.id_accession = c.id
          AND a.id_lot = ug.id_lot
          AND ug.id_usergroup = ANY (%s);
        """
      self.cur.execute(req.format(t), [name, groupsIds])
      res += [d['id_lot'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchLotIdByVarietyNameNear(self, groupsIds, name):
    """Search lots given a variety name

    :param groupsIds: the Ids of the user groups to use as filter
    :param name: the variety name to search by method of levenstein
    :return: a list of integer (lot ids)

    :type groupsIds: list of integer
    :type name: string
    :rtype: list

    """
    name = name.replace('=', '==').replace('%', '=%').replace('_', '=_')
    name = name.replace('*', '%')
    res = []
    for t in self.__lotTypes:
      req = """
        SELECT
          DISTINCT a.id_lot
        FROM
          plant.{0} a,
          plant.accession c,
          plant.variety v,
          plant.variety_name nv,
          plant.lot_has_usergroup ug
        WHERE
          (
            (levenshtein(NV.value, %s) < length(NV.value) / 2
              AND length(%s) <= length(NV.value))
            OR (levenshtein(NV.value, %s) < length(%s) / 2
              AND length(%s)>length(NV.value))
          )
          AND nv.id_variety = v.id_variete
          AND c.id_variety = v.id_variete
          AND a.id_accession = c.id
          AND a.id_lot = ug.id_lot
          AND ug.id_usergroup = ANY (%s);
        """
      self.cur.execute(
        req.format(t),
        [name, name, name, name, name, groupsIds]
        )
      res += [d['id_lot'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchLotIdByAccessionName(self, groupsIds, name):
    """Search lots given an accession name

    :param groupsIds: the Ids of the user groups to use as filter
    :param name: the accession name to search. This can search for partial
                 information using the '*' character
    :return: a list of integer (lot ids)

    :type groupsIds: list of integer
    :type name: string
    :rtype: list
    """
    name = name.replace('=', '==').replace('%', '=%').replace('_', '=_')
    name = name.replace('*', '%')
    res = []
    for t in self.__lotTypes:
      req = """
        SELECT
          DISTINCT a.id_lot
        FROM
          plant.{0} a,
          plant.accession c,
          plant.lot_has_usergroup ug
        WHERE
          c.introduction_name ILIKE %s ESCAPE '='
          AND a.id_accession = c.id
          AND a.id_lot = ug.id_lot
          AND ug.id_usergroup = ANY (%s);
        """
      self.cur.execute(req.format(t), [name, groupsIds])
      res += [d['id_lot'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchLotIdByLotName(self, groupsIds, name):
    """Search lots given a lot name

    :param groupsIds: the Ids of the user groups to use as filter
    :param name: the lot name to search. This can search for partial
                 information using the '*' character
    :return: a list of integer (lot ids)
    :type groupsIds: list of integer
    :type name: string
    :rtype: list
    """
    reqPart = "= %s"
    if (name.find("*") > -1):
      name = name.replace('=', '==').replace('%', '=%').replace('_', '=_')
      name = name.replace('*', '%')
      reqPart = "ILIKE %s ESCAPE '='"
    res = []
    for t in self.__lotTypes:
      req = """
        SELECT
          a.id_lot
        FROM
          plant.{0} a,
          plant.lot_has_usergroup ug
        WHERE
          ug.id_usergroup = ANY (%s)
          AND a.id_lot = ug.id_lot
          AND a.name {1}
        """
      self.cur.execute(req.format(t, reqPart), [groupsIds, name])
      res += [d['id_lot'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchLotIdByNotationValue(self, groupsIds, notationValues, campaign = None):
    """Search lots given notations

    The search is done on a set of given notation types with filter on values.
    The relation between filter is AND. The result is the list of trees that
    match all critreria of notation.
    By default the comparison is an equality.
    On int, double and date, one can prefix the value by >, <, >= or <=.
    On text, one can use * to match any number of character in the string. The
    comparison is case insensitive.
    ex: [
          {'id_notation_type': 134, 'value_notation': '<9'},
          {'id_notation_type': 131, 'value_notation': '2014'},
          {'id_notation_type': 164, 'value_notation': '*wil*'}
        ]
    Will match any tree having a notation of type 134 with a value less than 9
    and a notation of type 131 with a value equal to 2014 and a notation of
    type 164 with a value containg 'wil'.

    :param groupsIds: the Ids of the user groups to use as filter
    :param notationValues: the notations used to filter the results
    :param campaign: the campaign name on which the notation filter has to be
    done
    :return: a list of lot ids

    :type groupsIds: list of int
    :type notationValues: list of dict with 2 keys:
      id_notation_type (integer) and value_notation
    :type campaign: string
    :rtype: list of int
    """
    #rep = set()
    rep = []

    campFromPart = ""
    campWherePart = ""

    # Campaign aka Experiment
    experimentId = None
    if campaign != None:
      # campaign = campaign.strip()
      campaign = str(campaign)
      sql_exp = """
        SELECT
          e.id AS id
        FROM
          experiment.experiment e,
          experiment.experiment_has_usergroup ehu
        WHERE
          ehu.id_usergroup = ANY (%s)
          AND ehu.id_experiment = e.id
          AND e.name = %s
        """
      self.cur.execute(sql_exp, [groupsIds, campaign])
      expResp = self.cur.fetchone()
      if expResp:
        experimentId = expResp['id']
        campFromPart = """,
          experiment.experiment_has_notation ehn
        """
        campWherePart = """
          AND ehn.id_experiment = '{0}'
          AND ehn.id_notation = n.id_notation
        """
        campWherePart = campWherePart.format(experimentId)

    sql_type = """
      SELECT
        type AS type
      FROM
        notation.notation_type
      WHERE
        id_type_notation = %s
      """
    ids = {}
    for d in notationValues:
      self.cur.execute(sql_type, [d['id_notation_type']])
      nType = self.cur.fetchone()['type']
      nValue = d['value_notation'].strip()
      compOp = '='
      escape = ''
      if (nType in self.__orderedNotationTypes):
        if (nValue[0] == '>' or nValue[0] == '<'):
          nValue, compOp = nValue[1:], nValue[0]
          if (nValue[0] == '='):
            compOp = compOp + nValue[0]
            nValue = nValue[1:]
      if (nType in self.__textualNotationTypes):
        if (nValue.count("*")):
          compOp = 'ILIKE'
          escape = " ESCAPE '='"
          nValue = nValue.replace('=', '==').replace('%', '=%')
          nValue = nValue.replace('_', '=_').replace('*', '%')

      if experimentId != None:
        pass
      sql = """
        SELECT
          DISTINCT(lan.id_lot) AS id
        FROM
          notation.notation_{0} n,
          notation.lot_has_notation lan{3}
        WHERE
          n.value {1} %s{2}
          AND n.id_type_notation = %s
          AND n.id_notation = lan.id_notation{4}
        """
      self.cur.execute(
        sql.format(nType, compOp, escape, campFromPart, campWherePart),
        [nValue, d['id_notation_type']]
        )
      tmpIds = [v['id'] for v in self.cur.fetchall()]
      for d in tmpIds:
        if d in ids:
          ids[d] = ids[d] + 1
        else:
          ids[d] = 1
    nn = len(notationValues)
    for k, v in ids.items():
      if v == nn:
        rep.append(k)
    return rep

  def getLocationType(self, id = None, name = None):
    if not id and not name:
      raise Exception("Need at least an id or a name")
    lt = None
    req = """
      SELECT
        id,
        value
      FROM
        location.place_type
      WHERE
    """
    if id:
      req += "id = %s"
      self.cur.execute(req, [id])
    elif name:
      req += "value = %s"
      self.cur.execute(req, [name])
    else:
      raise Exception("Should not be here!!!")
    rep = self.cur.fetchone()
    lt = elvis.data.PlaceType(rep['id'], rep['value'])
    return lt

  def searchLotIdByPlaceValue(self, groupsIds, placeValues):
    """Search lots given places

    :param groupsIds: the Ids of the user groups to use as filter
    :param placeValues: the notations used to filter the results
    :return: a list of lot ids
    :type groupsIds: list of int
    :type placeValues: dict with 2 keys: id_place_type (integer) and value_place
    :rtype: list of int
    """
    res = []
    req = """
      SELECT
        P.id_lot
      FROM
        plant.place P,
        Location.place LP,
        location.place_type PT,
        plant.lot_has_usergroup LAU
      WHERE
        LP.id_plant_place = P.id
        AND PT.id=LP.id_place_type
        AND LAU.id_lot = P.id_lot
        AND LAU.id_usergroup = ANY (%s)
        AND PT.id = %s
        AND LP.value= %s
      ORDER BY
        LP.value
      """
    self.cur.execute(
      req,
      [groupsIds, placeValues['id_place_type'], placeValues['value_place']]
      )
    res += [d['id_lot'] for d in self.cur.fetchall()]
    return res

  def getLotIdsWithoutNotationType(self, groupsIds, notationType):
    """
    Doit renvoyer la liste des identifiants de lots accessible à l'utilisateur,
    ne possédant aucune notation pour le type donné.
    """
    sql = """
      SELECT
        tn.id_type_notation AS id,
        tn.name AS name,
        tn.type AS type
      FROM
        notation.notation_type tn
      WHERE
        tn.id_type_notation = %s
      """
    self.cur.execute(sql, [notationType])
    n = self.cur.fetchone()
    sql = """
      SELECT
        a.id_lot AS id
      FROM
        plant.tree a,
        plant.lot_has_usergroup lhu
      WHERE
        lhu.id_lot = a.id_lot
        AND lhu.id_usergroup = ANY (%s)
        AND a.id_lot NOT IN (
          SELECT
            lan.id_lot
          FROM
            notation.lot_has_notation lan,
            notation.notation_{0} n
          WHERE
            lan.id_notation = n.id_notation
            AND n.id_type_notation = %s
          )
      """
    rep = []
    if (n):
      self.cur.execute(sql.format(n['type']), [groupsIds, notationType])
      rep = rep + [v['id'] for v in self.cur.fetchall()]
    return rep

  def getNamesForIdLot(self, idLot):
    """
    Get names: lot + accession + nom usuel for a list of lot.id
    :param idLot: list of the Ids of the lots
    :return: a list of dictionary with keys:
      lot_name, introduction_name, variety_name
    """
    sql = """
      SELECT
        L.id_lot AS id,
        L.name AS arbre,
        L.id_accession AS id_accesssion,
        '' AS date,
        A.introduction_name AS nom_accession,
        A.comment AS remarque,
        V.value AS nom_variete,
        VNT.value AS name_type
      FROM
        plant.lot L
        JOIN plant.accession A
          ON L.id_accession = A.id
        JOIN plant.variety_name V
          ON V.id_variety = A.id_variety
        JOIN plant.variety_name_type VNT
          ON VNT.id_type_nom = V.id_variety_name_type
      WHERE
        L.id_lot = ANY (%s)
      ORDER BY
        id
      """
    self.cur.execute(sql, [idLot])
    rep = self.cur.fetchall()
    sqlDate = """
      SELECT
        S.id_lot AS id,
        S.harvesting_date AS date
      FROM
        plant.seed S
      WHERE
        S.id_lot = ANY (%s)
      """
    self.cur.execute(sqlDate, [idLot])
    repDate = self.cur.fetchall()
    for i in range(0,len(rep)):
      for j in range(0,len(repDate)):
        if (rep[i]['id'] == repDate[j]['id']):
          rep[i]['date'] = repDate[j]['date']
    liste = []
    #parcours la liste pour ne garder que les noms usuels
    idLot = rep[0]['id']
    name = rep[0]['arbre']
    id_accession = rep[0]['id_accesssion']
    date = rep[0]['date']
    nom_accession = rep[0]['nom_accession']
    remarque_accession = rep[0]['remarque']
    nom_variete = rep[0]['nom_variete']
    name_type = rep[0]['name_type']
    for i in range(0,len(rep)):
      # si on est au dernier element on garde les valeurs
      if i +1 == len(rep):
        liste.append(
          {
            "id": idLot,
            "lot_name": name,
            "id_accession": id_accession,
            "date_recolte": date,
            "introduction_name": nom_accession,
            "remarque": remarque_accession,
            "variety_name": nom_variete
          }
        )
      else:
        # si on change de idLot alors on garde les valeurs pour ce lot et
        # on charge les valeurs suivantes
        if idLot != rep[i+1]['id']:
          liste.append(
            {
              "id": idLot,
              "lot_name": name,
              "id_accession": id_accession,
              "date_recolte": date,
              "introduction_name": nom_accession,
              "remarque": remarque_accession,
              "variety_name": nom_variete
            }
          )
          idLot = rep[i + 1]['id']
          name = rep[i + 1]['arbre']
          id_accession = rep[i + 1]['id_accesssion']
          date = rep[i + 1]['date']
          nom_accession = rep[i + 1]['nom_accession']
          remarque_accession = rep[i + 1]['remarque']
          nom_variete = rep[i + 1]['nom_variete']
          name_type = rep[i + 1]['name_type']
        #sinon on verifie que le nom variete est nom_usuel
        else:
          if 'ari' in rep[i]['name_type']:
            idLot = rep[i]['id']
            name = rep[i]['arbre']
            id_accession = rep[i]['id_accesssion']
            date = rep[i]['date']
            nom_accession = rep[i]['nom_accession']
            remarque_accession = rep[i]['remarque']
            nom_variete = rep[i]['nom_variete']
          if rep[i]['name_type'] == 'nom usuel':
            idLot = rep[i]['id']
            name = rep[i]['arbre']
            id_accession = rep[i]['id_accesssion']
            date = rep[i]['date']
            nom_accession = rep[i]['nom_accession']
            remarque_accession = rep[i]['remarque']
            nom_variete = rep[i]['nom_variete']
            name_type = rep[i]['name_type']
    return liste

  #TODO Move to NotationTools
  def getNotationContextList(self, groupsIds):
    """
    Get the list of notation contexts accessible to the given usergroups
    ordered by context name

    :param groupsIds: the Ids of the user groups to use as filter
    :return: the list of context as dict with key: id, name, comment

    :type groupsIds: list of int
    :rtype: list of dict
    """
    rep = []
    sql = """
      SELECT
        cn.id AS id,
        cn.name AS name,
        '' AS comment
      FROM
        notation.context cn
      WHERE
        cn.id_usergroup = ANY (%s)
      ORDER BY
        cn.name
      """
    self.cur.execute(sql, [groupsIds])
    rep = self.cur.fetchall()
    return rep

  #TODO Move to NotationTools
  def getNotationTypesForContext(self, groupsIds, contextId):
    notationTypes = []
    idReq = isinstance(contextId, int)
    if (not idReq and not isinstance(contextId, basestring)):
      raise Exception("contextId must be int or sting")
    fieldName = "name"
    if idReq:
      fieldName = "id"
    sql = """
      SELECT
        tn.name,
        tn.type AS type,
        lcn.rank AS rank
      FROM
        notation.context cn,
        notation.lien_contexte_notation lcn,
        notation.notation_type tn
      WHERE
        cn.id_usergroup = ANY (%s)
        AND cn.id = lcn.id_context
        AND tn.id_type_notation = lcn.id_notation_type
        AND cn.{0} = %s
      ORDER BY
        lcn.rank
      """
    self.cur.execute(sql.format(fieldName), [groupsIds, contextId])
    notationTypes = self.cur.fetchall()
    return notationTypes

  #TODO Move to NotationTools
  def getNotations(self, groupsIds, notationIds):
    """
    Get notations given notation ids

    :param groupsIds: the Ids of the user groups to use as filter
    :param notationIds: the Ids of the notation to get
    :return: a list of dict with keys: id, name, value, type_id, type,
      comment, date, site_id, observer

    :type groupsIds: list of integer
    :type notationIds: list of integer
    :rtype: list of dict
    """
    notations = []
    rawReq = """
      SELECT
        n.id_notation AS id,
        tn.name,
        tn.id_type_notation AS type_id,
        n.value,
        tn.type AS type,
        n.comment AS comment,
        n.date AS date,
        n.id_site AS site_id,
        n.observer AS experimentator
      FROM
        notation.notation_{0} n,
        notation.notation_type tn,
        notation.notation_has_usergroup nau
      WHERE
        n.id_notation = ANY (%s)
        AND n.id_type_notation = tn.id_type_notation
        AND nau.id_notation = n.id_notation
        AND nau.id_usergroup = ANY (%s)
      """
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      self.cur.execute(req, [notationIds, groupsIds])
      notations = notations + self.cur.fetchall()
    return notations

  #TODO Move to NotationTools
  def getNotationIdsForLot(self, groupsIds, lotId, contextNotationId = None):
    rawReq = ''
    if (contextNotationId):
      rawReq = """
        SELECT
          lan.id_notation AS id
        FROM
          notation.lot_has_notation lan,
          notation.notation_{0} n,
          notation.notation_type tn,
          notation.lien_contexte_notation lcn,
          notation.context cn,
          notation.notation_has_usergroup nhu,
          experiment.experiment_has_notation ehn,
          experiment.experiment e
        WHERE
          nhu.id_usergroup = ANY (%s)
          AND nhu.id_notation = n.id_notation
          AND cn.id = %s
          AND lan.id_lot = %s
          AND e.id = ehn.id_experiment
          AND ehn.id_notation = n.id_notation
          AND cn.id = lcn.id_context
          AND lcn.id_notation_type = tn.id_type_notation
          AND n.id_notation = lan.id_notation
          AND n.id_type_notation = tn.id_type_notation
        """
    else:
      rawReq = """
        SELECT
          lan.id_notation AS id
        FROM
          notation.lot_has_notation lan,
          notation.notation_has_usergroup nhu
        WHERE
          lan.id_notation = nhu.id_notation
          AND nhu.id_usergroup = ANY (%s)
          AND lan.id_lot = %s
        """
    ids = set()
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      if (contextNotationId):
        #print self.cur.mogrify(req, [groupsIds, contextNotationId, lotId])
        self.cur.execute(req, [groupsIds, contextNotationId, lotId])
      else:
        self.cur.execute(req, [groupsIds, lotId])
      ids |= set([d['id'] for d in self.cur.fetchall()])
    return list(ids)

  def getNamesVarietyForGroup(self, GroupIds, filterText):
    liste=[]
    if filterText is None:
      req = """
        SELECT
          DISTINCT (VN.value) AS value
        FROM
          plant.variety_name VN,
          plant.accession A,
          plant.lot L,
          plant.lot_has_usergroup LAU
        WHERE
          LAU.id_lot = L.id_lot
          AND L.id_accession = A.id
          AND A.id_variety = VN.id_variety
          AND LAU.id_usergroup = ANY (%s)
        ORDER BY
          value
        """
    else:
      filterText = '*' + filterText + '*'
      filterText = filterText.replace('=', '==').replace('%', '=%')
      filterText = filterText.replace('_', '=_').replace('*', '%')
      req = """
        SELECT
          DISTINCT (VN.value) AS value
        FROM
          plant.variety_name VN,
          plant.accession A,
          plant.lot L,
          plant.lot_has_usergroup LAU
        WHERE
          LAU.id_lot = L.id_lot
          AND L.id_accession = A.id
          AND A.id_variety = VN.id_variety
          AND LAU.id_usergroup = ANY (%s)
          AND VN.value ILIKE %s ESCAPE '='
        ORDER BY
          value
        """
    self.cur.execute(req, [GroupIds,filterText])
    liste = self.cur.fetchall()
    return liste

  def getNamesAccessionForGroup(self, GroupIds, filterText):
    liste=[]
    if filterText is None:
      req = """
      SELECT
        DISTINCT (A.introduction_name) AS value
      FROM
        plant.accession A,
        plant.lot L,
        plant.lot_has_usergroup LAU
      WHERE
        LAU.id_lot = L.id_lot
        AND L.id_accession = A.id
        AND LAU.id_usergroup = ANY (%s)
      ORDER BY
        value
      """
    else:
      filterText = '*'+filterText+'*'
      filterText = filterText.replace('=', '==').replace('%', '=%')
      filterText = filterText.replace('_', '=_').replace('*', '%')
      req = """
        SELECT
          DISTINCT (A.introduction_name) AS value
        FROM
          plant.accession A,
          plant.lot L,
          plant.lot_has_usergroup LAU
        WHERE
          LAU.id_lot = L.id_lot
          AND L.id_accession = A.id
          AND LAU.id_usergroup = ANY (%s)
          AND A.introduction_name ILIKE %s ESCAPE '='
        ORDER BY
          value
      """
    self.cur.execute(req, [GroupIds, filterText])
    liste = self.cur.fetchall()
    return liste

  def getNamesLotForGroup(self, GroupIds, filterText):
    liste=[]
    if filterText is None:
      req = """
        SELECT
          DISTINCT (L.name) AS value
        FROM
          plant.lot L,
          plant.lot_has_usergroup LAU
        WHERE
          LAU.id_lot = L.id_lot
          AND LAU.id_usergroup = ANY (%s)
        ORDER BY
          value
        """
    else:
      filterText = '*'+filterText+'*'
      filterText = filterText.replace('=', '==').replace('%', '=%')
      filterText = filterText.replace('_', '=_').replace('*', '%')
      req = """
        SELECT
          DISTINCT (L.name) AS value
        FROM
          plant.lot L,
          plant.lot_has_usergroup LAU
        WHERE
          LAU.id_lot = L.id_lot
          AND LAU.id_usergroup = ANY (%s)
          AND L.name ILIKE %s ESCAPE '='
        ORDER BY
          value
      """
    self.cur.execute(req, [GroupIds, filterText])
    liste = self.cur.fetchall()
    return liste

  def getAllPlacesByIdLot(self, groupIds, idLot):
    req = """
      SELECT
        to_char(P.plantation_date, 'DD/MM/YYYY') AS plantation_date,
        to_char(P.removing_date, 'DD/MM/YYYY') AS removing_date,
        LP.value,
        PT.value AS type
      FROM
        plant.place P,
        Location.place LP,
        location.place_type PT,
        plant.lot_has_usergroup LAU
      WHERE
        P.id_lot = %s
        AND LP.id_plant_place = P.id
        AND PT.id=LP.id_place_type
        AND LAU.id_lot = P.id_lot
        AND LAU.id_usergroup = ANY (%s)
      """
    self.cur.execute(req, [idLot, groupIds])
    ids = self.cur.fetchall()
    return ids

  def getAllPlacesByIdLots(self, groupIds, idLot):
    req = """
      SELECT
        to_char(P.plantation_date, 'DD/MM/YYYY' ) AS plantation_date,
        to_char(P.removing_date, 'DD/MM/YYYY' ) AS removing_date,
        LP.value,
        PT.value AS type,
        P.id_lot AS id
      FROM
        plant.place P,
        Location.place LP,
        location.place_type PT,
        plant.lot_has_usergroup LAU
      WHERE
        P.id_lot = ANY (%s)
        AND LP.id_plant_place = P.id
        AND PT.id=LP.id_place_type
        AND LAU.id_lot = P.id_lot
        AND LAU.id_usergroup = ANY (%s)
      """
    self.cur.execute(req, [idLot, groupIds])
    ids = self.cur.fetchall()
    return ids

  def getAllPlaceNamesByGroup(self, groupIds):
    req = """
      SELECT
        DISTINCT (PT.value) AS name,
        PT.id AS id
      FROM
        plant.place P,
        Location.place LP,
        location.place_type PT,
        plant.lot_has_usergroup LAU
      WHERE
        LP.id_plant_place = P.id
        AND PT.id=LP.id_place_type
        AND LAU.id_lot = P.id_lot
        AND LAU.id_usergroup = ANY (%s)
      """
    self.cur.execute(req, [ groupIds, ])
    ids = self.cur.fetchall()
    return ids

  def getValuesPlacesByTypeAndGroup(self, groupIds, typePlaceId):
    req = """
      SELECT
        DISTINCT (LP.value) AS value
      FROM
        plant.place P,
        Location.place LP,
        location.place_type PT,
        plant.lot_has_usergroup LAU
      WHERE
        LP.id_plant_place = P.id
        AND PT.id=LP.id_place_type
        AND LAU.id_lot = P.id_lot
        AND LAU.id_usergroup = ANY (%s)
        AND PT.id = ANY (%s)
      ORDER BY
        LP.value
      """
    self.cur.execute(req, [groupIds, typePlaceId])
    ids = self.cur.fetchall()
    return ids

  def getAllNamesByVariete(self, idVariety):
    """
    Get all variety names for a variety id

    :param sessionId: the sessionId (see sessionTools)
    :param groupsIds: a list of usergroup ids to whitch trees will be attached
    :param idAccession: the id of the accession
    :return: a list of idLots
    """
    req = """
      SELECT
        VN.value AS value,
        VN.date AS date,
        VNT.value AS type,
        T.genus AS genus,
        T.specie AS species,
        V.comment AS comment,
        A1.firstname AS breeder_firstname,
        A1.lastname AS breeder_lastname,
        A2.firstname AS editor_firstname,
        A2.lastname AS editor_lastname
      FROM
        plant.variety_name VN
        JOIN  plant.variety_name_type VNT
          ON VN.id_variety_name_type = VNT.id_type_nom
        JOIN  plant.variety V
          ON  V.id_variete = VN.id_variety
        JOIN  plant.taxonomy T
          ON V.id_taxonomy = T.id
        LEFT JOIN address_book.address A1
          ON A1.id = V.breeder
        LEFT JOIN address_book.address A2
          ON A2.id = V.editor
      WHERE
        VN.id_variety = %s
      ORDER BY
        VNT.value
      """
    self.cur.execute(req, [idVariety, ])
    ids = self.cur.fetchall()
    return ids

  def getInformationsByIdAccession(self, groupIds, idAcc):
    req = """
      SELECT
        A.introduction_name AS name,
        A.comment AS informations,
        to_char(A.introduction_date,'DD/MM/YYYY') AS date,
        A.collection_date AS date_collecte,
        S.name AS site_collecte,
        P.firstname AS provider_firstname,
        P.lastname AS provider_lastname,
        O.introduction_name AS origine_introduction
      FROM
        plant.accession A
        LEFT JOIN address_book.site S
          ON S.id_site = A.collection_site
        LEFT JOIN address_book.address P
          ON P.id = A.provider
        LEFT JOIN plant.accession O
          ON O.id = A.introduction_clone
      WHERE
        A.id = %s
      """
    self.cur.execute(req, [idAcc, ])
    ids = self.cur.fetchall()
    return ids

  def getLot(self, id = None, type = "tree"):
    """
    Get object elvis.data.Lot from a lot id

    :param id: the lot id to get
    :param type: one element of tree (default), seed or lot
    :return: an elvis.data.Lot objects
    :type id: int
    :type type: string
    :rtype: object

    .. sectionauthor:: Sylvain Gaillard
    .. sectionauthor:: Sandra Pelletier
    """
    # TODO: Lorsque les tables seed et tree seront supprimées de la base,
    # enlever la gestion de l'héritage dans les requêtes
    #
    if not id:
      raise Exception("Need at least an id")
    if type not in ("tree", "seed", "lot"):
      raise Exception("Type must be 'tree', 'seed' or 'lot' [{0}]".format(type))
    req1 = """
      SELECT
        pl.id_lot AS id,
        pl.id_lot_type AS type,
        pl.name,
        pl.id_accession,
        pl.id_lot_source,
        pl.multiplication_date,
      """
    req3 = """
        pl.creation_date,
        pl.destruction_date,
        pp.id AS place
      FROM
        plant.{0} pl
        LEFT JOIN plant.place pp
          ON pp.id_lot = pl.id_lot
      WHERE
        pl.id_lot = %s
      """.format(type)
    # Gestion de l'héritage seed et tree
    if type == "seed":
      req2 = """
        pl.harvesting_date,
        pl.quantity,
        """
    elif type == "tree":
      req2 = """
        pl.death_date AS harvesting_date,
        """
    elif type == "lot":
      req2 = """
        pl.destruction_date AS harvesting_date,
        pl.element_number AS quantity,
        """
    req = req1 + req2 + req3
    try:
      self.cur.execute(req, [id])
    except Exception as e:
      raise e
    rep = self.cur.fetchone()
    if not rep:
      raise Exception("Lot not found [id: {0}]".format(id))
    # Gestion de l'héritage tree
    if type == "tree":
      rep['quantity'] = None
    lot = elvis.data.Lot(
      rep['id'],
      rep['type'],
      rep['name'],
      rep['id_accession'],
      rep['id_lot_source'],
      rep['multiplication_date'],
      rep['harvesting_date'],
      rep['creation_date'],
      rep['destruction_date'],
      rep['quantity'],
      rep['place']
      )
    return lot

  def getAllInformationsTreeByIdLot(self, groupIds, idLot):
    """
    Get informations given lot id
      :param groupsIds: the Ids of the user groups to use as filter
      :param idLot: the Id of the lot
      :return: a list of dict with keys: id, name, id_accession, id_lot_source,
      name_lot_source, multiplication_date, first_shoot_year, rootstock,
      death_date
    """
    req = """
      SELECT
        a.id_lot AS id,
        a.name AS name,
        a.id_accession,
        a.id_lot_source,
        l.name AS name_lot_source,
        a.multiplication_date,
        a.first_shoot_year,
        a.rootstock,
        a.death_date
      FROM
        plant.tree a
        JOIN plant.lot_has_usergroup lhu
          ON lhu.id_lot = a.id_lot
        LEFT JOIN plant.lot l
          ON l.id_lot = a.id_lot_source
      WHERE
        lhu.id_usergroup = ANY (%s)
        AND a.id_lot = %s
      """
    self.cur.execute(req, [groupIds, idLot])
    rep = self.cur.fetchall()
    if rep ==[]:
      return rep
    return rep[0]

  def getAllInformationsSeedByIdLot(self, groupIds, idLot):
    """
    Get informations given lot id
      :param groupsIds: the Ids of the user groups to use as filter
      :param idLot: the Id of the lot
      :return: a list of dict with keys: id, name, id_accession, id_lot_source,
      name_lot_source, multiplication_date, harvesting_date, quantity
    """
    req = """
      SELECT
        a.id_lot AS id,
        a.name AS name,
        a.id_accession,
        a.id_lot_source,
        l.name AS name_lot_source,
        a.multiplication_date,
        a.harvesting_date,
        a.quantity
      FROM
        plant.seed a
        JOIN plant.lot_has_usergroup lhu
          ON lhu.id_lot = a.id_lot
        LEFT JOIN plant.lot l
          ON l.id_lot = a.id_lot_source
      WHERE
        lhu.id_usergroup = ANY (%s)
        AND a.id_lot = %s
      """
    self.cur.execute(req, [groupIds, idLot])
    rep = self.cur.fetchall()
    if rep == []:
      return rep
    return rep[0]

  def getAllIdLotsTreeByIdAccession(self, groupIds, idAccession):
    """
    Get informations given lot id
      :param groupsIds: the Ids of the user groups to use as filter
      :param idLot: the Id of the lot
      :return: a list of dict with keys: id, name, id_accession, id_lot_source,
      name_lot_source, multiplication_date, harvesting_date, quantity
    """
    req = """
      SELECT
        a.id_lot AS id_lot,
        a.name AS numero_lot,
        a.id_accession AS clone,
        a.id_lot_source AS lot_parent,
        a.multiplication_date AS date_multiplication,
        a.first_shoot_year AS annee_premiere_pousse,
        a.rootstock AS porte_greffe
      FROM
        plant.tree a
        JOIN plant.lot_has_usergroup lhu
          ON lhu.id_lot = a.id_lot
      WHERE
        lhu.id_usergroup = ANY (%s)
        AND id_accession = %s
      """
    self.cur.execute(req, [groupIds, idAccession])
    rep = self.cur.fetchall()
    return rep

  def getAllIdLotsSeedByIdAccession(self, groupIds, idAccession):
    """
    Get informations given lot id
      :param groupsIds: the Ids of the user groups to use as filter
      :param idLot: the Id of the lot
      :return: a list of dict with keys: id, name, id_accession, id_lot_source,
      name_lot_source, multiplication_date, harvesting_date, quantity
    """
    req = """
      SELECT
        a.id_lot AS id_lot,
        a.name AS numero_lot,
        a.id_accession AS clone,
        a.id_lot_source AS lot_parent,
        a.multiplication_date AS date_multiplication,
        a.harvesting_date AS date_recolte,
        a.quantity AS quantite
      FROM
        plant.seed a
        JOIN plant.lot_has_usergroup lhu
          ON lhu.id_lot = a.id_lot
      WHERE
        lhu.id_usergroup = ANY (%s)
        AND id_accession = %s
        """
    self.cur.execute(req, [groupIds, idAccession])
    rep = self.cur.fetchall()
    return rep

  def getTaxonomiesFromListId(
    self,
    taxonomyListId = None,
    taxonomyTypeListId = None
    ):
    """
    Get all informations about taxonomy
    :param taxonomyListId: the list of id taxonomy
    :param taxonomyTypeListId: the list of id type taxonomy
    :return: a list of objects taxon.py
    """
    taxonList = []
    rep=[]
    req = """
      SELECT
        T.id,
        genus,
        specie,
        name AS type,
        TT.id as id_type
      FROM
        plant.taxonomy T
        LEFT JOIN plant.taxon_type TT
          ON TT.id = T.id_taxon_type
      """
    if not taxonomyListId and not taxonomyTypeListId :
      try:
        self.cur.execute(req, [])
      except Exception as e:
        raise e
      rep = self.cur.fetchall()
    if taxonomyListId and not taxonomyTypeListId :
      req = req + "WHERE T.id = ANY (%s)"
      try:
        self.cur.execute(req, [taxonomyListId,])
      except Exception as e:
        raise e
      rep = self.cur.fetchall()
    if not taxonomyListId and  taxonomyTypeListId :
      req = req + "WHERE TT.id = ANY (%s)"
      try:
        self.cur.execute(req, [taxonomyTypeListId,])
      except Exception as e:
        raise e
      rep = self.cur.fetchall()
    if taxonomyListId and  taxonomyTypeListId :
      req = req + "WHERE  T.id = ANY (%s) AND TT.id = ANY (%s)"
      try:
        self.cur.execute(req, [taxonomyListId,taxonomyTypeListId])
      except Exception as e:
        raise e
      rep = self.cur.fetchall()

    if rep:
      for  tax in rep :
        taxon = elvis.data.Taxon()
        taxon.setId(tax['id'])
        taxon.setType(tax['type'])
        taxon.setIdType(tax['id_type'])
        taxon.setGenus(tax['genus'])
        taxon.setSpecie(tax['specie'])
        taxonList.append(taxon)
    return taxonList

  def completeTaxonomyList(self, taxonList):
    """
    Get informations about taxon from id or from genus and specie
      :param taxonList: the list of objects taxon.py
      :return: a list of objects taxon.py
    """
    for taxon in taxonList:
      if not taxon.getId() and taxon.getGenus() and taxon.getSpecie():
        req = """
          SELECT
            T.id,
            genus,
            specie,
            name AS type
          FROM
            plant.taxonomy T
            LEFT JOIN plant.taxon_type TT
              ON TT.id = T.id_taxon_type
          WHERE
            genus = %s
            AND specie = %s
          """
        self.cur.execute(req, [taxon.getGenus(), taxon.getSpecie()])
        rep = self.cur.fetchall()
        if rep:
          taxon.setId(rep[0]['id'])
          taxon.setType(rep[0]['type'])

      if taxon.getId() and not taxon.getGenus() and not taxon.getSpecie():
        req = """
          SELECT
            T.id,
            genus,
            specie,
            name AS type
          FROM
            plant.taxonomy T
            LEFT JOIN plant.taxon_type TT
              ON TT.id = T.id_taxon_type
          WHERE
            id = %s
          """
        self.cur.execute(req, [taxon.getId, ])
        rep = self.cur.fetchall()
        if rep:
          taxon.setGenus(rep[0]['genus'])
          taxon.setSpecie(rep[0]['specie'])
          taxon.setType(rep[0]['type'])
    return taxonList

  def searchVarietyName(self,
      value = None,
      date = None,
      nameType = None,
      idNameType = None,
      idVariety = None,
      idTaxonomy = None
      ):
    """
    Get all id of a variety name
    Author: Fabrice Dupuis juin 2019
    :param nametype: the type of name
    :param value: the value of the name
    :param idTaxonomy: the value of the id of the taxonomy
    :return: a list of id
    :type nametype: character
    :type value: character
    :type idTaxonomy: integer
    :rtype: list of integer

    """
    listIdVarietyName = []
    if value and nameType and idTaxonomy:
      req = """
          SELECT
            v.id_variete AS id
          FROM
            plant.variety V
            JOIN plant.variety_name VN
              ON V. id_variete = VN.id_variety
            JOIN plant.variety_name_type VNT
              ON VNT. id_type_nom = VN.id_variety_name_type
          WHERE
            v.id_taxonomy = %s
            AND VN.value = %s
            AND VNT.value = %s
          """
      self.cur.execute(req, [idTaxonomy, value, nameType])
      rep = self.cur.fetchall()
      if rep:
        for i in rep:
          listIdVarietyName.append(i['id'])
    return listIdVarietyName

  def searchVariety (self, idTaxonomy = None, listNames = None):
    # TO BE CONTINUED ON OTHER SEARCH
    """
    search varietyId
    Author: Fabrice Dupuis juin 2019
    :param :idTaxonomy
    :param :listNames
    :type idTaxonomy: integer
    :typelistNames: dictionnary {typeName: valueName}
    :return: a list of id
    :rtype: list of integer
    """
    if idTaxonomy and listNames:
      listeIdVariete = []
      # parcours le dictionnaire de  noms
      for type, value in listNames.items():
        id = self.searchVarietyName(idTaxonomy = idTaxonomy, nameType = type, value = value)
        # liste des varietes pour 1 item
        if len(id) > 0:
          for i in id:
            if i not in listeIdVariete:
              listeIdVariete.append(i)
    return listeIdVariete

  def getVarietyNames(self, id):
    """
    Get list of object variety names

    :param id: variety id
    :return: a list of objects varietyName.py
    :type id: int
    :rtype: list

    .. sectionauthor:: Sandra Pelletier
    """
    varietyNameList = []
    req= """
      SELECT
        vn.id_nom_var as id,
        vn.value as name,
        vn.date as date,
        vnt.value as type,
        vnt.id_type_nom as id_type_nom,
        vn.id_variety as id_variety
      FROM plant.variety v
        LEFT JOIN plant.variety_name vn
          ON vn.id_variety = v.id_variete
        LEFT JOIN plant.variety_name_type vnt
          ON vnt.id_type_nom = vn.id_variety_name_type
      WHERE
        id_variete = %s
      """
    try:
      self.cur.execute(req, [id,])
    except Exception as e:
      raise e
    rep = self.cur.fetchall()
    varietyNameList = []
    if rep:
      for name in rep:
        varietyName = elvis.data.VarietyName()
        varietyName.setId(name['id'])
        varietyName.setValue(name['name'])
        varietyName.setDate(name['date'])
        varietyName.setType(name['type'])
        varietyName.setVarietyNameTypeId(name['id_type_nom'])
        varietyName.setVarietyId(name['id_variety'])
        varietyNameList.append(varietyName)
    return varietyNameList

  def getVariety(self, id):
    """
    Get list of variety objects from an id list

    :param id: variety id
    :return: elvis.data.Variety object
    :type id: int
    :rtype: object

    .. sectionauthor:: Sandra Pelletier
    """
    variety = elvis.data.Variety()
    req = """
      SELECT
        v.id_variete as id,
        v.comment as comment,
        t.genus as genus,
        t.specie as specie,
        tt.name as taxon_type,
        t.id as taxinomy_id,
        ab.lastname as breeder,
        ab.id as breeder_id,
        ae.lastname as editor,
        ae.id as editor_id
      FROM plant.variety v
        LEFT JOIN plant.taxonomy t ON t.id = v.id_taxonomy
        LEFT JOIN plant.taxon_type tt ON tt.id = t.id_taxon_type
        LEFT JOIN address_book.address ab ON ab.id = v.breeder
        LEFT JOIN address_book.address ae ON ae.id = v.editor
      WHERE
        id_variete = %s
      """
    try:
      self.cur.execute(req, [id,])
    except Exception as e:
      raise e
    rep = self.cur.fetchone()
    if rep:
      variety.setId(rep['id'])
      variety.setBreeder(rep['breeder'])
      variety.setBreederId(rep['breeder_id'])
      variety.setComment(rep['comment'])
      variety.setGenus(rep['genus'])
      variety.setTaxonomyId(rep['taxinomy_id'])
      variety.setTaxonType(rep['taxon_type'])
      variety.setSpecie(rep['specie'])
      variety.setEditor(rep['editor'])
      variety.setEditorId(rep['editor_id'])
      varietyNameList = self.getVarietyNames(id)
      variety.setVarietyNameList(varietyNameList)
      return variety
    return None

  def getAccession(self, id):
    """
    Get an accession object from accession id

    :param id: accession id
    :return: elvis.data.Accession object
    :type id: int
    :rtype: object

    .. sectionauthor:: Sandra Pelletier
    """
    accession = elvis.data.Accession()
    req = """
      SELECT
        pa.id,
        pa.introduction_name,
        pa.id_variety,
        pa.comment,
        pa.introduction_clone,
        ab.lastname AS provider,
        pa.provider AS provider_id,
        pa.introduction_date,
        pa.collection_date,
        pa.collection_site
      FROM
        plant.accession pa
      LEFT JOIN
        address_book.address ab
          ON ab.id = pa.provider
      WHERE
        pa.id = %s
      """
    try:
      self.cur.execute(req, [id,])
    except Exception as e:
      raise e
    rep = self.cur.fetchone()
    if rep:
      accession.setId(rep['id'])
      accession.setIntroductionName(rep['introduction_name'])
      accession.setVarietyId(rep['id_variety'])
      accession.setComment(rep['comment'])
      accession.setIntroductionCloneId(rep['introduction_clone'])
      accession.setProvider(rep['provider'])
      accession.setProviderId(rep['provider_id'])
      accession.setIntroductionDate(rep['introduction_date'])
      accession.setCollectionDate(rep['collection_date'])
      accession.setCollectionSiteId(rep['collection_site'])
    return accession

  def getVarietiesFromListId(self, idVarietyList):
    # TODO cette fonction existe déjà sous getVarieties
    """
    Get all informations about variety
    :param idVarietyList: the list of idVariety
    :return: a list of objects variety.py
    """
    nameList = []
    for idVar in idVarietyList:
      v = elvis.data.Variety.Variety()
      req = """
        SELECT
          v.id_variete as id,
          v.comment as comment,
          t.genus as genus,
          t.specie as specie,
          tt.name as taxon_type,
          t.id as taxinomy_id,
          ab.lastname as breeder,
          ab.id as breeder_id,
          ae.lastname as editor,
          ae.id as editor_id
        FROM plant.variety v
          LEFT JOIN plant.taxonomy t ON t.id = v.id_taxonomy
          LEFT JOIN plant.taxon_type tt ON tt.id = t.id_taxon_type
          LEFT JOIN address_book.address ab ON ab.id = v.breeder
          LEFT JOIN address_book.address ae ON ae.id = v.editor
        WHERE id_variete = %s
            """
      self.cur.execute(req, [idVar,])
      rep = self.cur.fetchone()
      if rep:
        v.setId(rep['id'])
        v.setComment(rep['comment'])
        v.setGenus(rep['genus'])
        v.setSpecie(rep['specie'])
        v.setTaxonomyId(rep['taxinomy_id'])
        v.setBreeder(rep['breeder'])
        v.setEditor(rep['editor'])
        v.setBreederId(rep['breeder_id'])
        v.setEditorId(rep['editor_id'])
        v.setTaxonType(rep['taxon_type'])
        # TODO ajouter infos sur noms variete
        varietyNameList = []
        req= """
          SELECT
            vnt.value as type,
            vn.value as name,
            vn.date as date,
            vn.id_nom_var as id,
            vnt.id_type_nom as id_type_nom,
            vn.id_variety as id_variety
          FROM plant.variety v
            LEFT JOIN plant.variety_name vn
              ON vn.id_variety = v.id_variete
            LEFT JOIN plant.variety_name_type vnt
              ON vnt.id_type_nom = VN.id_variety_name_type
          WHERE id_variete = %s
          """
        self.cur.execute(req, [idVar,])
        rep = self.cur.fetchall()
        if rep:
          for name in rep:
            vn = elvis.data.VarietyName.VarietyName()
            vn.setId(name['id'])
            vn.setValue(name['name'])
            vn.setNameType(name['type'])
            vn.setIdNameType(name['id_type_nom'])
            vn.setDate(name['date'])
            vn.setIdVariety(name['id_variety'])
            varietyNameList.append(vn)
        v.setVarietyNameList(varietyNameList)
        nameList.append(v)
    return nameList

  def updateVariety(self, varietyList):
    # TO BE CONTINUED
    """
    update informations about variety
    :param varietyList: the list of objects Variety

    """
  def insertVariety(self, varietyList):
    # TO BE CONTINUED
    """
    create variety
    :param varietyList: the list of objects Variety
    :return list idVariety
    """

# Methode pour maintenance
  def getLotHasNotationValues(self):
    req = """
      SELECT
        gan.id_notation,
        gal.id_lot
      FROM
        (
          SELECT
            gal.id_group,
            gal.id_lot
          FROM
            plant.group_has_lot gal
          GROUP BY
            gal.id_group,
            gal.id_lot
          HAVING COUNT(gal.id_lot) = 1
        ) gal,
        plant.group_has_notation gan
      WHERE
        gal.id_group = gan.id_group
      """
    self.cur.execute(req)
    ids = [(d['id_notation'], d['id_lot']) for d in self.cur.fetchall()]
    return ids

  def insertLotHasNotation(self, ln):
    req = """
      INSERT INTO notation.lot_has_notation (
        id_notation,
        id_lot
        )
      VALUES (
        %s,
        %s
        )
      """
    flag = False
    try:
      self.cur.execute(req, ln)
    except:
      self.conn.rollback()
    else:
      self.conn.commit()
      flag = True
    return flag

  def getLotHasUsergroupValues(self):
    req = """
      SELECT
        DISTINCT gl.id_lot,
        ug.id_usergroup
      FROM
        plant.group_has_lot gl,
        plant.group g,
        plant.group_has_usergroup ug
      WHERE
        ug.id_group = g.id_group
        AND g.id_group = gl.id_group
      """
    self.cur.execute(req)
    ids = [(d['id_lot'], d['id_usergroup']) for d in self.cur.fetchall()]
    return ids

  def insertLotHasUsergroup(self, ln):
    req = """
      INSERT INTO plant.lot_has_usergroup (
        id_lot,
        id_usergroup
        )
      VALUES (
        %s,
        %s
        )
      """
    flag = False
    try:
      self.cur.execute(req, ln)
    except:
      self.conn.rollback()
    else:
      self.conn.commit()
      flag = True
    return flag

  def getNotationIdsFromNAU(self):
    req = """
      SELECT
        nau.id_notation AS id
      FROM
        notation.notation_has_usergroup nau
      """
    self.cur.execute(req)
    ids = [d['id'] for d in self.cur.fetchall()]
    return ids

  def getNotationIdsFromLAN(self):
    req = """
      SELECT
        lan.id_notation,
        lhu.id_usergroup
      FROM
        notation.lot_has_notation lan,
        plant.lot_has_usergroup lhu
      WHERE
        lan.id_lot = lhu.id_lot
      """
    self.cur.execute(req)
    ids = [(d['id_notation'], d['id_usergroup']) for d in self.cur.fetchall()]
    return ids

  def insertNotationInNotatioHasUsergroup(self, nid, gid):
    req = """
      INSERT INTO notation.notation_has_usergroup (
        id_notation,
        id_usergroup,
        write
        )
      VALUES (
        %s,
        %s,
        TRUE
        )
      """
    flag = False
    try:
      self.cur.execute(req, [nid, gid])
    except:
      self.conn.rollback()
    else:
      self.conn.commit()
      flag = True
    return flag

  def getGeneticBackgroundList(
    self,
    taxonomyId,
    varietyRelationTypeName = 'mutant',
    varietyNameTypeValue = 'nom usuel'
    ):
    """
    Get the id list of variety for a taxonomy id
    which are considered as a genetic background reference

    :param taxonomyId: the taxonomy id
    :param varietyRelationTypeName: the type of relation between variety
    :param varietyNameTypeValue: the type of variety name
    :return: ids and values for each Genetic Background variety
    :type taxonomyId: int
    :type varietyRelationTypeName: string
    :type varietyNameTypeValue: string
    :rtype: a list of dictionary with keys: id, name

    .. sectionauthor:: Sandra Pelletier
    """
    # TODO: conf 'mutant'
    req = """
      SELECT
        pv.id_variete AS id,
        pvn.value AS name
      FROM
        plant.variety_name pvn
        JOIN plant.variety pv
          ON pv.id_variete = pvn.id_variety
        JOIN plant.variety_name_type pvrt
          ON pvrt.id_type_nom = pvn.id_variety_name_type
      WHERE
        pv.id_taxonomy = %s
        AND pv.id_variete NOT IN (
          SELECT
            pvr.id_child
          FROM
            plant.variety_relation pvr
            JOIN plant.variety_relation_type pvrt
              ON pvrt.id = pvr.id_type
          WHERE
            pvrt.name = %s
          )
        AND pvrt.value = %s
      """
    try:
      self.cur.execute(
        req,
        [taxonomyId, varietyRelationTypeName, varietyNameTypeValue]
        )
    except Exception as e:
      raise e
    ids = self.cur.fetchall()
    return ids

  def getVarietyNameType(self, taxonomyId=None):
    """
    Get the list of variety name for a taxon list (optional)

    :param taxonomyId: None or a list of taxon id
    :return: a list of couple id, value of variety name
    :type id: list of int
    :rtype: list of dict with keys : id, value

    .. sectionauthor:: Sandra Pelletier
    """
    req1 = """
      SELECT
        pvnt.id_type_nom AS id,
        pvnt.value
      FROM
        plant.variety_name_type pvnt
      """
    req2 = """
        JOIN plant.taxon_has_variety_name_type ptvnt
          ON ptvnt.id_variety_name_type = pvnt.id_type_nom
        JOIN plant.taxonomy pt
          ON pt.id = ptvnt.id_taxonomy
      WHERE
        pt.id = ANY (%s)
      """
    req3 = """
      ORDER BY
        pvnt.value
      """
    if taxonomyId == None:
      req = req1 + req3
    else:
      req = req1 + req2 + req3
    res = []
    try:
      self.cur.execute(req, [taxonomyId])
    except Exception as e:
      raise e
    res = self.cur.fetchall()
    return res

  def getVarietyIdsByGeneticBackgroundIds(
    self,
    geneticBackgroundIds,
    taxonomyId,
    varietyRelationType = "mutant"
    ):
    """
    Get list of variety ids for an id of genetic background

    Recovery of all the descendant variety ids of the genetic background
    as well as  the id of the wild variety itself

    :param geneticBackgroundIds: the id list of parent ids
    :param taxonomyId: the id of the current taxonomy
    :param varietyRelationType: the relation between variety (default: "mutant")
    :return: a list of variety ids
    :type geneticBackgroundIds: list of int
    :type taxonomyId: int
    :type varietyRelationType: string
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    .. note:: TODO : conf 'mutant'
    """
    req = """
      SELECT
        pvr.id_child
      FROM
        plant.variety_relation pvr
        JOIN plant.variety_relation_type pvrt
          ON pvrt.id = pvr.id_type
        JOIN plant.variety pv
          ON pv.id_variete = pvr.id_child
      WHERE
        pvrt.name = %s
        AND pvr.id_parent = ANY (%s)
        AND pv.id_taxonomy = %s
      """
    newIds = geneticBackgroundIds
    ids = []
    while newIds != []:
      ids = ids + newIds
      try:
        self.cur.execute(
          req,
          [varietyRelationType, newIds, taxonomyId]
          )
      except Exception as e:
        raise e
      newIds = [(d['id_child']) for d in self.cur.fetchall()]
    return ids

# TO REMOVE ?
  # def getLotIdsForVarietyIds(self, varietyIds, usergroupIds):
  #   """
  #   Get list of lot ids for a list of variety ids, function of usergroup ids
  #
  #   :param varietyIds: the list of variety ids
  #   :param usergroupIds: the list of usergroup ids
  #   :return: a list of lot ids for each varietyId
  #   :type varietyIds: list of int
  #   :type usergroupIds: list of int
  #   :rtype: dict of list of int
  #
  #   .. sectionauthor:: Sandra Pelletier
  #   """
  #   req = """
  #   SELECT
  #     ps.id_lot,
  #     pv.id_variete
  #   FROM
  #     plant.seed ps
  #     JOIN plant.accession pa
  #       ON pa.id = ps.id_accession
  #     JOIN plant.variety pv
  #       ON pv.id_variete = pa.id_variety
  #     JOIN plant.lot_has_usergroup plug
  #       ON plug.id_lot = ps.id_lot
  #     JOIN users.usergroup uug
  #       ON uug.id = plug.id_usergroup
  #   WHERE
  #     pv.id_variete = ANY (%s)
  #     AND uug.id = ANY (%s)
  #   """
  #   try:
  #     self.cur.execute(req, [varietyIds, usergroupIds])
  #   except Exception as e:
  #     raise e
  #   rep = self.cur.fetchall()
  #   ids = {}
  #   for var in varietyIds:
  #     ids[var] = []
  #   for lot in rep:
  #     ids[lot['id_variete']].append(lot['id_lot'])
  #   return ids

  def getVarieteByLot(self, lotIds):
    """
    Get list of variety ids for a list of lot ids
    """
    req = """
    SELECT
      id_variety AS variete,
      Ttype.name AS cultivated,
      T.genus AS genus,
      T.specie AS specie
    FROM
      plant.accession A,
      plant.lot L,
      plant.variety V,
      plant.taxonomy T
      LEFT JOIN plant.taxon_type Ttype
        ON Ttype.id = T.id_taxon_type
    WHERE
      A.id = L.id_accession
      AND A.id_variety = V.id_variete
      AND V.id_taxonomy = T.id
      AND L.id_lot = %s
    """
    try:
      self.cur.execute(req, [lotIds])
    except Exception as e:
      raise e
    res = []
    res = self.cur.fetchall()
    return res

  def getLotType(self):
    """
    Get the type of lot type

    :return: list lotType
    :rtype: list of string

    .. sectionauthor:: Sandra Pelletier
    """
    req = """
      SELECT name
      FROM plant.lot_type
      """
    try:
      self.cur.execute(req,)
    except Exception as e:
      raise e
    rep = self.cur.fetchall()
    return rep

  def getAccessionIdsByVarietyId(
    self,
    varietyId,
    depth = 0
    ):
    req = """
      SELECT id
      FROM plant.accession
      WHERE id_variety = %s
    """
    try:
      self.cur.execute(req, [varietyId, ])
    except Exception as e:
      raise e
    return self.cur.fetchall()

  def getAccessionsByVarietyId(
    self,
    varietyId,
    usergroupIds,
    depth
    ):
    rep = self.getAccessionIdsByVarietyId(varietyId)
    # Si existance d'accession
    accList = []
    if rep:
      # Récupération de chaque objet accession
      for acc in rep:
        accObj = self.getAccession(acc['id'])
        if depth > 0:
          lotList = self.getLotsByAccessionId(acc['id'], usergroupIds)
          accObj.setLotList(lotList)
        accList.append(accObj)
    return accList

  def getLotIdsByAccessionId(
    self,
    accessionId,
    usergroupIds
    ):
    """
    Get List of lots ids for an accession id

    :param accessionId: the accession id
    :param usergroupIds: the list of usergroup ids
    :return: List of elvis.data.Lot object
    :type accessionId: int
    :type usergroupIds: list of int
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    """
    req = """
      SELECT
        pl.id_lot AS id
      FROM
        plant.lot pl
        JOIN plant.lot_has_usergroup plhu
          ON plhu.id_lot = pl.id_lot
      WHERE
        pl.id_accession = %s
        AND plhu.id_usergroup = ANY (%s)
      """
    try:
      self.cur.execute(req, [accessionId, usergroupIds])
    except Exception as e:
      raise e
    return self.cur.fetchall()

  def getLotsByAccessionId(
    self,
    accessionId,
    usergroupIds
    ):
    """
    Get List of lots objects for an accession id

    :param accessionId: the accession id
    :param usergroupIds: the list of usergroup ids
    :return: List of elvis.data.Lot object
    :type accessionId: int
    :type usergroupIds: list of int
    :rtype: list of objects

    .. sectionauthor:: Sandra Pelletier
    """
    rep = self.getLotIdsByAccessionId(accessionId, usergroupIds)
    lotList = []
    if rep:
      # FIXME Gestion de l'héritage,
      # en attendant de supprimer les tables seed et tree
      lotType = self.getLotType()
      # Récupération de chaque objet lot
      for lot in rep:
        for type in lotType:
          lotObj = None
          try:
            lotObj = self.getLot(lot['id'], type['name'])
          except:
            pass
          if (lotObj != None):
            lotList.append(lotObj)
    return lotList

  def getLotRigthsByUserGroupIds(
    self,
    lotIds,
    usergroupIds
    ):
    """
    Get List of lots ids with rigths fonction of usergroups

    :param lotIds: list of lot ids
    :param usergroupIds: the list of usergroup ids
    :return: List of elvis.data.Lot object
    :type accessionId: list of int
    :type usergroupIds: list of int
    :rtype: list of dict (keys : id, write)

    .. sectionauthor:: Sandra Pelletier
    """
    req = """
      SELECT
        plhug.id_lot AS id,
        plhug.write
      FROM
        plant.lot_has_usergroup plhug
      WHERE
        plhug.id_lot = ANY (%s)
        AND plhug.id_usergroup = ANY (%s)
      """
    try:
      self.cur.execute(req, [lotIds, usergroupIds])
    except Exception as e:
      raise e
    rep = self.cur.fetchall()
    return rep

  def getLotIdsbyLotSourceId(
    self,
    lotSourceId,
    usergroupIds
    ):
    """
    Get List of lots ids matching with lotSourceId fonction of usergroups

    :param lotSourceId: the lot source id
    :param usergroupIds: the list of usergroup ids
    :return: List of lot ids which match
    :type lotSourceId: int
    :type usergroupIds: list of int
    :rtype: list of int

    .. sectionauthor:: Sandra Pelletier
    """
    req = """
      SELECT
        pl.id_lot AS id
      FROM
        plant.lot pl
        JOIN plant.lot_has_usergroup plhu
          ON plhu.id_lot = pl.id_lot
      WHERE
        pl.id_lot_source = %s
        AND plhu.id_usergroup = ANY (%s)
      """
    try:
      self.cur.execute(req, [lotSourceId, usergroupIds])
    except Exception as e:
      raise e
    rep = self.cur.fetchall()
    return rep

  def searchAccessionIdsByIntroductionName(
    self,
    name
    ):
    reqPart = "= %s"
    if (name.find("*") > -1):
      name = name.replace('=', '==').replace('%', '=%').replace('_', '=_')
      name = name.replace('*', '%')
      reqPart = "ILIKE %s ESCAPE '='"
    res = []
    req = """
      SELECT accession.id
      FROM plant.accession
      WHERE accession.introduction_name {0}
      """
    self.cur.execute(req.format(reqPart), [name])
    res += [d['id'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchAccessionIdsByIntroductionCloneId(
    self,
    introductionCloneId
    ):
    res = []
    req = """
      SELECT id
      FROM plant.accession
      WHERE introduction_clone = %s
      """
    self.cur.execute(req, [introductionCloneId,])
    res += [d['id'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchAccessionIdsByProvider(
    self,
    provider
    ):
    res = []
    req = """
      SELECT
        A.id
      FROM
        plant.accession A
        JOIN address_book.address P
          ON P.id = A.provider
      WHERE
        P.lastname = %s
      """
    self.cur.execute(req, [provider,])
    res += [d['id'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchAccessionIdsByProviderId(
    self,
    providerId
    ):
    res = []
    req = """
      SELECT id
      FROM plant.accession
      WHERE provider = %s
      """
    self.cur.execute(req, [providerId,])
    res += [d['id'] for d in self.cur.fetchall()]
    return sorted(res)

  def searchAccessionIdsByCollectionSiteId(
    self,
    collectionSiteId
    ):
    res = []
    req = """
      SELECT id
      FROM plant.accession
      WHERE introduction_clone = %s
      """
    self.cur.execute(req, [collectionSiteId,])
    res += [d['id'] for d in self.cur.fetchall()]
    return sorted(res)
