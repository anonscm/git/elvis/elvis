# -*- coding: utf-8 -*-

#  Calcul de l'index de selection poirier

class indexParam:
  __notation = {
      'textferm': {
        11: 100,
        12: 90,
        13: 80,
        21: 70,
        22: 60,
        23: 50,
        31: 20,
        32: 30,
        33: 40
        },
      'jus': {
        1: 20,
        2: 80,
        3: 100
        },
      'amer': {
        1: 80,
        2: 60,
        3: 20
        },
      'intens_arom': {
        1: 20,
        2: 50,
        3: 100
        },
      'echaud': {
        1: 100,
        2: 50,
        3: 20
        },
      'bruni': {
        1: 100,
        2: 50,
        3: 20
        },
      'eq_saveur': {
        1: 20,
        2: 30,
        3: 50,
        4: 90,
        5: 100
        },
      'astrin': {
        1: 80,
        2: 60,
        3: 20
        },
      'qual_arom': {
        1: 20,
        2: 50,
        3: 100
        },
      'bletti': {
        1: 100,
        2: 50,
        3: 20
        },
      'qte_prod': {
        1: 40,
        2: 40,
        3: 40,
        4: 70,
        5: 90
        },
      'attrait': {
        1: 50,
        2: 50,
        3: 50,
        4: 80,
        5: 100
        },
      'qualite': {
        1: 30,
        2: 30,
        3: 30,
        4: 60,
        5: 80
        }
      }

  __threshold = {
      1: {
        1: 65,
        2: 65,
        3: 83,
        4: 65
        },
      2: {
        1: 70,
        2: 70,
        3: 83,
        4: 70
        },
      3: {
        1: 72,
        2: 72,
        3: 83,
        4: 72
        }
      }

  def getParam(self, paramName):
    if (paramName == 'notation'):
      return self.__notation
    elif (paramName == 'threshold'):
      return self.__threshold
    else:
      raise Exception('Parameter not found: [' + paramName + ']')
############################################################################################
class SelectionIndex:
  __notation = {}
  __threshold = {}
  __observations = {}

  def __init__(self):
    ip = indexParam()
    self.__notation = ip.getParam('notation')
    self.__threshold = ip.getParam('threshold')
    self.__observations = {}

  def __str__(self):
    t = "Selection index:\n"
    t = t + "years: " + str(self.countYears()) + "\n"
    t = t + str(self.__observations)
    return t

  def setNotation(self, year, notation, value):
    if (year):
      if year not in self.__observations.keys():
        self.__observations[year] = {}
      self.__observations[year][notation] = value





#calcul de l'index pour un groupe de critères = "indexParam moyen
  def getI(self, criteria):
    for c in criteria:
      #print "# test:", c
      if c not in self.__notation:
        raise Exception("Criteria '" + c + "' not defined in notation data")
      for y in self.getYears():
        if c not in self.__observations[y].keys():
          #print 'pouet'
          raise Exception("Notation " + c + " not found for year " + str(y))

        #print "str(y), c=",str(y), c
        if not self.__observations[y][c]:
          print("Notation " + c + " not found for year " + str(y))
          return 0
          #raise Exception("Notation " + c + " not found for year " + str(y))

    g1 = 0     #index
    y_cpt = 0  #compteur des années
    for y in self.getYears():  #pour chaque année
      g = 0
      l_cpt = 0
      for l in criteria:
        g += self.__notation[l][self.__observations[y][l]] #cummul de l index
        l_cpt += 1
      if (l_cpt):
        g1 += g / l_cpt  #valeur moyenne
      y_cpt += 1
    if (y_cpt):
      g1 = g1 / y_cpt
      return g1
    return 0

  def getI1(self):
    for y in self.__observations.keys():
      if self.__observations[y].get('text') and self.__observations[y].get('ferm'):
        if self.__observations[y]['text'] == None and self.__observations[y]['ferm'] == None:
          self.__observations[y]['textferm'] = 0
        else:
          self.__observations[y]['textferm'] = int(str(self.__observations[y]['text']) + str(self.__observations[y]['ferm']))
    return self.getI(['textferm', 'jus'])

  def getI2(self):
    return self.getI(['amer', 'intens_arom', 'eq_saveur', 'astrin', 'qual_arom'])

  def getI3(self):
    return self.getI(['echaud', 'bruni', 'bletti'])

  def getI4(self):
    return self.getI(['qte_prod', 'attrait', 'qualite'])


  def getYears(self):
    return self.__observations.keys()

  def countYears(self):
    return len(self.__observations.keys())

  def getT1(self):
    ip = indexParam()  # est-on obligé ?
    return ip.getParam('threshold')[self.countYears()][1]
  def getT2(self):
    ip = indexParam()
    return ip.getParam('threshold')[self.countYears()][2]
  def getT3(self):
    ip = indexParam()
    return ip.getParam('threshold')[self.countYears()][3]
  def getT4(self):
    ip = indexParam()
    return ip.getParam('threshold')[self.countYears()][4]



  def testI1(self):
    return self.getI1() >= self.getT1()

  def testI2(self):
    return self.getI2() >= self.getT2()
  def testI3(self):
    return self.getI3() >= self.getT3()
  def testI4(self):
    return self.getI4() >= self.getT4()
  def testI(self):
    return self.testI1() and self.testI2() and self.testI3()
  def testJ(self):
    return ((self.testI1() and self.testI2()) or self.testI4()) and self.testI3()

##################
## TESTS SECTION #
##################

#if __name__ == "__main__":
#  def testParam(pclass, pname):
#    try:
#      p = pclass.getParam(pname)
#      #print pname + ":", p
#    except Exception as e:
#      print "*** error:", e

#  ip = indexParam()
#  testParam(ip, 'notation')
#  testParam(ip, 'threshold')
#
#  si = selectionIndex()
#  si.setNotation(2001, 'text', 2)
#  si.setNotation(2001, 'ferm', 1)
#  si.setNotation(2001, 'jus', 2)
#  try:
#    print "getI1:", si.getI1()
#    print "getT1", si.getT1()
#    print "testI1", si.testI1()
#  except Exception as e:
#    print "** error2:", e
