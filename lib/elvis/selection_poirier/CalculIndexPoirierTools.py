# -*- coding: utf-8 -*-

import sys
import datetime
from types import *
from elvis.selection_poirier.selectionIndex import *
from elvis.selection_poirier.SelectionPoirierTools import *
from elvis.crbTools import *

class CalculIndexPoirierTools():
  """
  utilise le programme développé par Sylvain sur la base Poirier
  pour calculer les indices de sélection   : SelectionIndex.py

  """

#  def __init__(self):
#    super(CalculIndexPoirierTools, self).__init__()


  def getTest(self):
    """  test """
    tabResp= self.sqlE("""SELECT * FROM hyb1 ;""",())
    return tabResp

# calcul de l' index à partir du numHyb ATTENTION NE TIENT PAS COMPTE DE LA CAMPAGNE DE NOTATION
  def getIndexByNumhynAn(self, numHyb,an1,an2,an3,userGroups):
# sur la base ELVIS
    spt = SelectionPoirierTools()
    resultat2 = spt.getCriteresMesures(numHyb,an1,an2,an3,userGroups)
    index = SelectionIndex()
    for l2 in resultat2:
        for crit in l2:
          index.setNotation(l2['annee'],crit, l2[crit])
    result = {}
    result['I1'] = index.getI1()
    result['I2'] = index.getI2()
    result['I3'] = index.getI3()
    result['I4'] = index.getI4()
    result['T1'] = index.getT1()
    result['T2'] = index.getT2()
    result['T3'] = index.getT3()
    result['T4'] = index.getT4()
    return result

# calcul de l' index à partir du numLot
  def getIndexByNumLotAn(self, numLot,an1,an2,an3,userGroups):
    spt = SelectionPoirierTools()
#    resultat2=spt.getCriteresMesuresByNumLot(numLot,an1,an2,an3)
    resultat2=spt.getCriteresMesuresByNumLotOnCampagne(numLot,an1,an2,an3,userGroups)
    index = SelectionIndex()
    for l2 in resultat2:
        for crit in l2:
          index.setNotation(l2['annee'],crit, l2[crit])
    result = {}
    result['I1'] = index.getI1()
    result['I2'] = index.getI2()
    result['I3'] = index.getI3()
    result['I4'] = index.getI4()
    result['T1'] = index.getT1()
    result['T2'] = index.getT2()
    result['T3'] = index.getT3()
    result['T4'] = index.getT4()
    return result

# Mise à jour de la base Elvis pour une année de référence
  def updateElvisPearByReferenceYear(self, annee,usergroup):
    resultat='';
    spt = SelectionPoirierTools()
#    listeLot=spt.getAllLotsPourIndex(annee,usergroup);
#    for lot in listeLot :
#      numLot= lot['numero_lot']
#      an1=spt.getValeurNotationsByNumLot(numLot,'année a')
#      resultat=resultat+ ' '+ str(an1[0]['valeur_notation'])
#      if(an1[0]!='.'):
#        an1=an1[0]['valeur_notation']
#      an2=spt.getValeurNotationsByNumLot(numLot,'année b')
#      if(an2[0]!='.'):
#        an2=an2[0]['valeur_notation']
#      an3=spt.getValeurNotationsByNumLot(numLot,'année c')
#      if (an3[0]!='.'):
#        an3=an3[0]['valeur_notation']
#      if (an2[0]=='.'):
#        an2=an1
#      if (an3[0]=='.'):
#        an3=an2

    listeLot=spt.getAllIdLotsPourIndex(annee,usergroup);
    for lot in listeLot :
      idLot = lot['id_lot']
      numLot = lot['numero_lot']
      an1=spt.getValeurNotationsByIdLot(idLot,'année a')
      if(an1[0]!='.'):
        an1=an1[0]['valeur_notation']
      an2=spt.getValeurNotationsByIdLot(idLot,'année b')
      if(an2[0]!='.'):
        an2=an2[0]['valeur_notation']
      an3=spt.getValeurNotationsByIdLot(idLot,'année c')
      if (an3[0]!='.'):
        an3=an3[0]['valeur_notation']
      if (an2[0]=='.'):
        an2=an1
      if (an3[0]=='.'):
        an3=an2

      try:
        index=self.getIndexByNumLotAn(numLot,an1,an2,an3,usergroup)
        indice= 'elimine = 0'
        elimine=0
        if(an1!=an2!=an3) and not(index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3'] or index['I4']<index['T4'] ):
          indice='elimine = 9'
          elimine=9
        if (index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3'] or index['I4']<index['T4'] ):
          indice= 'elimine = 8'
          elimine=8

      except Exception as e:
        indice='erreur de calcul des indices'
      resultat=resultat+ numLot+' '+indice+'\n'

      if (indice!='erreur de calcul des indices'):
#      Notation éliminé et date vérifiés ok
        crb = CrbTools()
        dateElim= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
        notations=crb.getNotationsByLot(idLot)
        for note in notations:
            if note['type_notation']== 'éliminé':
                idNot=note['id_notation']
                crb.updateNotation(idNot,dateElim,'valeur calculée par CalculIndexPoirierTools',None,'éliminé',elimine,'fd')
#        idNotation=crb.createNotation(dateElim,None,None,'date élimination',dateElim,'fd')[0]['id_notation']
#        idGroupe=crb.getGroupeByLot(numLot)[0]['id_groupe']
#        crb.setGroupeANotation(idGroupe,idNotation)
    return  'nombre de arbres évalués : '+str(len(listeLot))+'\n'+str(resultat)


# Mise à jour de la base Elvis pour un idLot
  def updateElvisPearIndexSelectionByLot(self, idLot, numLot,userGroups):
    resultat='';
    spt = SelectionPoirierTools()

    an1=spt.getValeurNotationsByIdLot(idLot,'année a')
    if(an1[0]!='.'):
      an1=an1[0]['valeur_notation']
    an2=spt.getValeurNotationsByIdLot(idLot,'année b')
    if(an2[0]!='.'):
      an2=an2[0]['valeur_notation']
    an3=spt.getValeurNotationsByIdLot(idLot,'année c')
    if (an3[0]!='.'):
      an3=an3[0]['valeur_notation']
    if (an2[0]=='.'):
      an2=an1
    if (an3[0]=='.'):
      an3=an2

    try:
        index=self.getIndexByNumLotAn(numLot,an1,an2,an3,userGroups)
        indice= 'elimine = 0'
        elimine=0
#        if(an1!=an2!=an3) and not(index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3'] or index['I4']<index['T4'] ):
#          indice='elimine = 9'
#          elimine=9
#        if (index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3'] or index['I4']<index['T4'] ):
#          indice= 'elimine = 8'
#          elimine=8
        if(an1!=an2!=an3) and not(index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3']  ):
          indice='elimine = 9'
          elimine=9
        if (index['I1']<index['T1'] or index['I2']<index['T2'] or index['I3']<index['T3'] ):
          indice= 'elimine = 8'
          elimine=8
          if ( index['I4'] > index['T4']):
            indice= 'elimine = 8->0'
            elimine=0
    except Exception as e:
        indice='erreur de calcul des indices'
    resultat= str(numLot)+' '+str(indice)+' '+str(index['I1'])+'/'+str(index['T1'])+' ' + str(index['I2'])+'/'+str(index['T2'])+' ' +str( index['I3'])+'/'+str(index['T3'])+' ' + str(index['I4'])+'/'+str(index['T4'])+' '+str(an1)+' '+str(an2)+' '+str(an3)+' '

    if (indice!='erreur de calcul des indices'):
#      Notation éliminé et date vérifiés ok
        crb = CrbTools()
        dateElim= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
        spt.createNotationNumLot(numLot ,'date élimination',dateElim ,'01/01/0001',None, None)
        notations=crb.getNotationsByLot(idLot)
        for note in notations:
            if note['type_notation']== 'éliminé':
                idNot=note['id_notation']
                crb.updateNotation(idNot,dateElim,'CalculIndexPoirierTools',None,'éliminé',elimine,'fd')

    return  'resultat : '+ str(resultat)
