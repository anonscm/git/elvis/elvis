# -*- coding: utf-8 -*-

import sys
from types import *
from elvis.DbConnection import *
from elvis.SessionTools import *
from elvis.crbTools import *
import socket
import datetime

from elvis import NotationTools as NT
from elvis import PlantTools as PT

class SelectionPoirierTools(DbConnection):
    """
    Classe contenant des méthodes pour interagir avec la base sélection.
    Cette classe contient la totalité des méthodes appelées par les services
    """
# copie de crbTools
    def createNotation(self,date,remarque,site,type_notation,valeur,observer):
        typeNotation = self.getTypeNotation(type_notation)[0];
        type_valeur = typeNotation["type"];
        type_notation = typeNotation["id_type_notation"]
        if (type_valeur =="integer"):
            return self.sqlI("INSERT INTO notation.notation_integer (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="texte"):
            return self.sqlI("INSERT INTO notation.notation_texte (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="double"):
            return self.sqlI("INSERT INTO notation.notation_double (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="date"):
            return self.sqlI("INSERT INTO notation.notation_date (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))


#//--------------------------------------- requetes selection poirier
    def getTypesNotationsByContexte(self, contexte):
        return self.sqlE("""select T.name AS nom, T.id_type_notation AS id from notation.context N
                            JOIN notation.lien_contexte_notation L ON N.id=L.id_context
                            JOIN notation.notation_type T ON T.id_type_notation=L.id_notation_type
                            WHERE N.name=%s
                            ORDER BY L.rank""",(contexte,))

    def getTypesNomPoirier(self,userGroup):
        req = """SELECT DISTINCT TNV.value AS valeur, TNV.id_type_nom AS id FROM plant.variety_name_type TNV
                 JOIN plant.variety_name NV ON NV.id_variety_name_type = TNV.id_type_nom
                 JOIN plant.accession C ON C.id_variety= NV.id_variety
                 JOIN plant.tree A ON A.id_accession=C.id
                 JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
                 JOIN plant.group_has_usergroup GG ON GG.id_group=GAL.id_group
                 WHERE GG.id_usergroup = ANY(%s)
                 ORDER BY TNV.value, TNV.id_type_nom"""
        return self.sqlE(req, (userGroup,))

    def getAllValeursByNom(self, nom,userGroup ):
        return self.sqlE("""SELECT DISTINCT (NV.value) as nom    FROM plant.variety_name_type TNV
                            JOIN plant.variety_name NV ON NV.id_variety_name_type = TNV.id_type_nom
                            JOIN plant.accession C ON C.id_variety= NV.id_variety
                            JOIN plant.tree A ON A.id_accession=C.id
                            JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
                            JOIN plant.group_has_usergroup GG ON GG.id_group=GAL.id_group
                            WHERE GG.id_usergroup = ANY(%s) AND TNV.value=%s
                            ORDER BY nom""",(userGroup,nom,))

    def getAllValeurByNotation(self,type_notation,userGroup):
        self.cur.execute("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE name=%s;",(type_notation,))
        rows = self.cur.fetchall()
        type_notation = rows[0]["type"]
        id_type_notation = rows[0]["id_type_notation"]
        return self.sqlE("""SELECT distinct value as valeur FROM notation.notation_"""+type_notation+""" N
                            JOIN plant.group_has_notation GAN ON GAN.id_notation=N.id_notation
                            JOIN plant.group_has_usergroup GG ON GG.id_group=GAN.id_group
                            WHERE N.id_type_notation=%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY value ASC""",(id_type_notation,userGroup,))

    def getTypeNotation(self,type_notation):
        return self.sqlE("""SELECT TN.id_type_notation, TN.name AS nom, TN.type, CTN.id, CTN.code, CTN.value AS valeur, CTN.id_type_notation AS type_notation FROM notation.notation_type TN
                           LEFT JOIN notation.notation_type_constraint CTN ON CTN.id_type_notation= TN.id_type_notation
                           WHERE TN.name =%s""",(type_notation,))

    def getLocHyb(self, groupe):
        requete= """SELECT NV.value AS nom   FROM plant.group G
                                 JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                 JOIN plant.accession C ON C.id=A.id_accession
                                 JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                                 JOIN plant.variety_name_type TNV ON NV.id_variety_name_type=TNV.id_type_nom
                                 WHERE TNV.value='loc_hyb' AND G.id_group= """+str(groupe)
        self.cur.execute(requete)
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows[0]['nom']
        return ''
    def getNumHyb(self, groupe):
        requete= """SELECT NV.value AS nom   FROM plant.group G
                                 JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                 JOIN plant.accession C ON C.id=A.id_accession
                                 JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                                 JOIN plant.variety_name_type TNV ON NV.id_variety_name_type=TNV.id_type_nom
                                 WHERE TNV.value='numéro hybride' AND G.id_group= """+str(groupe)
        self.cur.execute(requete)
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows[0]['nom']
        return ''
    def getMaxNumHyb(self, groupe):
        self.cur.execute("""SELECT MAX(CAST (NV.value  AS INTEGER))  FROM  plant.group_has_usergroup Ghg
                                 JOIN plant.group_has_lot GAL ON GAL.id_group=GHG.id_group
                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                 JOIN plant.accession C ON C.id=A.id_accession
                                 JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                                 JOIN plant.variety_name_type TNV ON NV.id_variety_name_type=TNV.id_type_nom
                                 WHERE TNV.value='numéro hybride' AND GHG.id_usergroup= ANY(%s)  AND NV.value ~ '^[0-9]*$'""", (groupe, ))
        n = self.cur.fetchall()
        if n is None :
          return 0
        return n


    def getNumIntro(self, groupe):
        requete= """SELECT NV.value AS nom   FROM plant.group G
                                 JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                 JOIN plant.accession C ON C.id=A.id_accession
                                 JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                                 JOIN plant.variety_name_type TNV ON NV.id_variety_name_type=TNV.id_type_nom
                                 WHERE TNV.value='numéro introduction' AND G.id_group= """+str(groupe)
        self.cur.execute(requete)
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows[0]['nom']
        return ''

    # renvoie la liste des hybrides en fonction de la liste des arguments des recherche (types de noms et types de notations)
    # en premier établi la liste des id_group selon les noms et notations
    # en second retourne la liste des arbres selon la liste des id_group
    def getHybridBySearch(self,listeArguments):
        requeteNom=''
        requeteNot=''
        for i in listeArguments :
            if i[0:9]=='TNV.value' :
                requeteNom=requeteNom+i+ ' AND '
            if i[0:8]=='NV.value' :
                requeteNom=requeteNom+i+ ' AND '
            if i[0:9]=='TNOT.name' :
                typeNot=self.getTypeNotation(i[13:len(i)-2])
                typeNot=typeNot[0]["type"]
                requeteNot=requeteNot+'('+i+ ' AND '
            if i[0:9]=='NOT.value' :
#                requeteNot=requeteNot+' valeur_notation_'+typeNot+ ' ' +i[11:]+ ')  OR '
                requeteNot=requeteNot+' '+typeNot+'.value ' +i[10:]+ ')  OR '
        rows=[]
       #requete sur les types de nom
        if len(requeteNom)>0 :
            requeteNom="""SELECT DISTINCT id_group FROM plant.group_has_lot GAL
                                           LEFT JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                           LEFT JOIN plant.accession C ON C.id=A.id_accession
                                           LEFT JOIN plant.variety_name NV ON NV.id_variety = C.id_variety
                                           LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
                                           WHERE """+requeteNom[:len(requeteNom)-5]

        # requete sur les notations Faire un split et un intersect pour avoir une recherche de type AND
        if len(requeteNot)>0 :
            requeteNotations=''
            requeteNotSplit=requeteNot.split(' OR ')
            for reqNot in requeteNotSplit :
                if len(reqNot) >10:
                    requeteNotations=requeteNotations+"""SELECT DISTINCT id_group FROM plant.group_has_notation GAN LEFT JOIN notation.notation_texte TEXTE ON GAN.id_notation = TEXTE.id_notation
                                                                               LEFT JOIN notation.notation_integer INTEGER ON GAN.id_notation= INTEGER.id_notation
                                                                               LEFT JOIN notation.notation_double DOUBLE ON GAN.id_notation= DOUBLE.id_notation
                                                                               LEFT JOIN notation.notation_date DATE ON GAN.id_notation= DATE.id_notation
                                                                               LEFT JOIN notation.notation CN ON CN.id_notation=GAN.id_notation
                                                                               LEFT JOIN notation.notation_type TNOT ON TNOT.id_type_notation= CN.id_type_notation
                                                                               WHERE """+reqNot
                    requeteNotations= requeteNotations+ " INTERSECT "
            requeteNot= requeteNotations[:len(requeteNotations)-11]

        #requete pour ne sélectionner que les hybrides
        requeteHyb="""SELECT DISTINCT id_group FROM plant.group_has_lot GAL
                                           LEFT JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                           LEFT JOIN plant.accession C ON C.id=A.id_accession
                                           LEFT JOIN plant.variety_name NV ON NV.id_variety = C.id_variety
                                           LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
                                           WHERE TNV.value~*'ro hybride' """
        if len(requeteNom)>0 :
            if len(requeteNot)>0 :
                requete= requeteNom +"  INTERSECT " +requeteNot+"  INTERSECT " +requeteHyb
            else :
                requete= requeteNom +"  INTERSECT " +requeteHyb
        else :
            requete= requeteNot+"  INTERSECT " +requeteHyb
        self.cur.execute(requete)
        rows = self.cur.fetchall()
        # matériel vegetal à partir des id_group
        listeIdGroupe=[]
        for i in rows :
            listeIdGroupe.append(i["id_group"])
        lg=str(listeIdGroupe).strip('[]')
        requeteArbre="""SELECT A.name AS arbre,C.introduction_name AS "nom accession",G.id_group FROM plant.group G
                                 JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
                                 JOIN plant.accession C ON C.id=A.id_accession
                                 WHERE G.id_group= ANY(\'{"""+lg+"""}\')"""
        self.cur.execute(requeteArbre)
        rows = self.cur.fetchall()
        #Ajout des loc_hyb et num_hyb
        for k in range(len(rows)):
            rows[k]["loc_hyb"]= self.getLocHyb(rows[k]["id_group"]);
            rows[k]["num_hyb"]=self.getNumHyb(rows[k]["id_group"]) ;
            rows[k]["num_intro"]=self.getNumIntro(rows[k]["id_group"]) ;
        return rows

# TODO delete because not used 24/01/2018
    # renvoie la liste des arbres en fonction de la liste des arguments des recherche (types de noms et types de notations)
    # en premier établi la liste des id_group selon les noms et notations
    # en second retourne la liste des arbres selon la liste des id_group
#    def getTreeBySearch(self,listeArguments):
#        requeteNom=''
#        requeteNot=''
#        for i in listeArguments :
#            if i[0:9]=='TNV.value' :
#                requeteNom=requeteNom+i+ ' AND '
#            if i[0:8]=='NV.value' :
#                requeteNom=requeteNom+i+ ' AND '
#            if i[0:9]=='TNOT.name' :
#                typeNot=self.getTypeNotation(i[13:len(i)-2])
#                typeNot=typeNot[0]["type"]
#                requeteNot=requeteNot+'('+i+ ' AND '
#            if i[0:9]=='NOT.value' :
##                requeteNot=requeteNot+' valeur_notation_'+typeNot+ ' ' +i[11:]+ ')  OR '
#                requeteNot=requeteNot+' '+typeNot+'.value = ' +i[11:]+ ')  OR '
#        rows=[]
#       #requete sur les types de nom
#        if len(requeteNom)>0 :
#            requeteNom="""SELECT DISTINCT id_group FROM plant.group_has_lot GAL
#                                           LEFT JOIN plant.tree A ON A.id_lot=GAL.id_lot
#                                           LEFT JOIN plant.accession C ON C.id=A.id_accession
#                                           LEFT JOIN plant.variety_name NV ON NV.id_variety = C.id_variety
#                                           LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
#                                           WHERE """+requeteNom[:len(requeteNom)-5]
#        # requete sur les notations Faire un split et un intersect pour avoir une recherche de type AND
#        if len(requeteNot)>0 :
#            requeteNotations=''
#            requeteNotSplit=requeteNot.split(' OR ')
#            for reqNot in requeteNotSplit :
#                if len(reqNot) >10:
#                    requeteNotations=requeteNotations+"""SELECT DISTINCT id_group FROM plant.group_has_notation GAN LEFT JOIN notation.notation_texte TEXTE ON GAN.id_notation= TEXTE.id_notation
#                                                                               LEFT JOIN notation.notation_integer INTEGER ON GAN.id_notation= INTEGER.id_notation
#                                                                               LEFT JOIN notation.notation_double DOUBLE ON GAN.id_notation= DOUBLE.id_notation
#                                                                               LEFT JOIN notation.notation_date DATE ON GAN.id_notation= DATE.id_notation
#                                                                               LEFT JOIN notation.notation CN ON CN.id_notation=GAN.id_notation
#                                                                               LEFT JOIN notation.notation_type TNOT ON TNOT.id_type_notation= CN.id_type_notation
#                                                                               WHERE """+reqNot
#                    requeteNotations= requeteNotations+ " INTERSECT "

#            requeteNot= requeteNotations[:len(requeteNotations)-11]

#        if len(requeteNom)>0 :
#            if len(requeteNot)>0 :
#                requete= requeteNom +"  INTERSECT " +requeteNot
#            else :
#                requete= requeteNom
#        else :
#            requete= requeteNot
#        self.cur.execute(requete)
#        rows = self.cur.fetchall()
#        # matériel vegetal à partir des id_group
#        listeIdGroupe=[]
#        for i in rows :
#            listeIdGroupe.append(i["id_group"])
#        lg=str(listeIdGroupe).strip('[]')
#        requeteArbre="""SELECT A.name AS arbre,C.introduction_name AS "nom accession",G.id_group FROM plant.group G
#                                 JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
#                                 JOIN plant.tree A ON A.id_lot=GAL.id_lot
#                                 JOIN plant.accession C ON C.id=A.id_accession
#                                 WHERE G.id_group= ANY(\'{"""+lg+"""}\')"""
#        self.cur.execute(requeteArbre)
#        rows = self.cur.fetchall()
#        #Ajout des loc_hyb et num_hyb
#        for k in range(len(rows)):
#            rows[k]["loc_hyb"]= self.getLocHyb(rows[k]["id_group"]);
#            rows[k]["num_hyb"]=self.getNumHyb(rows[k]["id_group"]) ;
#            rows[k]["num_intro"]=self.getNumIntro(rows[k]["id_group"]) ;
#        return rows

    def getValeurNotationsByNumHyb(self, numHyb,typeNot):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s
          ORDER BY N.date DESC"""
        else:
            requete= """SELECT CAST( N.value AS VARCHAR(10) ) AS valeur_notation, TN.name AS nom FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numHyb,typeNot))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return  '...'

    def getValeurNotationsByIdLot(self, idLot,typeNot):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom,A.name AS numero_lot FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.id_lot=%s AND TN.name=%s
          ORDER BY N.date DESC"""

        else:
            requete= """SELECT CAST( N.value AS VARCHAR(10) ) AS valeur_notation, TN.name AS nom,A.name AS numero_lot FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.id_lot=%s AND TN.name=%s
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(idLot,typeNot))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return  '.'

    def getValeurNotationsByNumLot(self, numLot,typeNot):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom,A.name AS numero_lot FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.name=%s AND TN.name=%s
          ORDER BY N.date DESC"""

        else:
            requete= """SELECT CAST( N.value AS VARCHAR(10) ) AS valeur_notation, TN.name AS nom,A.name AS numero_lot FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.name=%s AND TN.name=%s
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numLot,typeNot))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return  '.'

    def getValeurNotationsByNumHybAn(self, numHyb,typeNot,an,usergroup):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          JOIN experiment.experiment_has_notation EHN ON EHN.id_notation=N.id_notation
          JOIN experiment.experiment E ON E.id=EHN.id_experiment
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
          WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s AND E.name=%s AND EHU.id_usergroup= ANY(%s)
          ORDER BY N.date DESC"""

        else:
            requete= """SELECT CAST( N.value AS VARCHAR ) AS valeur_notation, TN.name AS nom FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot =A.id_lot
          JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          JOIN experiment.experiment_has_notation EHN ON EHN.id_notation=N.id_notation
          JOIN experiment.experiment E ON E.id=EHN.id_experiment
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
          WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s AND E.name=%s AND EHU.id_usergroup= ANY(%s)
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numHyb,typeNot,an,usergroup))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return None

    def getValeurNotationsByNumHybCampagne(self, numHyb,typeNot,campagne,groupe):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        requete = ""
        if typeN == 'date':
            requete += "SELECT to_char(N.value,'DD/MM/YYYY' )"
        else:
            requete += "SELECT CAST( N.value AS VARCHAR )"
        requete += """ AS valeur_notation, N.id_notation, TN.name AS nom, E.name
          FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type = TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete = NV.id_variety
          JOIN plant.accession C ON C.id_variety = V.id_variete
          JOIN plant.tree A ON A.id_accession = C.id
          JOIN notation.lot_has_notation GAN ON GAN.id_lot = A.id_lot
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation = GAN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
          JOIN experiment.experiment_has_notation EHN ON EHN.id_notation = N.id_notation
          JOIN experiment.experiment E ON E.id = EHN.id_experiment
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = E.id
          WHERE TNV.value='numéro hybride' AND NV.value = %s AND TN.name = %s
          AND E.name = %s AND EHU.id_usergroup = ANY(%s)
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numHyb,typeNot,campagne,groupe))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return None

    def deteleNotationNumHybCampagne(self,numHyb,typeNot,campagne,groupe,value) :
        notation=self.getValeurNotationsByNumHybCampagne(numHyb,typeNot,campagne,groupe)[0]
        if( notation['valeur_notation']== value) :
          idNotation= notation['id_notation']
          self.sqlI("DELETE FROM plant.group_has_notation WHERE id_notation=%s RETURNING *",(idNotation,))
          self.sqlI("DELETE FROM notation.notation_has_usergroup WHERE id_notation=%s RETURNING *",(idNotation,))
          self.sqlI("DELETE FROM experiment.experiment_has_notation WHERE id_notation=%s RETURNING *",(idNotation,))
          self.sqlI("DELETE FROM notation.notation WHERE id_notation=%s RETURNING *",(idNotation,))
        return idNotation

    def updateNotationNumHybCampagne(self,numHyb,typeNot,campagne,groupe,value) :
        notation=self.getValeurNotationsByNumHybCampagne(numHyb,typeNot,campagne,groupe)[0]
        idNotation= notation['id_notation']
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        requeteUpdate=""" UPDATE notation.notation_"""+typeN+""" SET value=\'"""+str(value)+"""\' WHERE id_notation= """+str(idNotation)+""" RETURNING * """
        self.cur.execute(requeteUpdate)
        rows = self.cur.fetchall()
        self.conn.commit()
        rows [0]['id_notation']
        return 'ok '+ str( rows [0]['id_notation'])



    def addNotationToExperiment(self, idNotation, name,usergroup) :
        """
        Duplicated in NotationTools.linkNotationsToExperiment
        """
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        requete = """ SELECT id FROM experiment.experiment E
                      JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
                      WHERE name=%s AND EHU.id_usergroup= ANY(%s)"""
        self.cur.execute(requete,(name,usergroup))
        idExperiment = self.cur.fetchall()[0]['id']

        try :
          self.cur.execute("INSERT INTO experiment.experiment_has_notation (id_notation, id_experiment) VALUES (%s,%s) RETURNING id_experiment",(idNotation, idExperiment))
        except Exception as e:
          self.conn.rollback()
          self.cur.close()
          self.conn.close()
          raise e
        else :
          self.conn.commit()
        return self.cur.fetchall()[0]['id_experiment']


    def getValeurNotationsByNumLotAn(self, numLot,typeNot,an):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom FROM  plant.tree A
          JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation= LHN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.name=%s AND TN.name=%s AND extract(year from N.date)=%s
          ORDER BY N.date DESC"""

        else:
            requete= """SELECT CAST( N.value AS VARCHAR(10) ) AS valeur_notation, TN.name AS nom FROM plant.tree A
          JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation= LHN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
          WHERE  A.name=%s AND TN.name=%s AND extract(year from N.date)=%s
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numLot,typeNot,an))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return None

    def getValeurNotationsByNumLotCampagne(self, numLot,typeNot,campagne,groupe):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if typeN=='date':
            requete= """SELECT to_char(N.value,'DD/MM/YYYY' ) AS valeur_notation, TN.name AS nom FROM  plant.tree A
          JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation = LHN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
          JOIN experiment.experiment_has_notation EHN ON EHN.id_notation = N.id_notation
          JOIN experiment.experiment E ON E.id = EHN.id_experiment
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = E.id
          WHERE  A.name=%s AND TN.name=%s  AND E.name=%s AND EHU.id_usergroup = ANY(%s)
          ORDER BY N.date DESC"""

        else:
            requete= """SELECT CAST( N.value AS VARCHAR(10) ) AS valeur_notation, TN.name AS nom FROM plant.tree A
          JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
          JOIN notation.notation_"""+typeN+""" N ON N.id_notation = LHN.id_notation
          JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
          JOIN experiment.experiment_has_notation EHN ON EHN.id_notation = N.id_notation
          JOIN experiment.experiment E ON E.id = EHN.id_experiment
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = E.id
          WHERE  A.name=%s AND TN.name=%s  AND E.name=%s AND EHU.id_usergroup = ANY(%s)
          ORDER BY N.date DESC"""
        self.cur.execute(requete,(numLot,typeNot,campagne,groupe))
        rows = self.cur.fetchall()
        if len(rows)>0 :
            return rows
        return None


    def getAnneesNotationsByNumHyb(self, numHyb):
        # requete= """SELECT DISTINCT CAST(  extract (year from N.date)AS INTEGER) AS an  FROM plant.variety_name_type TNV
        #   JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
        #   JOIN plant.variety V ON V.id_variete= NV.id_variety
        #   JOIN plant.accession C ON C.id_variety=V.id_variete
        #   JOIN plant.tree A ON A.id_accession=C.id
        #   JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
        #   JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
        #   JOIN notation.notation N ON N.id_notation=GAN.id_notation
        #   JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
        #   WHERE TNV.value='numéro hybride' AND NV.value=%s  AND  N.date IS NOT NULL
        #   ORDER BY an"""
        requete = """
          select distinct(e.name) as an
          from plant.lot l,
              notation.lot_has_notation lhn,
              notation.notation n,
              experiment.experiment_has_notation ehn,
              experiment.experiment e
          where l.name like %s
          and lhn.id_lot = l.id_lot
          and lhn.id_notation = n.id_notation
          and ehn.id_notation = n.id_notation
          and ehn.id_experiment = e.id
          order by e.name
        """
        self.cur.execute(requete,(numHyb + "-%",))
        rows = self.cur.fetchall()
        return  rows

    def getAnneesNotationsByNumLot(self, numlot):
        # requete= """SELECT DISTINCT CAST(  extract (year from N.date)AS INTEGER) AS an  FROM plant.variety_name_type TNV
        #   JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
        #   JOIN plant.variety V ON V.id_variete= NV.id_variety
        #   JOIN plant.accession C ON C.id_variety=V.id_variete
        #   JOIN plant.tree A ON A.id_accession=C.id
        #   JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
        #   JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
        #   JOIN notation.notation N ON N.id_notation=GAN.id_notation
        #   JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
        #   WHERE A.name=%s  AND  N.date IS NOT NULL
        #   ORDER BY an"""
        requete = """
          select distinct(e.name) as an
          from plant.lot l,
               notation.lot_has_notation lhn,
               notation.notation n,
               experiment.experiment_has_notation ehn,
               experiment.experiment e
          where l.name = %s
          and lhn.id_lot = l.id_lot
          and lhn.id_notation = n.id_notation
          and ehn.id_notation = n.id_notation
          and ehn.id_experiment = e.id
          order by e.name
        """
        self.cur.execute(requete,(numlot,))
        rows = self.cur.fetchall()
        return  rows


    #Renvoie toutes les notations du contexte notation pour un hybride
    def getAllNotationsByNumHyb(self, numHyb,usergroup):
        listeAnnees = self.getAnneesNotationsByNumHyb(numHyb)
        listeNotations=self.getTypesNotationsByContexte('selection poirier (notations hybrides)')
        listeResult=[]
        for typeNot in listeNotations :
            x={}
            for an in listeAnnees:
                x["nom"]=typeNot['nom']
                val=self.getValeurNotationsByNumHybCampagne(numHyb,typeNot['nom'],str(an['an']),usergroup)
                if val :
                    x[an['an']]=val[0]['valeur_notation']
                else :
                    x[an['an']]= None
            listeResult.append(x)
        return listeResult

    #Requete pour le calcul de l'index de sélection : selectionIndex.py(rédigé par Sylvain pour la base poirier)

    def getCriteresMesures(self, numHyb,an1,an2,an3,usergroup):
        listeAns=[an1,an2,an3]
        notations=[]
        for ann in listeAns:
            an=str(ann)
            notAn={}
            notAn["annee"]=an
            notAn["amer"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'amertume',an,usergroup)[0]['valeur_notation'])
            notAn["bletti"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'blettissement',an,usergroup)[0]['valeur_notation'])
            notAn["attrait"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'attrait',an,usergroup)[0]['valeur_notation'])
            notAn["bruni"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'brunissement',an,usergroup)[0]['valeur_notation'])
            notAn["intens_arom"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'intensité aromatique',an,usergroup)[0]['valeur_notation'])
            notAn["eq_saveur"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'saveur',an,usergroup)[0]['valeur_notation'])
            notAn["astrin"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'astringence',an,usergroup)[0]['valeur_notation'])
            notAn["qual_arom"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'qualité aromatique',an,usergroup)[0]['valeur_notation'])
            notAn["ferm"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'fermeté',an,usergroup)[0]['valeur_notation'])
            notAn["text"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'texture',an,usergroup)[0]['valeur_notation'])
            notAn["echaud"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'échaudure',an,usergroup)[0]['valeur_notation'])
            notAn["jus"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'jus',an,usergroup)[0]['valeur_notation'])
            notAn["qualite"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'qualité',an,usergroup)[0]['valeur_notation'])
            notAn["qte_prod"]=int(self.getValeurNotationsByNumHybCampagne(numHyb,'production',an,usergroup)[0]['valeur_notation'])
            notations.append(notAn)
        return notations



    #Requete pour le calcul de l'index de sélection : selectionIndex.py(rédigé par Sylvain pour la base poirier) appliqué aux campagnes de notation
    def getCriteresMesuresByNumLotOnCampagne(self, numLot,an1,an2,an3,usergroup):
        listeAns=[an1,an2,an3]
        notations=[]
        for campagne in listeAns:
            notAn={}
            notAn["annee"]=campagne
            notAn["amer"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'amertume',campagne,usergroup)[0]['valeur_notation']  )
            notAn["bletti"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'blettissement',campagne,usergroup)[0]['valeur_notation'] )
            notAn["attrait"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'attrait',campagne,usergroup)[0]['valeur_notation']    )
            notAn["bruni"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'brunissement',campagne,usergroup)[0]['valeur_notation']  )
            notAn["intens_arom"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'intensité aromatique',campagne,usergroup)[0]['valeur_notation'] )
            notAn["eq_saveur"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'saveur',campagne,usergroup)[0]['valeur_notation'] )
            notAn["astrin"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'astringence',campagne,usergroup)[0]['valeur_notation']   )
            notAn["qual_arom"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'qualité aromatique',campagne,usergroup)[0]['valeur_notation'])
            notAn["ferm"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'fermeté',campagne,usergroup)[0]['valeur_notation'] )
            notAn["text"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'texture',campagne,usergroup)[0]['valeur_notation']  )
            notAn["echaud"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'échaudure',campagne,usergroup)[0]['valeur_notation']  )
            notAn["jus"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'jus',campagne,usergroup)[0]['valeur_notation']  )
            notAn["qualite"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'qualité',campagne,usergroup)[0]['valeur_notation'] )
            notAn["qte_prod"]=int(self.getValeurNotationsByNumLotCampagne(numLot,'production',campagne,usergroup)[0]['valeur_notation'] )
            notations.append(notAn)


        return notations

    def updateNotationNumHyb(self, numHyb,typeNot, valeur,an):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        if an==0:
            requete= """SELECT N.id_notation FROM plant.variety_name_type TNV
              JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
              JOIN plant.variety V ON V.id_variete= NV.id_variety
              JOIN plant.accession C ON C.id_variety=V.id_variete
              JOIN plant.tree A ON A.id_accession=C.id
              JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
              JOIN notation.notation_"""+typeN+""" N ON N.id_notation= LHN.id_notation
              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
              WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s
              ORDER BY N.date DESC"""
            self.cur.execute(requete,(numHyb,typeNot))
        else:
            requete= """SELECT N.id_notation FROM plant.variety_name_type TNV
              JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
              JOIN plant.variety V ON V.id_variete= NV.id_variety
              JOIN plant.accession C ON C.id_variety=V.id_variete
              JOIN plant.tree A ON A.id_accession=C.id
              JOIN notation.lot_has_notation LHN ON LHN.id_lot = A.id_lot
              JOIN notation.notation_"""+typeN+""" N ON N.id_notation= LHN.id_notation
              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
              WHERE TNV.value='numéro hybride' AND NV.value=%s AND TN.name=%s AND extract(year from N.date) = %s
              ORDER BY N.date DESC"""
            self.cur.execute(requete,(numHyb,typeNot,an))
        rows = self.cur.fetchall()
        idNot=str(rows[0]['id_notation'])
        valeur=str(valeur)
        requeteUpdate=""" UPDATE notation.notation_"""+typeN+""" SET value =\'"""+valeur+"""\' WHERE id_notation= """+idNot+""" RETURNING * """
        self.cur.execute(requeteUpdate)
        rows2 = self.cur.fetchall()
        self.conn.commit()
        return rows2 [0]['id_notation']

    def createNotationNumHyb(self, numHyb,typeNot, valeur,date,remarque, observer):
        typeNotation=self.getTypeNotation(typeNot)[0]
        type_valeur = typeNotation["type"];
        type_notation = typeNotation["id_type_notation"]
        if date=='01/01/0001':
            date= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
        site=None
        if (type_valeur =="integer"):
            r= self.sqlI("INSERT INTO notation.notation_integer (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="texte"):
            r= self.sqlI("INSERT INTO notation.notation_texte (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="double"):
            r= self.sqlI("INSERT INTO notation.notation_double (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="date"):
            r= self.sqlI("INSERT INTO notation.notation_date (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        requete= """SELECT id_group FROM plant.variety_name_type TNV
          JOIN plant.variety_name NV ON NV.id_variety_name_type=TNV.id_type_nom
          JOIN plant.variety V ON V.id_variete= NV.id_variety
          JOIN plant.accession C ON C.id_variety=V.id_variete
          JOIN plant.tree A ON A.id_accession=C.id
          JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
          WHERE TNV.value='numéro hybride' AND NV.value=%s  """
        self.cur.execute(requete,(numHyb,))
        id_group=self.cur.fetchall()[0]['id_group']
        id_notation=r[0]['id_notation']
        i=self.sqlI("INSERT INTO plant.group_has_notation (id_group, id_notation) VALUES (%s,%s) RETURNING *",(id_group, id_notation))
        return i[0]['id_notation']

    def createNotationNumLot(self, numLot,typeNot, valeur,date,remarque, observer):
        typeNotation=self.getTypeNotation(typeNot)[0]
        type_valeur = typeNotation["type"];
        type_notation = typeNotation["id_type_notation"]
        if date=='01/01/0001':
            date= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
        site=None
        if (type_valeur =="integer"):
            r= self.sqlI("INSERT INTO notation.notation_integer (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="texte"):
            r= self.sqlI("INSERT INTO notation.notation_texte (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="double"):
            r= self.sqlI("INSERT INTO notation.notation_double (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="date"):
            r= self.sqlI("INSERT INTO notation.notation_date (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        requete= """SELECT id_group FROM plant.tree A
          JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
          WHERE A.name=%s  """
        self.cur.execute(requete,(numLot,))

        id_group=self.cur.fetchall()[0]['id_group']
        id_notation=r[0]['id_notation']
        i=self.sqlI("INSERT INTO plant.group_has_notation (id_group, id_notation) VALUES (%s,%s) RETURNING *",(id_group, id_notation))
        return i[0]['id_notation']

    def updateNotationNumLot(self, numLot,typeNot, valeur,date,remarque, notateur):
        typeN=self.getTypeNotation(typeNot)[0]["type"]
        requete= """SELECT N.id_notation FROM plant.tree A
              JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
              JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
              JOIN notation.notation_"""+typeN+""" N ON N.id_notation=GAN.id_notation
              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
              WHERE A.name=%s AND TN.name=%s
              ORDER BY N.date DESC"""
        self.cur.execute(requete,(numLot,typeNot))

        rows = self.cur.fetchall()
        idNot=str(rows[0]['id_notation'])
        valeur=str(valeur)
        requeteUpdate=""" UPDATE notation.notation_"""+typeN+""" SET value =\'"""+valeur+"""\' WHERE id_notation= """+idNot+""" RETURNING * """
        self.cur.execute(requeteUpdate)
        rows2 = self.cur.fetchall()
        self.conn.commit()
        return rows2 [0]['id_notation']

    def updateOrCreateNotationNumHyb(self, numHyb, typeNot, valeur,an,usergroup):
        present=self.getValeurNotationsByNumHybAn(numHyb, typeNot,an,usergroup)
        if present :
            #update notation
            r=self.updateNotationNumHyb(numHyb,typeNot, valeur,an)
            return 'update notation '+str(r)
        else :
            #create notation
            date='01/01/'+str(an)
            remarque=None
            notateur=None
            r=self.createNotationNumHyb( numHyb,typeNot, valeur,date,remarque, notateur)
            campagne= self.sqlE("""SELECT id FROM experiment.experiment where name= %s""",(str(an),))
            idExperiment=campagne[0]['id']
            self.sqlI("""INSERT INTO experiment.experiment_has_notation (id_experiment,id_notation) VALUES(%s,%s) RETURNING *""",(idExperiment,r,))
            return 'create notation '+str(r)

        return 'ok'

    def getlisteParcellesPoirier(self,userGroups):
        return self.sqlE("""SELECT DISTINCT(L.value) as valeur FROM location.place L
                            JOIN location.place_type TL ON TL.id=L.id_place_type
                            JOIN plant.place E ON E.id=L.id_plant_place
                            JOIN plant.tree A ON A.id_lot=E.id_lot
                            JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
                            JOIN plant.group_has_usergroup GG ON GG.id_group=GAL.id_group
                            WHERE GG.id_usergroup = ANY(%s) AND TL.value='parcelle'
                            ORDER BY L.value""",(userGroups,))


    def getListeArbreByParcellePoirier(self, parcelle, campagne, an, notation, floraison, quantite, calibre, groupIds):
      """Get a list of tree ids

      This method return a list of tree ids (lot ids) for one field (parcelle)
      and an experient name (campaign name).
      The trees are filtered on some notations:
      - they must have an "éliminé" notration == 0
      - depending on the "notation" param, they are filtered on thretholds
      (value must be >= to threthold) for three notations: "floraison",
      "production" and "calibre du fruit"

      :param parcelle: Parcelle name
      :param campagne: The experiment / campaign name to use when filtering data
      :param an: Year of first grow (not used for now)
      :param notation: The type of notation file to produce (may be replaced by notation context)
      :param floraison: "Floraison" threthold for filtering data
      :param quantite: "Quantite" threthold for filtering data
      :param calibre: "Calibre" threthold for filtering data
      :param groupIds: User groups to use
      :return: A list of strings formated as <num_hyb>\n<lot name>

      :type parcelle: String
      :type campagne: Integer
      :type an: Integer
      :type notation: String
      :type floraison: Integer
      :type quantite: Integer
      :type calibre: Integer
      :type groupIds: List of Integer
      :rtype: List of String
      """
      nto = NT()
      pto = PT()

      # Notation types
      ntElim = nto.getNotationType(name = "éliminé")
      ntFlo = nto.getNotationType(name = "floraison")
      ntQuant = nto.getNotationType(name = "production")
      ntCalib = nto.getNotationType(name = "calibre du fruit")

      # Location type
      placeType = pto.getLocationType(name = "parcelle")

      # Get all the trees (lotIds) for the location
      placeValue = {"id_place_type" : placeType.getId(), "value_place" : parcelle.strip()}
      listParcelle = set(pto.searchLotIdByPlaceValue(groupIds, placeValue))

      # List of notation critreria to filter trees
      notations = [{'id_notation_type': int(ntElim.getId()), 'value_notation': '>0'}]
      listElimTrees = set(pto.searchLotIdByNotationValue(groupIds, notations))

      listLotId = listParcelle - listElimTrees

      notations =  []
      if notation == "Floraison":
        pass
      elif notation == "Production":
        notations = [
          {
            'id_notation_type': int(ntFlo.getId()),
            'value_notation': '>=' + str(floraison)
          }
        ]
        pass
      elif notation == "Récolte":
        notations = [
          {
            'id_notation_type': int(ntQuant.getId()),
            'value_notation': '>=' + str(quantite)
          },
          {
            'id_notation_type': int(ntCalib.getId()),
            'value_notation': '>=' + str(calibre)
          }
        ]
        pass
      elif notation == "Circonférence":
        pass
      elif notation == "Juvénilite":
        pass
      elif notation == "Silhouette":
        pass
      #elif notation == u'Accident de végétation':
      else:
        pass
      if (len(notations)):
        listLotFiltered = set(pto.searchLotIdByNotationValue(groupIds, notations, campagne))
        listLotId = listLotId & listLotFiltered

      listLotId = list(listLotId)

      listeNumHyb = self.sqlE ("""SELECT A.name AS numero_lot, CAST ( NV.value AS integer)  FROM plant.tree A
                                  INNER JOIN plant.accession C ON C.id=A.id_accession
                                  INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                                  INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
                                  WHERE id_lot = ANY(%s) AND TNV.value='numéro hybride'
                                  ORDER BY value""", (listLotId,))
      liste=[]
      for lot in listeNumHyb:
        liste.append(str(lot['value']) + '\n' + lot['numero_lot'])
      return liste

    def insertListeNotationHybride(self, liste, typeNot,notateur,date,site,remarque,an):
        for i in range(0,len(liste)-3,3) :
            numHyb=liste[i].strip()
            valeur=liste[i+2].strip()
            if valeur=='10' :
                r=self.updateNotationNumHyb( numHyb,'éliminé', 10,0)
            elif len(valeur)>0 :
                r=self.createNotationNumHyb( numHyb,typeNot, valeur,date,remarque, notateur)
                campagne= self.sqlE("""SELECT id FROM experiment.experiment where name= %s""",(str(an),))
                idExperiment=campagne[0]['id']
                self.sqlI("""INSERT INTO experiment.experiment_has_notation (id_experiment,id_notation) VALUES(%s,%s) RETURNING *""",(idExperiment,r,))
        return r

    def insertListeLotsPoirierElimines(self, liste, notateur, date, site, remarque, an):
      for i in range(0, len(liste) - 2, 2):
        numLot = liste[i].strip()
        valeur = liste[i + 1].strip()
        id_notation = self.updateNotationNumLot(numLot, "éliminé", valeur, date, remarque, notateur)
        id_notation = self.createNotationNumLot(numLot, "date élimination", date, date, remarque, notateur)
      return id_notation

    def insertListeNotationDoubleHybride(self, liste, typeNot1,typeNot2,typeNot3,notateur,date,site,remarque,an):
        for i in range(0,len(liste)-5,5) :
            numHyb=liste[i].strip()
            valeur=liste[i+2].strip()
            if valeur=='10' :
                r=self.updateNotationNumHyb( numHyb,'éliminé', 10,0)
            elif len(valeur)>0 :
                r=self.createNotationNumHyb( numHyb,typeNot1, valeur,date,remarque, notateur)
                campagne= self.sqlE("""SELECT id FROM experiment.experiment where name= %s""",(str(an),))
                idExperiment=campagne[0]['id']
                self.sqlI("""INSERT INTO experiment.experiment_has_notation (id_experiment,id_notation) VALUES(%s,%s) RETURNING *""",(idExperiment,r,))
            valeur=liste[i+3].strip()
            if valeur=='10' :
                r=self.updateNotationNumHyb( numHyb,'éliminé', 10,0)
            elif len(valeur)>0 :
                r=self.createNotationNumHyb( numHyb,typeNot2, valeur,date,remarque, notateur)
                campagne= self.sqlE("""SELECT id FROM experiment.experiment where name= %s""",(str(an),))
                idExperiment=campagne[0]['id']
                self.sqlI("""INSERT INTO experiment.experiment_has_notation (id_experiment,id_notation) VALUES(%s,%s) RETURNING *""",(idExperiment,r,))
            valeur=liste[i+4].strip()
            if valeur=='10' :
                r=self.updateNotationNumHyb( numHyb,'éliminé', 10,0)
            elif len(valeur)>0 :
                r=self.createNotationNumHyb( numHyb,typeNot3, valeur,date,remarque, notateur)
                campagne= self.sqlE("""SELECT id FROM experiment.experiment where name= %s""",(str(an),))
                idExperiment=campagne[0]['id']
                self.sqlI("""INSERT INTO experiment.experiment_has_notation (id_experiment,id_notation) VALUES(%s,%s) RETURNING *""",(idExperiment,r,))
        return r

    def getPairLocHybNumHyb(self, lochyb, numhyb,userGroups):
        if lochyb is None :
         nh= self.sqlE ("""SELECT NV.value as nom FROM plant.variety_name NV
              INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
              WHERE TNV.value='loc_hyb' AND NV.id_variety IN (
               SELECT NV.id_variety FROM plant.tree A
               INNER JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
               INNER JOIN plant.group_has_usergroup GG ON GG.id_group=GAL.id_group
               INNER JOIN plant.accession  C ON C.id =A.id_accession
               INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
               INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
               WHERE GG.id_usergroup = ANY(%s) AND TNV.value='numéro hybride' AND NV.value=%s)""",(userGroups,numhyb,))
        if numhyb is None :
         nh= self.sqlE ("""SELECT NV.value as nom FROM plant.variety_name NV
              INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
              WHERE TNV.value='numéro hybride' AND NV.id_variety IN (
               SELECT NV.id_variety FROM plant.tree A
               INNER JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
               INNER JOIN plant.group_has_usergroup GG ON GG.id_group=GAL.id_group
               INNER JOIN plant.accession  C ON C.id =A.id_accession
               INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
               INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
               WHERE GG.id_usergroup = ANY(%s)  AND TNV.value='loc_hyb' AND NV.value=%s)""",(userGroups,lochyb,))
        return nh[0]['nom']

    def getAllCroisement(self,usergroup):
        return self.sqlE("""SELECT CR.name as nom, CR.year AS annee, CR.id FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            WHERE GG.id_usergroup = ANY(%s)
                            ORDER BY CR.name """,(usergroup,))
    def getAllAnCroisement(self,usergroup):
        return self.sqlE("""SELECT DISTINCT( CR.year) AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            WHERE GG.id_usergroup = ANY(%s)
                            ORDER BY annee """,(usergroup,))
    def getAllParentCroisement(self,usergroup):
        return self.sqlE("""SELECT DISTINCT( L.name) AS numero_lot FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            WHERE GG.id_usergroup = ANY(%s)
                            ORDER BY numero_lot""",(usergroup,))

    def getAllVarieteParentCroisement(self,usergroup):
        return self.sqlE("""SELECT DISTINCT(NV.value) FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                            WHERE GG.id_usergroup = ANY(%s)
                            ORDER BY NV.value""",(usergroup,))



    def getAllCroisementByNom (self,nom ,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            WHERE CR.name =%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY CR.name""",(nom, usergroup,))

    def getAllCroisementByAn(self,an,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            WHERE CR.year =%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY CR.name """,(an, usergroup,))
    def getAllCroisementByParentF(self,lot,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            WHERE L.name=%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY L.name""",(lot,usergroup,))
    def getAllCroisementByParentM(self,lot,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            WHERE L.name=%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY L.name""",(lot,usergroup,))
    def getAllCroisementByParent(self,lot,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            WHERE L.name=%s AND GG.id_usergroup = ANY(%s)
                            UNION
                            SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            WHERE L.name=%s AND GG.id_usergroup = ANY(%s)""",(lot,usergroup,lot,usergroup,))

    def getAllCroisementByVarieteParentF(self,var,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON C.id=L.id_accession
                            INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                            WHERE NV.value=%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY L.name""",(var,usergroup,))
    def getAllCroisementByVarieteParentM(self,var,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON C.id=L.id_accession
                            INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                            WHERE NV.value=%s AND GG.id_usergroup = ANY(%s)
                            ORDER BY L.name""",(var,usergroup,))
    def getAllCroisementByVarieteParent(self,var,usergroup):
        return self.sqlE("""SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON C.id=L.id_accession
                            INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                            WHERE NV.value=%s AND GG.id_usergroup = ANY(%s)
                            UNION
                            SELECT CR.id,CR.name as nom, CR.year AS annee FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON C.id=L.id_accession
                            INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
                            WHERE NV.value=%s AND GG.id_usergroup = ANY(%s)""",(var,usergroup,var,usergroup,))


    def getCroisementById(self,idCrois):   #est ce bien necessaire cette union ???? SG: En effet, il aurait mieux value faire l'union en python en appelant les 2 méthodes suivante pour ne pas réécrire les requêtes.
        return self.sqlE("""SELECT CR.name as nom, CR.year AS annee
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            WHERE CR.id=%s
                            UNION
                            SELECT CR.name as nom, CR.year AS annee
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            WHERE CR.id=%s
                            """,(idCrois,idCrois,))

    def getCroisementGrpFemById(self,idCrois):
        return self.sqlE("""SELECT CR.name as nom, CR.year AS annee, L.name AS numero_lot
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_female
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            WHERE CR.id=%s
                            """,(idCrois,))

    def getCroisementGrpMalById(self,idCrois):
        return self.sqlE("""SELECT CR.name as nom, CR.year AS annee, L.name AS numero_lot
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.couple CP ON CP.id=CR.id_couple
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=CP.id_group_male
                            INNER JOIN plant.group_has_lot GL ON GL.id_group=GG.id_group
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            WHERE CR.id=%s
                            """,(idCrois,))

    def getPrelevByCrois(self,idCrois):
        return self.sqlE("""SELECT CR.name  AS nom_croisement, P.name  AS nom_prelevement,P.seed_number AS nb_pepins_prel, to_char(P.date,'DD/MM/YYYY' ) AS date_stratification
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            WHERE CR.id=%s
                            """,(idCrois,))
    def getLotsByCrois(self,idCrois):
        return self.sqlE("""SELECT P.name  AS nom_prelevement, LP.name  AS nom_lot, ' ' AS emplacement
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed LP ON LP.id_taking=P.id
                            WHERE CR.id=%s
                            """,(idCrois,))
    def getLotsByCrois(self,idCrois):
        return self.sqlE("""SELECT P.name  AS nom_prelevement, LP.name  AS nom_lot, ' ' AS emplacement
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed LP ON LP.id_taking=P.id
                            WHERE CR.id=%s
                            """,(idCrois,))
    def getNotationsLotsByCrois(self,idCrois):
        return self.sqlE("""SELECT L.name  AS lot, TN.name AS notation, CAST( N.value AS VARCHAR(10) )AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed L ON L.id_taking=P.id
                            INNER JOIN notation.cross_seed_has_notation LPAN ON LPAN.id_lot_pepins=L.id
                            LEFT JOIN notation.notation_integer N ON N.id_notation=LPAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT L.name  AS lot, TN.name AS notation,  CAST( N.value AS VARCHAR(10) )AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed L ON L.id_taking=P.id
                            INNER JOIN notation.cross_seed_has_notation LPAN ON LPAN.id_lot_pepins=L.id
                            LEFT JOIN notation.notation_double N ON N.id_notation=LPAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT L.name  AS lot, TN.name AS notation, N.value AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed L ON L.id_taking=P.id
                            INNER JOIN notation.cross_seed_has_notation LPAN ON LPAN.id_lot_pepins=L.id
                            LEFT JOIN notation.notation_texte N ON N.id_notation=LPAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT L.name  AS lot, TN.name AS notation, to_char(N.value,'DD/MM/YYYY' ) AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN plant_cross.taking P ON P.id_plant_cross=CR.id
                            INNER JOIN plant_cross.cross_seed L ON L.id_taking=P.id
                            INNER JOIN notation.cross_seed_has_notation LPAN ON LPAN.id_lot_pepins=L.id
                            LEFT JOIN notation.notation_date N ON N.id_notation=LPAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
                            """,(idCrois,idCrois,idCrois,idCrois,))
    def getNotationsByCrois(self,idCrois):
        return self.sqlE("""SELECT TN.name AS notation, CAST( N.value AS VARCHAR(10) )AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_integer N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT TN.name AS notation,  CAST( N.value AS VARCHAR(10) )AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_double N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT  TN.name AS notation, N.value AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_texte N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
UNION SELECT TN.name AS notation, to_char(N.value,'DD/MM/YYYY' ) AS valeur
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_date N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s
                            """,(idCrois,idCrois,idCrois,idCrois,))

    def getTypeNotationsByCrois(self,idCrois,typeNot):
        return self.sqlE("""SELECT TN.name AS notation, CAST( N.value AS VARCHAR(10) )AS valeur, N.date AS date_notation
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_integer N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s AND TN.name = %s
UNION SELECT TN.name AS notation,  CAST( N.value AS VARCHAR(10) )AS valeur, N.date AS date_notation
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_double N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s AND TN.name = %s
UNION SELECT  TN.name AS notation, N.value AS valeur, N.date AS date_notation
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_texte N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s AND TN.name = %s
UNION SELECT TN.name AS notation, to_char(N.value,'DD/MM/YYYY' ) AS valeur, N.date AS date_notation
                            FROM plant_cross.plant_cross CR
                            INNER JOIN notation.cross_has_notation CAN ON CAN.id_cross=CR.id
                            LEFT JOIN notation.notation_date N ON N.id_notation=CAN.id_notation
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE CR.id=%s AND TN.name = %s
 ORDER BY date_notation DESC
                            """,(idCrois,typeNot,idCrois,typeNot,idCrois,typeNot,idCrois,typeNot,))

    def addTypeNotationsByCrois(self,idCrois,type_notation,valeur,userName):
        date= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
        idNot= self.createNotation(date,'notation sur id_croisement'+str(idCrois),None,type_notation,valeur,userName)[0]['id_notation']
        self.sqlI("""INSERT INTO notation.cross_has_notation (id_cross,id_notation) VALUES(%s,%s) RETURNING *""",(idCrois,idNot,))
        return 0

    def getAllPlantesFromCrois(self,idCrois,groupe): # attention le numero de croisement doit etre texte et non integer
        NomCrois = self.sqlE("""SELECT name as nom FROM plant_cross.plant_cross WHERE id=%s""",(idCrois,)) [0]['nom']
        return self.sqlE("""SELECT DISTINCT(  L.name ) AS nom, C.introduction_name AS accession, L.id_lot FROM plant.group_has_lot GL
                            INNER JOIN plant.lot L ON L.id_lot=GL.id_lot
                            INNER JOIN plant.accession C ON L.id_accession= C.id
                            INNER JOIN plant.group_has_usergroup GG ON GG.id_group=GL.id_group
                            INNER JOIN plant.group_has_notation GAN ON GAN.id_group=GL.id_group
                            INNER JOIN notation.notation_integer NT ON NT.id_notation =GAN.id_notation
                            INNER JOIN notation.notation_type TN ON TN.id_type_notation=NT.id_type_notation
                            WHERE GG.id_usergroup = ANY(%s) AND TN.name='numéro de croisement'
                                             AND NT.value=%s
                            """,(groupe,NomCrois))

    def getNumHybByNumLot(self,numLot):
        return self.sqlE("""SELECT NV.value AS nom FROM plant.lot L
        INNER JOIN plant.accession C ON C.id=L.id_accession
        INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
        INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
        WHERE L.name=%s AND TNV.value='numéro hybride'
                            """,(numLot,))

    def getNumIntroByNumLot(self,numLot):
        numIntro= self.sqlE("""SELECT NV.value as nom, L.name AS numero_lot FROM plant.lot L
        INNER JOIN plant.accession C ON C.id=L.id_accession
        INNER JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
        INNER JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
        WHERE L.name=%s AND TNV.value='numéro introduction'""",(numLot,))
        if len(numIntro)>0:
            numLot=numLot+'('+numIntro[0]['nom']+')'
        return(numLot)

    def getAllContextes(self,groupe):
        return self.sqlE("""SELECT DISTINCT(name) AS nom_contexte from notation.context CN
WHERE CN.id_usergroup = ANY(%s)
ORDER BY name """,(groupe,))

    def getAllNotationsByContexte(self,context):
        return self.sqlE("""SELECT TN.name AS nom, LCN.rank AS ordre from notation.context CN
LEFT JOIN notation.lien_contexte_notation LCN ON LCN.id_context=CN.id
LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
WHERE CN.name=%s
ORDER BY LCN.rank""",(context,))

    def getAllTypeNotation(self):
        return self.sqlE("""SELECT name AS nom FROM notation.notation_type ORDER BY name""",() )

    def addNotationToContexte(self,contexte,notation,ordre):
        id_type_notation= self.sqlE("""SELECT id_type_notation FROM notation.notation_type WHERE name=%s""",(notation,))[0]['id_type_notation']
        id_contexte= self.sqlE("""SELECT id FROM notation.context WHERE name=%s""",(contexte,))[0]['id']
        return self.sqlI("INSERT INTO notation.lien_contexte_notation  VALUES (%s,%s,%s) RETURNING *",(id_contexte,id_type_notation,ordre))

    def delAllNotationToContexte(self,contexte):
        id_contexte= self.sqlE("""SELECT id FROM notation.context WHERE name=%s""",(contexte,))[0]['id']
        return self.sqlI("DELETE FROM notation.lien_contexte_notation WHERE id_context=%s RETURNING *",(id_contexte,))

    def addPrelevOnCroisment(self,idCrois, nom,nbPepins,dateStrat):
        return self.sqlI(""" INSERT INTO plant_cross.taking (nom,id_plant_cross,seed_number,date) VALUES(%s,%s,%s,%s) RETURNING * """,(nom,idCrois,nbPepins,dateStrat))

    def addLotOnPrelevement(self,idCrois, nomPrelev,nomLot):
        id_prelev= self.sqlE("""SELECT id from plant_cross.taking WHERE id_plant_cross=%s AND nom = %s """ ,( idCrois,nomPrelev,))[0]['id']
        return self.sqlI(""" INSERT INTO plant_cross.cross_seed (name ,id_taking) VALUES(%s,%s) RETURNING *  """,(nomLot,id_prelev,))

    def createNotationOnCroisment(self,idCrois,date,remarque,site,type_notation,valeur,notateur ):
        idNot=self.createNotation(date,remarque,site,type_notation,valeur,notateur)[0]['id_notation']
        return self.sqlI("""INSERT INTO notation.cross_has_notation(id_cross,id_notation) VALUES(%s,%s) RETURNING * """, (idCrois,idNot,))

    def createNotationOnLot(self,idCrois,nom_lot,date,remarque,site,type_notation,valeur,notateur ):
        idNot=self.createNotation(date,remarque,site,type_notation,valeur,notateur)[0]['id_notation']
        idlot=self.sqlE("""select L.id from plant_cross.cross_seed L
                           join plant_cross.taking P ON P.id= L.id_taking
                           join plant_cross.plant_cross C ON C.id =P.id_plant_cross
                           WHERE C.id=%s AND L.name = %s""", (idCrois,nom_lot,))[0]['id']
        return self.sqlI("""INSERT INTO notation.cross_seed_has_notation(id_lot_pepins,id_notation) VALUES(%s,%s) RETURNING * """, (idlot,idNot,))

    def getAllLots(self,groupe):
        return self.sqlE("""SELECT A.name AS numero_lot from plant.tree A
          INNER JOIN plant.group_has_lot GAL ON GAL.id_lot=A.id_lot
          INNER JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
          INNER JOIN users.usergroup U ON U.id=GAG.id_usergroup
          WHERE id_usergroup = ANY(%s)
          ORDER BY A.name""",(groupe,))


#fonction sur les couples et croisements
    def getIdLotByNumeroLot(self, lot):
        return self.sqlE("SELECT id_lot FROM plant.tree WHERE name = %s;", (lot,))
    def createGroupe_a_lot(self,id_group,id_lot):
        return self.sqlI("INSERT INTO plant.group_has_lot (id_group,id_lot) VALUES (%s,%s) RETURNING *",(id_group,id_lot))
    def maxIdGroup(self):
        return self.sqlE("SELECT MAX(id_group) FROM plant.group",())
    def getIdGroupeByNom(self, nom):
        return self.sqlE("SELECT id FROM users.usergroup WHERE name = %s;", (nom,))
    def getIdGroupeByLots(self, lot):
        return self.sqlE("SELECT id_group FROM  plant.group_has_lot A JOIN plant.tree B ON A.id_lot=B.id_lot WHERE name = %s;", (lot,))
    def createGroupe(self,id_group, nom, description):
        return self.sqlI("INSERT INTO plant.group (id_group,name,description) VALUES (%s,%s,%s) RETURNING *",(id_group,nom,description))

    def createGroupe_a_ghgroup(self,id_group, id_usergroup):
        return self.sqlI("INSERT INTO plant.group_has_usergroup (id_group,id_usergroup) VALUES (%s,%s) RETURNING *",(id_group,id_usergroup))

    def createIdGroupeByLots(self, listeLot,writeGroup):
        idg=self.maxIdGroup()[0]["max"]+1
        self.createGroupe(idg,listeLot,'groupe croisement')
#        self.createGroupe_a_ghgroup (idg,writeGroup[0]);
        for w in writeGroup:
          self.createGroupe_a_ghgroup (idg,w);
        for i in listeLot :
          idLot=self.getIdLotByNumeroLot(i)[0]["id_lot"]
          self.createGroupe_a_lot(idg,idLot)
        return (idg)

    def getIdCouple(self,gr_fem, gr_mal):
        return self.sqlE("SELECT id FROM plant_cross.couple WHERE id_group_female = %s AND id_group_male = %s ;",(gr_fem, gr_mal,))
    def insertCouple(self,gr_fem, gr_mal):
        return self.sqlI("INSERT INTO plant_cross.couple (id_group_female, id_group_male) VALUES(%s, %s) RETURNING id;",(gr_fem, gr_mal,))

    def getIdCroisement(self,annee, id_couple):
        return self.sqlE("SELECT id, name  AS nom, year  AS annee, id_couple AS id_couple FROM plant_cross.plant_cross WHERE year  = %s AND id_couple = %s ;",(annee, id_couple,))

    def insertCroisement(self, nom, annee, id_couple):
        return self.sqlI("INSERT INTO plant_cross.plant_cross (name, year, id_couple) VALUES(%s, %s, %s) RETURNING *;", (nom, annee, id_couple,))

    def getOrCreateGroupeArbres(self, listeLot,writeGroup) :
        listeIdg = []    #liste des idgroupes différents
        compteurIdg = [] # liste de dictionnaire {idgroupe, compteur}
        for i in listeLot :
            groupeIdg=self.getIdGroupeByLots(i) #un lot peut appartenir à plusieurs groupes
            for ii in groupeIdg :
                idg=ii["id_group"]
                if idg :
                    if idg not in listeIdg :
                        listeIdg.append(idg)
                        groupe={'idgroupe':idg,'compteur':1}
                        compteurIdg.append(groupe)
                    else:
                        for j in compteurIdg :
                            if j["idgroupe"]==idg :
                                j["compteur"]=j["compteur"]+1
        #teste si un groupe contient tous les lots alors compteur = nombre de lots
        for g in compteurIdg :
            if g["compteur"]==len(listeLot) :
                return(g["idgroupe"])
        #cree un groupe avec tous ces numeros de lot
        return(self.createIdGroupeByLots(listeLot,writeGroup))

    def getAllContrainteNotation(self,userGroup):
        return self.sqlE("""SELECT name AS "Notation",type, value AS "Contrainte" FROM notation.notation_type TN
                            JOIN notation.notation_type_constraint CTN ON CTN.id_type_notation=TN.id_type_notation
                            ORDER BY name""",())
# contraintes sur notations
    def getTypeByNotation(self,nom):
        return self.sqlE("SELECT type FROM notation.notation_type WHERE name = %s ",(nom,))

    def CreateOrUpdateConstraint(self,nom,valeur):
        idTypeNot = self.sqlE("""SELECT id_type_notation from notation.notation_type WHERE name=%s """,(nom,))[0]['id_type_notation']
        idContraint=self.sqlE("""SELECT id from notation.notation_type TN
                            LEFT JOIN notation.notation_type_constraint CTN ON CTN.id_type_notation=TN.id_type_notation
                            WHERE TN.name=%s""",(nom,))  [0]['id']
        if idContraint ==None :
            return self.sqlI(""" INSERT INTO notation.notation_type_constraint (code,value,id_type_notation) VALUES (NULL,%s,%s) RETURNING * """,(valeur,idTypeNot,))
        else :
            return self.sqlI(""" UPDATE   notation.notation_type_constraint  SET value=%s WHERE id=%s  RETURNING * """,(valeur,idContraint,))
# creation groupe
    def linkPlantGroupToUserGroup(self, id_usergroup, id_group, auto_commit = True):
        sql = "INSERT INTO plant.group_has_usergroup (id_group,id_usergroup) VALUES (%s,%s)"
        try:
            self.cur.execute(sql, (id_group, id_usergroup))
        except Exception as e:
            self.conn.rollback()
        else:
            if (auto_commit):
                self.conn.commit()

    def setNotationAUsergroup(self,id_notation,id_usergroup,ecriture) :
        return self.sqlI("INSERT INTO notation.notation_has_usergroup (id_notation,id_usergroup,write) VALUES (%s,%s,%s) RETURNING *",(id_notation,id_usergroup,ecriture));
#    //modif de crbTools
    def createVariete(self,obtenteur,remarque,taxinomie,editeur):
        return self.sqlI("INSERT INTO plant.variety (breeder,comment,id_taxonomy,editor) VALUES (%s,%s,%s,%s) RETURNING *",(obtenteur,remarque,taxinomie,editeur,))

# creation hybride
    def createHybride(self, site, parcelle, rang, arbre, numHyb, datePlantation, AnPremPous, croisement, porteGreffe, genre,espece, userGroupIds, user):
        crb = CrbTools();
        userName = user['firstname'] + ' ' + user['lastname']
        siteData = crb.getSite(site)
        if len(siteData) == 0:
          raise Exception("Site not found")
        locHyb = parcelle + 'R' + str(rang) + 'A' + str(arbre)
        numArbre = str(numHyb) + '-' + locHyb + '-1'
        idTaxinomie = crb.getTaxinomieByGenreAndEspece(genre, espece)[0]['id']
        idVariete = self.createVariete(None, 'hybride poirier', idTaxinomie, None)[0]['id_variete']
        idClone = crb.createClone(numHyb, idVariete, 'hybride poirier', None, None, None, datePlantation)[0]['id_clone']
        idLot = crb.createArbre(idClone, numArbre)[0]['id_lot']
        idgroupe = crb.createGroupe(idLot, 'hybride poirier' + numArbre)[0]['id_group']
        for g in userGroupIds:
          self.linkPlantGroupToUserGroup(g, idgroupe)
        idtn1 = crb.getTypeNom('numéro hybride')[0]['id_type_nom']
        crb.createNomVariete(idVariete, numHyb, datePlantation, idtn1)
        idtn2 = crb.getTypeNom('loc_hyb')[0]['id_type_nom']
        crb.createNomVariete(idVariete, locHyb, datePlantation, idtn2)
        siteData = crb.getSite(site)
        if len(siteData) == 0:
          raise Exception("Site not found")
        idSite = siteData[0]['id_site']
        idEmplacement = crb.createEmplacement(idLot, idSite)[0]['id']
        idTypeLieuParcelle = crb.getTypeLieu('parcelle')[0]['id']
        idTypeLieuRang = crb.getTypeLieu('rang')[0]['id']
        idTypeLieuArbre = crb.getTypeLieu('arbre')[0]['id']

        idep = crb.createLieu(idEmplacement, idTypeLieuParcelle, parcelle)
        ider = crb.createLieu(idEmplacement, idTypeLieuRang, rang)
        idea = crb.createLieu(idEmplacement, idTypeLieuArbre, arbre)

        idNot1 = crb.createNotation(datePlantation, None, idSite, 'année de première pousse', AnPremPous, userName)[0]['id_notation']
        idNot2 = crb.createNotation(datePlantation, None, idSite, 'porte-greffe', porteGreffe, userName)[0]['id_notation']
        idNot3 = crb.createNotation(datePlantation, None, idSite, 'numéro de croisement', croisement, userName)[0]['id_notation']
        idNot4 = crb.createNotation(datePlantation, None, idSite, 'éliminé', 0, userName)[0]['id_notation']

        # TODO attach notations to lot and lot to user

        crb.setGroupeANotation(idgroupe, idNot1)
        crb.setGroupeANotation(idgroupe, idNot2)
        crb.setGroupeANotation(idgroupe, idNot3)
        crb.setGroupeANotation(idgroupe, idNot4)

        for g in userGroupIds:
          self.setNotationAUsergroup(idNot1, g, True)
          self.setNotationAUsergroup(idNot2, g, True)
          self.setNotationAUsergroup(idNot3, g, True)
          self.setNotationAUsergroup(idNot4, g, True)
        return 'OK'

# verifie que l'emplacement est libre
    def getDateArrachageByEmplacement(self,site,parcelle,rang,arbre):
        return self.sqlE("""SELECT E.id AS id_emplacement,removing_date AS date_arrachage,TL.value AS type,L.value AS valeur FROM plant.place E
                                                                     LEFT JOIN address_book.site S ON S.id_site=E.id_site
                                                                     LEFT JOIN location.place L ON L.id_plant_place=E.id
                                                                     LEFT JOIN location.place_type TL ON L.id_place_type = TL.id
                            WHERE E.id IN (select id_plant_place FROM location.place L
                                            JOIN location.place_type TL ON L.id_place_type =TL.id
                                            WHERE TL.value= 'rang' AND L.value= %s)
                             AND E.id IN (select id_plant_place FROM location.place L
                                            JOIN location.place_type TL ON L.id_place_type =TL.id
                                            WHERE TL.value='arbre' AND L.value= %s)

                             AND E.id IN (select id_plant_place FROM location.place L
                                            JOIN location.place_type TL ON L.id_place_type =TL.id
                                            WHERE TL.value = 'parcelle' AND L.value= %s)
                             AND S.name =%s   """,(rang,arbre,parcelle,site))

# verifie que l'emplacement est libre
    def updateDateArrachageByNumLot(self,date,num_lot,groupe):
        idEmpl = self.sqlE("""select E.id AS id_emplacement, L.id_lot from plant.place E
                              JOIN plant.lot L ON L.id_lot=E.id_lot
                              JOIN plant.group_has_lot GAL  ON GAL.id_lot = L.id_lot
                              JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
                              Where L.name= %s AND id_usergroup = ANY(%s)
                               ORDER BY E.id DESC""",(num_lot,groupe))
        if (idEmpl==None) :
            updateEmpl = self.sqlI("""INSERT INTO plant.place (id_lot,id_site,plantation_date ,removing_date) VALUES (%s,null,null,%s)  RETURNING *
 """,(idEmpl[0][id_lot],date))        ,
        else :
            updateEmpl = self.sqlI("""update plant.place SET removing_date=%s WHERE id= %s RETURNING *
 """,(date,idEmpl[0]['id_emplacement']))

# ajoute notation présence en pepiniere = 0

        self.createNotationNumLot(num_lot,'présence en pépinière', 0,date,None, None)
        return idEmpl [0]['id_emplacement']

# Liste des hybrides à indexer
    def getAllIdLotsPourIndex(self,campagne,usergroup):
      return self.sqlE("""
        SELECT DISTINCT L.id_lot, L.name AS numero_lot from plant.lot L
        JOIN notation.lot_has_notation LHN ON LHN.id_lot = L.id_lot
        JOIN plant.place E ON E.id_lot = L.id_lot
        JOIN notation.notation_integer N ON N.id_notation = LHN.id_notation
        JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
        JOIN experiment.experiment_has_notation EHN ON EHN.id_notation = N.id_notation
        JOIN experiment.experiment Ex ON Ex.id = EHN.id_experiment
        JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = Ex.id
        WHERE TN.name= 'qualité' AND N.value >0 AND EHU.id_usergroup = ANY(%s)
        AND E.removing_date IS NULL AND Ex.name = %s
INTERSECT
        SELECT L.id_lot, L.name AS numero_lot from plant.lot L
        JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot
        JOIN notation.lot_has_notation LHN ON LHN.id_lot = L.id_lot
        JOIN notation.notation_integer N ON N.id_notation = LHN.id_notation
        JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
        WHERE TN.name = 'éliminé' AND N.value = 0
INTERSECT
        SELECT L.id_lot, L.name AS numero_lot from plant.lot L
        JOIN notation.lot_has_notation LHN ON LHN.id_lot = L.id_lot
        JOIN notation.notation_integer N ON N.id_notation = LHN.id_notation
        JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
        JOIN experiment.experiment_has_notation EHN ON EHN.id_notation = N.id_notation
        JOIN experiment.experiment Ex ON Ex.id = EHN.id_experiment
        JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = Ex.id
        WHERE TN.name= 'dep' AND N.value = 0 AND Ex.name = %s AND EHU.id_usergroup = ANY(%s)"""
       ,(usergroup,campagne,campagne,usergroup));

    def getAllLotsPourIndex(self,an,usergroup):
        return self.sqlE("""select DISTINCT (L.name) AS numero_lot from plant.lot L
       JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot
       JOIN plant.group_has_notation GN ON GN.id_group = GL.id_group
       JOIN plant.group_has_usergroup GAG ON GAG.id_group = GL.id_group
       JOIN notation.notation_integer N ON N.id_notation = GN.id_notation
       JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
       WHERE TN.name= 'qualité' AND N.value >0 AND id_usergroup = ANY(%s)

INTERSECT
       select L.name AS numero_lot from plant.lot L
       JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot
       JOIN plant.group_has_notation GN ON GN.id_group = GL.id_group
       JOIN notation.notation_integer N ON N.id_notation = GN.id_notation
       JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
       WHERE TN.name= 'éliminé' AND N.value = 0

INTERSECT
        select L.name AS numero_lot from plant.lot L
       JOIN plant.group_has_lot GL ON GL.id_lot=L.id_lot
       JOIN plant.group_has_notation GN ON GN.id_group = GL.id_group
       JOIN notation.notation_integer N ON N.id_notation = GN.id_notation
       JOIN notation.notation_type TN ON TN.id_type_notation = N.id_type_notation
       WHERE TN.name = 'dep' AND N.value = 0"""
       ,(usergroup,));

    def getAllParcelles(self,usergroup):
        return self.sqlE("""SELECT DISTINCT (LI.value) AS "parcelle"  from location.place LI
        LEFT JOIN location.place_type TL ON TL.id=LI.id_place_type
        WHERE TL.value='parcelle' AND id_plant_place IN (
              SELECT DISTINCT (E.id) from plant.group_has_lot GAL
              LEFT JOIN plant.lot L ON L.id_lot=GAL.id_lot
              LEFT JOIN plant.place E ON E.id_lot=L.id_lot
              LEFT JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
              WHERE id_usergroup = ANY(%s) )""",(usergroup,));

    def getAllPlantesFromParcelle(self,parcelle,usergroup):
        return self.sqlE("""SELECT L.id_lot,L.name AS numero_lot FROM  plant.lot L
                         JOIN plant.place E on E.id_lot=L.id_lot
                         join location.place LI ON LI.id_plant_place=E.id
                         join location.place_type TL ON TL.id=LI.id_place_type
                         join plant.group_has_lot GAL ON GAL.id_lot = L.id_lot
                         JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
                         WHERE TL.value='parcelle' AND LI.value=%s AND id_usergroup = ANY(%s) """,(parcelle,usergroup,));

    def getAllEmplacementByArbre(self,idlot):
        return self.sqlE("""select L.id_lot,L.name AS numero_lot,L1.value AS "parcelle",L2.value AS "rang" ,L3.value AS "arbre"from plant.lot L
        LEFT JOIN plant.place E ON E.id_lot=L.id_lot
        LEFT JOIN location.place L1 on E.id=L1.id_plant_place
        LEFT JOIN location.place_type TL1 ON TL1.id=L1.id_place_type
        LEFT JOIN location.place L2 on E.id=L2.id_plant_place
        LEFT JOIN location.place_type TL2 ON TL2.id=L2.id_place_type
        LEFT JOIN location.place L3 on E.id=L3.id_plant_place
        LEFT JOIN location.place_type TL3 ON TL3.id=L3.id_place_type
        WHERE L.id_lot= %s AND TL1.value='parcelle' AND TL2.value='rang'AND TL3.value='arbre'""",(idlot,));

    def getAllLotByQuality(self,note,an1,an2,usergroup):
        return self.sqlE("""SELECT L.id_lot,L.name AS numero_lot FROM  plant.lot L
                         JOIN plant.group_has_lot GAL ON GAL.id_lot = L.id_lot
                         JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
                         LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
                         JOIN notation.notation_integer I ON I.id_notation=GAN.id_notation
                         JOIN notation.notation_type TN ON TN.id_type_notation=I.id_type_notation
                         WHERE TN.name ='qualité' AND I.value >= %s
                         AND extract(year from I.date) = %s
                         AND id_usergroup=ANY(%s)
     intersect
                        SELECT L.id_lot,L.name AS numero_lot FROM  plant.lot L
                         JOIN plant.group_has_lot GAL ON GAL.id_lot = L.id_lot
                         JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
                         LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=GAL.id_group
                         JOIN notation.notation_integer I ON I.id_notation=GAN.id_notation
                         JOIN notation.notation_type TN ON TN.id_type_notation=I.id_type_notation
                         WHERE TN.name ='qualité' AND I.value >= %s
                         AND extract(year from I.date) = %s
                         AND id_usergroup=ANY(%s)
        """,(note,an1,usergroup,note,an2,usergroup));

    def getAllTaxinomieByGroup(self):
        return self.sqlE("SELECT * FROM plant.taxonomy ", ())

    def getAllExperiment(self, userGroup ):
        return self.sqlE("""SELECT E.name FROM experiment.experiment E JOIN experiment.experiment_has_usergroup EHU ON E.id=EHU.id_experiment
                            WHERE id_usergroup = ANY(%s) AND writable=true
                            ORDER BY E.name """,(userGroup,))

    def getAnneesDeReferenceABC(self, listeLots, userGroup ):
      date= datetime.datetime.strftime(datetime.datetime.now(), '%d/%m/%Y')
      listeLots2=[]

      for l in listeLots :
        anA=0
        anB=0
        anC=0
        annee_a =self.sqlE("""select N.value, N.id_notation FROM notation.notation_integer N
                              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                              JOIN plant.group_has_notation GAN ON GAN.id_notation = N.id_notation
                              JOIN plant.group_has_lot GAL ON GAL.id_group = GAN.id_group
                              WHERE GAL.id_lot = %s AND TN.name ='année a' """,(l['id_lot'],))
        annee_b =self.sqlE("""select N.value, N.id_notation FROM notation.notation_integer N
                              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                              JOIN plant.group_has_notation GAN ON GAN.id_notation = N.id_notation
                              JOIN plant.group_has_lot GAL ON GAL.id_group = GAN.id_group
                              WHERE GAL.id_lot = %s AND TN.name ='année b' """,(l['id_lot'],))
        annee_c =self.sqlE("""select N.value, N.id_notation FROM notation.notation_integer N
                              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                              JOIN plant.group_has_notation GAN ON GAN.id_notation = N.id_notation
                              JOIN plant.group_has_lot GAL ON GAL.id_group = GAN.id_group
                              WHERE GAL.id_lot = %s AND TN.name ='année c' """,(l['id_lot'],))

        if len(annee_a) >0 :
          anA = annee_a[0]['value']
          idAnA = annee_a[0]['id_notation']
        if len(annee_b) >0 :
          anB = annee_b[0]['value']
          idAnB = annee_b[0]['id_notation']
        if len(annee_c) >0 :
          anC = annee_c[0]['value']
          idAnC = annee_c[0]['id_notation']

        #ensemble des campagnes valides pour ce idLot
        an=self.sqlE(""" select DISTINCT E.name from experiment.experiment E
              JOIN experiment.experiment_has_notation EHN ON EHN.id_experiment=E.id
              JOIN plant.group_has_notation GAN ON GAN.id_notation=EHN.id_notation
              JOIN plant.group_has_lot GAL ON GAL.id_group = GAN.id_group
              JOIN notation.notation_integer N ON N.id_notation= GAN.id_notation
              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
              WHERE GAL.id_lot = %s AND TN.name ='qualité' AND N.value >0

INTERSECT
              select DISTINCT E.name from experiment.experiment E
              JOIN experiment.experiment_has_notation EHN ON EHN.id_experiment=E.id
              JOIN plant.group_has_notation GAN ON GAN.id_notation=EHN.id_notation
              JOIN plant.group_has_lot GAL ON GAL.id_group = GAN.id_group
              JOIN notation.notation_integer N ON N.id_notation= GAN.id_notation
              JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
              WHERE GAL.id_lot = %s AND TN.name ='dep' AND N.value =0 """,(l['id_lot'],l['id_lot'],))

        annees = []
        if len(an)>0 :
          a_max = 0
          a_min = 10000
          for a in an :
            annees.append(int(a['name']))
            if int(a['name']) > a_max:
              a_max = int(a['name'])
            if int(a['name']) < a_min:
              a_min = int(a['name'])

          #ordonne la liste
          annees=sorted(annees)
#          print str(l['numero_lot'])+' '+str(anA)+' '+str(anB)+' '+str(anC)+' '+str(annees)
          imax = annees.index(a_max) #indice de l'année la plus récente
          if annees.index(a_max) - 3 >= 0 :
            imin = annees.index(a_max) - 3 # ne garde que les 3 dernières dates
            annees = annees[imin+1:imax+1]
          else:
            imin = 0      #indice de l'année dans les 2 ans qui précèdent sup
            annees = annees[imin:imax+1]

        #Mise à jour de la base
        if annees :
          if anA == 0 :
            self.createNotationNumLot(l['numero_lot'] ,'année a',annees[0] ,'01/01/0001',None, None)
          annees.pop(0) #supprime année min

        if annees:
          if anB==0 :
            self.createNotationNumLot(l['numero_lot'] ,'année b',annees[0] ,'01/01/0001',None, None)
          annees.pop(0) #supprime année min

        if annees:
          if anC==0 :
            self.createNotationNumLot(l['numero_lot'] ,'année c',annees[0] ,'01/01/0001',None, None)


      return  listeLots
