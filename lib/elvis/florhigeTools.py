# -*- coding: utf-8 -*-

import sys
from types import *
from elvis.DbConnection import DbConnection

class FlorhigeTools(DbConnection):
    """
    Classe contenant des méthodes pour interagir avec la base sélection.
    Cette classe contient la totalité des méthodes appelées par les services de
    florhigeVariete.py
    """
    ###########################################################################################################
    def getAllSites(self):
        return self.sqlE("SELECT id_site, name as nom_site FROM address_book.site ORDER BY name ASC;", ())

    def getAllOrigines(self):
        return self.sqlE("SELECT id_origine, description FROM plant.origine ORDER BY description ASC;", ())

    def getAllPays(self):
        return self.sqlE("SELECT id AS id_pays, name AS nom FROM address_book.country ORDER BY name ASC;", ())

    def getAllTypeNom(self):
        return self.sqlE("SELECT * FROM plant.variety_name_type;", ())

    def getAllTypeLieu(self):
        return self.sqlE("SELECT id, value AS type FROM location.place_type;", ())

    def getAllNomVariete(self):
        return self.sqlE("SELECT distinct nom FROM plant.variety_name;", ())

    def getAllGenre(self):
        return self.sqlE("SELECT distinct genus AS genre FROM plant.taxonomy WHERE genus IS NOT NULL;", ())

    def getAllEspece(self):
        return self.sqlE("SELECT distinct specie AS espece FROM plant.taxonomy WHERE specie IS NOT NULL;", ())

    def getAllPepinieriste(self):
        return self.sqlE("SELECT lastname as nom FROM address_book.address ORDER BY lastname ASC;", ())
##############################################  Obtenteur ########################################################
    def getAllObtenteur(self):
        return self.sqlE("SELECT A.id  AS id_pepinieriste, A.lastname as nom, address1 AS adresse1,address2 AS adresse2,city as ville,B.id AS id_pays, B.name AS Pays FROM address_book.address A LEFT JOIN address_book.country B ON A.id_country=B.id ORDER BY A.lastname DESC ;", ())

    def getObtenteurByIdVar(self, idVar):
        id_pep = self.getInfoVarieteById(idVar)[0]["obtenteur"]
        if id_pep <1 :
           id_pep=0
        result = self.sqlE("SELECT A.id AS id_pepinieriste, A.lastname as nom, address1 AS adresse1,address2 AS adresse2,city as ville,state AS etat, postal_code AS code_postal,email,telephone," " AS fax, B.id AS id_pays, B.name AS pays FROM address_book.address A LEFT JOIN address_book.country B ON A.id_country=B.id WHERE id_pepinieriste = %s;", (id_pep,))
        result[0]["id_variete"]=idVar
        return result

 ##############################################   ########################################################
    def getAllObtenteur(self):
        return self.sqlE("SELECT A.id AS id_pepinieriste, A.lastname as nom, address1 AS adresse1,address2 AS adresse2,city as ville,B.id AS id_pays, B.name AS Pays FROM address_book.address A LEFT JOIN address_book.country B ON A.id_country=B.id ORDER BY A.lastname DESC ;", ())

    def getAllStatutBio(self):
        return self.sqlE("SELECT id_statut, valeur FROM plant.statut_biologique ORDER BY valeur ASC;", ())

    def getAllPorteGreffe(self):
        return self.sqlE("SELECT * FROM plant.porte_greffe ORDER BY valeur ASC;", ())

    def getAllCollection(self):
        return self.sqlE("SELECT id_collection, name AS nom FROM collection.collection;",())

    def getAllClone(self):
        return self.sqlE("SELECT numero, id_variety AS variete, comment AS remarque, provider AS fournisseur FROM plant.accession;",())

    def getAllStatutBiologique(self):
        return self.sqlE("SELECT id_statut, valeur FROM plant.statut_biologique ORDER BY valeur ASC;", ())

    def getAllTypeGeniteur(self):
        return self.sqlE("SELECT * FROM plant.type_geniteur;",())

    def getPepinieriste(self, nom):
        return self.sqlE("SELECT id AS id_pepinieriste FROM address_book.address WHERE lastname = %s;", (nom,))

    ######################################################################################################################

    def getOrCreateVarieteByNom(self,nom):
        if nom == "" or nom is None:
            return None
        variete = self.getVarieteByNom(nom)
        if len(variete) == 0:
            id_variete = self.createVariete(None,None,None,None,None,None)[0]["id_variete"]
            self.createNomVariete(id_variete,nom,None,None)
            return id_variete
        else:
            return variete[0]["id_var"]

    def createVariete(self,origine,ctifl,statut_bio,obtenteur,remarque,editeur):
        origine = self.getOrCreateOrigine(origine)
        obtenteur = self.getOrCreatePepinieriste(obtenteur)
        editeur = self.getOrCreatePepinieriste(editeur)
        statut_bio = self.getOrCreateStatutBiologique(statut_bio)
        return self.sqlI("INSERT INTO plant.variety (origine,num_dif_ctifl,statut_bio,breeder,comment,editor,groupe_lecture,groupe_ecriture) VALUES (%s,%s,%s,%s,%s,%s,1,1) RETURNING id_variete",(origine,ctifl,statut_bio,obtenteur,remarque,editeur))

    def getVarieteByNom(self,nom):
        return self.sqlE("SELECT id_variety AS id_var FROM plant.variety_name WHERE value = %s;", (nom,))


    def createNomVariete(self,id_var,nom,date,type_nom):
        type_nom = self.getOrCreateTypeNom(type_nom)
        return self.sqlI("""INSERT INTO plant.variety_name (id_var,nom,date_var,type) VALUES (%s,%s,%s,%s) RETURNING id_nom_var;""", (id_var, nom, date, type_nom))

    def updateOrCreateClone(self, id_clone, numero, date_intro, variete, remarque, fournisseur):
        fournisseur = self.getOrCreatePepinieriste(fournisseur)
        if id_clone is None:
            return self.sqlI("""INSERT INTO plant.accession (introduction_name, introduction_date, id_variety, comment, provider) VALUES (%s, %s, %s, %s, %s) RETURNING id AS id_clone""",(numero, date_intro, variete, remarque, fournisseur))
        else:
            return "Update clone"

##############################################################################################################
    def updateOrCreateVariete(self,id_variete,obtenteur,remarque,editeur):

        if obtenteur!="":
            obtenteur = self.getPepinieriste(obtenteur)[0]["id_pepinieriste"]
        else :
            obtenteur = None
        if editeur!="":
            editeur = self.getPepinieriste(editeur)[0]["id_pepinieriste"]
        else :
            editeur = None

        if id_variete is None:
            return self.sqlI("INSERT INTO plant.variety (obtenteur,comment,editor,groupe_lecture,groupe_ecriture) VALUES (%s,%s,%s,1,1) RETURNING id_variete",(obtenteur,remarque,editeur))
        else :
            return self.sqlI("UPDATE plant.variety SET (obtenteur,comment,editor,groupe_lecture,groupe_ecriture) = (%s,%s,%s,1,1) WHERE id_variete = %s RETURNING id_variete",(obtenteur,remarque,editeur,id_variete))

    def deleteNomVariete(self, id_variete):
        return self.sqlI("DELETE FROM plant.variety_name WHERE id_variety =%s RETURNING id_nom_var",(id_variete,))


    def setGeniteur(self,id_variete, type_geniteur, geniteur_m, geniteur_f):
        if type_geniteur != "":
            type_geniteur = self.getTypeGeniteur(type_geniteur)[0]["id_type_geniteur"]
        else :
            type_geniteur = None
        self.cur.execute("SELECT * FROM plant.geniteur WHERE id_variete=%s",(id_variete,))
        if len(self.cur.fetchall())==0:
            return self.sqlI("INSERT INTO plant.geniteur (id_variete, femelle, male, type_geniteur) VALUES (%s,%s,%s,%s) RETURNING *",(id_variete,geniteur_f,geniteur_m,type_geniteur))
        else:
            return self.sqlI("UPDATE plant.geniteur SET (femelle, male, type_geniteur) = (%s,%s,%s) WHERE id_variete = %s RETURNING *",(geniteur_f,geniteur_m,type_geniteur,id_variete))

###################################################################################################################
    def getOrCreatePepinieriste(self, nom):
        if nom=="" or nom is None:
            return None
        pepinieriste = self.getPepinieristeByNom(nom)
        if len(pepinieriste) == 0:
            id_pepinieriste = self.createPepinieriste()[0]["id_pepinieriste"]
            return id_pepinieriste
        else:
            return pepinieriste[0]["id_pepinieriste"]

    def createPepinieriste(self, nom):
        return self.sqlI("INSERT INTO address_book.address (lastname) VALUES (%s) RETURNING id AS id_pepinieriste", (nom,))

    def getPepinieristeByNom(self, nom):
        return self.sqlE("SELECT id AS id_pepinieriste FROM address_book.address WHERE lastname = %s;", (nom,))

    def getOrCreateTaxinomie(self, genre, espece):
        if (genre is None and espece is None) or (genre=="" and espece==""):
            return None
        taxinomie = self.getTaxinomie(genre, espece)
        if len(taxinomie) == 0:
            return self.createTaxinomie(genre, espece)[0]["id"]
        else:
            return taxinomie[0]["id"]

    def createTaxinomie(self, genre, espece, sous_espece):
        return self.sqlI("INSERT INTO plant.taxonomy (genus, specie) VALUES (%s, %s) RETURNING id", (genre, espece))

    def getTaxinomie(self, genre, espece):
        return self.sqlE("SELECT id FROM plant.taxonomy WHERE genus = %s and specie = %s;", (genre, espece))

    def setTaxinomie(self, id_variete, genre, espece):
        taxinomie = self.getOrCreateTaxinomie(genre, espece)
        return self.sqlI("UPDATE plant.variety SET id_taxonomy = %s WHERE id_variete = %s RETURNING *;",(taxinomie, id_variete))

    def addCloneToCollection(self, id_clone, id_collection):
        return self.sqlI("INSERT INTO collection.collection_has_accession (id_collection,id_clone) VALUES (%s,%s) RETURNING *", (id_collection, id_clone))

    def getOrCreateCollection(self,nom):
        if nom is None or nom == "":
            return None
        collection = self.getCollection(nom)
        if len(collection) == 0:
            return self.createCollection(nom)[0]["id_collection"]
        else:
            return collection[0]["id_collection"]

    def getCollection(self,nom):
        return self.sqlE("SELECT id_collection FROM collection.collection WHERE name = %s",(nom,))

    def createCollection(self,nom):
        return self.sqlI("INSERT INTO collection.collection (name) VALUES (%s) RETURNING id_collection;",(nom,))

    def getIdCloneByNumero(self,numero):
        return self.sqlE("SELECT id AS id_clone FROM plant.accession WHERE introduction_name = %s;",(numero,))

    def createArbre(self,id_clone, numero, annee_premiere_pousse, porte_greffe):
        porte_greffe = self.getOrCreatePorteGreffe(porte_greffe)
        return self.sqlI("INSERT INTO plant.tree (name,id_accession,first_shoot_year,rootstock) VALUES (%s,%s,%s,%s) RETURNING id_lot",(numero,id_clone,annee_premiere_pousse, porte_greffe));

    def createGraines(self,id_clone, numero, equilibre, date_recolte, quantite, rang_evaluation):
        return self.sqlI("INSERT INTO plant.seed (name,id_accession,harvesting_date,quantity) VALUES (%s,%s,%s,%s) RETURNING id_lot",(numero, id_clone, date_recolte, quantite))

    def getOrCreatePorteGreffe(self, nom):
        if nom is None and nom == "":
            return None
        porte_greffe = self.getPorteGreffe(nom)
        if len(porte_greffe) == 0:
            return self.createPorteGreffe(nom)[0]["id_porte_greffe"]
        else:
            return porte_greffe[0]["id_porte_greffe"]

    def getPorteGreffe(self, nom):
        return self.sqlE("SELECT id_porte_greffe FROM plant.porte_greffe WHERE valeur = %s;",(nom,))

    def createPorteGreffe(self, nom):
        return self.sqlI("INSERT INTO plant.porte_greffe (valeur) VALUES (%s) RETURNING id_porte_greffe;",(nom,))

    def getOrCreateSite(self, nom):
        if nom is None or nom == "":
            return None
        site = self.getSite(nom)
        if len(site) == 0:
            return self.createSite(nom)[0]["id_site"]
        else :
            return site[0]["id_site"]

    def getSite(self,nom):
        return self.sqlE("SELECT id_site FROM address_book.site WHERE name = %s",(nom,))

    def createSite(self,nom):
        return self.sqlI("INSERT INTO address_book.site (name) VALUES (%s) RETURNING id_site",(nom,))

    def createEmplacement(self,id_lot,site):
        site = self.getOrCreateSite(site)
        return self.sqlI("INSERT INTO plant.place (id_lot,id_site) VALUES (%s,%s) RETURNING id;",(id_lot,site,))

    def getOrCreateTypeLieu(self, nom):
        if nom is None or nom == "":
            return None
        typeLieu = self.getTypeLieu(nom)
        if len(typeLieu) == 0:
            return self.createTypeLieu(nom)[0]["id"]
        else :
            return typeLieu[0]["id"]

    def getTypeLieu(self, nom):
        return self.sqlE("SELECT id FROM location.place_type WHERE value = %s",(nom,))

    def createTypeLieu(self, nom):
        return self.sqlE("INSERT INTO location.place_type (value) VALUES (%s) RETURNING id",(nom,))

    def createLieu(self, id_emplacement,type_lieu,valeur):
        type_lieu = self.getOrCreateTypeLieu(type_lieu)
        return self.sqlI("INSERT INTO location.place (id_plant_place, id_place_type, value) VALUES (%s,%s,%s) RETURNING *",(id_emplacement,type_lieu,valeur,))

    def getOrCreateStatutBiologique(self, nom):
        if nom is None or nom == "":
            return None
        statut_bio = self.getStatutBiologique(nom)
        if len(statut_bio) == 0:
            return self.createStatutBiologique(nom)[0]["id_statut"]
        else:
            return statut_bio[0]["id_statut"]

    def getStatutBiologique(self,nom):
        return self.sqlE("SELECT id_statut FROM plant.statut_biologique WHERE valeur = %s",(nom,))

    def createStatutBiologique(self,nom):
        return self.sqlI("INSERT INTO plant.statut_biologique (valeur) VALUES (%s) RETURNING id_statut",(nom,))

    def getOrCreateOrigine(self,nom):
        if nom is None or nom == "":
            return None
        origine = self.getOrigine(nom)
        if len(origine) == 0:
            return self.createOrigine(nom)[0]["id_origine"]
        else:
            return origine[0]["id_origine"]

    def getOrigine(self,nom):
        return self.sqlE("SELECT id_origine FROM plant.origine WHERE description = %s",(nom,))

    def createOrigine(self,nom):
        return self.sqlI("INSERT INTO plant.origine (description) VALUES (%s)",(nom,))

    def getOrCreateTypeNom(self,nom):
        if nom is None or nom == "":
            return None
        type_nom = self.getTypeNom(nom)
        if len(type_nom) == 0:
            return self.createTypeNom(nom)[0]["id_type_nom"]
        else:
            return type_nom[0]["id_type_nom"]

    def getTypeNom(self,nom):
        return self.sqlE("SELECT id_type_nom FROM plant.variety_name_type WHERE value = %s",(nom,))

    def createTypeNom(self,nom):
        return self.sqlI("INSERT INTO plant.variety_name_type (value) VALUES (%s) RETURNING id_type_nom",(nom,))

    def getVarieteByObtenteur(self,obtenteur,type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE obtenteur "+ type_recherche +"%s"
        self.cur.execute(query,(obtenteur,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByEditeur(self,editeur,type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE editeur "+ type_recherche +"%s"
        self.cur.execute(query,(editeur,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByGenre(self,genre,type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE genre "+ type_recherche +"%s"
        self.cur.execute(query,(genre,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByEspece(self,espece,type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE espece "+ type_recherche +"%s"
        self.cur.execute(query,(espece,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByNomVariete(self, nom, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.variety_name WHERE value " +type_recherche + "%s)"
        self.cur.execute(query,(nom,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByRemarque(self, nom, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE remarque " +type_recherche + "%s"
        self.cur.execute(query,(nom,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByOrigine(self, origine, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE origine " +type_recherche + "%s"
        self.cur.execute(query,(origine,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteBySousEspece(self, sous_espece, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE sous_espece " +type_recherche + "%s"
        self.cur.execute(query,(sous_espece,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByStatutBio(self, statut_bio, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE statut_biologique " +type_recherche + "%s"
        self.cur.execute(query,(statut_bio,))
        return self.addNomsVariete(self.cur.fetchall())

    def getVarieteByCtifl(self, ctifl, type_recherche):
        query = "SELECT * FROM plant.vue_variete WHERE numero_ctifl" +type_recherche + "%s"
        self.cur.execute(query,(ctifl,))
        return self.addNomsVariete(self.cur.fetchall())

    def getDistinctTaxinomie(self, critere):
        self.cur.execute("SELECT id_type_nom, value AS valeur FROM plant.variety_name_type WHERE value = %s;", (critere,))
        Notation=self.cur.fetchall()[0]
        idNom=str(Notation["id_type_nom"])

        query= "SELECT DISTINCT value as nom FROM plant.variety_name WHERE id_variety_name_type ="+idNom+ " ORDER BY value "
        self.cur.execute(query, ())

        return (self.cur.fetchall())


    def getTypeNomVariete(self):
        return self.sqlE("""SELECT value AS valeur FROM plant.variety_name_type WHERE id_type_nom IN
                            (SELECT distinct id_variety_name_type FROM plant.variety_name WHERE id_variety IN
                            (SELECT id_variety AS id_variete FROM bibliography.reference)) ORDER BY value""",())

    def getVarieteByValeurNotation(self, type_notation,valeur, type_recherche):
        type_notation_int = self.getIdTypeNotation(type_notation)[0]["id_type_notation"]
        type_notation = self.getIdTypeNotation(type_notation)[0]["type"]
        query = "SELECT * FROM plant.vue_variete WHERE id IN (SELECT id_variete FROM notation.variety_has_notation WHERE id_notation IN (SELECT id_notation FROM notation.notation_"+type_notation+" WHERE value "+ type_recherche + "%s AND id_type_notation = %s))"

        self.cur.execute(query,(valeur,type_notation_int))
        return self.addNomsVariete(self.cur.fetchall())


    def getReferenceByValeurNotation(self, type_notation,valeur, type_recherche):
        type_notation_int = self.getIdTypeNotation(type_notation)[0]["id_type_notation"]
        type_notation = self.getIdTypeNotation(type_notation)[0]["type"]
        query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group IN (SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup=1001)AND id_group IN (SELECT id_group FROM plant.group_has_notation WHERE id_notation IN (SELECT id_notation FROM  notation.notation_"+type_notation+" WHERE value "+ type_recherche + "%s AND id_type_notation = %s)))))"
        self.cur.execute(query,(valeur,type_notation_int))
        return self.addNomsVariete(self.cur.fetchall())

    def getReferenceByValeurNotation(self, type_notation,valeur, type_recherche):
        type_notation_int = self.getIdTypeNotation(type_notation)[0]["id_type_notation"]
        type_notation = self.getIdTypeNotation(type_notation)[0]["type"]
        query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group IN (SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup=1001)AND id_group IN (SELECT id_group FROM plant.group_has_notation WHERE id_notation IN (SELECT id_notation FROM  notation.notation_"+type_notation+" WHERE value "+ type_recherche + "%s AND id_type_notation = %s)))))"
        self.cur.execute(query,(valeur,type_notation_int))
        return self.addNomsVariete(self.cur.fetchall())

#    def getReferenceByValeurBibliographie(self, critere,valeur, type_recherche):
#        query = "SELECT id_variete, livret, title AS titre, page, plank AS planche, chapitre, tome, author AS author AS auteur, numero_variete , extract (year FROM date_reference) as annee FROM bibliography.reference WHERE id_variete IN (SELECT id_variete FROM notation.variety_has_notation WHERE id_variete IN (SELECT id_variete FROM bibliography.reference WHERE " + critere +" "+ type_recherche + " %s ))"
#        self.cur.execute(query,(valeur,))
#        return self.addNomsVariete(self.cur.fetchall())

    def getReferenceByValeurBibliographie(self, critere,valeur, type_recherche):
        query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variety AS id_variete FROM bibliography.reference WHERE " + critere +" "+ type_recherche + " %s )AND id_variety IN ( SELECT id_variety FROM plant.accession A JOIN plant.lot B ON A.id = B.id_accession JOIN plant.group_has_lot C ON B.id_lot = C.id_lot  WHERE id_group IN(SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup =1001))"
        self.cur.execute(query,(valeur,))
        return self.addNomsVariete(self.cur.fetchall())

    def getReferenceByValeurTaxinomie(self, critere,valeur, type_recherche):
        if critere == 'nom' :
           query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variete FROM plant.variety WHERE id_taxonomy = 3 AND id_variety IN (SELECT id_variety FROM plant.variety_name WHERE value "+ type_recherche + " %s ))AND id_variety IN ( SELECT id_variety FROM plant.accession A JOIN plant.lot B ON A.id = B.id_accession JOIN plant.group_has_lot C ON B.id_lot = C.id_lot  WHERE id_group IN(SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup =1001))"
           self.cur.execute(query,(valeur,))

        else :
           type_non_var_int =self.getIdTypeNomVariete(critere)[0]["id_type_nom"]
           query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variete FROM plant.variety WHERE id_taxonomy = 3 AND id_variety IN (SELECT id_variety FROM plant.variety_name WHERE value "+ type_recherche + " %s AND id_variety_name_type = %s ))AND id_variety IN ( SELECT id_variety FROM plant.accession A JOIN plant.lot B ON A.id = B.id_accession JOIN plant.group_has_lot C ON B.id_lot = C.id_lot  WHERE id_group IN(SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup =1001))"
           self.cur.execute(query,(valeur,type_non_var_int))
        return self.addNomsVariete(self.cur.fetchall())

    def getReferenceByValeurInformation(self, critere,valeur, type_recherche):
        if critere == "remarque":
            query = "SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variete FROM plant.variety WHERE id_taxonomy = 3 AND id_variety IN (SELECT id_variete FROM plant.variety WHERE " + critere +" "+type_recherche+"  %s));"

        if critere == "obtenteur":
            query="SELECT id_variety AS id_variete, livret, title AS titre, page, plank AS planche, chapter AS chapitre, tome, author AS auteur, variety_number AS numero_variete , extract (year FROM date) as annee FROM bibliography.reference WHERE id_variety IN (SELECT id_variete FROM plant.variety WHERE id_taxonomy = 3 AND breeder IN (SELECT id FROM address_book.address WHERE lastname "+type_recherche+"  %s));"

        self.cur.execute(query,(valeur,))
        return self.addNomsVariete(self.cur.fetchall())

    def addNomsVariete(self,result) :
      if len(result) <3000:
        for i in result:
            id_variete =  i["id_variete"]
            self.cur.execute("SELECT NV.value as nom, TNV.value as type_nom FROM plant.variety_name NV LEFT JOIN plant.variety_name_type TNV ON id_variety_name_type=id_type_nom WHERE id_variety = %s",(id_variete,))
            noms = self.cur.fetchall()
            i["variété"]=""
            i["hybride"]=""
            for j in noms:
                nom = j["nom"]
                type_nom = j["type_nom"]
                if type_nom =="variété" :
                  i[type_nom]=nom
                if type_nom =="hybride" :
                  i[type_nom]=nom
      return result




    def getIdTypeNomVariete(self,type_nom_variete):
        return self.sqlE("SELECT id_type_nom, value AS valeur FROM plant.variety_name_type WHERE value = %s",(type_nom_variete,))

    def getVarieteById(self, id):
        return self.sqlE("SELECT * FROM plant.vue_variete WHERE id = %s", (id,))

    def getInfoVarieteById(self, id):
        return self.sqlE("SELECT * FROM plant.variety WHERE id_variete = %s", (id,))

    def getNomsByVariete(self, id_variete):
        return self.sqlE("SELECT * FROM plant.vue_nom_variete WHERE id_variete = %s",(id_variete,))

    def getCloneByFournisseur(self,fournisseur):
        fournisseur = self.getOrCreatePepinieriste(fournisseur)
        return self.sqlE()
##############################################  Notations ########################################################

    def updateNotation(self,id_notation,valeur,type_notation):
        sql="UPDATE notation.notation_"+type_notation+" SET value = %s WHERE id_notation = %s RETURNING id_notation"
        return self.sqlI(sql,(valeur,id_notation,))
    def getNotationByVariete(self, id_var):
        self.cur.execute("""SELECT E.id_notation,to_char(date,'DD/MM/YYYY') as date_notation,E.comment AS remarque_notation,G.name as nom_site,F.name as type_notation,type,observer AS notateur FROM plant.group_has_notation A  LEFT JOIN plant.group_has_lot B ON A.id_group= B.id_group
                                        LEFT JOIN plant.tree C ON B.id_lot= C.id_lot
                                        LEFT JOIN plant.accession D ON C.id_accession= D.id
                                       JOIN notation.notation E ON A.id_notation=E.id_notation
                                       LEFT JOIN notation.notation_type F ON E.id_type_notation = F.id_type_notation
                                       LEFT JOIN address_book.site G ON E.id_site= G.id_site
            WHERE A.id_group IN (SELECT id_group from plant.group_has_usergroup where id_usergroup =1001) AND D.id_variety =%s""",(id_var,))
        rows = self.cur.fetchall()
        for row in rows:
            sql = "SELECT * FROM notation.notation_"+row['type']+" N ,notation.notation_type TN WHERE id_notation= %s AND N.id_type_notation=TN.id_type_notation"
            self.cur.execute(sql,(row['id_notation'],))
            valeur = self.cur.fetchall()[0]
#            champ = "valeur_notation_"+row['type']
            champ = "value"
            row['valeur']= valeur[champ]
        return rows



    def getIdTypeNotation(self,type_notation):
        return self.sqlE("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE name = %s",(type_notation,))


    def getTypeNotationVariete(self,userGroup):
        return self.sqlE("""SELECT name as nom FROM notation.notation_type WHERE id_type_notation IN (SELECT distinct id_type_notation FROM notation.notation WHERE id_notation IN (SELECT id_notation FROM plant.group_has_notation WHERE id_group IN (SELECT id_group FROM plant.group_has_usergroup WHERE id_usergroup = ANY(%s)))) ORDER BY name""",(userGroup,))


    def getDistinctNotation(self, critere):
        self.cur.execute("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE name = %s;", (critere,))
        Notation=self.cur.fetchall()[0]
        idNot=str(Notation["id_type_notation"])
        typeNot= Notation["type"]
        query= "SELECT DISTINCT value FROM notation.notation_"+typeNot+" WHERE type_notation="+idNot+ " ORDER BY value "
        self.cur.execute(query, ())
        return (self.cur.fetchall())

    def createNotation(self,date,remarque,site,type_notation,valeur,observer):
        if site != "":
            site = self.getSite(site)[0]["id_site"]
        else :
            site = None
        type_valeur=self.getTypeNotation(type_notation)[0]["type"]

        type_notation=self.getTypeNotation(type_notation)[0]["id_type_notation"]

        if (type_valeur =="integer"):
            return self.sqlI("INSERT INTO notation.notation_integer (date, comment, id_site, groupe_lecture, groupe_ecriture,id_type_notation, observer,value) VALUES (%s,%s,%s,1,1,%s,%s,%s) RETURNING id_notation",(date,remarque,site,type_notation,observer,valeur,))
        if (type_valeur =="texte"):
            return self.sqlI("INSERT INTO notation.notation_texte (date, comment, id_site, groupe_lecture, groupe_ecriture,id_type_notation,  observer,value) VALUES (%s,%s,%s,1,1,%s,%s,%s) RETURNING id_notation",(date,remarque,site,type_notation,observer,valeur,))
        if (type_valeur =="double"):
            return self.sqlI("INSERT INTO notation.notation_double (date, comment, id_site, groupe_lecture, groupe_ecriture,id_type_notation,observer,value) VALUES (%s,%s,%s,1,1,%s,%s,%s) RETURNING id_notation",(date,remarque,site,type_notation,observer,valeur,))
        if (type_valeur =="date"):
            return self.sqlI("INSERT INTO notation.notation_date (date, comment, id_site, groupe_lecture, groupe_ecriture, observer,value) VALUES (%s,%s,%s,1,1,%s,%s) RETURNING id_notation",(date,remarque,site,observer,valeur,))

    def getTypeNotation(self,type_notation):
        return self.sqlE("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE nom=%s",(type_notation,))


    def getNotationByIdNoType(self,id_variete,type_notation,liste_groupe):
        id_type = self.getIdTypeNotation(type_notation)[0]["id_type_notation"]
        typeNotation = self.getIdTypeNotation(type_notation)[0]["type"]
        if (typeNotation == "texte" ):
            return self.sqlE(""" SELECT id_variety AS id_variete ,value as valeur_notation,E.comment AS remarque_notation,observer AS notateur, to_char(date,'DD/MM/YYYY') as date , F.name AS nom, G.name as nom_site FROM plant.group_has_notation A LEFT JOIN plant.group_has_lot B ON A.id_group= B.id_group LEFT JOIN plant.tree C ON B.id_lot= C.id_lot LEFT JOIN plant.accession D ON C.id_accession= D.id LEFT JOIN notation.notation_texte E ON A.id_notation=E.id_notation LEFT JOIN notation.notation_type F ON E.id_type_notation = F.id_type_notation LEFT JOIN address_book.site G ON E.id_site= G.id_site WHERE A.id_group IN (SELECT id_group from plant.group_has_usergroup where id_usergroup = ANY(%s)) AND D.id_variety = %s AND E.id_type_notation=%s ;""",(liste_groupe, id_variete,id_type,))

        if (typeNotation == "integer" ):
            return self.sqlE(""" SELECT id_variety AS id_variete ,value as valeur_notation,E.comment AS remarque_notation,observer AS notateur, to_char(date,'DD/MM/YYYY') as date , F.name AS nom, G.name as nom_site FROM plant.group_has_notation A LEFT JOIN plant.group_has_lot B ON A.id_group= B.id_group LEFT JOIN plant.tree C ON B.id_lot= C.id_lot LEFT JOIN plant.accession D ON C.id_accession= D.id LEFT JOIN notation.notation_integer E ON A.id_notation=E.id_notation LEFT JOIN notation.notation_type F ON E.id_type_notation = F.id_type_notation LEFT JOIN address_book.site G ON E.id_site= G.id_site WHERE A.id_group IN (SELECT id_group from plant.group_has_usergroup where id_usergroup = ANY(%s)) AND D.id_variety = %s AND E.id_type_notation=%s ;""",(liste_groupe, id_variete,id_type,))
        if (typeNotation == "double" ):
            return self.sqlE(""" SELECT id_variety AS id_variete ,value as valeur_notation,E.comment AS remarque_notation,observer AS notateur, to_char(date,'DD/MM/YYYY') as date , F.name AS nom, G.name as nom_site FROM plant.group_has_notation A LEFT JOIN plant.group_has_lot B ON A.id_group= B.id_group LEFT JOIN plant.tree C ON B.id_lot= C.id_lot LEFT JOIN plant.accession D ON C.id_accession= D.id LEFT JOIN notation.notation_double E ON A.id_notation=E.id_notation LEFT JOIN notation.notation_type F ON E.id_type_notation = F.id_type_notation LEFT JOIN address_book.site G ON E.id_site= G.id_site WHERE A.id_group IN (SELECT id_group from plant.group_has_usergroup where id_usergroup = ANY(%s)) AND D.id_variety = %s AND E.id_type_notation=%s ;""",(liste_groupe, id_variete,id_type,))
############################################# variete_a_notation ####################################

    def createVarieteANotation(self,id_variete,id_notation):
        return self.sqlI("INSERT INTO plant.group_has_notation (id_group,id_notation) VALUES ((SELECT id_group FROM  plant.group_has_lot B LEFT JOIN plant.tree C ON B.id_lot= C.id_lot LEFT JOIN plant.accession D ON C.id_accession= D.id WHERE B.id_group IN (SELECT id_group from plant.group_has_usergroup where id_usergroup=1) AND D.id_variety=%s),%s)",(id_variete,id_notation))
