# -*- coding: utf-8 -*-

import psycopg2
import psycopg2.extras
import sys
import datetime
import elvis

from elvis.DbConnection import DbConnection

class GlamsTools_sample(DbConnection):

  """
  WebServices concerning the project database
  Rewritted from anandbTools.py (2016-12)

  AUTHORS :
  - Sylvain GAILLARD IRHS Bioinfo team
  - Sandra PELLETIER IRHS Bioinfo team
  - Fabrice DUPUIS   IRHS Bioinfo team
  - Hervé Handres (M2 2017)

#  SECTIONS :
# functions for contexte of notations #
# functions for series or groupes #
# functions for location sample #

  TODO :

  """

#--------------------------------#
#  functions for GlamsSample     #
#--------------------------------#
# ajoute une notation sur variete
  def setVarieteANotation(self, sessionId, tab): #modifié avec tab le 05/06/2018
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO notation.variety_has_notation (id_variete, id_notation) VALUES (%s,%s) RETURNING *",(tab['idVariete'], tab['idNotation']))
    except Exception as e:
      raise e
    else :
        self.conn.commit()
    return self.cur.fetchall()

# ajoute une notation sur action
  def setActionANotation(self, id_action, id_notation):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO notation.action_has_notation (id_action, id_notation) VALUES (%s,%s) RETURNING *",(id_action, id_notation))
    except Exception as e:
      raise e
    else :
        self.conn.commit()
    return self.cur.fetchall()
# ajoute une notation sur container
  def setContainerANotation(self, id_container, id_notation):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO notation.container_has_notation (id_container, id_notation) VALUES (%s,%s) RETURNING *",(id_container, id_notation))
    except Exception as e:
      raise e
    else :
        self.conn.commit()
    return self.cur.fetchall()
# ajoute un user sur une notation
  def addUserToNotation (self,idUser,idNotation):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO notation.notation_has_user (id_user,id_notation) VALUES (%s,%s) RETURNING id_user,id_notation ",(idUser,idNotation))
    except Exception as e:
      raise e
    else :
        self.conn.commit()
    return self.cur.fetchall()

  def getAllPrelevmentsGroup(self):
  # get All Prelevments
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT C.id AS id_container, P.id AS id_prelevement, T.type AS type_tissu, C.code AS code_contenant, TC.type AS type_contenant, EC.level AS etat_contenant, TCU.type AS type_contenu
      FROM sample.container C
      LEFT JOIN sample.taking P ON P.id_container=C.id
      LEFT JOIN plant.group_has_usergroup GAG ON GAG.id_group=P.id_plant_group
      LEFT JOIN sample.tissue_type T ON T.id=P.id_type_tissu
      LEFT JOIN sample.container_type TC ON TC.id = C.id_container_type
      LEFT JOIN sample.container_level EC ON EC.id= C.id_container_level
      LEFT JOIN sample.content_type TCU ON TCU.id = C.id.type_content
      WHERE id_usergroup = 1003""")
    return self.cur.fetchall()

  def getAllPrelevmentsGroupFiltred(self, sessionId, nameTaking):
    # TODO change value taking to id
  # get All Prelevments
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    taking = []
    sql = """
    SELECT
      C.id AS id_container,
      P.id AS id_prelevement,
      P.date AS date,
      T.type AS type_tissu,
      C.code AS code_contenant,
      TC.type AS type_contenant,
      EC.level AS etat_contenant,
      TCU.type AS type_contenu,
      C.comment as remarque
    FROM sample.container C
      LEFT JOIN sample.taking P ON P.id_container=C.id
      LEFT JOIN plant.group_has_usergroup GAG ON GAG.id_group=P.id_plant_group
      LEFT JOIN sample.tissue_type T ON T.id=P.id_tissue_type
      LEFT JOIN sample.container_type TC ON TC.id = C.id_container_type
      LEFT JOIN sample.container_level EC ON EC.id= C.id_container_level
      LEFT JOIN sample.content_type TCU ON TCU.id = C.id_content_type
    WHERE
      C.code = %s
      AND P.id IS NOT NULL
    """
    self.cur.execute(sql, [nameTaking])
    taking = self.cur.fetchall()
    return taking

  def getAllUserByPrelevment(self, idPrelevment):
  # get All users by Prelevments
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT
        P.id AS id_prelevement,
        U.firstname,
        U.lastname
      FROM
        sample.taking P
        LEFT JOIN sample.user_a_taking UT ON UT.id_taking = P.id
        LEFT JOIN users.user U ON U.id = UT.id_user
      WHERE P.id= %s""",(idPrelevment,))
    return self.cur.fetchall()

  def getLotGeneticsInformationsByPrelevment(self, id_prelevment):
  # get nom de la plante, genotype, mutant , genes mutes
  # mutant si fond génétique != genotype
  # si pas de génotype, on prend le numero d'accession
    id_prelevment=str(id_prelevment)
    info={}
    #recherche le numero lot et le génotype
    self.cur1 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur1.execute("""SELECT NV.value AS genotype, L.name AS numero_lot , CAST(P.date AS VARCHAR(10) ) AS date_prelevement
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_lot GAL ON GAL.id_group=P.id_plant_group
    LEFT JOIN plant.lot L ON L.id_lot=GAL.id_lot
    LEFT JOIN plant.accession C ON C.id=L.id_accession
    LEFT JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
    LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
    WHERE TNV.value='génotype' AND P.id =""" + id_prelevment + """
    """, ())
    names=self.cur1.fetchall()
    if len(names)>0 : # le type_nom genotype existe
      info['name']=names[0]['numero_lot']
      info['date prelevement']=names[0]['date_prelevement']
    else : # le type_nom genotype n'existe pas on prend le numero d'accession
      self.cur1.execute("""SELECT C.introduction_name AS genotype, L.name AS numero_lot , CAST(P.date AS VARCHAR(10) ) AS date_prelevement
      FROM sample.taking P
      LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
      LEFT JOIN plant.group_has_lot GAL ON GAL.id_group=P.id_plant_group
      LEFT JOIN plant.lot L ON L.id_lot=GAL.id_lot
      LEFT JOIN plant.accession C ON C.id=L.id_accession
      LEFT JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
      LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
      WHERE  P.id =""" + id_prelevment + """
      """, ())
      names=self.cur1.fetchall()
      info['name']=names[0]['numero_lot']
      info['date prelevement']=names[0]['date_prelevement']
    self.cur2 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur2.execute("""SELECT TN.name as nom, valeur_notation_texte
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_lot GAL ON GAL.id_group=P.id_plant_group
    LEFT JOIN plant.lot L ON L.id_lot=GAL.id_lot
    LEFT JOIN plant.accession C ON C.id=L.id_accession
    LEFT JOIN notation.variety_has_notation VAN ON VAN.id_variete=C.id_variety
    LEFT JOIN notation.notation_texte NT ON NT.id_notation=VAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE P.id =""" + id_prelevment + """
    """, ())
    notations=self.cur2.fetchall()
    if len(notations)>0 :
      info['fond genetique']=None
      info['gene mute']=None
      info['mutant']=names[0]['genotype']
      for n in notations :
        if n['nom']=='fond génétique':
          info['fond genetique']=n['valeur_notation_texte']
        if n['nom']=='gène muté':
          info['gene mute']=n['valeur_notation_texte']

      if info['fond genetique']==None :
        info['fond genetique']=names[0]['genotype']
        info['mutant']=None
    else :
      info['fond genetique']=info['genotype']
      info['gene mute']=None
      info['mutant']=None
    return info

  def getInfosPlantsByPrelevment(self, id_prelevment, contexte_notation):
  # get informations notations sur la plante
  # contexte "'sample : plants informations'"
    id_prelevment=str(id_prelevment)
    info={}
    #recherche les notations associées au contexte
    self.cur1 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur1.execute("""SELECT TN.name as nom FROM notation.context CN
LEFT JOIN notation.lien_contexte_notation LCN  ON LCN.id_context=CN.id
LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
where CN.name=""" + contexte_notation + """
    """, ())
    notation=self.cur1.fetchall()
    #recherche les valeurs de ces notations
    self.cur1 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    for n in notation :
      self.cur1.execute("""SELECT NT.valeur_notation_texte AS notation_lot
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=P.id_plant_group
    LEFT JOIN notation.notation_texte NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_integer AS VARCHAR(10) ) AS notation_lot
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=P.id_plant_group
    LEFT JOIN notation.notation_integer NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_double AS VARCHAR(10) ) AS notation_lot
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=P.id_plant_group
    LEFT JOIN notation.notation_double NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_date AS VARCHAR(10) ) AS notation_lot
    FROM sample.taking P
    LEFT JOIN plant.group G ON G.id_group=P.id_plant_group
    LEFT JOIN plant.group_has_notation GAN ON GAN.id_group=P.id_plant_group
    LEFT JOIN notation.notation_date NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
    """, ())
      notes=self.cur1.fetchall()
      info[n['nom']]=[]
      for i in notes:
        if i['notation_lot']:
          info[n['nom']].append(i['notation_lot'])
    return info

  def getInfosPrelevmentByPrelevment(self, id_prelevment, contexte_notation):
  # get informations sur le prelevement
  # contexte "'sample : prelevment informations'"
    id_prelevment=str(id_prelevment)
    info={}
    self.cur1 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #valeur du type tissu prélevé
    self.cur1.execute(""" SELECT TT.type
    from sample.taking P
    JOIN sample.tissue_type TT ON TT.id=P.id_tissue_type
    WHERE P.id =""" + id_prelevment + """
    """, ())
    typeT=self.cur1.fetchall()[0]['type']
    info['organe']=typeT
    #recherche les notations associées au contexte
#     self.cur1.execute("""SELECT nom FROM notation.context CN
#     LEFT JOIN notation.lien_contexte_notation LCN  ON LCN.id_context=CN.id
#     LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
#     where CN.name=""" + contexte_notation + """
#     """, ())
#     notation=self.cur1.fetchall()
#     #recherche les valeurs de ces notations
#     for n in notation:
#       self.cur1.execute("""
#       SELECT NT.valeur_notation_texte AS notation_prelevement
#     FROM sample.taking P
#     LEFT JOIN notation.taking_has_notation PAN ON PAN.id_taking= P.id
#     LEFT JOIN notation.notation_texte NT ON NT.id_notation=PAN.id_notation
#     LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
#     WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
# UNION
# SELECT CAST(NT.valeur_notation_integer AS VARCHAR(10) ) AS notation_prelevement
#     FROM sample.taking P
#     LEFT JOIN notation.taking_has_notation PAN ON PAN.id_taking=P.id
#     LEFT JOIN notation.notation_integer NT ON NT.id_notation=PAN.id_notation
#     LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
#     WHERE P.id =""" + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
# UNION
# SELECT CAST(NT.valeur_notation_double AS VARCHAR(10) ) AS notation_prelevement
#     FROM sample.taking P
#     LEFT JOIN notation.taking_has_notation PAN ON PAN.id_taking=P.id
#     LEFT JOIN notation.notation_double NT ON NT.id_notation=PAN.id_notation
#     LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
#     WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
# UNION
# SELECT CAST(NT.valeur_notation_date AS VARCHAR(10) ) AS notation_prelevement
#     FROM sample.taking P
#     LEFT JOIN notation.taking_has_notation PAN ON PAN.id_taking=P.id
#     LEFT JOIN notation.notation_date NT ON NT.id_notation=PAN.id_notation
#     LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
#     WHERE P.id = """ + id_prelevment + """ AND TN.name=\'""" + n['nom'] + """\'
#     """, ())
#       notes=self.cur1.fetchall()
#       info[n['nom']]=[]
#       for i in notes:
#         if i['notation_prelevement']:
#           info[n['nom']].append(i['notation_prelevement'])
    return info

  def getNotationsPrelevement(self, id_prelevment):
  # get notations sur le prelevement
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id_notation FROM notation.taking_has_notation
                         WHERE id_taking = %s""", (id_prelevment,))

    idNotations=self.cur.fetchall()
    notations=[]
    for i in idNotations :
      self.cur.execute ("""SELECT TN.name AS notation, CAST( N.valeur_notation_integer AS VARCHAR(10) )AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' ) AS date_notation
                           FROM notation.notation_integer N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation,  CAST( N.valeur_notation_double AS VARCHAR(10) )AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_double N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT  TN.name AS notation, N.valeur_notation_texte AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_texte N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation, to_char(N.valeur_notation_date,'DD/MM/YYYY' ) AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_date N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                            """,(i['id_notation'],i['id_notation'],i['id_notation'],i['id_notation'],))
      notes=self.cur.fetchall()
      self.cur.execute ("""SELECT id_user,firstname, lastname FROM notation.notation_has_user UAN
                           JOIN users.user U ON U.id= UAN.id_user
                           WHERE id_notation= %s """, (i['id_notation'],))
      user=self.cur.fetchall()
      notations.append({'notations':notes,'user':user})
    return notations

  def getAllLotsforPrelev(self, idGroup):
  # get All lot  by group  (en developpement [1008])
  #renvoie numero lot, numero accession, nom_variete_genotype + id_clone, id_lot, id_variete
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT L.name AS numero_lot ,C.introduction_name AS numero_accession, C.id AS id_clone, L.id_lot, C.id_variety AS id_variete , null AS genotype
    FROM plant.lot L
    JOIN plant.group_has_lot GAL ON GAL.id_lot=L.id_lot
    JOIN plant.accession C ON C.id=L.id_accession
    JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
    LEFT JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
    LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
    WHERE   GAG.id_usergroup= ANY(%s)
      """,(idGroup,))
    return self.cur.fetchall()

  def getAllLotsforPrelevFiltred(self, idGroup,string):
  # get All lot  by group  (en developpement [1008])
  #renvoie numero lot, numero accession, nom_variete_genotype + id_clone, id_lot, id_variete
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT DISTINCT L.name AS numero_lot ,C.introduction_name AS numero_accession, C.id AS id_clone, L.id_lot, C.id_variety AS id_variete
    FROM plant.lot L
    JOIN plant.group_has_lot GAL ON GAL.id_lot=L.id_lot
    JOIN plant.accession C ON C.id=L.id_accession
    JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
    LEFT JOIN plant.variety_name NV ON NV.id_variety =C.id_variety
    LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
    WHERE   GAG.id_usergroup= ANY(%s)   AND L.name ~* %s OR C.introduction_name ~* %s
      """,(idGroup,string,string,))
    return self.cur.fetchall()

  def getAllLotsforPrelevFiltredByGenre(self, sessionId,tab):
  # get All lot  by group  (en developpement [1008])
  #renvoie numero lot, numero accession, nom_variete_genotype + id_clone, id_lot, id_variete
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT DISTINCT L.name AS numero_lot ,C.introduction_name AS numero_accession, C.id AS id_clone, L.id_lot, C.id_variety AS id_variete
    FROM plant.lot L
    JOIN plant.group_has_lot GAL ON GAL.id_lot=L.id_lot
    JOIN plant.accession C ON C.id=L.id_accession
    LEFT JOIN plant.variety V ON V.id_variete=C.id_variety
    LEFT JOIN plant.taxonomy T ON T.id = V.id_taxonomy
    WHERE L.name ~* %s AND T.genus = %s
      """,(tab['filtredText'],tab['genre'],))
    return self.cur.fetchall()

  def getNotationsVarietesforPrelev(self, idVariete):
  #get gene mute et fond genetique des varietes

    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute(""" SELECT nom, valeur_notation_texte FROM notation.variety_has_notation VAN
    LEFT JOIN notation.notation_texte NT ON NT.id_notation=VAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE VAN.id_variete=%s """,(idVariete,))
    info={}
    info['génotype']= None
    for n in self.cur.fetchall() :
      info[n['nom']]=n['valeur_notation_texte']
    self.cur.execute("""
    SELECT  NV.value AS genotype
    FROM plant.variety_name NV
    LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
    WHERE TNV.valeur='génotype' AND  NV.id_variety = %s """,(idVariete,))
    for n in self.cur.fetchall() :
      info['génotype']=n['genotype']
    return info

  def getAllNomsVarieteByIdVariete(self, idVariete,tab):
  #get noms des varietes
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT  NV.value, TNV.valeur AS type
    FROM plant.variety_name NV
    JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
    WHERE NV.id_variety = %s """,(idVariete,))
    return self.cur.fetchall()

  def getNotationsLotsforPrelev(self, idlot,contexte_notation):
  #recherche les notations associées au lots définies dans un contexte
  # contexte "'sample : plants informations'"
    #--change le format date
    def newDate(date):
      if len(date) == 10 :
        d=date.split('-')
        return(d[2]+'/'+d[1] +'/'+d[0] )
      else :
        return None
    self.cur1 = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur1.execute("""SELECT nom FROM notation.context CN
    LEFT JOIN notation.lien_contexte_notation LCN  ON LCN.id_context=CN.id
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
    where CN.name=\'""" + contexte_notation + """\'""", ())
    notation=self.cur1.fetchall()
    #recherche les valeurs de ces notations
    info={}
    idlot=str(idlot)
    for n in notation :
      self.cur1.execute("""
      SELECT NT.valeur_notation_texte AS notation_prelevement, NT.date, NT.comment AS remarque_notation
    FROM plant.group_has_lot GAL
    left join plant.group_has_notation GAN ON GAL.id_group=GAN.id_group
    LEFT JOIN notation.notation_texte NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE GAL.id_lot=""" + idlot + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_integer AS VARCHAR(10) ) AS notation_prelevement, NT.date, NT.comment AS remarque_notation
    FROM plant.group_has_lot GAL
    left join plant.group_has_notation GAN ON GAL.id_group=GAN.id_group
    LEFT JOIN notation.notation_integer NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE GAL.id_lot=""" + idlot + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_double AS VARCHAR(10) ) AS notation_prelevement , NT.date, NT.comment AS remarque_notation
    FROM plant.group_has_lot GAL
    left join plant.group_has_notation GAN ON GAL.id_group=GAN.id_group
    LEFT JOIN notation.notation_double NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE GAL.id_lot= """ +idlot + """ AND TN.name=\'""" + n['nom'] + """\'
UNION
SELECT CAST(NT.valeur_notation_date AS VARCHAR(10) ) AS notation_prelevement , NT.date, NT.comment AS remarque_notation
    FROM plant.group_has_lot GAL
    left join plant.group_has_notation GAN ON GAL.id_group=GAN.id_group
    LEFT JOIN notation.notation_date NT ON NT.id_notation=GAN.id_notation
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation = NT.id_type_notation
    WHERE GAL.id_lot= """ + idlot + """ AND TN.name=\'""" + n['nom'] + """\'
    """)
      notes=self.cur1.fetchall()
      info[n['nom']]=[]
      info[n['nom']+'.date']=[]
      #info[n['nom']+'.notateur']=[]
      info[n['nom']+'.remarque']=[]
      for i in notes:
        if i['notation_prelevement']:
          info[n['nom']].append(i['notation_prelevement'])
          info[n['nom']+'.date'].append(newDate(str(i['date_notation'])))
          #info[n['nom']+'.notateur'].append(i['notateur'])
          info[n['nom']+'.remarque'].append(i['remarque_notation'])
    return info

  def getAllTypeTissu(self):
  #liste les types de tissus prélevés
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT type FROM sample.tissue_type""")
    return self.cur.fetchall()

  def getAllContentType(self,sessionId,tab):
  #liste les types de contenus des echantillons
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT type FROM sample.content_type""")
    return self.cur.fetchall()

  def getAllActionType(self,sessionId,tab):
  #liste les types de contenus des echantillons
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id, type FROM sample.action_type""")
    return self.cur.fetchall()

  def getAllContainerLevel(self,sessionId,tab):
  #liste les container level des echantillons
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT level FROM sample.container_level""")
    return self.cur.fetchall()

  def getAllContainerType(self,sessionId,tab):
  #liste les container level des echantillons
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,type FROM sample.container_type""")
    return self.cur.fetchall()

  def getAllTypeContenu(self):
  #liste les types contenus dans les contenants (Exemple : adn, feuilles broyée)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,type FROM sample.content_type""")
    return self.cur.fetchall()
  def getAllTypeContenant(self):
  #liste les types contenants
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id, type FROM sample.container_type""")
    return self.cur.fetchall()
  def getAllTypeContainerByBox(self, sessionId,tab):
  #liste les types contenants par type de boite
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT CT.type FROM sample.container_type CT
    join sample.box_type_a_container_type BTACT ON BTACT.id_container_type=CT.id
    JOIN sample.box_type BT ON BT.id=BTACT.id_box_type
    WHERE BT.name=%s""",(tab['typeBox'],))
    return self.cur.fetchall()
  def getEtatContenant(self):
  #liste les états contenants (exemple plein, vide)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT level FROM sample.container_level""")
    return self.cur.fetchall()
  def getAllUser(self):
  #liste tous les utilisateurs valides
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id, firstname,lastname,login,email,phone FROM users.user WHERE active=true ORDER BY lastname""")
    return self.cur.fetchall()
  def getAllGroupsByLot(self,idLot):
  # liste les groupes auxquels appartient ce lot (bien souvent un seul lot ...)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT G.id_group AS id_groupe, name AS nom, description FROM plant.group G
    JOIN plant.group_has_lot GAL ON GAL.id_group=G.id_group
    WHERE id_lot=%s""",(idLot,))
    return self.cur.fetchall()

#  def createPrelev(self, idGroup, date,typeTissu,typeContenant, etatContenant,typeContenu, codeContenant, valide, utilisable, remarque,idBox,colName,rowName ):
#  #create prelevment + contenant+ informations associées
#  #exemple :
#  #createPrelev(118105,'11/04/2017','feuille','tube','plein','feuille congelée','1er tube test','true','false','prelevement test')
#    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#    #identidie l'id du typeTissu
#    self.cur.execute("""SELECT id FROM sample.tissue_type WHERE type=%s """,(typeTissu,))
#    idTypeTissu= self.cur.fetchall()[0]['id']
#    #identidie l'id du typeContenant
#    self.cur.execute("""SELECT id FROM sample.container_type WHERE type=%s """,(typeContenant,))
#    idTypeContenant= self.cur.fetchall()[0]['id']
#    #identidie l'id du etatContenant
#    self.cur.execute("""SELECT id FROM sample.container_level WHERE level=%s """,(etatContenant,))
#    idEtatContenant= self.cur.fetchall()[0]['id']
#    #identidie l'id du typeContenu
#    self.cur.execute("""SELECT id FROM sample.content_type WHERE type=%s """,(typeContenu,))
#    idTypeContenu= self.cur.fetchall()[0]['id']
#
#    #1create contenant
#    try :
#      self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,remark) VALUES (%s,%s,%s,%s,%s,%s,%s )
#    RETURNING id""",(codeContenant,valide, utilisable,idTypeContenant,idEtatContenant,idTypeContenu,remarque))
#      idContenant=self.cur.fetchall()[0]['id']
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#
#    #3create prelevement
#    try :
#      self.cur.execute("""INSERT INTO sample.taking (id_plant_group,date,id_type_tissu,id_container) VALUES (%s, %s,%s, %s)
#    RETURNING id""",(idGroup,date,idTypeTissu,idContenant,))
#      idPrelev=self.cur.fetchall()[0]['id']
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#    return [{'id_container':idContenant,'id_prelevement':idPrelev}]

  def createPrelevWithTab(self, tab):
  #create prelevment + contenant+ informations associées
  #exemple :
  #createPrelev(118105,'11/04/2017','feuille','tube','plein','feuille congelée','1er tube test','true','false','prelevement test')

    idGroup = tab['idGroup']
    date = tab['date']
    typeTissu = tab['type_tissu']
    typeContenant = tab['type_contenant']
    etatContenant = tab['etat']
    typeContenu = tab['type_contenu']
    codeContenant = tab['code_contenant']
    valide = tab['valide']
    utilisable = tab['utilisable']
    remarque = tab['remarque']
    idBox=tab['idBox']
    colName=tab['colName']
    rowName=tab['rowName']

    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #identidie l'id du typeTissu
    self.cur.execute("""SELECT id FROM sample.tissue_type WHERE type=%s """,(typeTissu,))
    idTypeTissu= self.cur.fetchall()[0]['id']
    #identidie l'id du typeContenant
    self.cur.execute("""SELECT id FROM sample.container_type WHERE type=%s """,(typeContenant,))
    idTypeContenant= self.cur.fetchall()[0]['id']
    #identidie l'id du etatContenant
    self.cur.execute("""SELECT id FROM sample.container_level WHERE level=%s """,(etatContenant,))
    idEtatContenant= self.cur.fetchall()[0]['id']
    #identidie l'id du typeContenu
    self.cur.execute("""SELECT id FROM sample.content_type WHERE type=%s """,(typeContenu,))
    idTypeContenu= self.cur.fetchall()[0]['id']

    #create contenant
    try :
      self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,comment) VALUES (%s,%s,%s,%s,%s,%s,%s )
    RETURNING id""",(codeContenant,valide, utilisable,idTypeContenant,idEtatContenant,idTypeContenu,remarque))
      idContenant=self.cur.fetchall()[0]['id']
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      #2-creation location
    try :
      self.cur.execute("""insert into sample.location (id_box,id_container,"column","row",date_on) values (%s,%s,%s,%s,%s) returning * """,(idBox,idContenant,colName,rowName,date))
    except Exception as e :
      raise e
    else :
      self.conn.commit()
      self.cur.fetchall()
    #create prelevement
    try :
      self.cur.execute("""INSERT INTO sample.taking (id_plant_group,date,id_tissue_type,id_container) VALUES (%s, %s,%s, %s)
    RETURNING id""",(idGroup,date,idTypeTissu,idContenant,))
      idPrelev=self.cur.fetchall()[0]['id']
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    return [{'id_container':idContenant,'id_taking':idPrelev}]

#  #Action de transformation d'un échantillon par une action, avec un tube origine et un tube enfant
##  // NE FONCTIONNE PLUS DU A MODIFICATION DE LA BASE //
#  def transformSample(self,userGroup, typeAction, idContainerOrigine,idUser, date,ListeNotationsAction,typeContenant, etatContenant,typeContenu, codeContenant, valide, utilisable, remarque):
#  #create action + contenant+ informations associées
#  #exemple qui marche:
#  #glams.transformSample([1009],'extraction ADN',6100,1018,'07/07/2017', None,'tube 1.5 ml','plein','ADN en solution','2eme tube test','true','false','prelevement test')
#
#  #Alors sera possible : transformSample([1009],'extraction ADN',6100,1018,'07/07/2017',  [{'type_notation':'manipulation','valeur_notation':'Aisée','date_notation':'2017-07-07','remarque_notation':'No Problem','id_site':2,'listeUser':[1007,1018]}] ,'tube 1.5 ml','plein','ADN en solution','2eme tube test','true','false','prelevement test')
#
#    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#    crb= elvis.CrbTools()
#    #identidie l'id du typeAction
#    self.cur.execute("""SELECT id FROM sample.action_type WHERE type=%s """,(typeAction,))
#    idTypeAction= self.cur.fetchall()[0]['id']
#    #identidie l'id du typeContenant
#    self.cur.execute("""SELECT id FROM sample.container_type WHERE type=%s """,(typeContenant,))
#    idTypeContenant= self.cur.fetchall()[0]['id']
#    #identidie l'id du etatContenant
#    self.cur.execute("""SELECT id FROM sample.container_level WHERE level=%s """,(etatContenant,))
#    idEtatContenant= self.cur.fetchall()[0]['id']
#    #identidie l'id du typeContenu
#    self.cur.execute("""SELECT id FROM sample.content_type WHERE type=%s """,(typeContenu,))
#    idTypeContenu= self.cur.fetchall()[0]['id']
#
#    #1 create contenant
#    try :
#      self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,remark) VALUES (%s,%s,%s,%s,%s,%s,%s )
#    RETURNING id""",(codeContenant,valide, utilisable,idTypeContenant,idEtatContenant,idTypeContenu,remarque))
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#      idContainerChild=self.cur.fetchall()[0]['id']

#    #2 create action
#    try :
#      self.cur.execute("""INSERT INTO sample.action (id_action_type,id_container_origin,id_container_child,id_user,date) VALUES(%s,%s,%s,%s,%s) RETURNING id""",(idTypeAction,idContainerOrigine,idContainerChild,idUser,date,))
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#      idAction=self.cur.fetchall()[0]['id']
#

#    #3 Ajout des notations
#    if ListeNotationsAction is not None :
#      for n in ListeNotationsAction:
#        typeNotation=n['type_notation']
#        valeurNotation=n['valeur_notation']
#        date=n['date_notation']
#        remarque=n['remarque_notation']
#        site=n['id_site']
#        listeUser=n['listeUser']
#        idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
#        #affecte un groupe à la notation
#        for u in userGroup:
#          self.setUserGroupeANotation(u, idNotation)
#        if listeUser is not None:
#          for idUser in listeUser:
#            self.addUserToNotation (idUser,idNotation)
#        #ajout à la table action_a_notation (à quelle action est affectée la notations)
#        self.setActionANotation(idAction, idNotation)
#    return [{'id_action':idAction,'id_container_child':idContainerChild}]

  def addUserToPrelevment(self,idUser,idPrlvmt ):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
        self.cur.execute(""" INSERT INTO  sample.user_a_prelevment (id_user,id_prelevment) VALUES (%s,%s) RETURNING id_user,id_prelevment """,(idUser,idPrlvmt))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return [{'id_user': idUser, 'id_prelevment':idPrlvmt}]

  def addNotation(self,sessionId,tab) :
    type_notation=tab['idTypeNotation']
    site=tab['site']
    date=tab['date']
    remarque=tab['remarque']
    valeur=tab['valeur']

    crb= elvis.CrbTools()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #1 recherche type_notation et id_site
    typeNotation = crb.getTypeNotation(type_notation)[0];
    type_valeur = typeNotation["type"];
    type_notation = typeNotation["id_type_notation"]
    if site is None:
      idSite = None
    else:
      idSite = crb.getSite(site)[0]['id_site']
    #2 Ajout de la notation
    if (type_valeur =="integer"):
      try :
        self.cur.execute("INSERT INTO notation.notation_integer (date, comment, id_site,valeur_notation_integer,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="date"):
      try :
        self.cur.execute("INSERT INTO notation.notation_date (date, comment, id_site,valeur_notation_date,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="texte"):
      try :
        self.cur.execute("INSERT INTO notation.notation_texte (date, comment, id_site,valeur_notation_texte,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="double"):
      try :
        self.cur.execute("INSERT INTO notation.notation_double (date, comment, id_site,valeur_notation_double,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()

    return idNotation

  def addNotationPrelevOriginal(self,idPrelev,date,remarque,site,type_notation,valeur):
    crb= elvis.CrbTools()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #1 recherche type_notation et id_site
    typeNotation = crb.getTypeNotation(type_notation)[0];
    type_valeur = typeNotation["type"];
    type_notation = typeNotation["id_type_notation"]
    if site is None:
      idSite = None
    else:
      idSite = crb.getSite(site)[0]['id_site']
    #2 Ajout de la notation
    if (type_valeur =="integer"):
      try :
        self.cur.execute("INSERT INTO notation.notation_integer (date, comment, id_site,valeur_notation_integer,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="date"):
      try :
        self.cur.execute("INSERT INTO notation.notation_date (date, comment, id_site,valeur_notation_date,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="texte"):
      try :
        self.cur.execute("INSERT INTO notation.notation_texte (date, comment, id_site,valeur_notation_texte,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="double"):
      try :
        self.cur.execute("INSERT INTO notation.notation_double (date, comment, id_site,valeur_notation_double,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    #2 ajout dans prelevement_a_notation
    try :
      self.cur.execute("INSERT INTO notation.taking_has_notation (id_taking, id_notation) VALUES (%s,%s) RETURNING id_taking, id_notation",(idPrelev,idNotation))
      idNotation=self.cur.fetchall()[0]['id_notation']
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    return  idNotation

  def getUserByNotation(self,idNotation ):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT U.firstname, U.lastname , U.id AS id_user, UAN.id_notation from notation.notation_has_user UAN
    left join users.user U ON U.id=UAN.id_user WHERE id_notation=%s""",(idNotation,))
    return self.cur.fetchall()

  def addNotationPrelev(self, tab,sessionId):
    usergroup = tab['usergroup']
    idPrelev = tab['idPrelev']
    date = tab['date']
    remarque = tab['remarque']
    site = tab['site']
    type_notation = tab['type_notation']
    valeur = tab['valeur']

    crb= elvis.CrbTools()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #1 recherche type_notation et id_site
    typeNotation = crb.getTypeNotation(type_notation)[0];
    type_valeur = typeNotation["type"];
    type_notation = typeNotation["id_type_notation"]
    if site is None:
      idSite = None
    else:
      idSite = crb.getSite(site)[0]['id_site']
    #2 Ajout de la notation
    if (type_valeur =="integer"):
      try :
        self.cur.execute("INSERT INTO notation.notation_integer (date, comment, id_site,valeur_notation_integer,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="date"):
      try :
        self.cur.execute("INSERT INTO notation.notation_date (date, comment, id_site,valeur_notation_date,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="texte"):
      try :
        self.cur.execute("INSERT INTO notation.notation_texte (date, comment, id_site,valeur_notation_texte,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    if (type_valeur =="double"):
      try :
        self.cur.execute("INSERT INTO notation.notation_double (date, comment, id_site,valeur_notation_double,id_type_notation) VALUES (%s,%s,%s,%s,%s) RETURNING *",(date,remarque,idSite,valeur,type_notation))
        idNotation=self.cur.fetchall()[0]['id_notation']
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    #2 ajout dans notation_a_usergroup
    try :
      self.cur.execute("INSERT INTO notation.notation_has_usergroup (id_usergroup, id_notation) VALUES (%s,%s) RETURNING id_usergroup, id_notation",(usergroup,idNotation))
      idNotation=self.cur.fetchall()[0]['id_notation']
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    #3 ajout dans prelevement_a_notation
    try :
      self.cur.execute("INSERT INTO notation.taking_has_notation (id_taking, id_notation) VALUES (%s,%s) RETURNING id_taking, id_notation",(idPrelev,idNotation))
      idNotation=self.cur.fetchall()[0]['id_notation']
    except Exception as e:
      raise e
    else :
      self.conn.commit()

    return  idNotation

  def createLotFromLotParent(self,idLotParent,numeroLot,dateMultiplication,annePremierePousse,porteGreffe,equilibre,dateRecolte,quantite,rangEvaluation ):
  #cree un lot à partir d'un lot partent.
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #1 teste si arbre ou graines
    self.cur.execute("""SELECT id_lot, id_accession AS clone FROM plant.tree WHERE id_lot=%s """,(idLotParent,))
    lotParent=self.cur.fetchall()
    typeLot='arbre'
    if len(lotParent)==0 :
      self.cur.execute("""SELECT id_lot,id_accession AS clone FROM plant.seed WHERE id_lot=%s """,(idLotParent,))
      lotParent=self.cur.fetchall()
      typeLot='graines'
    #3 cree le new lot
    if typeLot=='arbre':
      try :
        self.cur.execute("""INSERT INTO plant.tree (name,id_accession,id_lot_source,multiplication_date,first_shoot_year,rootstock)  VALUES (%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(numeroLot,lotParent[0]['clone'],idLotParent,dateMultiplication,annePremierePousse,porteGreffe))
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    else :
      try :
        self.cur.execute("""INSERT INTO plant.seed (name,id_accession,id_lot_source,multiplication_date,harvesting_date,quantity)  VALUES (%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(numeroLot,lotParent[0]['clone'],idLotParent,dateMultiplication,dateRecolte,quantite))
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    return self.cur.fetchall()[0]['id_lot']

#affecte un groupe_plante à une liste de usergroup
  def setGhGroup(self, id_ghgroup, id_group):  #id_ghgroup est une liste des groupe
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    for idghgroup in id_ghgroup:
      try :
        self.cur.execute("INSERT INTO plant.group_has_usergroup (id_group,id_usergroup) VALUES (%s,%s) RETURNING *",(id_group,idghgroup));
      except Exception as e:
        raise e
      else :
        self.conn.commit()
#Ajout la notation à un usergroup
  def setUserGroupeANotation(self,usergroup, idNotation):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO notation.notation_has_usergroup (id_usergroup, id_notation) VALUES (%s,%s) RETURNING id_usergroup, id_notation",(usergroup,idNotation))
      idNotation=self.cur.fetchall()[0]['id_notation']
    except Exception as e:
      raise e
    else :
      self.conn.commit()
# recupère la liste des variété dont un nom contient la chaine de carractere données
  def getListeVarieteByNomByGenre(self,sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    liste=[]
    self.cur.execute("""SELECT DISTINCT id_variete, genus AS genre, specie AS espece from plant.variety V
                        LEFT JOIN plant.variety_name NV ON NV.id_variety =V.id_variete
                        LEFT JOIN plant.taxonomy T ON T.id=V.id_taxonomy
                        LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                        LEFT JOIN plant.lot L ON L.id_accession=C.id
                        where NV.value ~* %s AND T.genus=%s """,(tab['nomVar'],tab['genre'],))

    listeIdVar=self.cur.fetchall()
    for i in listeIdVar:
        variete={}
        idVar=i['id_variete']
        variete['genre']=i['genre']
        variete['espèce']=i['espece']
        variete['id_variete']=idVar
        variete['noms']=[]
        self.cur.execute("""select id_nom_var, nom,valeur from plant.variety V
                            LEFT JOIN plant.variety_name NV ON NV.id_variety =V.id_variete
                            LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
                            where id_variete=%s""",(idVar,))
        listeNoms=self.cur.fetchall()

        if listeNoms is not None :
          for n in listeNoms:
            coupleNoms={}
            coupleNoms['typeNom']=n['valeur']
            coupleNoms['valeur']=n['nom']
            variete['noms'].append(coupleNoms)
        # recupère les numeros de accession
        variete['accession']=[]
        self.cur.execute("""select C.introduction_name as numero from plant.variety V
                            LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                            where id_variete=%s""",(idVar,))
        listeClones=self.cur.fetchall()

        if listeClones is not None :
          for n in listeClones:
            variete['accession'].append(n['numero'])
        # recupère les numeros de lot
        variete['lot']=[]
        self.cur.execute("""select L.name AS numero_lot from plant.variety V
                            LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                            LEFT JOIN plant.lot L ON L.id_accession=C.id
                            where id_variete=%s""",(idVar,))
        listeLots=self.cur.fetchall()
        if listeLots is not None :
          for n in listeLots:
            variete['lot'].append(n['numero_lot'])


        liste.append(variete)
    return liste

# recupère la liste des variété dont un nom contient la chaine de carractere données
  def getListeAccessionByNomByGenre(self,sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    liste=[]
    self.cur.execute("""SELECT DISTINCT id_variete, genus AS genre, specie AS espece, C.introduction_name as numero ,C.id AS id_clone from plant.variety V
                        LEFT JOIN plant.variety_name NV ON NV.id_variety =V.id_variete
                        LEFT JOIN plant.taxonomy T ON T.id=V.id_taxonomy
                        LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                        LEFT JOIN plant.lot L ON L.id_accession=C.id
                        where C.introduction_name ~* %s AND  T.genus=%s""",(tab['nomAcc'],tab['genre'],))
    listeIdVar=self.cur.fetchall()
    for i in listeIdVar:
        variete={}
        idVar=i['id_variete']
        variete['idClone']=i['id_clone']
        variete['genre']=i['genre']
        variete['espèce']=i['espece']
        variete['id_variete']=idVar
        variete['noms']=[]
        self.cur.execute("""select id_nom_var, nom,valeur from plant.variety V
                            LEFT JOIN plant.variety_name NV ON NV.id_variety =V.id_variete
                            LEFT JOIN plant.variety_name_type TNV ON TNV.id_type_nom=NV.id_variety_name_type
                            where id_variete=%s""",(idVar,))
        listeNoms=self.cur.fetchall()

        if listeNoms is not None :
          for n in listeNoms:
            coupleNoms={}
            coupleNoms['typeNom']=n['valeur']
            coupleNoms['valeur']=n['nom']
            variete['noms'].append(coupleNoms)
        # recupère les numeros de accession
        variete['accession']=[]
        self.cur.execute("""select C.introduction_name as numero from plant.variety V
                            LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                            where id_variete=%s""",(idVar,))
        listeClones=self.cur.fetchall()

        if listeClones is not None :
          for n in listeClones:
            variete['accession'].append(n['numero'])
        # recupère les numeros de lot
        variete['lot']=[]
        self.cur.execute("""select L.name AS numero_lot from plant.variety V
                            LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                            LEFT JOIN plant.lot L ON L.id_accession=C.id
                            where id_variete=%s""",(idVar,))
        listeLots=self.cur.fetchall()
        if listeLots is not None :
          for n in listeLots:
            variete['lot'].append(n['numero_lot'])


        liste.append(variete)

    return liste
# recupère la liste des genres utilisés
  def getAllGenres(self,sessionId,tab):
    self.cur.execute("""SELECT DISTINCT genus AS genre from plant.taxonomy ORDER BY genus ASC""",())
    return self.cur.fetchall()

# recupère la liste des genres utilisés par un groupe
  def getAllGenresBygGoup(self,sessionId,tab):
    self.cur.execute("""SELECT DISTINCT genus AS genre from plant.variety V
                        LEFT JOIN plant.variety_name NV ON NV.id_variety =V.id_variete
                        LEFT JOIN plant.taxonomy T ON T.id=V.id_taxonomy
                        LEFT JOIN plant.accession C ON C.id_variety=V.id_variete
                        LEFT JOIN plant.lot L ON L.id_accession=C.id
                        LEFT JOIN plant.group_has_lot GAL ON GAL.id_lot=L.id_lot
                        LEFT JOIN plant.group_has_usergroup GAG ON GAG.id_group=GAL.id_group
                        where  GAG.id_usergroup = ANY(%s) """,(tab['userGroup'],))
    return self.cur.fetchall()
# recupère la liste especes pour un genre
  def getAllEspeceByGenre(self,sessionId,tab):
    self.cur.execute("""SELECT specie AS espece FROM plant.taxonomy WHERE genus= %s """,(tab['genre'],))
    return self.cur.fetchall()

  def deletePrelevementTest(self):
  #delete les prelevements avec le nom test
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""delete from sample.user_a_prelevment
                        where id_prelevment in (select id from sample.taking P
                                                where id_container in (select id from sample.container
                                                                       where code ~* 'test'))""")
    self.conn.commit()
    self.cur.execute(""" delete from notation.notation Where id_notation IN (select id_notation FROM notation.taking_has_notation
                                                                                      where id_taking IN(select id from sample.taking P
                                                                                                              where id_container in (select id from sample.container
                                                                                                                                     where code ~* 'test')));""")
    self.conn.commit()
    self.cur.execute(""" delete FROM notation.taking_has_notation where id_taking IN(select id from sample.taking P
                                                                                                      where id_container in (select id from sample.container
                                                                                                                             where code ~* 'test'));  """)
    self.conn.commit()

    self.cur.execute(""" delete from sample.taking P
                         where id_container in (select id from sample.container
                                                where code ~* 'test') """)
    self.conn.commit()

    self.cur.execute("""delete from sample.location L
                         where id_container in( select id  from sample.container
                        where code ~* 'test')""")

    self.conn.commit()
    self.cur.execute("""delete from sample.container
                        where code ~* 'test'""")

    self.conn.commit()
    return 'ok'

#-------------------------------------#
# functions for contexte of notations #
#-------------------------------------#
# #liste les notations pour un contexte donné
# #exemple WHERE nom_contexte = 'sample : prelevment informations' AND CN.id_usergroup=1009
  def getNotationsTypeByContexte(self,sessionId, nomContexte,UserGroup):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT TN.name AS nom, TN.type, LCN.rank AS order FROM notation.context CN
    LEFT JOIN notation.lien_contexte_notation LCN ON LCN.id_context= CN.id
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
    WHERE CN.name = %s AND CN.id_usergroup=%s
    ORDER BY LCN.rank """,(nomContexte,UserGroup,))
    self.conn.commit()
    return self.cur.fetchall()

 #liste les notations pour un contexte donné
  def getAllTypeNotationByContexte(self,sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT TN.name AS nom, TN.type, LCN.rank FROM notation.context CN
    LEFT JOIN notation.lien_contexte_notation LCN ON LCN.id_context= CN.id
    LEFT JOIN notation.notation_type TN ON TN.id_type_notation=LCN.id_notation_type
    WHERE CN.name = %s AND CN.id_usergroup= ANY(%s)
    ORDER BY LCN.rank """,(tab['nomContexte'],tab['userGroup'],))
    self.conn.commit()
    return self.cur.fetchall()
##---------------------------------#
## functions for series or groupes #
##---------------------------------#
#  #ajout un lot à un groupe
  def addGroupe_a_lot(self,id_group,id_lot):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO plant.group_has_lot (id_group,id_lot) VALUES (%s,%s) RETURNING *",(id_group,id_lot))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    return self.cur.fetchall()

  #selection le numero de groupe max
  def maxIdGroup(self):
    self.cur.execute("SELECT MAX(id_group) FROM plant.group",())
    return self.cur.fetchall()

  #liste des groupes auxquels appartient un lot
  def getIdGroupeByLots(self, idlot):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT G.id_group, G.name as nom, G.description  FROM plant.group G
    JOIN plant.group_has_lot A ON A.id_group=G.id_group
    JOIN plant.tree B ON A.id_lot=B.id_lot WHERE id_lot = %s;""", (idlot,))
    return self.cur.fetchall()

  #création d'un groupe ou d'une série
  def createGroupe(self,id_group, nom, description):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO plant.group (id_group,name,description) VALUES (%s,%s,%s) RETURNING *",(id_group,nom,description))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    return self.cur.fetchall()

  #rattache un groupe à un usergroup
  def addGroupe_a_ghgroup(self,sessionId,id_group, id_usergroup):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("INSERT INTO plant.group_has_usergroup (id_group,id_usergroup) VALUES (%s,%s) RETURNING *",(id_group,id_usergroup))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
    return self.cur.fetchall()

  #cree un groupe  avec une liste de lots, une liste des usergroup,
  def createIdGroupeByLots(self, sessionId,listeidLot,userGroup,name,remarque):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    idg=self.maxIdGroup()[0]["max"]+1
    self.createGroupe(idg,name,remarque)
    for w in userGroup:
      self.addGroupe_a_ghgroup (sessionId,idg,w);
    for idLot in listeidLot :
      self.addGroupe_a_lot(idg,idLot)
    return (idg)

  def getOrCreateGroupeArbres(self,sessionId, listeLot,userGroup,name,remarque) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    listeIdg = []    #liste des idgroupes différents
    compteurIdg = [] # liste de dictionnaire {idgroupe, compteur}
    for i in listeLot :
        groupeIdg=self.getIdGroupeByLots(i) #un lot peut appartenir à plusieurs groupes
        for ii in groupeIdg :
            idg=ii["id_group"]
            if idg :
                if idg not in listeIdg :
                    listeIdg.append(idg)
                    groupe={'idgroupe':idg,'compteur':1}
                    compteurIdg.append(groupe)
                else:
                    for j in compteurIdg :
                        if j["idgroupe"]==idg :
                            j["compteur"]=j["compteur"]+1
    #teste si un groupe contient tous les lots alors compteur = nombre de lots
    for g in compteurIdg :
        if g["compteur"]==len(listeLot) :
            return(g["idgroupe"])
    #cree un groupe avec tous ces numeros de lot
    return(self.createIdGroupeByLots(sessionId,listeLot,userGroup,name,remarque))
#
#------------------------------------#
# functions for location sample      #
#------------------------------------#
  #recupère la place d'un contenant
  def getContainerPlace(self,sessionId,id_container):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#    self.cur.execute(""" SELECT B.id AS id_box, B.name AS boxName, BT.name AS boxType , L.column AS column , L.row AS row , C.code AS containerName , CT.type AS containertype , L.date_on, L.date_off FROM sample.location L
#                         LEFT JOIN sample.box B ON B.id=L.id_box
#                         LEFT JOIN sample.box_type  BT ON BT.id=B.id_box_type
#                         LEFT JOIN sample.container C ON C.id=L.id_container
#                         LEFT JOIN sample.container_type CT ON CT.id=C.id_container_type
#                         WHERE id_container=%s""",(id_container,))
    self.cur.execute(""" SELECT B.id AS id_box, B.name AS boxName, BT.name AS boxType , L.column AS column , L.row AS row , C.code AS containerName , CT.type AS containertype ,CAST( L.date_on AS VARCHAR(10))AS date_on, L.date_off FROM sample.location L
                         LEFT JOIN sample.box B ON B.id=L.id_box
                         LEFT JOIN sample.box_type  BT ON BT.id=B.id_box_type
                         LEFT JOIN sample.container C ON C.id=L.id_container
                         LEFT JOIN sample.container_type CT ON CT.id=C.id_container_type
                         WHERE id_container=%s""",(id_container,))

    return self.cur.fetchall()

  #recupère la place d'une boite
  def getBoxPlace(self, sessionId, id_box):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute(""" SELECT  TLI.value AS type, LI.valeur  FROM sample.box B
                         LEFT JOIN sample.box_type  BT ON BT.id=B.id_box_type
                         LEFT JOIN sample.box_has_place BAL ON BAL.id_box=B.id
                         LEFT JOIN location.place LI ON LI.id_lieu = BAL.id_place
                         LEFT JOIN location.place_type TLI ON TLI.id = LI.id_place_type
                         WHERE B.id=%s""",(id_box,))
    return self.cur.fetchall()

  #recupère la liste des boites existantes
  def getAllBox(self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT b.name as nameBox, bt.name as type  FROM sample.box B
                         LEFT JOIN sample.box_type  BT ON BT.id=B.id_box_type
                         where id_usergroup= %s """,(tab['writeGroup'],))
    return self.cur.fetchall()

  #recupère la liste des types de boites
  def getAllBoxType(self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,name as type,row_number,column_number  FROM sample.box_type """,())
    return self.cur.fetchall()

  #recupère la liste des correspondances types de boites / type de container
  def getAllBoxTypeAContainerType(self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id_box_type,id_container_type  FROM sample.box_type_a_container_type """,())
    return self.cur.fetchall()

  #recupère idBoxType selon le type de boite
  def getIdBoxByType (self, sessionId, boxType):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id FROM sample.box_type WHERE name=%s""",(boxType,))
    return self.cur.fetchall()

  #create box type
  def createBoxType (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""INSERT INTO sample.box_type ( name, row_number, column_number)  VALUES(%s,%s,%s) RETURNING id""",(tab['type'],tab['rowNumber'],tab['colNumber'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idBox= self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête
      #mise à jour de box_type_a_container_type
      if len(tab['typeContainerCorresponding'])>0 :
        for c in tab['typeContainerCorresponding'] :
          if c['value'] == True :
           try :
             self.cur.execute("""INSERT INTO sample.box_type_a_container_type ( id_box_type, id_container_type) VALUES (%s,%s) RETURNING id_box_type""",( idBox,c['id']))
           except Exception as e:
             self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
             self.cur.close()
             self.conn.close()
             raise e #lance une exception donc arrête le déroulement du code
           else :
             self.conn.commit()  #valide les modifications faites par le cursor
             self.cur.fetchall()  # renvoie les lignes resultat de la requête


  # update box type
  def updateBoxType (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""UPDATE sample.box_type SET name=%s , row_number=%s, column_number=%s where id=%s RETURNING id""",(tab['type'],tab['rowNumber'],tab['colNumber'],tab['id'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
#      return self.cur.fetchall()  # renvoie les lignes resultat de la requête
      #mise à jour de box_type_a_container_type
      if len(tab['typeContainerCorresponding'])>0 :
        self.cur.execute("""DELETE FROM sample.box_type_a_container_type WHERE id_box_type=%s RETURNING id_box_type""",( tab['id'],))
        self.conn.commit()
        for c in tab['typeContainerCorresponding'] :
          if c['value'] == True :
           try :
             self.cur.execute("""INSERT INTO sample.box_type_a_container_type ( id_box_type, id_container_type) VALUES (%s,%s) RETURNING id_box_type""",( tab['id'],c['id']))
           except Exception as e:
             self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
             self.cur.close()
             self.conn.close()
             raise e #lance une exception donc arrête le déroulement du code
           else :
             self.conn.commit()  #valide les modifications faites par le cursor
             self.cur.fetchall()  # renvoie les lignes resultat de la requête
      return 'ok'
  #create box type
  def createContainerType (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""INSERT INTO sample.container_type (type)  VALUES(%s) RETURNING id""",(tab['type'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      return self.cur.fetchall()  # renvoie les lignes resultat de la requête

  # update container type
  def updateContainerType (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""UPDATE sample.container_type SET type=%s where id=%s RETURNING id""",(tab['type'],tab['id'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      return self.cur.fetchall()  # renvoie les lignes resultat de la requête

  #recupère la liste des types de boites disponibles pour un type de contenant
  def getAllBoxTypeByContainer(self, sessionId, typeContenant):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #identidie l'id du typeContenant
    self.cur.execute("""SELECT id FROM sample.container_type WHERE type=%s """,(typeContenant,))
    id_container_type = self.cur.fetchall()[0]['id']

    self.cur.execute("""SELECT id, name as type  FROM sample.box_type BT
                        LEFT JOIN sample.box_type_a_container_type BTACT ON BTACT.id_box_type = BT.id
                        where id_container_type = %s
                         """,(id_container_type,))
    return self.cur.fetchall()

#  #insere un container dans une boite à une position
#  #si la boite n'existe pas, (idBox=null ) il la crée
#  def insertContainerIntoBox(self, sessionId, id_container,id_box,nameBox, typeBox, row,column):
#    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#    if id_box is None:
#      idUserGroup=1009
#      id_box_type=self.getIdBoxByType(typeBox)[0]['id']
#      try :
#        self.cur.execute("INSERT INTO sample.box (name,id_usergroup,id_box_type) VALUES (%s,%s,%s) RETURNING *",(nameBox,idUserGroup,id_box_type)  )
#      except Exception as e:
#        raise e
#      else :
#        self.conn.commit()
#        id_box=self.cur.fetchall()[0]['id']
#    try :
#      self.cur.execute("INSERT INTO sample.location (id_box,id_container,\"column\",\"row\") VALUES (%s,%s,%s,%s) RETURNING *",(id_box,id_container,column,row))
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#    return self.cur.fetchall()
#
#      #insere un container dans une boite à une position
#  #si la boite n'existe pas, (idBox=null ) il la crée
#  def insertTypeContainerIntoBox(self, sessionId, typeContenant, id_box, nameBox, typeBox, row, column):
#    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#
#    #identidie l'id du typeContenant
#    self.cur.execute("""SELECT id FROM sample.container_type WHERE type=%s """,(typeContenant,))
#    id_container = self.cur.fetchall()[0]['id']
#
#    if id_box is None:
#      idUserGroup=1009
#      id_box_type=self.getIdBoxByType(typeBox)[0]['id']
#      try :
#        self.cur.execute("INSERT INTO sample.box (name,id_usergroup,id_box_type) VALUES (%s,%s,%s) RETURNING *",(nameBox,idUserGroup,id_box_type)  )
#      except Exception as e:
#        raise e
#      else :
#        self.conn.commit()
#        id_box=self.cur.fetchall()[0]['id']
#    try :
#      self.cur.execute("INSERT INTO sample.location (id_box,id_container,\"column\",\"row\") VALUES (%s,%s,%s,%s) RETURNING *",(id_box,id_container,column,row))
#    except Exception as e:
#      raise e
#    else :
#      self.conn.commit()
#    return self.cur.fetchall()
#

  #recupère tous les types de lieu
  def getAlltypesLieux (self, sessionId):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id, value AS type  FROM location.place_type """,())
    return self.cur.fetchall()

  #recupère tous les types de lieu à partir de l'onthologie
  def getAlltypesLieuxO (self, sessionId):
    term = elvis.terminology()
    idContext = term.getContextByName('Général')['id']
    typeLieu=term.getTermsByTerminologyName('Place',idContext)
    for p in typeLieu :
      p['context_id']= idContext
    return typeLieu

  #cree l'emplacement d'une boite
  #argument une liste des distionnaires : idTypeLieu, valeur
  #exemple listeLieuxTypeslieux=[{'idTypeLieu': 6, 'valeur': 'F'},{'idTypeLieu': 7, 'valeur': 'bio-info'}]
  def createSituationBox ( self, sessionId,idBox, listeLieuxTypeslieux):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    for l in listeLieuxTypeslieux :
      idTypeLieu = l['idTypeLieu']
      valeur = l['valeur']
      try :
        self.cur.execute("INSERT INTO location.place (id_place_type,valeur) VALUES (%s,%s) RETURNING *",(idTypeLieu,valeur))
      except Exception as e:
        raise e
      else :
        self.conn.commit()
        idLieu=self.cur.fetchall()[0]['id_lieu']
      try :
        self.cur.execute("INSERT INTO sample.box_has_place (id_box, id_place) VALUES (%s,%s) RETURNING *",(idBox,idLieu))
      except Exception as e:
        raise e
      else :
        self.conn.commit()
    return 'ok'

  #recupere la liste des noms des boites tab {typeBox,userGroup,selectString}
  def getAllNameBoxByType(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    if tab['selectString'] is None or tab['selectString']==''  :
      self.cur.execute("""select B.name ,B.id from sample.box B
                        JOIN sample.box_type BT ON BT.id = B.id_box_type
                        WHERE BT.name= %s AND B.id_userGroup= ANY (%s)
                        ORDER BY B.name """,(tab['typeBox'],tab['userGroup'],))
      return self.cur.fetchall()
    else :
      self.cur.execute("""select B.name ,B.id from sample.box B
                        JOIN sample.box_type BT ON BT.id = B.id_box_type
                        WHERE BT.name= %s AND B.id_userGroup= ANY(%s) AND B.name ~* %s
                        ORDER BY B.name """,(tab['typeBox'],tab['userGroup'],tab['selectString'],))
      return self.cur.fetchall()

  #recupere la liste des types de boites selon le type de container tab {typeContainer}
  def getAllTypeBoxByType(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""select DISTINCT name  from sample.box_type BT
                        JOIN sample.box_type_a_container_type BAC ON BT.id=BAC.id_box_type
                        JOIN sample.container_type CT ON CT.id=BAC.id_container_type
                        where CT.type=%s
                        ORDER BY name""",(tab['typeContainer'],))
    return self.cur.fetchall()

  #recupere la liste des valeurs de lieu selon le type de lieu
  def getAllValuesLieuxFiltred(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT DISTINCT valeur  from location.place L
                        JOIN location.place_type TL ON TL.id=L.id_place_type
                        WHERE TL.value=%s AND L.valeur ~* %s
                        ORDER BY valeur""",(tab['typeLieu'],tab['filtredText'],))
    return self.cur.fetchall()

  #recherche une boite avec un nom et des emplacements
  def getValidationCreateBox(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    validation = True
    #teste si le nom existe
    self.cur.execute("""SELECT B.id FROM sample.box B
                        JOIN  sample.box_type BT ON BT.id = B.id_box_type
                        WHERE B.name = %s and BT.name=%s """,(tab['nameBox'],tab['nameBoxType'] ))
    listeIdBox=self.cur.fetchall()
    if len(listeIdBox) == 0 :
      return 'Validation = '+str( validation)
    else :
#      #le nom existe, teste si la position est identique
#      #recupère toutes les positions des boites de ce nom
#      for i in listeIdBox :
#        self.cur.execute("""SELECT DISTINCT TL.type, L.valeur AS name FROM sample.box_has_place BAL
#                            JOIN location.place L ON L.id_lieu = BAL.id_lieu
#                            JOIN location.place_type TL ON TL.id=L.id_place_type
#                            WHERE BAL.id_box=%s""",(i['id'],))
#        listeLieux = self.cur.fetchall()
#
#        #teste si listeLieux inclus dans tab['placesBox']
#        flag=0
#        for e in tab['placesBox'] :
#          if e not in listeLieux :
#            flag =+1
#        if flag ==0 :
#          validation = False
#        #teste si tab['placesBox'] inclus dans listeLieux
#        flag=0
#        for e in listeLieux :
#          if e not in tab['placesBox'] :
#            flag =+1
#        if flag ==0 :
#          validation = False
#      if validation :
#        return 'Validation = '+str( validation)
#      return  'boite existante '+str(listeLieux   )+ ' / ' + str(tab['placesBox'])+ ' / ' + 'Validation = '+str( validation)
      return  'Boite existante '

  #création une boite avec un nom et des emplacements
  def setCreateBox(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id from sample.box_type where name= %s""" ,(tab['nameBoxType'],))
    tab['idTypeBox'] =self.cur.fetchall()[0]['id']
    ok=tab['idTypeBox']
    #emplacements
    listeIdLieu=[]
    for p in tab['placesBox'] :
      #emplacement avec terminologie
      self.cur.execute("""select id_type_lieu FROM  location.place_type_has_terminology WHERE id_concept= %s AND id_context= %s""",(p['concept_id'] ,p['context_id'] ,))
      idTypeLieu = self.cur.fetchall()[0]['id_type_lieu']
      self.cur.execute("""SELECT id_lieu, id_plant_place AS emplacement,id_place_type AS type_lieu, valeur, TL.id, TL.value AS type from location.place L
                          JOIN location.place_type TL ON TL.id=L.id_place_type
                          WHERE L.valeur= %s AND L.id_place_type=%s AND id_plant_place is null """,(p['name'],idTypeLieu))
      idLieu= self.cur.fetchall()
      if(len(idLieu))>0 :
        listeIdLieu.append(idLieu[0]['id_lieu'])
      else :
        #create nouveau lieu
        #idTypeLieu déjà present avec la terminologie
        try :
          self.cur.execute("""INSERT INTO location.place (id_plant_place,id_place_type,valeur) VALUES (null, %s, %s) RETURNING id_lieu""",(idTypeLieu,p['name']))
        except Exception as e:
          raise e
        else :
          self.conn.commit()
          idLieu= self.cur.fetchall()
          listeIdLieu.append(idLieu[0]['id_lieu'])
    #creation de la boites
    self.cur.execute("""SELECT id from sample.box_type WHERE name= %s """,(tab['nameBoxType'],))
    idTypeBox= self.cur.fetchall()[0]['id']
    try :
      self.cur.execute("""INSERT INTO sample.box (name,id_usergroup,id_box_type) VALUES (%s, %s, %s) RETURNING id""",(tab['nameBox'],tab['userGroup'],idTypeBox,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      idBox= self.cur.fetchall()[0]['id']
    #ajout des positions
    for p in listeIdLieu :
      try :
        self.cur.execute("""INSERT INTO sample.box_has_place (id_place,id_box,date_on) VALUES (%s, %s,%s) RETURNING id_place as id_lieu""",(p,idBox,tab['date_on']))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e
      else :
        self.conn.commit()
        i=self.cur.fetchall()[0]['id_lieu']
    return str(i)


  #recupere les informations sur le couple Box et boxtype
  def getInfosBoxBoxType(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT BT.name as typeBox, BT.row_number, BT.column_number, B.id_usergroup, B.name as nameBox, B.id AS idbox  from sample.box B
                        JOIN sample.box_type BT ON BT.id=B.id_box_type
                        WHERE B.name=%s AND B.id_usergroup = %s AND BT.name= %s""",(tab['name'],tab['userGroup'],tab['type'] ))
    return self.cur.fetchall()

  #recupere les informations sur les containers dans une boite
  def getAllContainersByBox(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT L.id_box , L.column, L.row,L.id_container, CAST( L.date_on AS VARCHAR(10))AS date_on, CAST( L.date_off AS VARCHAR(10))AS date_off, C.code,C.valid, C.usable,C.comment as remark , CT.type AS typecontainer, CL.level , CTT.type AS contenttype
                        FROM sample.location L
                        JOIN sample.container C ON C.id=L.id_container
                        JOIN sample.container_type CT ON CT.id =C.id_container_type
                        JOIN sample.container_level CL ON CL.id=C.id_container_level
                        JOIN sample.content_type CTT ON CTT.id =C.id_content_type
                        WHERE L.id_box= %s AND L.date_off ISNULL""",(tab['idBox'],))
    listeContainer= self.cur.fetchall()
    resultat={}
    resultat['container']=listeContainer

    return resultat

  #recupere toutes les informations une boite
  def getAllInformationsByBox(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute(""" SELECT B.id , L.column, L.row,L.id_container, CAST( L.date_on AS VARCHAR(10))AS date_on, CAST( L.date_off AS VARCHAR(10))AS date_off, C.code,C.valid, C.usable,C.comment as remark , CT.type AS typecontainer, CL.level , CTT.type AS contenttype, B.name, B.id_usergroup, BT.row_number, BT.column_number
                        FROM sample.box B
                        JOIN sample.box_type BT ON BT.id=B.id_box_type
                        LEFT JOIN sample.location L  ON B.id=L.id_box
                        LEFT JOIN sample.container C ON C.id=L.id_container
                        LEFT JOIN sample.container_type CT ON CT.id =C.id_container_type
                        LEFT JOIN sample.container_level CL ON CL.id=C.id_container_level
                        LEFT JOIN sample.content_type CTT ON CTT.id =C.id_content_type
                        WHERE B.id= %s""",(tab['idBox'],))
    resultat=self.cur.fetchall()
    return resultat

  #recupere les notations sur container
  def getNotationsByContainer(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    idc = tab['idContainer']
    self.cur.execute("""SELECT CAN.id_container, NI.valeur_notation_texte AS valeur , CAN.id_container, TN.name AS notation,CAST(NI.date AS VARCHAR(10))AS date,NI.comment AS remarque,NI.observer AS notateur,NI.id_notation
                          FROM notation.container_has_notation CAN
                          JOIN notation.notation_texte NI ON NI.id_notation=CAN.id_notation
                          JOIN notation.notation_type TN ON TN.id_type_notation = NI.id_type_notation
                          WHERE id_container= %s
                          UNION
                          SELECT CAN.id_container, CAST(NI.valeur_notation_integer AS VARCHAR(10) ) AS valeur, CAN.id_container, TN.name AS notation,CAST(NI.date AS VARCHAR(10))AS date,NI.comment AS remarque,NI.observer AS notateur,NI.id_notation
                          FROM notation.container_has_notation CAN
                          JOIN notation.notation_integer NI ON NI.id_notation=CAN.id_notation
                          JOIN notation.notation_type TN ON TN.id_type_notation = NI.id_type_notation
                          WHERE id_container= %s
                          UNION
                          SELECT CAN.id_container, CAST(NI.valeur_notation_double AS VARCHAR(10) ) AS valeur, CAN.id_container, TN.name AS notation,CAST(NI.date AS VARCHAR(10))AS date,NI.comment AS remarque,NI.observer AS notateur,NI.id_notation
                          FROM notation.container_has_notation CAN
                          JOIN notation.notation_double NI ON NI.id_notation=CAN.id_notation
                          JOIN notation.notation_type TN ON TN.id_type_notation = NI.id_type_notation
                          WHERE id_container= %s
                          UNION
                          SELECT CAN.id_container, CAST(NI.valeur_notation_date AS VARCHAR(10) ) AS valeur, CAN.id_container, TN.name AS notation,CAST(NI.date AS VARCHAR(10))AS date,NI.comment AS remarque,NI.observer AS notateur,NI.id_notation
                          FROM notation.container_has_notation CAN
                          JOIN notation.notation_date NI ON NI.id_notation=CAN.id_notation
                          JOIN notation.notation_type TN ON TN.id_type_notation = NI.id_type_notation
                          WHERE id_container= %s
                          """,(idc,idc,idc,idc))
    return self.cur.fetchall()


  #recupere les informations sur les types de boite
  def getInfosBoxByType(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT  id, name, row_number, column_number
                        FROM sample.box_type
                        WHERE name= %s""",(tab['typeBox'],))
    return self.cur.fetchall()

  #recupere les informations sur les groupes
  def getAllGroupsFiltred(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT  name AS nom, description
                        FROM plant.group G
                        JOIN plant.group_has_usergroup GAG ON GAG.id_group=G.id_group
                        WHERE name ~* %s AND id_usergroup=%s""",(tab['filtredText'],tab['userGroup'],))
    return self.cur.fetchall()

  #creation d'un echantillon
  def setCreateContainer(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id from sample.box_type WHERE name= %s
                        """,(tab['typeBox'],))
    idBT=self.cur.fetchall()[0]['id']
    self.cur.execute("""SELECT id from sample.container_type WHERE type= %s
                        """,(tab['typeContainer'],))
    idCT=self.cur.fetchall()[0]['id']
    self.cur.execute("""SELECT id from sample.box WHERE name= %s AND id_box_type= %s
                        """,(tab['nameBox'],idBT,))
    idBox=self.cur.fetchall()[0]['id']
    self.cur.execute("""SELECT id from sample.content_type WHERE type= %s
                        """,(tab['typeContent'],))
    idCoT=self.cur.fetchall()[0]['id']

    #1-creation container
    try :
      #container_level plein=2
      self.cur.execute("INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,comment,id_action) VALUES (%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id",(tab['code'],'true','true',idCT,2,idCoT,tab['remark'],tab['id_action']))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      idCont=self.cur.fetchall()[0]['id']
      marque=idCont
    #1.1 affecte un container à une série
    #insert experience sur container
    if len(tab['serie']) >0 :
      for e in tab['serie'] :
        ids={}
        ids['idExperiment'] = e['id']
        ids['idContainer'] = idCont
        self.addExperimentToContainer(None, ids)

    #2-creation location
    try :
      self.cur.execute("""INSERT INTO sample.location (id_box,id_container,"column","row",date_on) VALUES (%s,%s,%s,%s,%s) RETURNING * """,(idBox,idCont,tab['colName'],tab['rowName'],tab['date_on']))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      idCont2=self.cur.fetchall()
    #3-creation notations
    if len(tab['notations'])>0:
      for n in tab['notations']:
        typeNotation=n['type']
        valeurNotation=n['value']
        date=tab['date_on']
        site=None
        remarque= "notation echantillon "+ str(idCont)
        # dabs CrbTools : def createNotation(self,date,remarque,site,type_notation,valeur,notateur):
        crb= elvis.CrbTools()
        idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
        #ajout des user et usergroup
        listeUser=n['listeUser']
        for u in tab['userGroup']:
          self.setUserGroupeANotation(u, idNotation)
        if listeUser is not None:
          for idUser in listeUser:
            self.addUserToNotation (idUser,idNotation)
        #4-lien notation_container
        try :
          self.cur.execute("""INSERT INTO notation.container_has_notation (id_notation,id_container) VALUES (%s,%s) RETURNING * """,(idNotation,idCont,))
        except Exception as e:
          raise e
        else :
          self.conn.commit()
          self.cur.fetchall()
    return (idCont)

  #mise à jour des informations d'un container
  def updateContainer(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id FROM sample.container_type WHERE type= %s""",(tab['typeContainer'],))
    idContainerType= self.cur.fetchall()[0]['id']
    self.cur.execute("""SELECT id FROM sample.content_type WHERE type= %s""",(tab['typeContent'],))
    idContentType= self.cur.fetchall()[0]['id']
    self.cur.execute("""SELECT id FROM sample.container_level WHERE level= %s""",(tab['level'],))
    idLevel= self.cur.fetchall()[0]['id']
    try :
      self.cur.execute("UPDATE sample.container SET code= %s,valid=%s,usable=%s,id_container_type=%s,id_container_level=%s,id_content_type=%s,comment=%s WHERE id=%s",( tab['code'],tab['valid'],tab['usable'],idContainerType,idLevel,idContentType,tab['remark'] ,tab['id'], ))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
#      self.cur.fetchall()  # renvoie les lignes resultat de la requête
    marque=''
    #ajout des nouvelles notations
    if tab['notations'] != None :
      for n in  tab['notations'] :
        t=n['type']
        marque=marque+'**** '
        if n['idNotation']==None : # enregistrer la nouvelle notation
          marque=marque+' +++'
          typeNotation=n['type']
          valeurNotation=n['value']
          date=n['date']
          site=None
          notateur=n['notateur']
          remarque= "notation container "
          # dabs CrbTools : def createNotation(self,date,remarque,site,type_notation,valeur,notateur):
          crb= elvis.CrbTools()
          idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,notateur)[0]['id_notation']
          marque=marque+'***'+str(idNotation)
#          #ajout des user et usergroup
#          listeUser=n['listeUser']
          for u in tab['userGroup']:
            self.setUserGroupeANotation(u, idNotation)
          if notateur is not None:
            for idUser in notateur:
              self.addUserToNotation (idUser,idNotation)
          #-lien notation_container
          try :
            self.cur.execute("""INSERT INTO notation.container_has_notation (id_notation,id_container) VALUES (%s,%s) RETURNING * """,(idNotation,tab['id'],))
          except Exception as e:
            raise e
          else :
            self.conn.commit()
#            self.cur.fetchall()

    # si suppression du tube ou mobilite
    if tab['dateOff'] != None :
      if  tab['column'] != None:
        try :
          self.cur.execute("""UPDATE sample.location SET date_off= %s WHERE id_box=%s AND id_container=%s AND date_off is null AND "column"=%s AND "row" =%s""",( tab['dateOff'],tab['idBox'],tab['id'], tab['column'],tab['row'], ))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e
        else :
          self.conn.commit()
      else :
        try :
          self.cur.execute("""UPDATE sample.location SET date_off= %s WHERE id_box=%s AND id_container=%s AND date_off is null AND "column" is null AND "row" is null""",( tab['dateOff'],tab['idBox'],tab['id'], ))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e
        else :
          self.conn.commit()
    else :  # update la position si mouvement
      if tab['dateOn'] != None :
        position = self.getContainerPlace(None, tab['id'])
        col=None
        row=None
        idBox=None
        for p in position :
          if p['date_off'] is None : #dernière position du container
            col=p['column']
            row = p['row']
            idBox= p['id_box']

      #teste si le container a changé de place affecte au container sa nouvelle place
#      marque='no change place '+ str(tab['column']) +' '+str(tab['row']) +' '+str(tab['idBox'])+' / '+str(col)+ ' '+str(row)+' '+str(idBox)
      if col!= tab['column'] or row != tab['row'] or idBox != tab['idBox'] :
#        marque=' change place'
        try :
          self.cur.execute("""INSERT INTO sample.location (id_box,id_container,"column","row",date_on) VALUES (%s,%s,%s,%s,%s) RETURNING * """,(tab['idBox'],tab['id'],tab['column'],tab['row'],tab['dateOn']))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
#         self.cur.fetchall()  # renvoie les lignes resultat de la requête
    return marque

# get informations on container by idContainer
  def getContainer(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,code,valid,usable,id_container_type,id_container_level,id_content_type,comment as remark  FROM sample.container WHERE id= %s""",(tab['id'],))
    return self.cur.fetchall()


  #mise à jour des informations d'une boite
  def updateBox(self,sessionId,tab) :
    x='updateBox.py '
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    for t in tab['listeContainer'] :
      x = x + ' '+self.updateContainer(sessionId,t)+'/'+str(t['id'])


    return x

  #getAllContainersFiltred
  def getAllContainersFiltred(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT DISTINCT C.id,C.code
                        FROM sample.container C
                        JOIN sample.location L ON L.id_container=C.id
                        JOIN sample.box B ON B.id=L.id_box
                        WHERE c.code ~* %s AND B.id_usergroup=ANY (%s ) AND L.date_off IS NULL
                        ORDER BY C.code""",(tab['filtredText'],tab['userGroup'],))
    resultat = self.cur.fetchall()
    return resultat

  #getAllBoxsFiltred
  def getAllBoxsFiltred(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT B.id,B.name
                        FROM sample.box B
                        WHERE B.name ~* %s AND B.id_usergroup = ANY (%s )
                        ORDER BY B.name""",(tab['filtredText'],tab['userGroup'],))
    resultat = self.cur.fetchall()
    return resultat

  #getPlaceContainer
  def getPlaceContainer(self,sessionId,tab) :
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT C.id, C.code,C.valid, C.usable,C.comment AS remark , CT.type AS typecontainer, CL.level, CTT.type AS contenttype, L.column, L.row,CAST(L.date_on AS VARCHAR(10) ),B.name,BT.name AS type_name, B.id AS idBox
                        FROM sample.container C
                        JOIN sample.container_type CT ON CT.id =C.id_container_type
                        JOIN sample.container_level CL ON CL.id=C.id_container_level
                        JOIN sample.content_type CTT ON CTT.id =C.id_content_type
                        JOIN sample.location L ON L.id_container=C.id
                        JOIN sample.box B ON B.id=L.id_box
                        JOIN sample.box_type BT ON B.id_box_type=BT.id
                        WHERE c.code = %s AND B.id_usergroup=ANY(%s) AND L.date_off IS NULL""",(tab['filtredText'],tab['userGroup'],))
    resultat = self.cur.fetchall()
    return resultat

  #getPlaceBox
  def getPlaceBox(self,sessionId,tab) :
    term = elvis.terminology()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT B.id AS idbox, name, valeur,CAST(date_on AS VARCHAR(10) ),id_concept, id_context
                        FROM  sample.box B
                        LEFT JOIN sample.box_has_place BAL ON BAL.id_box=B.id
                        LEFT JOIN location.place L ON L.id_lieu=BAL.id_place
                        LEFT JOIN location.place_type TL ON TL.id=L.id_place_type
                        LEFT JOIN location.place_type_has_terminology TLAA ON TLAA.id_type_lieu = TL.id
                        WHERE B.name = %s AND B.id_usergroup= ANY(%s) AND date_off IS NULL""",(tab['filtredText'],tab['userGroup'],))
    resultat = self.cur.fetchall()
    for l in resultat :
      l['type'] = term.getTermsByConceptContext(l['id_concept'], l['id_context']) [0]['label']
    return resultat

  #recupère tous les types de Box
  def getAllTypesBox (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id, name,row_number,column_number  FROM sample.box_type""",())
    return self.cur.fetchall()

  #createBoxs     Fonction utilisée dans InsertFileBox.js Créations de Boxs à partir d'un fichier texte
  def createBoxs(self,sessionId,tab) :
#    # recupère le numero des colonnes en fonction du nom
#    sys.stderr.write('******************************************\n')
    i=0
    colBoite=100
    colType=100
    colDate=100
    colLieux=[]
    for c in tab['cols'] :
      if c == 'boite' :
        colBoite= i
      if c == 'type' :
        colType=i
      if c == 'date' :
        colDate=i
      if c[0:5]=='lieu:':
        l={}
        l['indice']=i
        l['typeLieu']=c[5:]
        #self.cur.execute("""SELECT id FROM location.place_type WHERE type =%s """,(l['typeLieu'],))
        #recupère l'idTypeLieu via onthologie
        allTypesLieu =self.getAlltypesLieuxO(None)
        for tl in allTypesLieu :
          if l['typeLieu'].encode('utf8') == tl['term_label'] :
            idConcept = tl['concept_id']
            idContext = tl['context_id']
            self.cur.execute("""SELECT id_type_lieu  FROM  location.place_type_has_terminology
                            WHERE id_concept=%s AND id_context=%s  """,(idConcept,idContext,))
            l['idtl'] = self.cur.fetchall()[0]['id_type_lieu']
            colLieux.append(l)
      i=i+1

    for l in tab['lines'] :
      if len(l)>1 :
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        self.cur.execute("""SELECT id  FROM sample.box_type WHERE name=%s """,(l[colType],))
        idTypeBox = self.cur.fetchall()[0]['id']
        try :
          self.cur.execute("""INSERT INTO sample.box (name,id_usergroup,id_box_type) VALUES (%s,%s,%s) RETURNING id""",(l[colBoite],str(tab['writeGroup']),idTypeBox))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idBox=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête

        # ajouter le lieu de rangement de la boite
        for tl in colLieux :
          a= tl['indice']   #numero de la colonne
          b= tl['typeLieu'] #type de lieu
          c= tl['idtl']     # id du type de lieu
          v= l[a]           # valeur sur la ligne du type de lieu
          if v != ''  :
            # creation ou recuperation de l'id_lieu
            self.cur.execute("""SELECT id_lieu FROM location.place WHERE id_place_type= %s AND id_plant_place is null AND valeur=%s""" ,(c,v,))
            idL = self.cur.fetchall()
            if len(idL)==0 : #création du lieu
                try :
                  self.cur.execute("""INSERT INTO location.place (id_plant_place, id_place_type,valeur) VALUES (%s,%s,%s) RETURNING id_lieu""",(None,c ,v))
                except Exception as e:
                  self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                  self.cur.close()
                  self.conn.close()
                  raise e #lance une exception donc arrête le déroulement du code
                else :
                  self.conn.commit()  #valide les modifications faites par le cursor
                  idL=self.cur.fetchall()[0]['id_lieu']  # renvoie les lignes resultat de la requête

            else : #lieu déjà déclaré
               idL = idL[0]['id_lieu']
            #ajout id_lieu sur box_a_lieu
            try :
              self.cur.execute("""INSERT INTO sample.box_has_place (id_place,id_box,date_on) VALUES (%s,%s,%s) RETURNING id_place as id_lieu""",(idL,idBox,l[colDate]))
            except Exception as e:
              self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
              self.cur.close()
              self.conn.close()
              raise e #lance une exception donc arrête le déroulement du code
            else :
              self.conn.commit()  #valide les modifications faites par le cursor
              self.cur.fetchall() # renvoie les lignes resultat de la requête

#    return str( colLieux)
    return 'ok'

  #createSamples
  def createSamples(self,sessionId,tab) :
    session= elvis.SessionTools();
    login = session.getUserInfos(sessionId)
    listeNotCont=[]
    listeExperiment=[]
    note=""
    # recupère le numero des colonnes en fonction du nom
    i=0

    for c in tab['cols'] :
      if c == u'code' :
        colCode= i
      if c == u'boite' :
        colBoxName= i
      if c == u'type contenu' :
        colTypeContent=i
      if c == u'type contenant' :
        colTypeContainer=i
      if c == u'date' :
        colDate=i
      if c == u'ligne' :
        colRow=i
      if c == u'colonne' :
        colColumn=i
      if c == u'valide' :
        colValid=i
      if c == u'utilisable' :
        colUsable=i
      if c == u'niveau' :
        colLevel=i
      if c == u'expérience' :
        listeExperiment.append (i)
      if c[:10]==u'notationC:' :
        listeNotCont.append (i)
      i=i+1

#   Vérifie que la place est libre pour le tube crée
    for l in tab['lines'] :
      if len(l)>1 :
        if len(l[colColumn]) >0 :
          self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup=%s""",(l[colBoxName],tab['userGroup']))
          idBox = self.cur.fetchall()[0]['id']
          self.cur.execute("""SELECT id_container FROM sample.location L WHERE id_box=%s AND "column"=%s AND "row"=%s AND date_off is null """,(idBox,l[colColumn],l[colRow] ))
          if len( self.cur.fetchall()) >0 :
            return str( "Emplacement tube ")+str(l[colCode])+ str(" déjà occupé dans boite : ")+str(l[colBoxName])

    for l in tab['lines'] :
      if len(l)>1 :
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        self.cur.execute("""SELECT id  FROM sample.content_type WHERE type=%s """,(l[colTypeContent],))
        idTypeContent = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.container_type WHERE type=%s """,(l[colTypeContainer],))
        idTypeContainer = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.container_level WHERE level=%s """,(l[colLevel],))
        idLevel = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup=%s""",(l[colBoxName],tab['userGroup']))
        idBox = self.cur.fetchall()[0]['id']
        # insert container
        try :
          self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type) VALUES (%s,%s,%s,%s,%s,%s) RETURNING id""",(l[colCode],l[colValid],l[colUsable],idTypeContainer,idLevel,idTypeContent))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idContainer=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête
        # insert location
        try :
          self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
          self.cur.execute("""INSERT INTO sample.location (id_box,id_container,"column","row",date_on,date_off) VALUES (%s,%s,%s,%s,%s,null) RETURNING id_box""",(idBox,idContainer,l[colColumn],l[colRow],l[colDate]))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idBox=self.cur.fetchall()[0]['id_box']  # renvoie les lignes resultat de la requête
        #insert notation sur container
        crb= elvis.CrbTools()
        if len(listeNotCont) >0 :
          for n in listeNotCont :
            if l[n] is not None :
              valNot= l[n]
              typNot=tab['cols'][n][10:]
              idNotation=crb.createNotation(l[colDate],'notation sur container',None,typNot,valNot,login['login'])[0]['id_notation']
              self.setContainerANotation(idContainer,idNotation)
              #ajout des user et usergroup
#              listeUser=n['listeUser']
              note=note+" "+str(l[n])
              self.setUserGroupeANotation(tab['userGroup'], idNotation)
#              if listeUser is not None:
#                for idUser in listeUser:
#                  self.addUserToNotation (idUser,idNotation)
        #insert experiment sur container
        if len(listeExperiment) >0 :
          for n in listeExperiment :
            if l[n] is not None :
              experiment = l[n]
              self.cur.execute("""SELECT id  FROM experiment.experiment E
                                  JOIN experiment.experiment_has_usergroup EU ON E.id=EU.id_experiment WHERE name=%s AND id_usergroup = %s""",(experiment,tab['userGroup'],))
              idExp = self.cur.fetchall()[0]['id']
              try :
                self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
                self.cur.execute("""INSERT INTO experiment.experiment_has_container (id_experiment,id_container) VALUES (%s,%s) RETURNING id_experiment""",(idExp,idContainer))
              except Exception as e:
                self.conn.rollback()
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
    return str( "enregistrement réalisé")

###################################################################################################################################
  #recupère tous les types de Notations
  def getAllTypeNotationAll (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT name as nom, type  FROM notation.notation_type""",())
    return self.cur.fetchall()

  #recupère tous les expérimentations
  def getAllExperiments (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,name,description, CAST(creation_date AS VARCHAR(10) )
                        FROM experiment.experiment E
                        JOIN experiment.experiment_has_usergroup EU ON EU.id_experiment=E.id
                        WHERE id_usergroup=ANY(%s) ORDER BY name""",(tab['userGroup'],))
    return self.cur.fetchall()

  #recupère tous les expérimentations
  def getAllExperimentsFiltred (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT id,name,description, CAST(creation_date AS VARCHAR(10) )
                        FROM experiment.experiment E
                        JOIN experiment.experiment_has_usergroup EU ON EU.id_experiment=E.id
                        WHERE id_usergroup=ANY(%s) AND name ~* %s""",(tab['userGroup'],tab['filtredText']))
    return self.cur.fetchall()

  #create  expérimentations  tab['name'],tab['description'],tab['creationDate'],tab['listeIdGroupes']tab['lecture'],tab['ecriture']
  def createExperiment (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""INSERT INTO experiment.experiment ( name, description, creation_date) VALUES( %s,%s,%s) RETURNING id""",(tab['name'],tab['description'],tab['creationDate'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idExp= self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête
      #ajout dans experiment_has_usergroup
      for i in tab['listeIdGroupes'] :
        try :
          self.cur.execute("""INSERT INTO experiment.experiment_has_usergroup ( id_experiment,id_usergroup,readable,writable) VALUES( %s,%s,%s,%s) RETURNING id_experiment""",(idExp,i,tab['lecture'],tab['ecriture'],))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          self.cur.fetchall() # renvoie les lignes resultat de la requête

        #insert experience dependant experience
        if len(tab['experience']) >0 :
          for e in tab['experience'] :
            ids={}
            ids['idExperiment'] = e['id']
            ids['idExperimentChild'] =idExp
            self.addExperimentToExperiment (None,ids)
      return idExp

  #updated  expérimentations
  def updateExperiment (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""UPDATE experiment.experiment SET name=%s, description=%s, creation_date=%s WHERE id=%s RETURNING id""",(tab['name'],tab['description'],tab['creationDate'],tab['id'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      return self.cur.fetchall()  # renvoie les lignes resultat de la requête


  #recupère tous les valeurs pour une notation
  def getAllValeurByNotation (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("SELECT type,id_type_notation FROM notation.notation_type WHERE name =%s;",(tab['typeNotation'],))
    rows = self.cur.fetchall()
    type_notation = rows[0]["type"]
    id_type_notation = rows[0]["id_type_notation"]
    self.cur.execute("SELECT distinct valeur_notation_"+type_notation+" as valeur FROM notation.notation_"+type_notation+" WHERE id_type_notation=%s ORDER BY valeur_notation_"+type_notation+ " ASC",(id_type_notation,))
    return self.cur.fetchall()

  #recupère tous les types de nom de variété
  def getAllTypesNomVariete (self, sessionId,tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("SELECT value AS valeur FROM plant.variety_name_type",())
    return self.cur.fetchall()

  #createActions   à partir d'un fichier texte
  def createActions(self,sessionId,tab) :
    session= elvis.SessionTools();
    login = session.getUserInfos(tab['sessionId'])
    listeNotCont=[]
    listeNotAct=[]
    # recupère le numero des colonnes en fonction du nom
    #"M.code","M.boite","M.ligne","M.colonne","type action","C.code","C.typeContenant","C.typeContenu","C.boite","C.colonne","C.ligne","date","userLogin","valid","usable","level"
    i=0
    for c in tab['cols'] :
      if c == 'M.code' :
        colMcode= i
      if c == 'M.boite' :
        colMboite= i
      if c == 'M.ligne' :
        colMligne= i
      if c == 'M.colonne' :
        colMcolonne= i
      if c == 'type action' :
        coltypeaction= i
      if c == 'C.code' :
        colCcode= i
      if c == 'C.typeContenant' :
        colCtypeContenant= i
      if c == 'C.typeContenu' :
        colCtypeContenu=  i
      if c == 'C.boite' :
        colCboite= i
      if c == 'C.colonne' :
        colCcolonne= i
      if c == 'C.ligne' :
        colCligne= i
      if c == 'date' :
        coldate= i
      if c == 'userLogin' :
        coluserLogin= i
      if c == 'valid' :
        colvalid= i
      if c == 'usable' :
        colusable= i
      if c == 'level' :
        collevel= i
      if c[:10]=='notationC:' :
        listeNotCont.append (i)
      if c[:10]=='notationA:' :
        listeNotAct.append (i)
      i=i+1
#   Vérifie que le tube Mother est bien présent
    for l in tab['lines'] :
      if len(l)>1 :
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup= ANY(%s)""",(l[colMboite],tab['userGroup']))
        idBoxM = self.cur.fetchall()[0]['id']
        #dans le cas d'une boite ordonnée
        if len(l[colMcolonne]) >0 :
          self.cur.execute("""SELECT C.code FROM sample.location L JOIN sample.container C ON L.id_container=C.id
                                      WHERE id_box=%s AND "column"=%s AND "row"=%s AND date_off is null """,(idBoxM,l[colMcolonne],l[colMligne] ))
          codeTubeMother = self.cur.fetchall()[0]['code']
          if codeTubeMother != l[colMcode] :
            return str( "erreur de localisation du tube origine: ")+str(l[colMcode])
        #dans le cas d'une boite vrac
        else :
          self.cur.execute("""SELECT C.code FROM sample.location L JOIN sample.container C ON L.id_container=C.id
                                      WHERE id_box=%s AND date_off is null """,(idBoxM, ))
          listeCodes=[]
          for c in self.cur.fetchall():
            listeCodes.append(c['code'])
          if l[colMcode] not in listeCodes :
            return str( "tube origine: ")+str(l[colMcode])+ str( " absent")

#   Vérifie que la place est libre pour le tube child
    for l in tab['lines'] :
      if len(l)>1 :
        if len(l[colCcolonne]) >0 :
          self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup=ANY(%s)""",(l[colCboite],tab['userGroup']))
          idBoxC = self.cur.fetchall()[0]['id']
          self.cur.execute("""SELECT id_container FROM sample.location L WHERE id_box=%s AND "column"=%s AND "row"=%s AND date_off is null """,(idBoxC,l[colCcolonne],l[colCligne] ))
          if len( self.cur.fetchall()) >0 :
            return str( "Emplacement tube ")+str(l[colCcode])+ str(" déjà occupé dans boite : ")+str(l[colCboite])+" "+str(l[colCcolonne])+" "+str(l[colCligne] )

#   Entrée des données
    for l in tab['lines'] :
      if len(l)>1 :
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        self.cur.execute("""SELECT id  FROM sample.content_type WHERE type=%s """,(l[colCtypeContenu],))
        idTypeContent = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.container_type WHERE type=%s """,(l[colCtypeContenant],))
        idTypeContainer = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.container_level WHERE level=%s """,(l[collevel],))
        idLevel = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup=ANY(%s)""",(l[colMboite],tab['userGroup']))
        idBoxM = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.box WHERE name=%s AND id_usergroup=ANY(%s)""",(l[colCboite],tab['userGroup']))
        idBoxC = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM sample.action_type WHERE type=%s """,(l[coltypeaction],))
        idTypeAction = self.cur.fetchall()[0]['id']
        self.cur.execute("""SELECT id  FROM users.user WHERE login=%s""",(l[coluserLogin],))
        idUser= self.cur.fetchall()[0]['id']
        if len(l[colMcolonne]) >0 :
          self.cur.execute("""SELECT id_container  FROM sample.location WHERE id_box=%s AND "row" = %s AND "column"=%s AND date_off is null """,(idBoxM,l[colMligne],l[colMcolonne], ))
          idContainerM=self.cur.fetchall()[0]['id_container']
        else :
          self.cur.execute("""SELECT L.id_container  FROM sample.location L JOIN sample.container C ON L.id_container=C.id
                            WHERE id_box=%s AND C.code=%s AND date_off is null """,(idBoxM,l[colMcode], ))
          idContainerM=self.cur.fetchall()[0]['id_container']

        #crée l'action
        try :
          self.cur.execute("""INSERT INTO sample.action (id_action_type, id_user,date) VALUES (%s,%s,%s) RETURNING id""",(idTypeAction,idUser,l[coldate],))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idAction=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête

        # insert container
        try :
          self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,id_action) VALUES (%s,%s,%s,%s,%s,%s,%s) RETURNING id""",(l[colCcode],l[colvalid],l[colusable],idTypeContainer,idLevel,idTypeContent,idAction))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idContainerC=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête

        # insert location
        try :
          self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
          self.cur.execute("""INSERT INTO sample.location (id_box,id_container,"column","row",date_on,date_off) VALUES (%s,%s,%s,%s,%s,null) RETURNING id_box""",(idBoxC,idContainerC,l[colCcolonne],l[colCligne],l[coldate]))
        except Exception as e:
          self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
          self.cur.close()
          self.conn.close()
          raise e #lance une exception donc arrête le déroulement du code
        else :
          self.conn.commit()  #valide les modifications faites par le cursor
          idBox=self.cur.fetchall()[0]['id_box']  # renvoie les lignes resultat de la requête

#        return str('return de createAction  ')+str(idContainerC)

        #insert notation sur container
        crb= elvis.CrbTools()
        if len(listeNotCont) >0 :
          for n in listeNotCont :
            if l[n] is not None :
              valNot= l[n]
              typNot=tab['cols'][n][10:]
              idNotation=crb.createNotation(l[coldate],'notation sur container',None,typNot,valNot,l[coluserLogin])[0]['id_notation']
              self.setContainerANotation(idContainerC,idNotation)
              #ajout des user et usergroup
              listeUser=n['listeUser']
              for u in tab['userGroup']:
                self.setUserGroupeANotation(u, idNotation)
              if listeUser is not None:
                for idUser in listeUser:
                  self.addUserToNotation (idUser,idNotation)
        #insert action_a_container_origine
        try :
          self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
          self.cur.execute("""INSERT INTO sample.action_a_container_origin (id_action,id_container) VALUES(%s,%s) RETURNING id_action""",(idAction,idContainerM,))
        except Exception as e:
          self.conn.rollback()
          self.cur.close()
          self.conn.close()
          raise e
        else :
          self.conn.commit()
          idAction=self.cur.fetchall()[0]['id_action']

        #insert notations sur  action
        crb= elvis.CrbTools()
        if len(listeNotCont) >0 :
          for n in listeNotAct :
            if l[n] is not None :
              valNot= l[n]
              typNot=tab['cols'][n][10:]
              idNotation=crb.createNotation(l[coldate],'notation sur action',None,typNot,valNot,l[coluserLogin])[0]['id_notation']
              self.setActionANotation(idAction,idNotation)
              #ajout des user et usergroup
              listeUser=n['listeUser']
              for u in tab['userGroup']:
                self.setUserGroupeANotation(u, idNotation)
              if listeUser is not None:
                for idUser in listeUser:
                  self.addUserToNotation (idUser,idNotation)
    return str( "enregistrement réalisé  ")

  #recupere le premier container issu du prelevement a partir de n'importe quel container
  def getPrelevementContainer (self, idContainer, sessionId):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT AACO.id_container FROM sample.container C
                        LEFT JOIN sample.action A ON A.id = C.id_action
                        LEFT JOIN sample.action_a_container_origin AACO ON AACO.id_action = A.id
                        WHERE C.id= %s""",(idContainer,))
    origine=self.cur.fetchall()

    if origine[0]['id_container'] :
      return self.getPrelevementContainer(origine[0]['id_container'], sessionId)
    else :
      return idContainer

  #recupere la suite des containerMother/action/containerChlid
  def getListContainerAction (self, idContainer,result):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT C.id AS idContM,A.id AS idAction,CP.id AS idContC from sample.container C
                        LEFT JOIN sample.action_a_container_origin AACO  ON C.id= AACO.id_container
                        LEFT JOIN sample.action A ON A.id= AACO.id_action
                        LEFT JOIN sample.action_type AT ON AT.id=A.id_action_type
                        LEFT JOIN sample.container CP ON CP.id_action=A.id
                        where C.id =%s""",(idContainer,))
    cac=self.cur.fetchall()
    result.append(cac)
    #poursuit sur le container child
    for a in cac :
      if a['idcontc'] :
        self.getListContainerAction(a['idcontc'],result)

    return result

#  #recupere la liste des prelevements pour un lot
#  def getAllPrelevByLot(self,sessionId,idGroupe):
#    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#    self.cur.execute("select id from sample.taking where id_group = %s",(idGroupe,))
#    liste=self.cur.fetchall()
#    result=[]
#    for c in liste:
#      result.append(c['id_container'])
#      result.append(self.getListContainerAction(c['id_container'],0))
#    return result


  #create  new Box +containers+actions à partir d'une boite existante ( utilisé par ActionOnBoxForm)
  def setCreateBoxAndContainer(self,sessionId,tab) :
    if(tab['nameOriginBox'] != None ):
      if(['nameOriginBox'] != '' ):

#      creation de la nouvelle boites
       #tab['nameBoxType']
       #tab['placesBox'] ['name'] + ['type']
       #tab['nameBox']
       #tab['userGroup']
       #tab['date_on']
        self.setCreateBox(sessionId,tab)
#      recupère la liste des containers de la boite mother
        #idBoxMoter renvoyé directement par filtredComboBox
#        self.cur.execute("""SELECT B.id  from sample.box B
#                        JOIN sample.box_type BT ON BT.id=B.id_box_type
#                        WHERE B.name=%s AND B.id_usergroup = %s """,(tab['nameOriginBox'],tab['userGroup'], ))
#        tab['idBox']=self.cur.fetchall()[0]['id']
        listeContainer=self.getAllContainersByBox(sessionId,tab)
##       pour chaque container
        for c in listeContainer ['container'] :
          #crée l'action
          self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
          #identidie l'id du typeAction
          self.cur.execute("""SELECT id FROM sample.action_type WHERE type=%s """,(tab['typeAction'],))
          idTypeAction= self.cur.fetchall()[0]['id']
          # creation action
          #crée l'action
          try :
            self.cur.execute("""INSERT INTO sample.action (id_action_type, id_user,date) VALUES (%s,%s,%s) RETURNING id""",(idTypeAction,tab['idUser'],tab['date_on']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            idAction=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête
          #ajoute la liaison avec le container origine
          try :
            self.cur.execute("""INSERT INTO sample.action_a_container_origin (id_action,id_container) VALUES (%s,%s) RETURNING id_action""",(idAction,c['id_container']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor

          #crée le container child
          tabb={}
          tabb['typeBox']=tab['nameBoxType']
          tabb['typeContainer']=tab['typeContainer']
          tabb['nameBox']=tab['nameBox']
          tabb['typeContent']=tab['typeContent']
          tabb['remark']=u'créé par action sur boite'
          tabb['colName']=c['column']
          tabb['rowName']=c['row']
          tabb['date_on']=tab['date_on']
          tabb['notations']=tab['notationsContainer']
          tabb['code']=c['code']
          tabb['id_action']= idAction
          tabb['serie']=[]
          idContainerNew=self.setCreateContainer(sessionId,tabb)
#          #affecte le container à une experience
#              #insert experience sur container
#          if len(tab['experience']) >0 :
#            for e in tab['experience'] :
#              ids={}
#              ids['idExperiment'] = e['id']
#              ids['idContainer'] = idContainerC
#              self.addExperimentToContainer (None,ids)
#
#        tab['ok']='ok'

    return str(tabb)

  #create protocol
  def createProtocol (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""INSERT INTO documentation.protocol (name, adress,summary,reference_laboratory_handbook, reference_alfresco)  VALUES(%s,%s,%s,%s,%s) RETURNING id""",(tab['name'],tab['adress'],tab['summary'],tab['referenceLaboratoryHandbook'],tab['referenceAlfresco']))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      return self.cur.fetchall()  # renvoie les lignes resultat de la requête

  #update protocol
  def updateProtocol (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""UPDATE documentation.protocol SET name=%s , adress=%s,summary=%s,reference_laboratory_handbook=%s, reference_alfresco=%s  WHERE id=%s RETURNING id""",(tab['name'],tab['adress'],tab['summary'],tab['referenceLaboratoryHandbook'],tab['referenceAlfresco'],tab['id']))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      return self.cur.fetchall()  # renvoie les lignes resultat de la requête

  #getAllProtocolFiltred
  def getAllProtocolFiltred (self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute(""" SELECT id,name, adress,summary,reference_laboratory_handbook, reference_alfresco FROM  documentation.protocol WHERE name ~* %s""",(tab['filtredName'],))
    return self.cur.fetchall()

  #createemplacement
  def createEmplacement (self, idLot,date, places):
    #1 cree emplacements
    try :
      self.cur.execute("""INSERT INTO plant.place (id_lot,plantation_date ) VALUES (%s,%s) RETURNING id;""",(idLot,date,))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idEmpl=  self.cur.fetchall()[0]['id_emplacement']  # renvoie les lignes resultat de la requête
    #2 ajout  les lieux
    for p in places :
      #2.1 recupère le id_type_lieu
      ok=p['context_id']
      self.cur.execute("""select id_type_lieu FROM  location.place_type_has_terminology WHERE id_concept= %s AND id_context= %s""",(p['concept_id'] ,p['context_id'] ,))
      idTypeLieu = self.cur.fetchall()[0]['id_type_lieu']
      #2.2 insere le lieu
      try :
        self.cur.execute("""INSERT INTO location.place (id_plant_place,id_place_type,valeur) VALUES (%s,%s,%s) RETURNING id_lieu""",(idEmpl,idTypeLieu,p['name']))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idlieu=  self.cur.fetchall() # renvoie les lignes resultat de la requête
    return 'ok'

#creation d'un arbre ou d'une culture
#tab['lotOrigine']={'idLot':'','idClone':''}
#tab['accOrigine']={'idClone':''}
#tab['userGroup']= [1003,1004]
#tab['date'] : date de création
#tab['name']  : nom du nouveau lot
#tab['first_shoot_year']
#tab['porte_greffe'] : nom du porte greffe
#tab['protocol']= idProtocol
#tab['listeNotationsLot']=[{'type_notation':'conditions de culture','valeur_notation':'Culture Bio','date_notation':'2017-06-30','remarque_notation':'No pesticid','id_site':2,'listeUser':[1007,1018],'protocol':'1'}],

  #creation d'un arbre ou d'une culture
  def createArbre(self,sessionId,tab):
    crb= elvis.CrbTools()
    # création d'un lot à partir d'un lot
    ok= tab['date']
    if tab['from']=='lot' :
      lotOrig = tab['lotOrigine']
      self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
      #1 creation du lot
      try :
        self.cur.execute("""INSERT INTO plant.tree (name,id_accession,id_lot_source,multiplication_date, first_shoot_year, rootstock) VALUES(%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(tab['name'],lotOrig['idClone'],lotOrig['idLot'],tab['date'],None,None))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idLot=  self.cur.fetchall()[0]['id_lot']  # renvoie les lignes resultat de la requête

        #2 affecte au lot un groupe de plante et usergrpoups
        idGroupe=self.createIdGroupeByLots(sessionId,[idLot],tab['userGroup'],tab['name'],'culture à partir d\'un lot')

        #3ajoute le protocole de création du lot
        if tab['protocol'] is not None :
          try :
            self.cur.execute(""" INSERT INTO plant.lot_has_protocol (id_lot,id_protocol) VALUES(%s,%s) RETURNING id_lot""",(idLot,tab['protocol']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall() # renvoie les lignes resultat de la requête

        #4 ajoute un emplacement (place du lot)
        if tab['place'] :
          self.createEmplacement(idLot,tab['date'],tab['place'])

        #5 ajoute les notations au lot
        if tab['listeNotationsLot'] is not None :
          ok='il y a des notations'
          for n in tab['listeNotationsLot'] :
            typeNotation=n['type_notation']
            valeurNotation=n['valeur_notation']
            date=tab['date']
            remarque=n['remarque_notation']
            site= None
            listeUser=n['listeUser']
            idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
            crb.setGroupeANotation(idGroupe, idNotation)
            for u in tab['userGroup']:
              self.setUserGroupeANotation(u, idNotation)
            if listeUser is not None:
              for idUser in listeUser:
                self.addUserToNotation (idUser,idNotation)
            #protocole sur notation
            if n['protocol'] is not None:
              try :
                self.cur.execute(""" INSERT INTO notation.notation_has_protocol (id_notation,id_protocol) VALUES(%s,%s) RETURNING id_protocol""",(idNotation,n['protocol']))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall()[0]['id_protocol'] # renvoie les lignes resultat de la requête
        #6 affecte le lot à une collection
        if tab['experience'] is not None :
          for c in tab['experience'] :
            self.cur.execute(""" SELECT id FROM experiment.experiment E
                                JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
                                WHERE name=%s AND  id_usergroup =ANY(%s) """,(c['name'],tab['userGroup']))
            idExpe = self.cur.fetchall()
            if len(idExpe)>0 :
              #collection existante
              idExpe=idExpe[0]['id']
              #insertion dans experiment_has_groupe
              try :
                self.cur.execute(""" INSERT INTO experiment.experiment_has_group (id_experiment,id_group) VALUES(%s,%s) RETURNING id_experiment""",(idExpe,idGroupe))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall() # renvoie les lignes resultat de la requête
                ok=idExpe
            else :
              #creer nouvelle experience
              #create  expérimentations  tab['name'],tab['description'],tab['creationDate'],tab['listeIdGroupes']tab['lecture'],tab['ecriture']
              ok='newexp'
              tab2={}
              tab2['name']=c['name']
              tab2['description']= None
              tab2['creationDate']= tab['date']
              tab2['listeIdGroupes']=tab['userGroup']
              tab2['lecture']= True
              tab2['ecriture']= True
              ok= tab2['lecture']
              idExpe=self.createExperiment (None,tab2)
              ok=idExpe



#    # création d'un lot à partir d'une accession
    if tab['from']=='accession' :
      ok='acc'
      accOrig = tab['accOrigine']
      self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
      try :
        self.cur.execute("""INSERT INTO plant.tree (name,id_accession,id_lot_source,multiplication_date, first_shoot_year, rootstock) VALUES(%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(tab['name'],accOrig,None,tab['date'],None,None))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idLot=  self.cur.fetchall()[0]['id_lot']  # renvoie les lignes resultat de la requête

        # affecte au lot un groupe de plante et usergrpoups
        idGroupe=self.createIdGroupeByLots(sessionId,[idLot],tab['userGroup'],tab['name'],'culture à partir d\'une accession')

        #3ajoute le protocole de création du lot
        if tab['protocol'] is not None :
          try :
            self.cur.execute(""" INSERT INTO plant.lot_has_protocol (id_lot,id_protocol) VALUES(%s,%s) RETURNING id_lot""",(idLot,tab['protocol']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall() # renvoie les lignes resultat de la requête

        #4 ajoute un emplacement (place du lot)
        self.createEmplacement(idLot,tab['date'],tab['place'])

        #5 ajoute les notations au lot
        if tab['listeNotationsLot'] is not None :
          ok='il y a des notations'
          for n in tab['listeNotationsLot'] :
            typeNotation=n['type_notation']
            valeurNotation=n['valeur_notation']
            date=tab['date']
            remarque=n['remarque_notation']
            site= None
            listeUser=n['listeUser']
            idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
            crb.setGroupeANotation(idGroupe, idNotation)
            for u in tab['userGroup']:
              self.setUserGroupeANotation(u, idNotation)
            if listeUser is not None:
              for idUser in listeUser:
                self.addUserToNotation (idUser,idNotation)
            #protocole sur notation
            if n['protocol'] is not None:
              try :
                self.cur.execute(""" INSERT INTO notation.notation_has_protocol (id_notation,id_protocol) VALUES(%s,%s) RETURNING id_protocol""",(idNotation,n['protocol']))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall()[0]['id_protocol'] # renvoie les lignes resultat de la requête
        #6 affecte le lot à une collection
        if tab['experience'] is not None :
          for c in tab['experience'] :
            self.cur.execute(""" SELECT id FROM experiment.experiment E
                                JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
                                WHERE name=%s AND  id_usergroup =ANY(%s) """,(c['name'],tab['userGroup']))
            idExpe = self.cur.fetchall()
            if len(idExpe)>0 :
              #collection existante
              idExpe=idExpe[0]['id']
              #insertion dans experiment_has_groupe
              try :
                self.cur.execute(""" INSERT INTO experiment.experiment_has_group (id_experiment,id_group) VALUES(%s,%s) RETURNING id_experiment""",(idExpe,idGroupe))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall() # renvoie les lignes resultat de la requête
                ok=idExpe
            else :
              #creer nouvelle experience
              #create  expérimentations  tab['name'],tab['description'],tab['creationDate'],tab['listeIdGroupes']tab['lecture'],tab['ecriture']
              ok='newexp'
              tab2={}
              tab2['name']=c['name']
              tab2['description']= None
              tab2['creationDate']= tab['date']
              tab2['listeIdGroupes']=tab['userGroup']
              tab2['lecture']= True
              tab2['ecriture']= True
              ok= tab2['lecture']
              idExpe=self.createExperiment (None,tab2)
              ok=idExpe




#    # création d'un lot à partir d'une variete
    if tab['from']=='variete' :
      idVarOrig = tab['varOrigine']
      numeroClone= tab['newAcc']
      accRem= tab['remAcc']
      self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
      try :
        self.cur.execute("""INSERT INTO plant.accession (numero,id_variety,comment,introduction_date) VALUES(%s,%s,%s,%s) RETURNING id AS id_clone""",(numeroClone,idVarOrig,accRem,tab['date']))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idClone=  self.cur.fetchall()[0]['id_clone']  # renvoie les lignes resultat de la requête
      # création d'un lot
      try :
        self.cur.execute("""INSERT INTO plant.tree (name,id_accession,id_lot_source,multiplication_date, first_shoot_year, rootstock) VALUES(%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(tab['name'],idClone,None,tab['date'],None,None))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idLot=  self.cur.fetchall()[0]['id_lot']  # renvoie les lignes resultat de la requête

        # affecte au lot un groupe de plante et usergrpoups
        idGroupe=self.createIdGroupeByLots(sessionId,[idLot],tab['userGroup'],tab['name'],'culture à partir d\'une variete')

        #3ajoute le protocole de création du lot
        if tab['protocol'] is not None :
          try :
            self.cur.execute(""" INSERT INTO plant.lot_has_protocol (id_lot,id_protocol) VALUES(%s,%s) RETURNING id_lot""",(idLot,tab['protocol']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall() # renvoie les lignes resultat de la requête

        #4 ajoute un emplacement (place du lot)
        self.createEmplacement(idLot,tab['date'],tab['place'])

        #5 ajoute les notations au lot
        if tab['listeNotationsLot'] is not None :
          ok='il y a des notations'
          for n in tab['listeNotationsLot'] :
            typeNotation=n['type_notation']
            valeurNotation=n['valeur_notation']
            date=tab['date']
            remarque=n['remarque_notation']
            site= None
            listeUser=n['listeUser']
            idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
            crb.setGroupeANotation(idGroupe, idNotation)
            for u in tab['userGroup']:
              self.setUserGroupeANotation(u, idNotation)
            if listeUser is not None:
              for idUser in listeUser:
                self.addUserToNotation (idUser,idNotation)
            #protocole sur notation
            if n['protocol'] is not None:
              try :
                self.cur.execute(""" INSERT INTO notation.notation_has_protocol (id_notation,id_protocol) VALUES(%s,%s) RETURNING id_protocol""",(idNotation,n['protocol']))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall()[0]['id_protocol'] # renvoie les lignes resultat de la requête
        #6 affecte le lot à une collection
        if tab['experience'] is not None :
          for c in tab['experience'] :
            self.cur.execute(""" SELECT id FROM experiment.experiment E
                                JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
                                WHERE name=%s AND  id_usergroup =ANY(%s) """,(c['name'],tab['userGroup']))
            idExpe = self.cur.fetchall()
            if len(idExpe)>0 :
              #collection existante
              idExpe=idExpe[0]['id']
              #insertion dans experiment_has_groupe
              try :
                self.cur.execute(""" INSERT INTO experiment.experiment_has_group (id_experiment,id_group) VALUES(%s,%s) RETURNING id_experiment""",(idExpe,idGroupe))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall() # renvoie les lignes resultat de la requête
                ok=idExpe
            else :
              #creer nouvelle experience
              #create  expérimentations  tab['name'],tab['description'],tab['creationDate'],tab['listeIdGroupes']tab['lecture'],tab['ecriture']
              ok='newexp'
              tab2={}
              tab2['name']=c['name']
              tab2['description']= None
              tab2['creationDate']= tab['date']
              tab2['listeIdGroupes']=tab['userGroup']
              tab2['lecture']= True
              tab2['ecriture']= True
              ok= tab2['lecture']
              idExpe=self.createExperiment (None,tab2)
              ok=idExpe



    # création d'un lot à partir de rien
    if tab['from']=='rien' :
      ok='rien'
      #1 identification de la taxinomie
      idTaxinomie=crb.getTaxinomieByGenreAndEspece(tab['genre'],tab['espece'])[0]['id']
      #2 creation de la variete (avoir testé si elle existe)
      idVariete = crb.createVariete(tab['obtenteur'], tab['remVar'], tab['editeur'])[0]['id_variete']
      crb.setTaxinomie(idVariete,idTaxinomie)
      #3.1 creation des noms variete
      for v in tab['newVar']:
        nom=v['nom']
        date=tab['date']
        typeNomVariete=v['type']
        idTypeNomVariete=crb.getTypeNom(typeNomVariete)[0]['id_type_nom']
        idNomVar=crb.createNomVariete(idVariete,nom,date,idTypeNomVariete)[0]['id_nom_var']
      #3.2 creation des notations sur variete
      if tab['listeNotationsVariete'] is not None :
          ok='il y a des notationsvariete'
          for n in tab['listeNotationsVariete'] :
            typeNotation=n['type_notation']
            valeurNotation=n['valeur_notation']
            date=tab['date']
            remarque=n['remarque_notation']
            site= None
            listeUser=n['listeUser']
            idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
            self.setVarieteANotation(None, {'idVariete':idVariete, 'idNotation':idNotation})
            for u in tab['userGroup']:
              self.setUserGroupeANotation(u, idNotation)
            if listeUser is not None:
              for idUser in listeUser:
                self.addUserToNotation (idUser,idNotation)
            #protocole sur notation
            if n['protocol'] is not None:
              try :
                self.cur.execute(""" INSERT INTO notation.notation_has_protocol (id_notation,id_protocol) VALUES(%s,%s) RETURNING id_protocol""",(idNotation,n['protocol']))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall()[0]['id_protocol'] # renvoie les lignes resultat de la requête

      #4 creation de l'accession
      try :
        self.cur.execute("""INSERT INTO plant.accession (numero,id_variety,comment,introduction_date) VALUES(%s,%s,%s,%s) RETURNING id AS id_clone""",(tab['newAcc'],idVariete,tab['remAcc'],tab['date']))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idClone=  self.cur.fetchall()[0]['id_clone']  # renvoie les lignes resultat de la requête
      # création d'un lot
      try :
        self.cur.execute("""INSERT INTO plant.tree (name,id_accession,id_lot_source,multiplication_date, first_shoot_year, rootstock) VALUES(%s,%s,%s,%s,%s,%s) RETURNING id_lot""",(tab['name'],idClone,None,tab['date'],None,None))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
        idLot=  self.cur.fetchall()[0]['id_lot']  # renvoie les lignes resultat de la requête

        # affecte au lot un groupe de plante et usergrpoups
        idGroupe=self.createIdGroupeByLots(sessionId,[idLot],tab['userGroup'],tab['name'],'')

        #3ajoute le protocole de création du lot
        if tab['protocol'] is not None :
          try :
            self.cur.execute(""" INSERT INTO plant.lot_has_protocol (id_lot,id_protocol) VALUES(%s,%s) RETURNING id_lot""",(idLot,tab['protocol']))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall() # renvoie les lignes resultat de la requête

        #4 ajoute un emplacement (place du lot)
        self.createEmplacement(idLot,tab['date'],tab['place'])

        #5 ajoute les notations au lot
        if tab['listeNotationsLot'] is not None :
          ok='il y a des notations'
          for n in tab['listeNotationsLot'] :
            typeNotation=n['type_notation']
            valeurNotation=n['valeur_notation']
            date=tab['date']
            remarque=n['remarque_notation']
            site= None
            listeUser=n['listeUser']
            idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
            crb.setGroupeANotation(idGroupe, idNotation)
            for u in tab['userGroup']:
              self.setUserGroupeANotation(u, idNotation)
            if listeUser is not None:
              for idUser in listeUser:
                self.addUserToNotation (idUser,idNotation)
            #protocole sur notation
            if n['protocol'] is not None:
              try :
                self.cur.execute(""" INSERT INTO notation.notation_has_protocol (id_notation,id_protocol) VALUES(%s,%s) RETURNING id_protocol""",(idNotation,n['protocol']))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall()[0]['id_protocol'] # renvoie les lignes resultat de la requête
        #6 affecte le lot à une collection
        if tab['experience'] is not None :
          for c in tab['experience'] :
            self.cur.execute(""" SELECT id FROM experiment.experiment E
                                JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment=E.id
                                WHERE name=%s AND  id_usergroup =ANY(%s) """,(c['name'],tab['userGroup']))
            idExpe = self.cur.fetchall()
            if len(idExpe)>0 :
              #collection existante
              idExpe=idExpe[0]['id']
              #insertion dans experiment_has_groupe
              try :
                self.cur.execute(""" INSERT INTO experiment.experiment_has_group (id_experiment,id_group) VALUES(%s,%s) RETURNING id_experiment""",(idExpe,idGroupe))
              except Exception as e:
                self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
                self.cur.close()
                self.conn.close()
                raise e #lance une exception donc arrête le déroulement du code
              else :
                self.conn.commit()  #valide les modifications faites par le cursor
                self.cur.fetchall() # renvoie les lignes resultat de la requête
                ok=idExpe
            else :
              #creer nouvelle experience
              #create  expérimentations  tab['name'],tab['description'],tab['creationDate'],tab['listeIdGroupes']tab['lecture'],tab['ecriture']
              ok='newexp'
              tab2={}
              tab2['name']=c['name']
              tab2['description']= None
              tab2['creationDate']= tab['date']
              tab2['listeIdGroupes']=tab['userGroup']
              tab2['lecture']= True
              tab2['ecriture']= True
              ok= tab2['lecture']
              idExpe=self.createExperiment (None,tab2)
              ok=idExpe



    return idLot

  def getAllPepinieriste(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute ("SELECT DISTINCT lastname AS nom, address1 AS adresse1,address2 AS adresse2,city as ville,state AS etat,postal_code AS code_postal,id_country AS pays, email,phone AS telephone," " AS fax,id AS id_pepinieriste FROM address_book.address WHERE  lastname ~* %s ORDER BY lastname ASC""", (tab['filtredText'],))
    return self.cur.fetchall()

  def synchronyseAllPlaces(self, sessionId, tab):
    ok='ok'
    #recupère tous les termes de place
    term = elvis.terminology()
    idContext=term.getContextByName(tab['context'])['id']
    listePlace=term.getTermsByTerminologyName(tab['name'],idContext)
    ok=listePlace
    ok=''
    #verifie leur présence dans la table
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    for p in listePlace :
      self.cur.execute ("SELECT * FROM location.place_type_has_terminology WHERE id_context=%s AND id_concept= %s""", (idContext,p['concept_id']))
      result=self.cur.fetchall()
      if len(result)==0 :
       #les ajoute si absent
       #vérifie que le terme est déjà présent
        self.cur.execute ("SELECT id FROM location.place_type WHERE value =%s """, (p['term_label'],))
        result=self.cur.fetchall()
        #si le terme n'est pas présent :
        if len(result)==0 :
          try :
            self.cur.execute(""" INSERT INTO  location.place_type (value) VALUES(%s) RETURNING id""",(p['term_label'],))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            idType=self.cur.fetchall()[0]['id'] # renvoie les lignes resultat de la requête
            ok=  str(ok)+' '+str(  idType )
            try :
              self.cur.execute("""INSERT INTO location.place_type_has_terminology(id_type_lieu,id_context,id_concept) VALUES(%s,%s,%s) RETURNING id_concept""",(idType,idContext,p['concept_id']))
            except Exception as e:
               self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
               self.cur.close()
               self.conn.close()
               raise e #lance une exception donc arrête le déroulement du code
            else :
              self.conn.commit()  #valide les modifications faites par le cursor
              self.cur.fetchall() # renvoie les lignes resultat de la requête
        #si le terme est présent
        else:
          idType=result[0]['id']
          ok=  str(ok)+' '+str( idType)
          try :
            self.cur.execute("""INSERT INTO location.place_type_has_terminology(id_type_lieu,id_context,id_concept) VALUES(%s,%s,%s) RETURNING id_concept""",(idType,idContext,p['concept_id']))
          except Exception as e:
             self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
             self.cur.close()
             self.conn.close()
             raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall() # renvoie les lignes resultat de la requête

    return   ok

  def getProtocolCultureByLot(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute ("""SELECT CAST(L.multiplication_date AS VARCHAR(10) ) AS date_multiplication,P.name,P.adress,P.summary,P.reference_laboratory_handbook ,P.reference_alfresco FROM plant.lot L
              JOIN plant.lot_has_protocol LAP ON LAP.id_lot=L.id_lot
              JOIN documentation.protocol P ON P.id=LAP.id_protocol
              WHERE L.id_lot=%s """, (tab['idLot'],))
    return   self.cur.fetchall()

  def getProtocolCultureLotByPrelevment(self, sessionId, idPrelevement):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute (""" SELECT P.name,P.summary, P.reference_laboratory_handbook, P.reference_alfresco,P.adress from documentation.protocol P
                          JOIN plant.lot_has_protocol LAP ON P.id=LAP.id_protocol
                          JOIN plant.group_has_lot GAL ON GAL.id_lot=LAP.id_lot
                          JOIN sample.taking PR ON PR.id_plant_group = GAL.id_group
                          WHERE PR.id =%s """, (idPrelevement,))
    return   self.cur.fetchall()

  def createAction(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #create action
    #add notations to action
    #lie l'action au container d'origine
    #create container child
    #add notations sur container
    #add loccation of the contaienr
    #crée l'action

    try :
      self.cur.execute("""INSERT INTO sample.action (id_action_type, id_user,date) VALUES (%s,%s,%s) RETURNING id""",(tab['idTypeAction'],tab['idUser'],tab['date'],))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idAction=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête

    # insert container
    try :
      self.cur.execute("""INSERT INTO sample.container (code,valid,usable,id_container_type,id_container_level,id_content_type,id_action) VALUES (%s,%s,%s,%s,%s,%s,%s) RETURNING id""",(tab['Ccode'],tab['valid'],tab['usable'],tab['idTypeContainer'],tab['idLevel'],tab['idTypeContent'],idAction))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idContainerC=self.cur.fetchall()[0]['id']  # renvoie les lignes resultat de la requête

    # insert location
    try :
      self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
      self.cur.execute("""INSERT INTO sample.location (id_box,id_container,"column","row",date_on,date_off) VALUES (%s,%s,%s,%s,%s,null) RETURNING id_box""",(tab['idBoxC'],idContainerC,tab['Ccolonne'],tab['Cligne'],tab['date']))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()  #valide les modifications faites par le cursor
      idBox=self.cur.fetchall()[0]['id_box']  # renvoie les lignes resultat de la requête

    #insert notation sur container
    crb= elvis.CrbTools()
    if len(tab['listeNotationsCont']) >0 :
      for n in tab['listeNotationsCont'] :
        typeNotation=n['type_notation']
        valeurNotation=n['valeur_notation']
        date=n['date']
        remarque=n['remarque_notation']
        site=None
        idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
        self.setContainerANotation(idContainerC,idNotation)
        #ajout des user et usergroup
        listeUser=n['notateur']
        for u in n['userGroup']:
          self.setUserGroupeANotation(u, idNotation)
        if listeUser is not None:
          for idUser in listeUser:
            self.addUserToNotation (idUser,idNotation)

    #insert action_a_container_origine
    try :
      self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
      self.cur.execute("""INSERT INTO sample.action_a_container_origin (id_action,id_container) VALUES(%s,%s) RETURNING id_action""",(idAction,tab['idContainerM'],))
    except Exception as e:
      self.conn.rollback()
      self.cur.close()
      self.conn.close()
      raise e
    else :
      self.conn.commit()
      idAction=self.cur.fetchall()[0]['id_action']

    #insert notation sur action
    crb= elvis.CrbTools()
    if len(tab['listeNotationsAct']) >0 :
      for n in tab['listeNotationsAct'] :
        typeNotation=n['type_notation']
        valeurNotation=n['valeur_notation']
        date=n['date']
        remarque=n['remarque_notation']
        site=None
        idNotation=crb.createNotation(date,remarque,site,typeNotation,valeurNotation,None)[0]['id_notation']
        self.setActionANotation(idAction,idNotation)
#        #ajout des user et usergroup
        listeUser=n['notateur']
        for u in n['userGroup']:
          self.setUserGroupeANotation(u, idNotation)
        if listeUser is not None:
          for idUser in listeUser:
            self.addUserToNotation (idUser,idNotation)

    #insert experience sur container
    if len(tab['experience']) >0 :
      for e in tab['experience'] :
        ids={}
        ids['idExperiment'] = e['id']
        ids['idContainer'] = idContainerC
        self.addExperimentToContainer (None,ids)

    #inert protocole sur action
    if tab['idProtocol'] is not None :
      try :
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        self.cur.execute("""INSERT INTO sample.action_a_protocol (id_action,id_protocol) VALUES(%s,%s) RETURNING id_action""",(idAction,tab['idProtocol'],))
      except Exception as e:
        self.conn.rollback()
        self.cur.close()
        self.conn.close()
        raise e
      else :
        self.conn.commit()
        idAction=self.cur.fetchall()[0]['id_action']


    return  'ok'

  def getInfosOnAction(self, sessionId, tab):
    rtn={}
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute ("""SELECT A.id,to_char(date,'DD/MM/YYYY' ) AS date,type, U.firstname, U.lastname FROM sample.action A
       LEFT JOIN sample.action_type AT ON AT.id=A.id_action_type
       LEFT JOIN users.user U ON U.id=A.id_user
       WHERE A.id = %s""", (tab['idAction'],))
    rtn['informations']=self.cur.fetchall()

    self.cur.execute ("""SELECT A.id,to_char(date,'DD/MM/YYYY' ) AS date,P.id AS id_protocol ,P.name,P.adress, P.summary,P.reference_laboratory_handbook, P.reference_alfresco FROM sample.action A
       LEFT JOIN sample.action_type AT ON AT.id=A.id_action_type
       LEFT JOIN sample.action_a_protocol AP ON AP.id_action=A.id
       LEFT JOIN documentation.protocol P ON P.id=AP.id_protocol
       WHERE A.id = %s""", (tab['idAction'],))
    rtn['protocole']=self.cur.fetchall()

    self.cur.execute ("""SELECT A.id,ACO.id_container FROM sample.action A
       LEFT JOIN sample.action_a_container_origin ACO ON ACO.id_action=A.id
       WHERE A.id = %s""", (tab['idAction'],))
    rtn['IdContainerOrigin']=self.cur.fetchall()

    self.cur.execute ("""SELECT A.id,ACO.id AS id_container FROM sample.action A
       LEFT JOIN sample.container ACO ON ACO.id_action=A.id
       WHERE A.id = %s""", (tab['idAction'],))
    rtn['IdContainerDest']=self.cur.fetchall()

    self.cur.execute (""" SELECT A.id,AAN.id_notation FROM sample.action A
      LEFT JOIN notation.action_has_notation AAN ON AAN.id_action=A.id
      WHERE A.id = %s""", (tab['idAction'],))
    idNotations=self.cur.fetchall()
    rtn['notation']=[]
    for i in idNotations :
      self.cur.execute ("""SELECT TN.name AS notation, CAST( N.valeur_notation_integer AS VARCHAR(10) )AS valeur, N.comment AS remarque_notation,to_char(N.date,'DD/MM/YYYY' ) AS date_notation
                           FROM notation.notation_integer N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation,  CAST( N.valeur_notation_double AS VARCHAR(10) )AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_double N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT  TN.name AS notation, N.valeur_notation_texte AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_texte N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation, to_char(N.valeur_notation_date,'DD/MM/YYYY' ) AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_date N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                            """,(i['id_notation'],i['id_notation'],i['id_notation'],i['id_notation'],))
      notes=self.cur.fetchall()
      self.cur.execute ("""SELECT id_user,firstname, lastname FROM notation.notation_has_user UAN
                           JOIN users.user U ON U.id= UAN.id_user
                           WHERE id_notation= %s """, (i['id_notation'],))
      user=self.cur.fetchall()
      rtn['notation'].append({'notations':notes,'user':user})

    return  rtn

  def getInfosOnContainer(self, sessionId, tab):
    rtn={}
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute (""" SELECT C.id, code, valid, usable, C.comment AS remark, CT.type, level, CET.type AS content,C.id_action, L.column, L.row, B.name AS box_name,to_char(L.date_on,'DD/MM/YYYY' ) AS date_on ,B.id AS id_box, id_action
                          FROM sample.container C
                          JOIN sample.container_type CT ON CT.id=C.id_container_type
                          JOIN sample.container_level CL ON CL.id=C.id_container_level
                          JOIN sample.content_type CET ON CET.id=C.id_content_type
                          LEFT JOIN sample.location L ON L.id_container=C.id
                          LEFT JOIN sample.box B ON B.id= L.id_box
                          WHERE  L.date_off is null AND C.id =%s """, (tab['idContainer'],))
    rtn['infos']=self.cur.fetchall()
    self.cur.execute (""" SELECT C.id, E.name, E.description, to_char(E.creation_date,'DD/MM/YYYY' )AS date_creation, E.id AS id_experiment
                          FROM sample.container C
                          LEFT JOIN experiment.experiment_has_container EC ON EC.id_container=C.id
                          LEFT JOIN experiment.experiment E ON E.id=EC.id_experiment
                          WHERE   C.id = %s """, (tab['idContainer'],))
    rtn['experience']=self.cur.fetchall()
    self.cur.execute ("""SELECT C.id,CAN.id_notation FROM sample.container C
                         LEFT JOIN notation.container_has_notation CAN ON CAN.id_container=C.id
                         WHERE C.id =  %s """, (tab['idContainer'],))
    idNotations=self.cur.fetchall()
    rtn['notation']=[]
    for i in idNotations :
      self.cur.execute ("""SELECT TN.name AS notation, CAST( N.valeur_notation_integer AS VARCHAR(10) )AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' ) AS date_notation
                           FROM notation.notation_integer N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation,  CAST( N.valeur_notation_double AS VARCHAR(10) )AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_double N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT  TN.name AS notation, N.valeur_notation_texte AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_texte N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                      UNION SELECT TN.name AS notation, to_char(N.valeur_notation_date,'DD/MM/YYYY' ) AS valeur, N.comment,to_char(N.date,'DD/MM/YYYY' )AS date_notation
                           FROM notation.notation_date N
                            LEFT JOIN notation.notation_type TN ON TN.id_type_notation=N.id_type_notation
                            WHERE N.id_notation=%s
                            """,(i['id_notation'],i['id_notation'],i['id_notation'],i['id_notation'],))
      notes=self.cur.fetchall()
      self.cur.execute ("""SELECT id_user,firstname, lastname FROM notation.notation_has_user UAN
                           JOIN users.user U ON U.id= UAN.id_user
                           WHERE id_notation= %s """, (i['id_notation'],))
      user=self.cur.fetchall()
      rtn['notation'].append({'notations':notes,'user':user})
      rtn['action']=[]
      idAction=rtn['infos'][0]['id_action']
      rtn['action']=[]
    if not idAction is None :
      idA = {}
      idA['idAction']=idAction
      rtn['action'].append(self.getInfosOnAction(None,idA))
    #recherche prelèvement
    rtn['prelevement']=[]
    rtn['lotOrigine']=[]
    rtn['userPrelevement']= None
    rtn['notationsPrelevement']= None
    self.cur.execute ("""SELECT P.id AS id_prelevement ,to_char(P.date,'DD/MM/YYYY' ) AS date , TT.type FROM sample.taking P
                         JOIN sample.tissue_type TT ON TT.id=P.id_tissue_type
                         WHERE P.id_container = %s """, (tab['idContainer'],))
    xp =  self.cur.fetchall()
    if len(xp)>0 :
      idPrelev = xp[0]['id_prelevement']
      rtn['lotOrigine'].append( self.getLotGeneticsInformationsByPrelevment (idPrelev))
      rtn['prelevement'].append(xp)
      #recherche des notations sur ce prelevement
      rtn['notationsPrelevement']= self.getNotationsPrelevement(idPrelev)
      #user prelevement
      self.cur.execute ("""SELECT firstname, lastname FROM sample.user_a_prelevment P
                         JOIN users.user U ON U.id=P.id_user
                         WHERE P.id_prelevment = %s """, (idPrelev,))
      rtn['userPrelevement']=self.cur.fetchall()
    return  rtn

  def addExperimentToContainer(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #affecte le container à une experience
    if(tab['idExperiment']!= None):
      try :
        self.cur.execute("""INSERT INTO experiment.experiment_has_container (id_experiment,id_container) VALUES (%s,%s) RETURNING id_experiment""",(tab['idExperiment'],tab['idContainer'],))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
    return  self.cur.fetchall()

  def addExperimentToExperiment(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #affecte le container à une experience
    if(tab['idExperiment']!= None):
      try :
        self.cur.execute("""INSERT INTO experiment.experiment_has_experiment (id_experiment,id_experiment_child) VALUES (%s,%s) RETURNING id_experiment""",(tab['idExperiment'],tab['idExperimentChild'],))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
    return  self.cur.fetchall()

# recherche les informations pour tracer un arbre à partir de l'id container
  def getTreeFromContainer(self, sessionId, idContainer):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT code AS name, CT.type, AT.type AS type_action FROM sample.container C
                        JOIN sample.container_type CT ON C.id_container_type= CT.id
                        LEFT JOIN sample.action A ON A.id=C.id_action
                        LEFT JOIN sample.action_type AT ON AT.id = A.id_action_type
                        WHERE C.id =%s""",(idContainer,))
    gr=self.cur.fetchall()
    children=[]
    #recherche les containers child
    self.cur.execute("""SELECT AACO.id_action, C.id AS idCont ,AT.type AS type_action FROM sample.action_a_container_origin AACO
                        JOIN sample.action A ON A.id=AACO.id_action
                        JOIN sample.action_type AT ON AT.id = A.id_action_type
                        JOIN sample.container C ON C.id_action=A.id
                        WHERE  AACO.id_container =%s""",(idContainer,))
    actions = self.cur.fetchall()
    if len(actions)> 0 :
      for a in actions :
        children.append(self.getTreeFromContainer(sessionId,a['idcont']))
    return {'name' : gr[0]['name'] ,
   'typeContainer' : gr[0]['type'] ,
              'id' : idContainer ,
            'type' : 'container',
          'action' : gr[0]['type_action'] ,
        'children' : children}


# recherche les informations pour tracer un arbre à partir de l'id groupe
  def getTreeFromGroup(self, sessionId, idGroupe):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""SELECT L.name, C.introduction_name AS accession   FROM plant.group G
                        JOIN plant.group_has_lot GAL ON GAL.id_group = G.id_group
                        JOIN plant.lot L ON L.id_lot= GAL.id_lot
                        JOIN plant.accession C ON C. id = L.id_accession
                        WHERE G.id_group =%s""",(idGroupe,))
    gr=self.cur.fetchall()
    children=[]
    #recherche les prelèvements
    self.cur.execute("""SELECT id_container FROM sample.taking P WHERE  id_plant_group =%s""",(idGroupe,))
    prelev = self.cur.fetchall()
    if len(prelev)> 0 :
      for p in prelev :
        children.append(self.getTreeFromContainer(sessionId,p['id_container']))

    return {'name' : str(gr[0]['accession']+' : '+gr[0]['name'] ),
       'accession' : gr[0]['accession'],
              'id' : idGroupe,
            'type' : 'groupe',
            'action' : None,
        'children' : children}

# recherche les informations pour tracer un arbre à partir de l'id experiment
  def getTreeFromExperiment(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #informations sur l experiment
    self.cur.execute("""SELECT name, description, creation_date  FROM experiment.experiment where id =%s""",(tab['idExperiment'],))
    infos = self.cur.fetchall()
    # recherche les experiments child
    self.cur.execute("""SELECT id_experiment_child FROM experiment.experiment_has_experiment where id_experiment=%s""",(tab['idExperiment'],))
    child =  self.cur.fetchall()
    children=[]
#    if len(child)> 0 :
#      for c in child :
#        children.append (self.getTreeFromExperiment(sessionId, {'idExperiment': c['id_experiment_child']}))
#
#   #recherche les lots associés
#    self.cur.execute("""SELECT id_group FROM experiment.experiment_has_group where id_experiment=%s""",(tab['idExperiment'],))
#    lot =  self.cur.fetchall()
#    if len(lot)> 0 :
#      for l in lot :
#         children.append (self.getTreeFromGroup(sessionId,l['id_group']))
#    #recherche des containers associées
#    self.cur.execute("""SELECT id_container FROM experiment.experiment_has_container where id_experiment=%s""",(tab['idExperiment'],))
#    cont =  self.cur.fetchall()
#    if len(cont)> 0 :
#      for p in cont :
#        children.append(self.getTreeFromContainer(sessionId,p['id_container']))
    if len(child)> 0 :
      for c in child :
        children.append (self.getTreeFromExperiment(sessionId, {'idExperiment': c['id_experiment_child']}))
    else :
      #recherche les lots associés
      self.cur.execute("""SELECT id_group FROM experiment.experiment_has_group where id_experiment=%s""",(tab['idExperiment'],))
      lot =  self.cur.fetchall()
      if len(lot)> 0 :
        for l in lot :
          children.append (self.getTreeFromGroup(sessionId,l['id_group']))
      #recherche des containers associées
      self.cur.execute("""SELECT id_container FROM experiment.experiment_has_container where id_experiment=%s""",(tab['idExperiment'],))
      cont =  self.cur.fetchall()
      if len(cont)> 0 :
        for p in cont :
          children.append(self.getTreeFromContainer(sessionId,p['id_container']))

    return {'name' : infos[0]['name'] ,
              'id' : tab['idExperiment'],
            'type' : 'experiment',
          'action' : None ,
        'children' : children}

  def getGraphFromContainer(self, sessionId, tab):
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #crée la table grph si besoin
    if 'graph'  not in list(tab.keys()) :
      tab['graph'] = {'node' : [] , 'link':[]}
      tab['id']=0
      tab['source']=1
    # recherche infos sur le container
    node={}
    link={}
    self.cur.execute("""SELECT code AS name, CT.type, AT.type AS type_action, AAC.id_container AS id_container_origine FROM sample.container C
                        JOIN sample.container_type CT ON C.id_container_type= CT.id
                        LEFT JOIN sample.action A ON A.id=C.id_action
                        LEFT JOIN sample.action_type AT ON AT.id = A.id_action_type
                        LEFT JOIN sample.action_a_container_origin AAC ON AAC.id_action=C.id_action
                        WHERE C.id =%s""",(tab['idContainer'],))
    gr=self.cur.fetchall()
    tab['id']=tab['id']+1
    node = {'name' : gr[0]['name'] ,
    'typeContainer' : gr[0]['type'] ,
               'id' : tab['id'] ,
             'type' : 'container',
           'action' : gr[0]['type_action'] }

    link={'source': tab['source'], 'target' :tab['id'] }
    #ajoute le noeud et le lien
    tab['graph']['node'].append(node)
    tab['graph']['link'].append(link)
    # recherche les container  origine
    tab['source'] = tab['id']
    for c in gr :
      if c['id_container_origine'] != None :
        tab['idContainer'] = c['id_container_origine']
        self.getGraphFromContainer(sessionId, tab)

    #recherche des containers child
    self.cur.execute("""SELECT C.id FROM sample.container C
                        LEFT JOIN sample.action A ON A.id=C.id_action
                        LEFT JOIN sample.action_type AT ON AT.id = A.id_action_type
                        LEFT JOIN sample.action_a_container_origin AAC ON AAC.id_action=C.id_action
                        WHERE AAC.id_container = %s""",(tab['idContainer'],))
    child=self.cur.fetchall()
    for c in child :
      tab['idContainer'] = c['id']
#      self.getGraphFromContainer(sessionId, tab)
    return tab['graph']
