# -*- coding: utf-8 -*-

from elvis.DbConnection import DbConnection
import psycopg2.extras
from elvis.SessionTools import SessionTools
import sys
import json

import elvis.data.Address

class AddressBookTools(DbConnection):
  """
    Classe contenant des méthodes qui intéragissent avec ELVIS
    Cette classe contient les méthodes référentes au shémas address_book
    création juin 2017

  AUTHORS :
  - Fabrice DUPUIS   IRHS Bioinfo team

  TODO :

  """
#    sys.stderr.write("test config: " + str(name)+" "+str(groupsIds)+ "\n")
  def __init__(self):
    super(AddressBookTools, self).__init__()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.__sessionTools = SessionTools()

  # TO BE CONTINUED : getAddress
  #  ajouter service de test
  def getAddress(self, ids):
    """Search address

    :param ids: the Ids of the address
    :return: a list of objects address

    :type ids: list of integer
    :rtype: list
    """
    req = """
      SELECT
        a.id as id,
        firstname,
        lastname,
        address1,
        address2,
        city,
        state,
        postal_code,
        id_country,
        c.name as country,
        email,
        phone
      FROM
        address_book.address a
        JOIN address_book.country c on a.id_country = c.id
      WHERE
        a.id = ANY (%s)
      """

    self.cur.execute(req, [ids])
    rep = self.cur.fetchall()

    if not rep:
      listAddress = []
    else:
      listAddress = []
      for r in rep:
        address = elvis.data.Address.Address(
          r['id'],
          r['firstname'],
          r['lastname'],
          r['address1'],
          r['address2'],
          r['city'],
          r['state'],
          r['postal_code'],
          r['id_country'],
          r['country'],
          r['email'],
          r['phone']
        )
        listAddress.append(address)
    return listAddress

  # TO BE CONTINUED : searchAddress on other than lastname
  #  ajouter un service de test
  def searchAddress(self,
    firstname = None,
    lastname = None,
    address1 = None,
    address2 = None,
    city = None,
    state = None,
    postalCode = None,
    idCountry = None,
    country = None,
    email = None,
    phone = None
    ):

    req = """
      SELECT
        id
      FROM
        address_book.address
      WHERE
      """
    if lastname:
      req = req + "lastname = %s"
      self.cur.execute(req, [lastname])
    else:
      raise Exception('Bad request')
    rep = self.cur.fetchall()
    if not rep:
      listIdAddress = []
    else:
      listIdAddress = []
      for r in rep:
        listIdAddress.append(r['id'])
    return listIdAddress
