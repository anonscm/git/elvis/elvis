from elvis.DbConnection import DbConnection
from elvis.SessionTools import SessionTools

import elvis.data.Project

class ProjectTools(DbConnection):
  def __init__(self):
    super(ProjectTools, self).__init__()
    self.__sessionTools = SessionTools()

  def getExperiments(self, groupIds, ids):
    """Get experiments given a list of id

    :param groupIds: the user groups to filter data
    :param ids: the experiment ids to fetch
    :type groupIds: list of integer
    :type ids: list of integer

    :return: list of Project objects # FIXME with Experiment object?
    :rtype: list of Project objects
    """
    rep = []
    sql = """
    SELECT
      e.id AS id,
      e.name AS name,
      e.creation_date AS creation_date,
      e.description AS description,
      e.title AS title,
      e.active AS active,
      e.date_start AS date_start,
      e.date_end AS date_end,
      e.code AS CODE,
      e.id_user AS id_user,
      e.id_experiment_type AS id_experiment_type
    FROM
      experiment.experiment e,
      experiment.experiment_has_usergroup ehu
    WHERE
      e.id = ehu.id_experiment
      AND ehu.id_usergroup = ANY (%s)
      AND e.id = ANY (%s)
    """
    self.cur.execute(sql, (groupIds, ids))
    rep = self.cur.fetchall() # FIXME need to be converted to list of objects
    return rep

  def getChildrenExperimentIds(self, id = None):
    """Get the ids of the experiments belonging to the given experiment

    :param id: The experiment id from which children experiments must be retrived
    If id == None then first level Experiment are retrived (aka: Projects)
    :type id: Integer | None

    :return: list of experiment id
    :rtype: list of Integers
    """
    rep = []
    sql = ""
    if (id != None):
      sql = """
      SELECT
        id_experiment_child AS id
      FROM
        experiment.experiment_has_experiment
      WHERE
        id_experiment = %s
      """
      self.cur.execute(sql, [id])
    else:
      sql = """
      SELECT
        e.id
      FROM
        experiment.experiment e
      WHERE
        e.id NOT IN (
          SELECT ehe.id_experiment_child
          FROM experiment.experiment_has_experiment ehe
        )
      """
      self.cur.execute(sql)
    rep += [d['id'] for d in self.cur.fetchall()]
    return rep

  def getNameProject(self, type = 'project'):
    """
    Get list of experiment names

    Author: Lysiane Hauguel 2019-07

    :param type: the type of experiment.
    :return: a list of string and integer (name, id)
    :type type: string
    :rtype: list
    """
    name = []
    sql = """
    SELECT DISTINCT
      e.name AS name,
      e.id AS id,
      uu.firstname AS firstname,
      uu.lastname AS lastname
    FROM
      experiment.experiment e
      JOIN experiment.experiment_type et
        ON e.id_experiment_type = et.id
      JOIN users.user uu
        ON e.id_user = uu.id
    WHERE
      et.name = %s
    ORDER BY
      e.name
    """
    # À ajouter quand il y aura les status :
    # es.name AS nameStatus,
    # JOIN experiment.experiment_has_status ehs ON e.id = ehs.id_experiment
    # JOIN experiment.status es ON ehs.id_status = es.id
    try:
      self.cur.execute(sql, [type])
    except Exception as e:
      raise e
    name = self.cur.fetchall()
    return name

  def getFinancingTypesName(self):
    """
    Get list of financing names

    Author: Lysiane Hauguel 2019-07

    :return: a list of string and integer (name, id)
    :rtype: list
    """
    financingTypes = []
    sql = """
    SELECT
      ef.name AS name,
      ef.id AS id
    FROM
      experiment.financing ef
    """
    try:
      self.cur.execute(sql)
    except Exception as e:
      raise e
    financingTypes = self.cur.fetchall()
    return financingTypes

  def getAxisTypesName(self):
    """
    Get list of axis names

    Author: Lysiane Hauguel 2019-07

    :return: a list of string and integer (name, id)
    :rtype: list
    """
    axisTypes = []
    sql = """
    SELECT
      ea.name AS name,
      ea.id AS id
    FROM
      experiment.axis ea
    """
    try:
      self.cur.execute(sql)
    except Exception as e:
      raise e
    axisTypes = self.cur.fetchall()
    return axisTypes

  def getAllInfoProject(self, nameProject, userId):
    # TODO changer avec IdProject !
    """
    Get a project object from a user id and project name

    Author: Lysiane Hauguel 2019-07

    :param nameProject: a project name.
    :param userId: an user id.
    :return: a project.py object
    :type nameProject: string
    :type userId: integer
    :rtype: dict
    """
    rep = []
    sql = """
    SELECT DISTINCT
      e.name AS name,
      e.id AS id,
      e.date_start AS datestart,
      e.date_end AS dateend,
      es.name AS namestatus,
      e.description AS description,
      e.title AS title,
      ea.name AS axis,
      ef.name AS financing
    FROM
      users.user u
      JOIN users.belongs_to_group ub
        ON u.id = ub.user_id
      JOIN users.usergroup uu
        ON ub.group_id = uu.id
      JOIN experiment.experiment_has_usergroup ehu
        ON uu.id = ehu.id_usergroup
      JOIN experiment.experiment e
        ON ehu.id_experiment = e.id
      JOIN experiment.experiment_type et
        ON e.id_experiment_type = et.id
      JOIN experiment.experiment_has_status ehs
        ON e.id = ehs.id_experiment
      JOIN experiment.status es
        ON ehs.id_status = es.id
      JOIN experiment.experiment_has_axis eha
        ON e.id = eha.id_experiment
      JOIN experiment.axis ea
       ON eha.id_axis = ea.id
      JOIN experiment.experiment_has_financing ehf
        ON e.id = ehf.id_experiment
      JOIN experiment.financing ef
        ON ehf.id_financing = ef.id
    WHERE
      e.name = %s
      AND u.id = %s
    """
    try:
      self.cur.execute(sql, [nameProject, userId])
    except Exception as e:
      raise e
    rep = self.cur.fetchone()
    project = elvis.data.Project(rep['id'], rep['name'], None, rep['title'], rep['description'], rep['datestart'], rep['dateend'], rep['namestatus'], rep['axis'], rep['financing'])
    return project.toJson()

  def getUserWritable(self, idProject, groupsId):
    """
    Get the information user writable on a project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :param groupsId: an groups user id.
    :return: boolean if user is writable or not
    :type idProject: integer
    :type groupsId: integer
    :rtype: boolean
    """
    writable = []
    sql = """
    SELECT DISTINCT
      writable AS writable,
      e.id_experiment AS id
    FROM
      experiment.experiment_has_usergroup e
    WHERE
      e.id_experiment = %s
      AND ARRAY[e.id_usergroup] && ARRAY[%s]
    """
    try:
      self.cur.execute(sql, [idProject, groupsId])
    except Exception as e:
      raise e
    writable = self.cur.fetchall()
    return writable

  def getChildProject(self, idProject):
    """
    Get child project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :return: list with experiment name his parent id and child id
    :type idProject: integer
    :rtype: list of string and integer
    """
    child = []
    sql = """
    SELECT DISTINCT
      e.name AS name,
      ehe.id_experiment_child AS id_child,
      ehe.id_experiment AS id_parent
    FROM
      experiment.experiment_has_experiment ehe
      JOIN experiment.experiment e ON e.id = ehe.id_experiment_child
    WHERE
      ehe.id_experiment_child = %s
    """
    try:
      self.cur.execute(sql, [idProject])
    except Exception as e:
      raise e
    child = self.cur.fetchone()
    return child

  def getTakingByProject(self, idProject):
    """
    Get taking by project

    Author: Lysiane Hauguel 2019-07

    :param idProject: a project id.
    :return: list with ids and names of experiment and his taking
    :type idProject: integer
    :rtype: list of string and integer
    """
    taking = []
    sql = """
    SELECT
      e.id AS id_project,
      e.name AS name_project,
      c.code AS code,
      c.id AS id_taking
    FROM
      sample.container c
      JOIN experiment.experiment_has_container ehc ON c.id = ehc.id_container
      JOIN experiment.experiment e ON e.id = ehc.id_experiment
      JOIN sample.taking t ON t.id_container = c.id
    WHERE
      e.id = %s
    ORDER BY
      c.code
    """
    try:
      self.cur.execute(sql, [idProject])
    except Exception as e:
      raise e
    taking = self.cur.fetchall()
    return taking

  def getActionByTaking(self, nameTaking):
    """
    Get action by taking

    Author: Lysiane Hauguel 2019-07

    :param nameTaking: a taking name.
    :return: list with ids and names of taking and his action
    :type nameTaking: string
    :rtype: list of string and integer
    """
    action = []
    sql = """
    SELECT
      C.id AS id_container,
      C.code AS code,
      CT.type AS name
    FROM sample.container C
      JOIN sample.content_type CT ON CT.id= C.id_content_type
    WHERE
      C.code = %s
      AND C.id_action IS NOT NULL
    """
    try:
      self.cur.execute(sql, [nameTaking])
    except Exception as e:
      raise e
    action = self.cur.fetchall()
    return action
