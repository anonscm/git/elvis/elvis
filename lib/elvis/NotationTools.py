# -*- coding: utf-8 -*-

from elvis.DbConnection import DbConnection
import psycopg2.extras
from elvis.SessionTools import SessionTools
import sys

import elvis.data.NotationType
import elvis.data.ExtendedNotationType

class NotationTools(DbConnection):
  def __init__(self):
    super(NotationTools, self).__init__()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.__sessionTools = SessionTools()

    self.__textualNotationTypes = ['texte', 'document']
    self.__orderedNotationTypes = ['integer', 'double', 'date']
    self.__notationTypes = self.__textualNotationTypes + self.__orderedNotationTypes

  def getNotationContextList(self, groupsIds):
    """
    TODO use object
    Get the list of notation contexts accessible to the given usergroups ordered by context name

    :param groupsIds: the Ids of the user groups to use as filter
    :return: the list of context as dict with key: id, name, comment

    :type groupsIds: list of int
    :rtype: list of dict
    """
    rep = []
    sql = """
    SELECT
      cn.id AS id,
      cn.name AS name,
      cn.comment AS comment
    FROM
      notation.context cn,
      notation.context_has_usergroup chu
    WHERE
      chu.id_context = cn.id
      AND chu.id_usergroup = ANY (%s)
    ORDER BY cn.name
    """
    self.cur.execute(sql, [groupsIds])
    rep = self.cur.fetchall()
    return rep

  def getNotationTypesForContext(self, groupsIds, contextId):
    notationTypes = []
    list = []
    idReq = isinstance(contextId, int)
    if (not idReq and not isinstance(contextId, basestring)):
      raise Exception("contextId must be int or sting")
    fieldName = "name"
    if idReq:
      fieldName = "id"
    sql = """
    SELECT
      tn.id_type_notation AS id, tn.name, tn.type AS type, lcn.rank AS rank, ntc.value AS constraint
    FROM
      notation.context cn,
      notation.lien_contexte_notation lcn,
      notation.context_has_usergroup chu ,
      notation.notation_type tn
      LEFT JOIN notation.notation_type_constraint  ntc ON ntc.id_type_notation = tn.id_type_notation
    WHERE
      chu.id_usergroup = ANY (%s)
      AND chu.id_context = cn.id
      AND cn.id = lcn.id_context
      AND tn.id_type_notation = lcn.id_notation_type
      AND cn.{0} = %s
    ORDER BY lcn.rank
    """
    self.cur.execute(sql.format(fieldName), [groupsIds, contextId])
    notationTypes = self.cur.fetchall()
    # ajoute la liste des valeurs si type liste
    for n in notationTypes:
      ent = elvis.data.ExtendedNotationType()
      ent.setConstraint(n['constraint'])
      ent.setName(n['name'])
      ent.setId(n['id'])
      ent.setType(n['type'])

      if n['type'] == 'list':
        sql = """
        SELECT code as code, value as value
        FROM notation.notation_list_values
        WHERE id_type_notation = %s
        """
        self.cur.execute(sql, [n['id']])
        ent.setList(self.cur.fetchall())
      else:
        ent.setList([])

      list.append(ent.toDict())
    return list


  def getNotations(self, groupsIds, notationIds):
    """
    Get notations given notation ids

    :param groupsIds: the Ids of the user groups to use as filter
    :param notationIds: the Ids of the notation to get
    :return: a list of dict with keys: id, name, value, type_id, type, comment, date, site_id, observer

    :type groupsIds: list of integer
    :type notationIds: list of integer
    :rtype: list of dict
    """
    notations = []
    rawReq = """
    SELECT  n.id_notation AS id,
            tn.name,
            tn.id_type_notation AS type_id,
            n.value,
            tn.type AS type,
            n.comment AS comment,
            n.date AS date,
            n.id_site AS site_id,
            n.observer AS experimentator
    FROM notation.notation_{0} n, notation.notation_type tn, notation.notation_has_usergroup nau
    WHERE n.id_notation = ANY (%s)
    AND n.id_type_notation = tn.id_type_notation
    AND nau.id_notation = n.id_notation
    AND nau.id_usergroup = ANY (%s)
    """
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      self.cur.execute(req, [notationIds, groupsIds])
      notations = notations + self.cur.fetchall()
    return notations

  def getNotationIdsForLot(self, groupsIds, lotId, contextNotationId = None):
    rawReq = ''
    if (contextNotationId):
      rawReq = """
      SELECT
        lan.id_notation AS id
      FROM
        notation.lot_has_notation lan,
        notation.notation_{0} n,
        notation.notation_type tn,
        notation.lien_contexte_notation lcn,
        notation.context cn,
        notation.notation_has_usergroup nhu,
        experiment.experiment_has_notation ehn,
        experiment.experiment e
      WHERE
        nhu.id_usergroup = ANY(%s)
        AND nhu.id_notation = n.id_notation
        AND cn.id = %s
        AND lan.id_lot = %s
        AND e.id = ehn.id_experiment
        AND ehn.id_notation = n.id_notation
        AND cn.id = lcn.id_context
        AND lcn.id_notation_type = tn.id_type_notation
        AND n.id_notation = lan.id_notation
        AND n.id_type_notation = tn.id_type_notation
      """
    else:
      rawReq = """
      SELECT
        lan.id_notation AS id
      FROM
        notation.lot_has_notation lan,
        notation.notation_has_usergroup nhu
      WHERE
        lan.id_notation = nhu.id_notation
        AND nhu.id_usergroup = ANY(%s)
        AND lan.id_lot = %s
      """
    ids = set()
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      if (contextNotationId):
        #print self.cur.mogrify(req, [groupsIds, contextNotationId, lotId])
        self.cur.execute(req, [groupsIds, contextNotationId, lotId])
      else:
        self.cur.execute(req, [groupsIds, lotId])
      ids |= set([d['id'] for d in self.cur.fetchall()])
    return list(ids)

  def getNotationIdsForVariety(self, groupsIds, varId, contextNotationId = None):
    rawReq = ''
    if (contextNotationId):
      rawReq = """
      SELECT
        van.id_notation AS id
      FROM
        notation.variety_has_notation van,
        notation.notation_{0} n,
        notation.notation_type tn,
        notation.lien_contexte_notation lcn,
        notation.context cn,
        notation.notation_has_usergroup nhu,
        experiment.experiment_has_notation ehn,
        experiment.experiment e
      WHERE
        nhu.id_usergroup = ANY(%s)
        AND nhu.id_notation = n.id_notation
        AND cn.id = %s
        AND van.id_variety = %s
        AND e.id = ehn.id_experiment
        AND ehn.id_notation = n.id_notation
        AND cn.id = lcn.id_context
        AND lcn.id_notation_type = tn.id_type_notation
        AND n.id_notation = van.id_notation
        AND n.id_type_notation = tn.id_type_notation
      """
    else:
      rawReq = """
      SELECT
        van.id_notation AS id
      FROM
        notation.variety_has_notation van,
        notation.notation_has_usergroup nhu
      WHERE
        van.id_notation = nhu.id_notation
        AND nhu.id_usergroup = ANY(%s)
        AND van.id_variete = %s
      """
    ids = set()
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      if (contextNotationId):
        self.cur.execute(req, [groupsIds, contextNotationId, varId])
      else:
        self.cur.execute(req, [groupsIds, varId])
      ids |= set([d['id'] for d in self.cur.fetchall()])
    return list(ids)

  def getTypeNotationByGroup(self, groupsIds):
    """
    Get type notation used by userGroup
    :param groupsIds: the Ids of the user groups to use as filter
    :return: a list of dict with keys: id, name,
    """
    typeNotations = []
    req = """
    SELECT DISTINCT (NT.id_type_notation) as id, NT.name
      FROM  notation.notation_has_usergroup  NAU
      JOIN notation.notation N ON N.id_notation = NAU.id_notation
      JOIN notation.notation_type NT ON NT.id_type_notation = N.id_type_notation
      WHERE id_usergroup = ANY (%s)
      ORDER BY NT.name
    """
    self.cur.execute(req, [groupsIds,])
    typeNotations = self.cur.fetchall()
    return typeNotations

  def getValuesNotationsByTypeNotationAndGroup(self, groupsIds,typeNotIds):
    """
    Get distinc values for a type notation used by userGroup
    :param groupsIds: the Ids of the user groups to use as filter
    :param typeNotID: the Ids of the notation type to use as filter
    :return: a list of values
    """
    valuesNotations = []
    rawReq = """
    SELECT  DISTINCT( n.value)
    FROM notation.notation_{0} n, notation.notation_has_usergroup nau
    WHERE
    n.id_type_notation = ANY (%s)
    AND nau.id_notation = n.id_notation
    AND nau.id_usergroup = ANY (%s)
    ORDER BY value
    """
    for nt in self.__notationTypes:
      req = rawReq.format(nt)
      self.cur.execute(req, [typeNotIds, groupsIds])
      valuesNotations = valuesNotations + self.cur.fetchall()
    return valuesNotations

  def linkNotationsToGroups(self, groupsIds, notationIds):
    """Link a list of notation ids to a list of usergroup ids
    """
    for g in groupsIds:
      for i in notationIds:
        try:
          req = """
                INSERT INTO notation.notation_has_usergroup(id_notation, id_usergroup)
                VALUES (%s, %s)
                """
          self.cur.execute(req, (i, g))
        except Exception as e:
          pass
    return True

  def linkNotationsToLot(self, lotId, notationIds):
    """Link a list of notation ids to a lot id
    """
    try:
      for i in notationIds:
        req = """
              INSERT INTO notation.lot_has_notation(id_notation, id_lot)
              VALUES (%s, %s)
              """
        #print(self.cur.mogrify(req, (i, lotId)))
        self.cur.execute(req, (i, lotId))
    except Exception as e:
      raise e
    return True

  def linkNotationsToExperiment(self, groupsIds, notationIds, idExperiment):
    req = """
          SELECT id FROM experiment.experiment E
          JOIN experiment.experiment_has_usergroup EHU ON EHU.id_experiment = E.id
          WHERE id = %s AND EHU.id_usergroup = ANY(%s)
          """
    self.cur.execute(req, (idExperiment, groupsIds))
    rep = self.cur.fetchone()
    if not rep:
      raise Exception("Experiment name not found [{0}]".format(idExperiment))
    experimentId = rep['id']

    req = """
          INSERT INTO experiment.experiment_has_notation (id_notation, id_experiment)
          VALUES (%s, %s)
          """
    for i in notationIds:
      try :
        self.cur.execute(req, (i, experimentId))
      except Exception as e:
        raise e
    return True

  def linkNotationsToProtocol(self, groupsIds, idProtocol, notationId):
    req = """
          SELECT id FROM documentation.protocol
          WHERE id = %s
          """
    self.cur.execute(req, [idProtocol])
    rep = self.cur.fetchone()
    if not rep:
      raise Exception("Protocol not found [{0}]".format(idProtocol))
    protocolId = rep['id']
    req = """
          INSERT INTO notation.notation_has_protocol (id_notation, id_protocol)
          VALUES (%s, %s)
          """
    try :
      self.cur.execute(req, [notationId, protocolId])
    except Exception as e:
      raise e
    return True

  def linkNotationsToUsers(self, userId, notationId):
    req = """
          SELECT id FROM users.user
          WHERE id = %s
          """
    self.cur.execute(req, [userId])
    rep = self.cur.fetchone()
    if not rep:
      raise Exception("User not found [{0}]".format(userId))
    userId = rep['id']
    req = """
          INSERT INTO notation.notation_has_user (id_notation, id_user)
          VALUES (%s, %s)
          """
    try :
      self.cur.execute(req, [notationId, userId])
    except Exception as e:
      raise e
    return True

  def linkNotationsToVariety(self, varId, notationIdList):
    req = """
          SELECT id_variete as id FROM plant.variety
          WHERE id_variete = %s
          """
    self.cur.execute(req, [varId])
    rep = self.cur.fetchone()
    if not rep:
      raise Exception("Variety not found [{0}]".format(varId))
    varId = rep['id']
    for notationId in notationIdList:
      req = """
          INSERT INTO notation.variety_has_notation (id_notation, id_variete)
          VALUES (%s, %s)
          """
      try :
        self.cur.execute(req, [notationId, varId])
      except Exception as e:
        raise e
    return True

  def saveNotations(self, groupsIds, notations):
    """Save notations and attach them to user groups

    If notation has an id then it is updated else it is created
    """
    notationIds = []
    for notation in notations:
      req = ""

      notationType = self.getNotationType(name = notation['type'])
      notation['typeNotationId'] = notationType.getId()
      if notation['id']:
        id = notation['id']
        req = """
        UPDATE notation.notation_{0}
        SET (value, date, comment, id_site, id_type_notation, observer)
        VALUES (%s, %s, %s, %s, %s, %s)
        WHERE id_notation = %s
        """
        try:
          self.cur.execute(req.format(notationType.getType()), (
            notation['value'], notation['date'], notation['comment'],
            notation['siteId'], notation['typeNotationId'], notation['observer'],
            id
          ))
          notationIds.append(id)
        except Exception as e:
          raise e
      else:
        req = """
        INSERT INTO notation.notation_{0} (value, date, comment, id_site, id_type_notation, observer)
        VALUES (%s, %s, %s, %s, %s, %s)
        RETURNING id_notation AS id
        """
        try:
          self.cur.execute(req.format(notationType.getType()), (
            notation['value'], notation['date'], notation['comment'],
            notation['siteId'], notation['typeNotationId'], notation['observer']
          ))
          idN = self.cur.fetchone()['id']
          notationIds.append(idN)
        except Exception as e:
          raise e

        # Ajoute le lien au protocole
        if notation['idProtocolList']:
          for idProtocol in notation['idProtocolList']:
            self.linkNotationsToProtocol(groupsIds, idProtocol, idN)
        # Ajoute le lien aux users
        if notation['idUserList']:
          for idUser in notation['idUserList']:
            self.linkNotationsToUsers(idUser, idN)

    self.linkNotationsToGroups(groupsIds, notationIds)

    return notationIds

  def getNotationType(self, id = None, name = None):
    if (not id and not name):
      raise Exception("Need an id or a name, none were given")
    req = """
          SELECT id_type_notation AS id, name AS name, type AS type
          FROM notation.notation_type
          WHERE {0} = %s
          """
    if (name):
      self.cur.execute(req.format("name"), [name])
    if (id):
      self.cur.execute(req.format("id_type_notation"), [id])
    data = self.cur.fetchone()
    if not data:
      raise Exception("Notation type not found (id:{0} name:{1})".format(id, name))
    (id, name, type) = (data['id'], data['name'], data['type'])
    nt = elvis.data.NotationType(id, name, type)
    return nt

# Methode pour maintenance
  def getNotationIdsFromNAU(self):
    req = "select nau.id_notation as id from notation.notation_has_usergroup nau"
    self.cur.execute(req)
    ids = [d['id'] for d in self.cur.fetchall()]
    return ids

  def getNotationIdsFromLAN(self):
    req = "select lan.id_notation, lhu.id_usergroup from contexte_notation.lot_a_notation lan, materiel_vegetal.lot_has_usergroup lhu where lan.id_lot = lhu.id_lot"
    self.cur.execute(req)
    ids = [(d['id_notation'], d['id_usergroup']) for d in self.cur.fetchall()]
    return ids

  def insertNotationInNotatioHasUsergroup(self, nid, gid):
    req = "insert into contexte_notation.notation_a_usergroup(id_notation, id_usergroup, write) values (%s, %s, TRUE)"
    self.cur.execute(req, [nid, gid])
    self.conn.commit()
    return True

  def getNotationIdsLotsIdsByGroup(self):
    req = """select GAN.id_notation , GAL.id_lot
             from plant.group_has_notation GAN
             join plant.group_has_lot GAL ON GAL.id_group=GAN.id_group"""
    self.cur.execute(req)
    ids = [(d['id_notation'], d['id_lot']) for d in self.cur.fetchall()]
    return ids

  def getNotationIdsLotsIds(self):
    req = """select id_notation , id_lot
             from notation.lot_has_notation"""
    self.cur.execute(req)
    ids = [(d['id_notation'], d['id_lot']) for d in self.cur.fetchall()]
    return ids
