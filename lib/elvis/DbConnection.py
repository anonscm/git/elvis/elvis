# -*- coding: utf-8 -*-

import psycopg2
import psycopg2.extras
import socket
import json

class DbConnection(object):
  """Cette classe instancie la connection a la base de donnee"""

  def __init__(self):
    # Connection a la base de donnee
    self.conn = self.getNewConnection()
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    socket.setdefaulttimeout(10000)

  def __del__(self):
    # Destructeur
    self.cur.close()
    self.conn.close()

  def commit(self):
    self.conn.commit()

  def rollback(self):
    self.conn.rollback()

  def readConf(self, confFile):
    conf = {}
    try:
      cf = open(confFile)
      conf = json.load(cf)
      cf.close()
    except IOError as e:
      raise e
    except Exception as e:
      raise e
    return conf

  def getNewConnection(self):
    conf = self.readConf("/etc/elvis.json")
    connection = psycopg2.connect(
        database = conf["database"]["name"],
        user = conf["database"]["user"],
        password = conf["database"]["password"],
        host = conf["database"]["host"],
        port = conf["database"]["port"]
        )
    return connection

  def getNewCursor(self):
    return self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

  #/////////// Methodes qui executent les requetes /////////////////#
  def sqlE(self, texte, tupleAt):
    """ Exécute une requête SELECT
    Execute la requete passee en parametre seulement de type SELECT

    Paramètres :
    texte -- la requête
    tupleAt -- le tuple contenant les variables à remplacer dans la requête

    Retourne :
    Un tableau de dictionnaires
    """

    self.cur.execute(texte, tupleAt)
    return self.cur.fetchall()
    """
    tab = []
    for row in self.cur:
      tab.append(dict(row.items()))
    return tab
    """


  def sqlI(self, texte, tupleAt):
    """Exécute une requête INSERT ou UPDATE

    Execute la requete passee en parametre seulement de type INSERT ou UPDATE

    """

    try :
      self.cur.execute(texte, tupleAt)
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchall()
      """
      tab = []
      for row in self.cur:
        tab.append(dict(row.items()))
      return tab
      """
