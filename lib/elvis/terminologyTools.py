# -*- coding: utf-8 -*-

import psycopg2
import psycopg2.extras
import sys

from datetime import datetime

from elvis.DbConnection import DbConnection

class terminology(DbConnection):

  """
  WebServices concerning the terminology database schema

  AUTHORS :
  - Julie BOURBEILLON IRHS Bioinfo team


  SECTIONS :
  - Terminology management
  - Context management
  - Concept management
  - Term management
  - Concept Graph management
  - Relation management
  - Language management
  - Translation management
  - User interfaces
  """

#--------------------------#
#  TERMINOLOGY MANAGEMENT  #
#--------------------------#

  def getTerminologyCount(self):
  # Count the number of terminologies in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(terminology.id)
      FROM terminology.terminology
    """,)
    return self.cur.fetchone()

  def getTerminologyById(self, id_terminology):
  # Select terminology based on its id : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT terminology.id, terminology.name, terminology.description, to_char(terminology.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.terminology
      WHERE terminology.id=%s
    """, (id_terminology,))
    return self.cur.fetchone()

  def getTerminologyByName(self, terminology_name):
  # Select terminology based on its name using a case insensitive system : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT terminology.id, terminology.name, terminology.description, to_char(terminology.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.terminology
      WHERE name ILIKE %s
    """, ('%'+terminology_name+'%',))
    return self.cur.fetchone()

  def getAllTerminology(self, status):
  # Select all terminologies having a given status. Status may be :
  # - all : all terminologies
  # - active : only active terminologies that is to say those which haven't an obsolete timestamp
  # - obsolete : only terminologies which have an obsolete timestamp
  # other status values raise a ValueError exception
  # dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    if status == 'all':
      self.cur.execute("""
        SELECT terminology.id, terminology.name, terminology.description, to_char(terminology.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
        FROM terminology.terminology
        ORDER BY terminology.name
      """)
    elif status == 'active':
      self.cur.execute("""
        SELECT terminology.id, terminology.name, terminology.description, obsolete
        FROM terminology.terminology
        WHERE terminology.obsolete is null
        ORDER BY terminology.name
      """)
    elif status == 'obsolete':
      self.cur.execute("""
        SELECT terminology.id, terminology.name, terminology.description, to_char(terminology.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
        FROM terminology.terminology
        WHERE terminology.obsolete is not null
        ORDER BY terminology.name
      """)
    return self.cur.fetchall()

  def addTerminology(self, name, description):
  # Create a new terminology : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.terminology (name, description)
        VALUES (%s, %s)
        RETURNING id, name, description, obsolete;
      """, (name, description))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def checkTerminologyExists(self, name):
  # Check if a terminology with the same name already exists : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.terminology
        WHERE name = %s
      """, (name,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True


  def linkTerminologyToContext(self, id_terminology, id_context):
  # Link a terminology to a context : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.terminology_context
        VALUES (%s, %s)
      """, (id_context, id_terminology))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return True

  def setTerminologyDetails(self, name, description, obsolete, id_terminology):
  # Update details for a terminology : dico(id, name, description, obsolete)
  # Obsolete timestamp either None or string in "%d/%m/%Y - %H:%M:%S" format
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      if obsolete is None:
        self.cur.execute("""
          UPDATE terminology.terminology
          SET name = %s, description = %s, obsolete = %s
          WHERE id = %s
          RETURNING id, name, description, obsolete
        """, (name, description, obsolete, id_terminology))
      else:
        self.cur.execute("""
          UPDATE terminology.terminology
          SET name = %s, description = %s, obsolete = %s
          WHERE id = %s
          RETURNING id, name, description, to_char(terminology.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
        """, (name, description, datetime.datetime.strptime(obsolete,"%d/%m/%Y - %H:%M:%S"), id_terminology))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def setTerminologyContexts(self, id_terminology, contexts):
    # set the list of contexts a terminology belongs to : dico(id, contexts)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        DELETE FROM terminology.terminology_context
        WHERE id_terminology=%s
        RETURNING id_terminology""",( id_terminology,))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()
      if len(contexts)>0 :
        for c in contexts :
          try :
            self.cur.execute("""
            INSERT INTO terminology.terminology_context ( id_context, id_terminology)
            VALUES (%s,%s)
            RETURNING id_terminology""",(c,id_terminology))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall()  # renvoie les lignes resultat de la requête
      return 'ok'

#----------------------#
#  CONTEXT MANAGEMENT  #
#----------------------#

  def getContextCount(self):
  # Count the number of contexts in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(context.id)
      FROM terminology.context
    """,)
    return self.cur.fetchone()

  def getContextById(self, id_context):
  # Select context based on its id : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT context.id, context.name, context.description
      FROM terminology.context
      WHERE context.id = %s
    """, (id_context,))
    return self.cur.fetchone()

  def getContextByName(self, context_name):
  # Select context based on its name using a case insensitive system : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT context.id, context.name, context.description
      FROM terminology.context
      WHERE context.name ILIKE %s
    """, ('%'+context_name+'%',))
    return self.cur.fetchone()

  def getAllContext(self):
  # Select all contexts : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT context.id, context.name, context.description
      FROM terminology.context
      ORDER BY context.name
    """,)
    return self.cur.fetchall()

  def getContextByTerminology(self, id_terminology):
  # Select all contexts for a given terminology : dico(id, name, description, id_terminology)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT context.id, context.name, context.description, terminology_context.id_terminology
      FROM terminology.context, terminology.terminology_context
      WHERE terminology_context.id_context = context.id
      AND terminology_context.id_terminology = %s
      ORDER BY context.name
    """, (id_terminology,))
    return self.cur.fetchall()

  def getContextByTerm(self, id_term):
  # Select all contexts for a given term : dico(id, name, description, id_term)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT context.id, context.name, context.description, term_context.id_term
      FROM terminology.context, terminology.term_context
      WHERE term_context.id_context = context.id
      AND term_context.id_term = %s
      ORDER BY context.name
    """, (id_term,))
    return self.cur.fetchall()

  def addContext(self, name, description):
  # Create a new context : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.context (name, description)
        VALUES (%s, %s)
        RETURNING id, name, description;
      """, (name, description,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def setContextDetails(self, name, description, id_context):
  # Update details for a context : dico(id, namùe, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        UPDATE terminology.context
        SET name = %s, description = %s
        WHERE id = %s
        RETURNING id, name, description;
      """, (name, description, id_context,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def checkContextExists(self, name):
  # Check if a context with the same name already exists : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.context
        WHERE name = %s
      """, (name,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

#----------------------#
#  CONCEPT MANAGEMENT  #
#----------------------#

  def getConceptCount(self):
  # Count the number of concept in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(concept.id)
      FROM terminology.concept
    """,)
    return self.cur.fetchone()

  def getConceptById(self, id_concept):
  # Select concept based on its id : dico(id, label, description, id_terminology, name_terminology, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.concept, terminology.terminology
      WHERE terminology.id = concept.id_terminology
      AND concept.id = %s
    """, (id_concept,))
    return self.cur.fetchone()

  def getConceptByLabel(self, concept_label):
  # Select concept based on its label using a case insensitive system : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.concept
      WHERE concept.label ILIKE %s
      AND terminology.id = concept.id_terminology
    """, ('%'+concept_label+'%',))
    return self.cur.fetchone()

  def getConceptByTerminology(self, id_terminology):
  # Select concept based on the terminology it belongs to : dico(id, name, description)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.concept, terminology.terminology
      WHERE concept.id_terminology = %s
      AND terminology.id = concept.id_terminology
      ORDER BY concept.label
    """, (id_terminology,))
    return self.cur.fetchall()

  def getAllConcept(self):
  # Select all concepts: dico(id, label, description, id_terminology, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.concept , terminology.terminology
      WHERE terminology.id = concept.id_terminology
      ORDER BY concept.label
    """,)
    return self.cur.fetchall()

  def checkConceptExists(self, label, id_terminology):
  # Check if a concept with the same label within the same terminology already exists : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.concept
        WHERE concept.label = %s
        AND concept.id_terminology = %s
      """, (label, id_terminology,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

  def addConcept(self, label, description, id_terminology):
  # Create a new concept : dico(id, label, description, id_terminology, name_terminology, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.concept (label, description, id_terminology)
        VALUES (%s, %s, %s)
        RETURNING id;
      """, (label, description, id_terminology,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      id_concept = self.cur.fetchone()["id"];
      try :
        self.cur.execute("""
          SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
          FROM terminology.concept, terminology.terminology
          WHERE terminology.id = concept.id_terminology
          AND concept.id = %s
        """, (id_concept,))
      except Exception as e:
        raise e
      else :
        return self.cur.fetchone()

  def setConceptDetails(self, label, description, obsolete, id_terminology, id_concept):
  # Update details for a concept : dico(id, label, description, id_terminology, name_terminology, obsolete)
  # Obsolete timestamp either None or string in "%d/%m/%Y - %H:%M:%S" format
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      if obsolete is None:
        self.cur.execute("""
          UPDATE terminology.concept
          SET label = %s, description = %s, obsolete = %s, id_terminology = %s
          WHERE id = %s
          RETURNING id
        """, (label, description, obsolete, id_terminology, id_concept))
      else:
        self.cur.execute("""
          UPDATE terminology.concept
          SET label = %s, description = %s, obsolete = %s, id_terminology = %s
          WHERE id = %s
          RETURNING id
        """, (label, description, datetime.datetime.strptime(obsolete,"%d/%m/%Y - %H:%M:%S"), id_terminology, id_concept))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      id_concept = self.cur.fetchone()["id"];
      try :
        self.cur.execute("""
          SELECT concept.id, concept.label, concept.description, concept.id_terminology, terminology.name AS name_terminology, to_char(concept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
          FROM terminology.concept, terminology.terminology
          WHERE terminology.id = concept.id_terminology
          AND concept.id = %s
        """, (id_concept,))
      except Exception as e:
        raise e
      else :
        return self.cur.fetchone()

#-------------------#
#  TERM MANAGEMENT  #
#-------------------#

  def getTermCount(self):
  # Count the number of terms in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(term.id)
      FROM terminology.term
    """,)
    return self.cur.fetchone()

  def getAllTerm(self):
  # Select all terms : dico(id, name, id_concept, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT term.id, term.label, term.id_concept, concept.label AS label_concept, to_char(term.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.term, terminology.concept
      WHERE term.id_concept = concept.id
      ORDER BY term.label
    """)
    return self.cur.fetchall()

  def getTermById(self, id_term):
  # Select term based on its id : dico(id, name, id_concept, label_concept, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT term.id, term.label, term.id_concept, concept.label AS label_concept, to_char(term.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.term, terminology.concept
      WHERE term.id = %s
      AND term.id_concept = concept.id
    """, (id_term,))
    return self.cur.fetchone()

  def getTermByConcept(self, id_concept):
  # Select term based on the concept it belongs to : dico(id, name, id_concept, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT term.id, term.label, term.id_concept, concept.label AS label_concept, to_char(term.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.term, terminology.concept
      WHERE term.id_concept = %s
      AND term.id_concept = concept.id
      ORDER BY term.label
    """, (id_concept,))
    return self.cur.fetchall()

  def checkTermExists(self, label, id_concept):
  # Check if a term with the same label already exists for the same concept : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.term
        WHERE term.label = %s
        AND term.id_concept = %s
      """, (label, id_concept,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

  def addTerm(self, label, id_concept):
  # Create a new term : dico(id, label, id_concept, label_concept, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.term (label, id_concept)
        VALUES (%s, %s)
        RETURNING id;
      """, (label, id_concept,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      id_term = self.cur.fetchone()["id"];
      try :
        self.cur.execute("""
          SELECT term.id, term.label, term.id_concept, concept.label AS label_concept, to_char(term.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
          FROM terminology.concept, terminology.term
          WHERE concept.id = term.id_concept
          AND term.id = %s
        """, (id_term,))
      except Exception as e:
        raise e
      else :
        return self.cur.fetchone()

  def setTermDetails(self, label, obsolete, id_concept, id_term):
  # Update details for a term : dico(id, label, id_concept, label_concept, obsolete)
  # Obsolete timestamp either None or string in "%d/%m/%Y - %H:%M:%S" format
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      if obsolete is None:
        self.cur.execute("""
          UPDATE terminology.term
          SET label = %s, obsolete = %s, id_concept = %s
          WHERE id = %s
          RETURNING id
        """, (label, obsolete, id_concept, id_term))
      else:
        self.cur.execute("""
          UPDATE terminology.term
          SET label = %s, obsolete = %s, id_concept = %s
          WHERE id = %s
          RETURNING id
        """, (label, datetime.datetime.strptime(obsolete,"%d/%m/%Y - %H:%M:%S"), id_concept, id_term))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      id_term = self.cur.fetchone()["id"];
      try :
        self.cur.execute("""
          SELECT term.id, term.label, term.id_concept, concept.label AS label_concept, to_char(term.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
          FROM terminology.concept, terminology.term
          WHERE concept.id = term.id_concept
          AND term.id = %s
        """, (id_term,))
      except Exception as e:
        raise e
      else :
        return self.cur.fetchone()

  def setTermContexts(self, id_term, contexts):
    # set the list of contexts a terminology belongs to : dico(id, contexts)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        DELETE FROM terminology.term_context
        WHERE id_term=%s
        RETURNING id_term""",( id_term,))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()
      if len(contexts)>0 :
        for c in contexts :
          try :
            self.cur.execute("""
            INSERT INTO terminology.term_context ( id_context, id_term)
            VALUES (%s,%s)
            RETURNING id_term""",(c, id_term,))
          except Exception as e:
            self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
            self.cur.close()
            self.conn.close()
            raise e #lance une exception donc arrête le déroulement du code
          else :
            self.conn.commit()  #valide les modifications faites par le cursor
            self.cur.fetchall()  # renvoie les lignes resultat de la requête
      return 'ok'

#----------------------------#
#  CONCEPT GRAPH MANAGEMENT  #
#----------------------------#

  def getConceptGraphCount(self):
  # Count the number of relationships in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(concepts_graph.id_left_concept)
      FROM terminology.concepts_graph
    """,)
    return self.cur.fetchone()

  def getConceptGraphByTerminology(self, id_terminology):
  # Select concept graph based on the terminolgy the concepts belong to : dico(id_left_concept, label_left_concept, id_relationship, name_relationship, id_right_concept, label_right_concept)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concepts_graph.id_left_concept, lconcept.label as label_left_concept, lconcept.description as description_left_concept, to_char(lconcept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete_left_concept, lconcept.id_terminology as id_terminology_left_concept, terminology.name as name_terminology_left_concept,
        concepts_graph.id_relationship, relationship.name as name_relationship, relationship.description as description_relationship, to_char(relationship.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete_relationship,
        concepts_graph.id_right_concept, rconcept.label as label_right_concept, rconcept.description as description_right_concept, to_char(rconcept.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete_right_concept, rconcept.id_terminology as id_terminology_right_concept, terminology.name as name_terminology_right_concept
      FROM terminology.concept as lconcept,
        terminology.concept as rconcept,
        terminology.concepts_graph,
        terminology.relationship,
        terminology.terminology
      WHERE concepts_graph.id_relationship = relationship.id
      AND concepts_graph.id_left_concept = lconcept.id
      AND concepts_graph.id_right_concept = rconcept.id
      AND (lconcept.id_terminology = %s OR rconcept.id_terminology = %s)
      AND terminology.id = %s;

    """, (id_terminology, id_terminology, id_terminology,))
    return self.cur.fetchall()

  def checkTripleExists(self, id_left_concept, id_relationship, id_right_concept):
  # Check if a triple is already present in the concept_graph table : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT count(concepts_graph.id_left_concept) FROM terminology.concepts_graph
        WHERE concepts_graph.id_left_concept = %s
        AND concepts_graph.id_relationship = %s
        AND concepts_graph.id_right_concept = %s
      """, (id_left_concept, id_relationship, id_right_concept,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

  def setTriple(self, id_left_concept, id_relationship, id_right_concept, id_left_concept_old, id_relationship_old, id_right_concept_old):
  # Update details for a triple : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        UPDATE terminology.concepts_graph
        SET id_left_concept = %s, id_relationship = %s, id_right_concept = %s
        WHERE id_left_concept = %s
        AND id_relationship = %s
        AND id_right_concept = %s
        RETURNING id_left_concept, id_relationship, id_right_concept;
      """, (id_left_concept, id_relationship, id_right_concept, id_left_concept_old, id_relationship_old, id_right_concept_old,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def addTriple(self, id_left_concept, id_relationship, id_right_concept):
  # Update details for a triple : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.concepts_graph (id_left_concept, id_relationship, id_right_concept)
        VALUES (%s, %s, %s)
        RETURNING id_left_concept, id_relationship, id_right_concept;
      """, (id_left_concept, id_relationship, id_right_concept,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

#-----------------------#
#  RELATION MANAGEMENT  #
#-----------------------#

  def getRelationCount(self):
  # Count the number of relation types in the database : dico(count)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT count(relationship.id)
      FROM terminology.relationship
    """,)
    return self.cur.fetchone()

  def getAllRelation(self):
  # Select all relations : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT relationship.id, relationship.name, relationship.description, to_char(relationship.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.relationship
      ORDER BY relationship.name
    """)
    return self.cur.fetchall()

  def getRelationById(self, id_relation):
  # Select relation based on its id : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT relationship.id, relationship.name, relationship.description, to_char(relationship.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
      FROM terminology.relationship
      WHERE relationship.id=%s
    """, (id_relation,))
    return self.cur.fetchone()

  def checkRelationExists(self, name):
  # Check if a relationship with the same name already exists : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.relationship
        WHERE name = %s
      """, (name,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

  def addRelation(self, name, description):
  # Create a new relationship : dico(id, name, description, obsolete)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.relationship (name, description)
        VALUES (%s, %s)
        RETURNING id, name, description, obsolete;
      """, (name, description))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def setRelationDetails(self, name, description, obsolete, id_relationship):
  # Update details for a relation : dico(id, name, description, obsolete)
  # Obsolete timestamp either None or string in "%d/%m/%Y - %H:%M:%S" format
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      if obsolete is None:
        self.cur.execute("""
          UPDATE terminology.relationship
          SET name = %s, description = %s, obsolete = %s
          WHERE id = %s
          RETURNING id, name, description, obsolete
        """, (name, description, obsolete, id_relationship))
      else:
        self.cur.execute("""
          UPDATE terminology.relationship
          SET name = %s, description = %s, obsolete = %s
          WHERE id = %s
          RETURNING id, name, description, to_char(relationship.obsolete, 'DD/MM/YYYY - HH24:MI:SS') as obsolete
        """, (name, description, datetime.datetime.strptime(obsolete,"%d/%m/%Y - %H:%M:%S"), id_relationship))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

#-----------------------#
#  LANGUAGE MANAGEMENT  #
#-----------------------#

  def getAllLanguage(self):
  # Select all languages : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT language.id, language.code, language.name
      FROM terminology.language
      ORDER BY language.code
    """)
    return self.cur.fetchall()

  def getLanguageById(self, id_language):
  # Select relation based on its id : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT language.id, language.code, language.name
      FROM terminology.language
      WHERE language.id=%s
    """, (id_language,))
    return self.cur.fetchone()

  def checkLanguageExists(self, code):
  # Check if a language with the same code already exists : dico(Boolean)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        SELECT COUNT(id) from terminology.language
        WHERE code = %s
      """, (code,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      res = self.cur.fetchone()
      if res['count'] == 0:
        return False
      else:
        return True

  def addLanguage(self, code, name):
  # Create a new language : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.language (code, name)
        VALUES (%s, %s)
        RETURNING id, code, name;
      """, (code, name))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

  def setLanguageDetails(self, code, name, id_language):
  # Update details for a language : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        UPDATE terminology.language
        SET code = %s, name = %s
        WHERE id = %s
        RETURNING id, code, name;
      """, (code, name, id_language))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      return self.cur.fetchone()

#--------------------------#
#  TRANSLATION MANAGEMENT  #
#--------------------------#

  def getTranslationByTerminology(self, id_terminology):
  # Select relation based on its id : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT i18n.id as i18n_id, i18n.label as i18n_label, language.id as language_id, language.code as language_code, language.name as language_name
      FROM terminology.terminology_translation, terminology.i18n, terminology.language
      WHERE terminology.terminology_translation.id_translation = terminology.i18n.id
      AND terminology.i18n.id_language = terminology.language.id
      AND id_terminology = %s;
    """, (id_terminology,))
    return self.cur.fetchall()

  def getTranslationByTerm(self, id_term):
  # Select relation based on its id : dico(id, code, name)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT i18n.id as i18n_id, i18n.label as i18n_label, language.id as language_id, language.code as language_code, language.name as language_name
      FROM terminology.term_translation, terminology.i18n, terminology.language
      WHERE terminology.term_translation.id_translation = terminology.i18n.id
      AND terminology.i18n.id_language = terminology.language.id
      AND id_term = %s;
    """, (id_term,))
    return self.cur.fetchall()

  def addTerminologyTranslation(self, label, id_language, id_terminology):
  # Add a new translation to a terminology : dico(id, label, id_language)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        INSERT INTO terminology.i18n (label, id_language)
        VALUES (%s, %s)
        RETURNING id, label, id_language;
      """, (label, id_language,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      translation = self.cur.fetchall()[0]['id']
      try :
        self.cur.execute("""
          INSERT INTO terminology.terminology_translation (id_terminology, id_translation)
          VALUES (%s, %s)
          RETURNING id_terminology, id_translation;
        """, (id_terminology, translation,))
      except Exception as e:
        raise e
      else :
        self.conn.commit()
        self.cur.execute("""
          SELECT i18n.id as i18n_id, i18n.label as i18n_label, language.id as language_id, language.code as language_code, language.name as language_name
          FROM terminology.terminology_translation, terminology.i18n, terminology.language
          WHERE terminology.terminology_translation.id_translation = terminology.i18n.id
          AND terminology.i18n.id_language = terminology.language.id
          AND i18n.id = %s;
        """, (translation,))
        return self.cur.fetchone()

  def setTranslationDetails(self, label, id_language, id_translation):
  # Update details for a translation : dico(id, label, id_language)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        UPDATE terminology.i18n
        SET label = %s, id_language = %s
        WHERE id = %s
        RETURNING id, label, id_language;
      """, (label, id_language, id_translation,))
    except Exception as e:
      raise e
    else :
      self.conn.commit()
      translation = self.cur.fetchall()[0]['id']
      self.cur.execute("""
          SELECT i18n.id as i18n_id, i18n.label as i18n_label, language.id as language_id, language.code as language_code, language.name as language_name
          FROM terminology.terminology_translation, terminology.i18n, terminology.language
          WHERE terminology.terminology_translation.id_translation = terminology.i18n.id
          AND terminology.i18n.id_language = terminology.language.id
          AND i18n.id = %s;
        """, (translation,))
      return self.cur.fetchone()

  def deleteTerminologyTranslation(self, id_translation):
    # delete a translation for a terminology : dico('ok')
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try :
      self.cur.execute("""
        DELETE FROM terminology.terminology_translation
        WHERE id_translation=%s
      """,( id_translation,))
    except Exception as e:
      self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
      self.cur.close()
      self.conn.close()
      raise e #lance une exception donc arrête le déroulement du code
    else :
      self.conn.commit()
      try :
        self.cur.execute("""
        DELETE FROM terminology.i18n
        WHERE id=%s
      """,( id_translation,))
      except Exception as e:
        self.conn.rollback() # voir doc psycopg2 annule les modifs faites par le cursor
        self.cur.close()
        self.conn.close()
        raise e #lance une exception donc arrête le déroulement du code
      else :
        self.conn.commit()  #valide les modifications faites par le cursor
      return 'ok'

#-------------------#
#  USER INTERFACES  #
#-------------------#
# Queries design for external user interfaces which are exploiting the terminology database schema

  def getTermsList(self, id_terminology, id_context):
  # Returns concept ids and associated term ids and labels for a given ontology and context : dico(concept_id, concept_label, term_id, term_label)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concept.id AS concept_id, concept.label AS concept_label, term.id AS term_id, term.label AS term_label
      FROM terminology.terminology
        INNER JOIN terminology.concept
          ON terminology.id = concept.id_terminology
        INNER JOIN terminology.term
          ON concept.id = term.id_concept
        LEFT OUTER JOIN terminology.term_context
          ON term_context.id_term = term.id
      WHERE (term.id NOT IN
        (SELECT term.id
        FROM terminology.term, terminology.concept
        WHERE term.id_concept = concept.id
        AND concept.id in
          (SELECT concept.id
          FROM terminology.term_context, terminology.term, terminology.concept
          WHERE term.id = term_context.id_term
          AND term.id_concept = concept.id
          )
        )
        OR term.id IN (SELECT term_context.id_term FROM terminology.term_context WHERE term_context.id_context = %s)
       )
      AND terminology.id = %s
      AND term.obsolete is null
      AND concept.obsolete is null
      ORDER BY term.label
    """, (id_context, id_terminology,))
    return self.cur.fetchall()

  def getTermsByTerminologyName(self, terminology_name, id_context):
  # Returns concept ids and associated term ids and labels for a given ontology and context : dico(concept.id, term.id, term.label)
    termin = self.getTerminologyByName(terminology_name)
    return self.getTermsList(termin["id"], id_context)

  def getConceptNeighbours(self, id_concept):
  # Returns the neighbours of a given concept (concept graph where it is either the left concept or the right concept) : dico(id_left_concept, label_left_concept, id_relationship, name_relationship, id_right_concept, label_right_concept)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT concepts_graph.id_left_concept, lconcept.label as label_left_concept, concepts_graph.id_relationship, relationship.name as name_relationship, concepts_graph.id_right_concept, rconcept.label as label_right_concept
      FROM terminology.concept as lconcept, terminology.concept as rconcept, terminology.concepts_graph, terminology.relationship
      WHERE concepts_graph.id_relationship = relationship.id
      AND concepts_graph.id_left_concept = lconcept.id
      AND concepts_graph.id_right_concept = rconcept.id
      AND (lconcept.id = %s OR rconcept.id = %s);
    """, (id_concept, id_concept))
    return self.cur.fetchall()

  def getTermsByConceptContext(self, id_concept, id_context):
  # Returns the terms for a given concept and concept : dico(id, label)
    self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    self.cur.execute("""
      SELECT term.id, term.label
      FROM terminology.term, terminology.concept, terminology.term_context
      WHERE concept.id = term.id_concept
      AND term_context.id_term = term.id
      AND concept.id = %s
      AND term_context.id_context = %s
      AND term.obsolete is null
      ORDER BY term.label
    """, (id_concept, id_context))
    return self.cur.fetchall()
