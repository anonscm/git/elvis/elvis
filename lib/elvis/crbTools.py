# -*- coding: utf-8 -*-

import sys
#from types import *
from elvis.DbConnection import DbConnection
import socket

class CrbTools(DbConnection):
    """
    Classe contenant des méthodes pour interagir avec la base sélection.
    Cette classe contient la totalité des méthodes appelées par les services de
    crbVariete.py
    """
#//--------------------------------------- Users
    # Move to UserTools
    def getUserById(self, user_id):
        return self.sqlE("SELECT * FROM users.user WHERE id=%s",(user_id,))

    # Move to UserTools
    def getGroupsByUser(self, idUser):
        return self.sqlE("SELECT * FROM users.belongs_to_group WHERE user_id=%s",(idUser,))

    def getHostByName(self):
        return socket.gethostbyname(socket.gethostname());


#//--------------------------------------- gh groupes
    # Move to PlantTools
    def setGhGroup(self, id_ghgroup, id_groupe):  #id_ghgroup est une liste des groupe (utilisé par PREMS uniquement dans la classe Lot)
        id_ghgroup=id_ghgroup[1:len(id_ghgroup)-1].split(',')
        for i in id_ghgroup:
           idghgroup=i
           insert = self.sqlI("INSERT INTO plant.group_has_usergroup (id_group,id_usergroup) VALUES (%s,%s) RETURNING *",(id_groupe,idghgroup));
        return insert

    # Move to UserTools
    def getGhGroupById(self, id_usergroup):
        return self.sqlE("SELECT * FROM users.usergroup WHERE id=%s",(id_usergroup,));

#//--------------------------------------- Sites
    # Move to LocationTools
    def getAllSites(self):
        return self.sqlE("SELECT id_site, name as nom_site FROM address_book.site ORDER BY name ASC;", ())

    # Move to LocationTools
    def getSite(self,nom): #TODO rename to getSiteId
        return self.sqlE("SELECT id_site FROM address_book.site WHERE name = %s",(nom,))

    # Move to LocationTools
    def getSiteById(self,id_site):
        return self.sqlE("SELECT id_address AS pepiniere, name AS nom_site, latitude, longitude, elevation FROM address_book.site WHERE id_site = %s",(id_site,))

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def createSite(self,nom):
        return self.sqlI("INSERT INTO address_book.site (name) VALUES (%s) RETURNING id_site",(nom,))

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def updateSite(self,nom,id_site):
        return self.sqlI("UPDATE address_book.site SET (name) = (%s) WHERE id_site=%s RETURNING id_site",(nom,id_site))

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def createSiteWithCoord(self,nom,latitude,longitude,elevation):
        return self.sqlI("INSERT INTO address_book.site (name,latitude,longitude,elevation) VALUES (%s,%s,%s,%s) RETURNING id_site",(nom,latitude,longitude,elevation))

    # Move to PlantTools
    #TODO voir utilisation dans l'interface
    def getSiteByLot(self, id_lot):
        return self.sqlE("SELECT * FROM address_book.site WHERE id_site IN (SELECT id_site FROM plant.place WHERE id_lot=%s)",(id_lot,))

#//--------------------------------------- Origine

    # def getAllOrigines(self):
    #     return self.sqlE("SELECT id_origine, description FROM plant.origine ORDER BY description ASC;", ())
    #
    # def getOrigineByDescription(self,description):
    #     return self.sqlE("SELECT * FROM plant.origine WHERE description = %s",(description,))
    #
    # def getOrigineById(self, id_origine):
    #     return self.sqlE("SELECT * FROM plant.origine WHERE id_origine = %s",(id_origine,))
    #
    # def createOrigine(self,nom):
    #     return self.sqlI("INSERT INTO plant.origine (description) VALUES (%s) RETURNING *",(nom,))
    #
    # def updateOrigine(self,id_origine,nom):
    #     return self.sqlI("UPDATE plant.origine SET (description) = (%s) WHERE id_origine=%s RETURNING *",(nom,id_origine))

#//--------------------------------------- Pays

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def getAllPays(self):
        return self.sqlE("SELECT id AS id_pays, name AS nom FROM address_book.country ORDER BY nom ASC;", ())

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def getPays(self, nom):
        return self.sqlE("SELECT id AS id_pays FROM address_book.country WHERE name = %s;", (nom,))

    # Move to LocationTools
    #TODO voir utilisation dans l'interface
    def getPaysById(self, id_pays):
        return self.sqlE("SELECT name AS nom FROM address_book.country WHERE id = %s;", (id_pays,))

#//--------------------------------------- Numéro CTIFL
    # def getAllNumeroCtifl(self):
    #     return self.sqlE("SELECT distinct num_dif_ctifl FROM plant.variety",())

#//--------------------------------------- Lieux

    # Move to LocationTools
    def getAllTypeLieu(self):
        return self.sqlE("SELECT id, value AS type FROM location.place_type ORDER BY value ASC;", ())

    # Move to LocationTools
    def getTypeLieu(self, nom):
        return self.sqlE("SELECT id FROM location.place_type WHERE value = %s",(nom,))

    # Move to LocationTools
    def getTypeLieuById(self, id_type_lieu):
        return self.sqlE("SELECT id, value AS type FROM location.place_type WHERE id = %s",(id_type_lieu,))

    # Move to LocationTools
    def createTypeLieu(self, nom):
        return self.sqlI("INSERT INTO location.place_type (value) VALUES (%s) RETURNING *",(nom,))

    # Move to LocationTools
    def updateTypeLieu(self, nom,id_type_lieu):
        return self.sqlI("UPDATE location.place_type SET (value) = (%s) WHERE id=%s RETURNING *",(nom,id_type_lieu))

#//---------------------------------------Noms variété

    # Move to PlantTools + vérifier utilisation interface
    def getAllNomVariete(self,userGroups):
        return self.sqlE("""SELECT DISTINCT  CAST(LOWER(NV.value)AS Character Varying(150))AS nom FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group
                            JOIN plant.lot L ON L.id_lot=B.id_lot
                            JOIN plant.accession C ON L.id_accession=C.id
                            JOIN plant.variety_name NV ON C.id_variety= NV.id_variety
                            WHERE id_usergroup = ANY(%s) ORDER BY nom ASC""",(userGroups,))

    # Move to PlantTools + vérifier utilisation interface
    def getAllNomsByTypeNom(self,type_nom,userGroups):
        return self.sqlE("SELECT distinct value AS nom FROM plant.variety_name WHERE id_variety_name_type IN (SELECT id_type_nom FROM plant.variety_name_type WHERE value = %s) AND id_variety IN (SELECT id_variete FROM plant.variety WHERE id_variete IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))) ORDER BY nom ASC",(type_nom,userGroups))


    # Méthode à supprimer
    # mettre à jour les données pour ajouter un "nom usuel" à toutes les variétés
    # vérifier que la création d'une variété implique l'entrée d'un nom usuel
    def addNomsVariete(self,result) :
        for i in result:
            id_variete =  i["id"]
            self.cur.execute("SELECT NV.value as nom, TNV.value as type_nom FROM plant.variety_name NV LEFT JOIN plant.variety_name_type TNV ON id_variety_name_type=id_type_nom WHERE id_variety = %s",(id_variete,))
            noms = self.cur.fetchall()
            nom = noms[0]["nom"]
            i["nom usuel"]=nom
        return result

    # pareil qu'au dessus
    def addNomsClone(self,result) :
        for i in result:
            id_variete =  i["id_variete"]
            self.cur.execute("SELECT NV.value as nom, TNV.value as type_nom FROM plant.variety_name NV LEFT JOIN plant.variety_name_type TNV ON id_variety_name_type=id_type_nom WHERE id_variety = %s",(id_variete,))
            noms = self.cur.fetchall()
            nom = noms[0]["nom"]
            i["nom usuel"]=nom
        return result

    # pareil qu'au dessus
    def addNomsLot(self,result) : #ne doit plus être utilisé
        for i in result:
            id_variete =  i["id_variete"]
            self.cur.execute("SELECT value as nom FROM plant.variety_name WHERE id_variety = %s",(id_variete,))
            noms = self.cur.fetchall()
            i["nom usuel"]=noms[0]["nom"]
        return result

    # Sortir les chaines de caractères et les mettre dans le fichier de conf
    # revoir le choix des types de nom (usuel, variétal) et leur ordre
    # Move to PlantTools
    # mettre à jour les données pour ajouter un "nom usuel" à toutes les variétés
    # vérifier que la création d'une variété implique l'entrée d'un nom usuel
    def getNomUsuelByIdVariete (self, id_var):
        """renvoie le nom usuel pour chaque id_variété
           nom usuel si présent sinon nom variété ou variétal sinon premier nom
        """
        self.cur.execute("""SELECT NV.value as nom, TNV.value AS valeur FROM plant.variety_name NV JOIN plant.variety_name_type TNV ON NV.id_variety_name_type=TNV.id_type_nom
                            WHERE id_variety = %s""",(id_var,)) #FIXME: date du nom
        names = self.cur.fetchall()
        for i in range(len(names)): #FIXME: for i in names ... i["valeur"]
            if names[i]["valeur"]=="nom usuel":
                return names[i] ["nom"]
        for i in range(len(names)):
            if "ariét" in names[i]["valeur"]:
                return names[i] ["nom"]
        return names[0]["valeur"]

    # Move to PlantTools
    def getNomsByVariete(self, id_variete):
        return self.sqlE("SELECT * FROM plant.vue_nom_variete WHERE id_variete = %s",(id_variete,))

    # Move to PlantTools
    def getNomVarieteById(self, id_nom_var):
    # Move to PlantTools
        return self.sqlE("SELECT id_nom_var AS id_nom_var, id_variety AS id_var, value AS nom, date AS date_var, id_variety_name_type AS type FROM plant.variety_name WHERE id_nom_var = %s",(id_nom_var,))

    def getNomVarieteByClone(self, id_clone):
        return self.sqlE("SELECT value AS nom,id_variety_name_type AS type FROM plant.variety_name WHERE id_variety IN (SELECT id_variete FROM plant.variety WHERE id_variete IN (SELECT id_variety FROM plant.accession WHERE id=%s))",(id_clone,))

    # Move to PlantTools
    def getNomByTypeNomAndVariete(self, type_nom, id_variete):
        return self.sqlE("SELECT NV.value as nom, TNV.value as type_nom FROM plant.variety_name NV LEFT JOIN plant.variety_name_type TNV ON id_variety_name_type=id_type_nom WHERE id_variety = %s and TNV.value = %s",(id_variete,type_nom))

    # Move to PlantTools
    def createNomVariete(self,id_var,nom,date_var,type_nom_var):
        return self.sqlI("INSERT INTO plant.variety_name (id_variety, value, date, id_variety_name_type) VALUES (%s,%s,%s,%s) RETURNING id_nom_var",(id_var,nom,date_var,type_nom_var,))

    # Move to PlantTools
    def updateNomVariete(self,id_nom_var,id_var,nom,date_var,type_nom_var):
        return self.sqlI("UPDATE plant.variety_name SET (id_var,value,date,id_variety_name_type) = (%s,%s,%s,%s) WHERE id_nom_var=%s RETURNING id_nom_var",(id_var,nom,date_var,type_nom_var,id_nom_var,))

    def deleteNomVariete(self,id_nom):
        return self.sqlI("DELETE FROM plant.variety_name WHERE id_nom_var=%s RETURNING *;",(id_nom,))

#//---------------------------------------Types noms variété
# Move to PlantTools
# Vérifier l'utilité
    def getAllTypeNom(self):
        return self.sqlE("SELECT id_type_nom, value AS valeur FROM plant.variety_name_type ORDER BY value ASC", ())

    def getTypeNom(self,nom):
        return self.sqlE("SELECT  id_type_nom, value AS valeur FROM plant.variety_name_type WHERE value = %s",(nom,))

    def getTypeNomById(self, id_type_nom):
        return self.sqlE("SELECT value AS valeur FROM plant.variety_name_type WHERE id_type_nom=%s",(id_type_nom,))

    def createTypeNom(self, nom):
        return self.sqlI("INSERT INTO plant.variety_name_type (value) VALUES (%s) RETURNING id_type_nom",(nom,))

    def updateTypeNom(self,nom,id_type_nom):
        return self.sqlI("UPDATE plant.variety_name_type SET (value)=(%s) WHERE id_type_nom=%s RETURNING *",(nom,id_type_nom))
    def getAllNomVarieteByTypeNom(self, type_nom):
        type_nom = self.getTypeNom(type_nom)[0]["id_type_nom"]
        return self.sqlE("SELECT distinct value AS nom FROM plant.variety_name WHERE id_variety_name_type=%s",(type_nom,))

#//---------------------------------------Taxinomie

    def getAllGenre(self):
        return self.sqlE("SELECT distinct genus AS genre FROM plant.taxonomy WHERE genus IS NOT NULL ORDER BY genus ASC;", ())

    def getAllEspece(self):
        return self.sqlE("SELECT distinct specie AS espece FROM plant.taxonomy WHERE specie IS NOT NULL ORDER BY specie ASC;", ())

    def getAllTaxinomie(self):
        return self.sqlE("SELECT * FROM plant.taxonomy;", ())

    def getTaxinomieByGenreAndEspece(self, genre, espece):
        return self.sqlE("SELECT id FROM plant.taxonomy WHERE genus = %s and specie = %s;", (genre, espece))

    def getTaxinomieById(self, id_taxinomie):
        return self.sqlE("SELECT id, genus AS genre, specie AS espece FROM plant.taxonomy WHERE id=%s",(id_taxinomie,))

    def createTaxinomie(self, genre, espece):
        return self.sqlI("INSERT INTO plant.taxonomy (genus, specie) VALUES (%s, %s) RETURNING id", (genre, espece))

    def updateTaxinomie(self, genre, espece,id_taxinomie):
        return self.sqlI("UPDATE plant.taxonomy SET (genus, specie) = (%s, %s) WHERE id=%s RETURNING id", (genre, espece,id_taxinomie))

    def getTaxinomieByVariete(self, id_variete):
        return self.sqlE("SELECT id_variete,genus AS genre,specie AS espece FROM plant.taxonomy LEFT JOIN plant.variety ON id_taxonomy = id WHERE id_variete=%s",(id_variete,))

    def getAllEspeceByGenre(self,genre):
        return self.sqlE("SELECT id, genus AS genre, specie AS espece FROM plant.taxonomy WHERE genus=%s",(genre,))

#//---------------------------------------Pepinieriste

    def getAllPepinieriste(self):
        return self.sqlE("SELECT DISTINCT lastname AS nom FROM address_book.address ORDER BY nom ASC;", ())

    def getAllPepinieristePays(self):
        return self.sqlE("SELECT A.id AS id_pepinieriste, A.lastname AS nom, address1 as adresse1,address2 as adresse2,city as ville,state AS etat, postal_code AS code_postal,email,phone AS  telephone, '-' AS fax,B.id AS id_pays, B.name AS Pays FROM address_book.address A LEFT JOIN address_book.country B ON A.id_country=B.id ORDER BY A.lastname ASC ;", ())

    def getPepinieristeByNom(self, nom):
        return self.sqlE("SELECT id AS id_pepinieriste FROM address_book.address WHERE lastname = %s;", (nom,))

    def getPepinieristeById(self,id_pepinieriste):
        return self.sqlE("SELECT A.id AS id_pepinieriste, A.lastname AS nom, address1 as adresse1,address2 as adresse2,city as ville,state AS etat, postal_code AS code_postal,email,phone AS telephone, '-' AS fax,B.id AS id_pays, B.name AS Pays FROM address_book.address A LEFT JOIN address_book.country B ON A.id_country=B.id WHERE A.id = %s;",(id_pepinieriste,))

    def createPepinieriste(self, nom, adresse1, adresse2, ville, etat, code_postal, pays, email, telephone, fax):
        if pays != "":
            pays = self.getPays(pays)[0]["id_pays"]
        else :
            pays = None
        return self.sqlI("INSERT INTO address_book.address (lastname,address1,address2,city,state,postal_code,id_country,email,phone) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING *;", (nom,adresse1,adresse2,ville,etat,code_postal,pays,email,telephone))

    def updatePepinieriste(self,id_pep, nom, adresse1, adresse2, ville, etat, code_postal, pays, email, telephone, fax):
        if pays != "":
            pays = self.getPays(pays)[0]["id_pays"]
        else :
            pays = None
        return self.sqlI("UPDATE address_book.address SET (lastname,address1,address2,city,state,postal_code,id_country,email,phone) = (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) WHERE id = %s RETURNING *;", (nom,adresse1,adresse2,ville,etat,code_postal,pays,email,telephone,id_pep))


#//---------------------------------------Collection

    def getAllCollection(self):
        return self.sqlE("SELECT id_collection, name AS nom FROM collection.collection ORDER BY name ASC;",())

    def getCollection(self,nom):
        return self.sqlE("SELECT id_collection FROM collection.collection WHERE name = %s",(nom,))

    def getCollectionById(self,id_collection):
        return self.sqlE("SELECT id_collection, name AS nom FROM collection.collection WHERE id_collection = %s",(id_collection,))

    def createCollection(self,nom):
        return self.sqlI("INSERT INTO collection.collection (name) VALUES (%s) RETURNING id_collection;",(nom,))

    def updateCollection(self,id_collection,nom):
        return self.sqlI("UPDATE collection.collection SET (name)=(%s) WHERE id_collection=%s RETURNING *;",(nom,id_collection,))

    def addCloneToCollection(self,collection, id_clone):
        collection= self.getCollection(collection)[0]["id_collection"]
        return self.sqlI("INSERT INTO collection.collection_has_accession (id_collection, id_clone) VALUES (%s,%s) RETURNING *;", (collection, id_clone))

    def removeCloneFromCollection(self,collection, id_clone):
        collection= self.getCollection(collection)[0]["id_collection"]
        return self.sqlI("DELETE FROM collection.collection_has_accession WHERE id_collection = %s AND id_clone = %s RETURNING *;", (collection, id_clone))

    def getCollectionsByClone(self, id_clone):
        return self.sqlE("SELECT id_collection, name AS nom FROM collection.collection WHERE id_collection IN (SELECT id_collection FROM collection.collection_has_accession WHERE id_clone = %s)",(id_clone,))

    def deleteCollectionClone(self, id_clone):
        return self.sqlI("DELETE FROM collection.collection_has_accession WHERE id_clone=%s RETURNING *",(id_clone,));


#//---------------------------------------Variété

    def updateVariete(self,id_variete,obtenteur,remarque,editeur):
        return self.sqlI("UPDATE plant.variety SET (breeder,comment,editor) = (%s,%s,%s) WHERE id_variete = %s RETURNING *",(obtenteur,remarque,editeur,id_variete))

    def createVariete(self,obtenteur,remarque,editeur):
        return self.sqlI("INSERT INTO plant.variety (breeder,comment,editor) VALUES (%s,%s,%s) RETURNING *",(obtenteur,remarque,editeur,))

    def setTaxinomie(self,id_variete,id_taxinomie):
        return self.sqlI("UPDATE plant.variety SET id_taxonomy = %s WHERE id_variete = %s RETURNING *;",(id_taxinomie,id_variete,))

    def deleteVariete(self,id_var):
        return self.sqlI("DELETE FROM plant.variety WHERE id_variete=%s RETURNING *;",(id_var,))
#//---------------------------------------Requêtes sur les variétés

    def getAllVariete(self, groups):
        query = """
        SELECT * FROM plant.vue_variete
        WHERE id IN (
          SELECT id_variety FROM plant.accession
          WHERE id IN (
            SELECT id_accession FROM  plant.lot
            WHERE id_lot IN (
              SELECT id_lot FROM plant.group_has_usergroup A
              JOIN  plant.group_has_lot B ON A.id_group=B.id_group
              WHERE id_usergroup = ANY(%s)
            )
          )
        )"""
        self.cur.execute(query, (groups,))
        result = self.cur.fetchall()
        return result

    def getVarieteById(self, id_variete):
        return self.sqlE("SELECT id_variete,breeder AS obtenteur , comment AS remarque, id_taxonomy AS id_taxinomie, editor AS editeur FROM plant.variety WHERE id_variete = %s", (id_variete,))

    def getVarieteByClone(self,id_clone):
        return self.sqlE("SELECT * FROM plant.variety WHERE id_variete IN (SELECT id_variety FROM plant.accession WHERE id=%s)",(id_clone,))

    def getNomVarieteByClone(self,id_clone):
        return self.sqlE("SELECT * FROM plant.vue_nom_variete WHERE id_variete IN (SELECT id_variety FROM plant.accession WHERE id=%s)",(id_clone,))

    # def getVarieteByNom(self,nom,groups):
    #     return self.sqlE("""
    #     SELECT id,nom,genre,espece,obtenteur
    #     FROM plant.vue_variete
    #     WHERE id IN (SELECT id_variety
    #                 FROM plant.variety_name WHERE value = %s)
    #       AND id IN (SELECT id_variety
    #                  FROM plant.accession
    #                  WHERE id IN (SELECT id_accession
    #                               FROM  plant.lot
    #                               WHERE id_lot IN (SELECT id_lot
    #                                                FROM plant.group_has_usergroup A
    #                                                JOIN  plant.group_has_lot B ON A.id_group=B.id_group
    #                                                WHERE id_usergroup = ANY(%s))))""", (nom,groups))

    def getVarieteByNomClone(self,nom,type_recherche,groups):
        query = "SELECT id,genre,espece,obtenteur FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.accession WHERE introduction_name"+type_recherche+"%s) AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN  (SELECT id_lot FROM plant.group_has_usergroup A JOIN plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByObtenteur(self,obtenteur,type_recherche,groups):
        query = "SELECT id,genre,espece,obtenteur  FROM plant.vue_variete WHERE obtenteur "+ type_recherche +"%s AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(obtenteur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByEditeur(self,editeur,type_recherche,groups):####
        query = "SELECT id,genre,espece,obtenteur  FROM plant.vue_variete WHERE editeur "+ type_recherche +"%s AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(editeur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByGenre(self,genre,type_recherche,groups):####
        query = "SELECT id,genre,espece,obtenteur  FROM plant.vue_variete WHERE genre "+ type_recherche +"%s AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(genre,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByEspece(self,espece,type_recherche,groups):####
        query = "SELECT id,genre,espece,obtenteur FROM plant.vue_variete WHERE espece "+ type_recherche +"%s AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(espece,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByNomVariete(self, nom, type_recherche,groups): ### Select variete by Nom
        query = "SELECT id,genre,espece,obtenteur FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.variety_name WHERE value " +type_recherche + "%s) AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByNomAndTypeNom(self,nom,type_nom_,type_recherche,groups):
        type_nom = self.getTypeNom(type_nom_)[0]["id_type_nom"]
        query = """SELECT DISTINCT id,genre,espece,obtenteur,NV.value AS \" """ + type_nom_  +"""\"  FROM plant.vue_variete VV
                                  JOIN plant.variety_name NV ON VV.id = NV.id_variety
                                  JOIN plant.accession C ON VV.id = C.id_variety
                                  JOIN plant.lot L ON C.id = L.id_accession
                                  JOIN plant.group_has_lot GL ON L.id_lot = GL.id_lot
                                  JOIN plant.group_has_usergroup GG ON GG.id_group=GL.id_group
                   WHERE NV.value """+ type_recherche+""" %s and NV.id_variety_name_type= %s AND GG.id_usergroup =ANY(%s) """
        self.cur.execute(query,(nom,type_nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByNom(self,nom,groups):
        return self.sqlE("""
        SELECT distinct id_variety AS id_var
        FROM plant.variety_name
        WHERE value=%s
        AND id_variety IN (SELECT id_variety
                           FROM plant.accession
                           WHERE id IN (SELECT id_accession
                                        FROM plant.lot
                                        WHERE id_lot IN (SELECT id_lot
                                                         FROM plant.group_has_usergroup A
                                                         JOIN  plant.group_has_lot B ON A.id_group=B.id_group
                                                         WHERE id_usergroup = ANY(%s))))
        """,(nom,groups,))

    def getVarieteByNomAndTypeNomWithoutUserGroup(self,nom,type_nom,type_recherche): ## pas utilisé ?
        type_nom = self.getTypeNom(type_nom)[0]["id_type_nom"]
        query = "SELECT id,genre,espece,obtenteur FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.variety_name WHERE value "+ type_recherche+"%s and id_variety_name_type=%s)"
        self.cur.execute(query,(nom,type_nom))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByNumeroClone(self,numero,type_recherche,groups): ## by numero d'accession
        query = """SELECT DISTINCT V.id,genre,espece,obtenteur,introduction_name AS \" Numero d'accession\"  FROM plant.vue_variete V
                            JOIN plant.accession C ON V.id = C.id_variety
                            JOIN plant.lot L ON C.id = L.id_accession
                            JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot
                            JOIN plant.group_has_usergroup GG ON GG.id_group=GL.id_group
                  WHERE introduction_name"""+type_recherche+"""%s AND  GG.id_usergroup = ANY(%s) """
        self.cur.execute(query,(numero,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByDateIntroduction(self,date,type_recherche,groups):
        query = """SELECT DISTINCT V.id,genre,espece,obtenteur,date_intro AS \"Date d'introduction\" FROM plant.vue_variete V
                           JOIN plant.accession C ON  V.id = C.id_variety
                           JOIN plant.lot L ON C.id = L.id_accession
                           JOIN plant.group_has_lot GL ON GL.id_lot= L.id_lot
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE C.introduction_date """+type_recherche+""" to_date('"""+date+"""', 'DD/MM/YYYY' ) AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(groups,))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByFournisseur(self,nom,type_recherche,groups):
        fournisseur = self.getPepinieristeByNom(nom)[0]["id_pepinieriste"]
        query = """ SELECT DISTINCT V.id,genre,espece,obtenteur,P.lastname AS \" Fournisseur\" FROM plant.vue_variete V
                          JOIN plant.accession C ON  V.id = C.id_variety
                          JOIN address_book.address P ON P.id = C.provider
                          JOIN plant.lot L ON C.id = L.id_accession
                          JOIN plant.group_has_lot GL ON GL.id_lot= L.id_lot
                          JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                          WHERE P.lastname"""+ type_recherche+""" %s AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByCollection(self,collection,type_recherche,groups):
        query = "SELECT DISTINCT id,genre,espece,obtenteur FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_clone FROM collection.collection_has_accession WHERE id_collection IN (SELECT id_collection FROM collection.collection WHERE name"+type_recherche+"%s))) AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
        self.cur.execute(query,(collection,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByNotation(self,valeur,type_notation_,type_recherche,groups):
        type_notation_valeur = self.getTypeNotation(type_notation_)[0]["type"]
        type_notation = self.getTypeNotation(type_notation_)[0]["id_type_notation"]
        if type_recherche != None:
            query = " SELECT DISTINCT id,genre,espece,obtenteur,ND.value  AS \" " + type_notation_  +"\" FROM plant.vue_variete VV JOIN plant.accession C  ON VV.id = C.id_variety JOIN plant.lot L  ON C.id = L.id_accession JOIN plant.group_has_lot GAL ON L.id_lot = GAL.id_lot JOIN plant.group_has_usergroup GAG ON GAG.id_group = GAL.id_group JOIN plant.group_has_notation GAN ON GAL.id_group = GAN.id_group  JOIN notation.notation_"+type_notation_valeur+" ND ON ND.id_notation = GAN.id_notation WHERE ND.value_"+type_recherche +" %s AND ND.id_type_notation= %s AND GAG.id_usergroup =ANY(%s) "
            self.cur.execute(query,(valeur,type_notation,groups))
        else :
            query = "SELECT id,genre,espece,obtenteur FROM plant.vue_variete WHERE id NOT IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT clone FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group IN (SELECT id_group FROM plant.group_has_notation WHERE id_notation IN (SELECT id_notation FROM notation.notation WHERE id_type_notation=%s))))) AND id IN (SELECT id_variety FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))))"
            self.cur.execute(query,(type_notation,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteByLieu(self, lieu, type_lieu, type_recherche,groups):
        query = "SELECT DISTINCT V.id,genre,espece,obtenteur,LI.value AS \""+type_lieu+"\" FROM plant.vue_variete V JOIN plant.accession C ON C.id_variety= V.id JOIN plant.lot L ON C.id =  L.id_accession JOIN plant.place E  ON L.id_lot = E.id_lot  JOIN  location.place LI ON E.id = LI.id_plant_place JOIN location.place_type TL  ON LI.id_place_type = TL.id JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot JOIN plant.group_has_usergroup GG  ON GG.id_group= GL.id_group WHERE LI.value "+type_recherche+" %s AND TL.value= %s AND  GG.id_usergroup = ANY(%s)"
        self.cur.execute(query,(lieu,type_lieu,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getVarieteBySite(self, site, type_recherche,groups):
        query = """SELECT DISTINCT V.id,variete AS id_variete,genre,espece, S.name  AS Site
                           FROM plant.vue_variete V
                           JOIN plant.accession C ON C.id_variety= V.id
                           JOIN plant.lot L ON C.id=L.id_accession
                           JOIN plant.place E ON id_lot =E.id_lot
                           JOIN address_book.site S ON E.id_site = S.id_site
                            WHERE S.name """+type_recherche+""" %s
                           AND  id_lot IN (SELECT id_lot
       FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group
       WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(site,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result
#//---------------------------------------Clone

    def getAllNumeroClone(self,groups):####
        return self.sqlE("SELECT distinct introduction_name AS numero,introduction_name AS nom FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.tree WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))) ORDER BY introduction_name ASC; ", (groups,))

    def createIntroductionClone(self,etat_sanitaire,mode_introduction,numero_passeport_phyto,lieu_greffage):
        return self.sqlI("INSERT INTO import_export.introduction_clone (etat_sanitaire,mode_introduction,numero_passeport_phytosanitaire,lieu_greffage) VALUES (%s,%s,%s,%s) RETURNING id",(etat_sanitaire,mode_introduction,numero_passeport_phyto,lieu_greffage))

    def updateIntroductionClone(self,id_introduction,etat_sanitaire,mode_introduction,numero_passeport_phyto,lieu_greffage):
        return self.sqlI("UPDATE import_export.introduction_clone SET (etat_sanitaire,mode_introduction,numero_passeport_phytosanitaire,lieu_greffage) = (%s,%s,%s,%s) WHERE id=%s RETURNING id",(etat_sanitaire,mode_introduction,numero_passeport_phyto,lieu_greffage))

    def createClone(self,numero,id_variete,remarque,introduction_clone,nom,fournisseur,date_intro):
        return self.sqlI("INSERT INTO plant.accession (introduction_name,id_variety,comment,introduction_clone,provider,introduction_date) VALUES (%s,%s,%s,%s,%s,%s) RETURNING id AS id_clone",(numero,id_variete,remarque,introduction_clone,fournisseur,date_intro))

    def updateClone(self,id_clone,numero,id_variete,remarque,introduction_clone,nom,fournisseur,date_intro):
        return self.sqlI("UPDATE plant.accession SET (id_variety,introduction_name,comment,introduction_clone,provider,introduction_date)=(%s,%s,%s,%s,%s,%s) WHERE id=%s RETURNING *;",(id_variete,numero,remarque,introduction_clone,fournisseur,date_intro,id_clone))

    def createIntroduction(self, groupe, date_intro, fournisseur):
        if fournisseur != "":
            fournisseur = self.getPepinieristeByNom(fournisseur)[0]["id_pepinieriste"]
        else:
            fournisseur = None
        return self.sqlI("INSERT INTO import_export.introduction (groupe,date_intro,fournisseur) VALUES (%s,%s,%s) RETURNING id_intro",(groupe,date_intro,fournisseur,))

    def updateIntroduction(self, groupe, date_intro, fournisseur):
        return self.sqlI("UPDATE import_export.introduction SET (date_intro,fournisseur) = (%s,%s) WHERE groupe=%s RETURNING id_intro",(date_intro,fournisseur,groupe))

    def createMaterielVegetal(self,id_intro, type_materiel, nb, description):
      return None
#        if type_materiel != "":
#            type_materiel = self.getTypeMateriel(type_materiel)[0]["id_type"]
#        else:
#            type_materiel = None
#        return self.sqlI("INSERT INTO import_export.materiel_vegetal (id_intro, type_materiel, nb, description) VALUES (%s,%s,%s,%s) RETURNING *",(id_intro, type_materiel, nb, description))

    def updateMaterielVegetal(self,id_intro, type_materiel, nb, description):
      return None
#        if type_materiel != "":
#            type_materiel = self.getTypeMateriel(type_materiel)[0]["id_type"]
#        else:
#            type_materiel = None
#        return self.sqlI("UPDATE import_export.materiel_vegetal SET (type_materiel, nb, description)=(%s,%s,%s) WHERE id_intro=%s RETURNING *",(type_materiel, nb, description,id_intro))

    def getIntroductionCloneById(self, id_introduction_clone):
        return self.sqlE("SELECT * FROM import_export.introduction_clone WHERE id=%s",(id_introduction_clone,))


#//---------------------------------------Requêtes clone

    def getAllClone(self,groups):
        return self.sqlE("SELECT id AS id_clone, introduction_name AS numero,id_variety AS variete,comment AS remarque,introduction_clone,provider AS fournisseur,introduction_date AS date_intro,collection_date AS date_collecte,collection_site AS site_collecte, introduction_name AS nom FROM plant.accession WHERE id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))) ORDER BY introduction_name ASC; ", (groups,))

    def getCloneById(self, id_clone):
        return self.sqlE("SELECT  id AS id_clone, introduction_name AS numero,id_variety AS variete,comment AS remarque,introduction_clone,provider AS fournisseur,introduction_date AS date_intro,collection_date AS date_collecte,collection_site AS site_collecte, introduction_name AS nom  FROM plant.accession WHERE id = %s ",(id_clone,))

    def getCloneByIdVariete(self, id_variete):
        return self.sqlE("SELECT  id AS id_clone, introduction_name AS numero,id_variety AS variete,comment AS remarque,introduction_clone,provider AS fournisseur,introduction_date AS date_intro,collection_date AS date_collecte,collection_site AS site_collecte, introduction_name AS nom  FROM plant.accession WHERE id_variety = %s", (id_variete,))

    def getCloneByNumero3(self, numero):
        return self.sqlE("SELECT id AS id_clone, introduction_name AS numero,id_variety AS variete,comment AS remarque,introduction_clone,provider AS fournisseur,introduction_date AS date_intro,collection_date AS date_collecte,collection_site AS site_collecte, introduction_name AS nom FROM plant.accession WHERE introduction_name=%s",(numero,))

    def getCloneByNumero(self,numero, type_recherche,groups):
        query = "SELECT id, id_variety AS id_variete, numero AS \"Nom accession\",genre, espece FROM plant.vue_clone WHERE introduction_name"+type_recherche+ "%s AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"
        self.cur.execute(query,(numero,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByNumeroWithoutGroups(self,numero, type_recherche):
        query = "SELECT * FROM plant.vue_clone WHERE introduction_name"+type_recherche+ "%s"
        self.cur.execute(query,(numero,))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByNom(self,nom, type_recherche,groups):
        query = "SELECT * FROM plant.vue_clone WHERE nom"+type_recherche+ "%s AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByNumero2(self, numero,groups):
        return self.sqlE("SELECT * FROM plant.vue_clone WHERE introduction_name=%s AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))",(numero,groups))

    def getCloneByNomVariete(self,nom, type_recherche,groups):
        query = """SELECT id, variete AS id_variete, numero AS \"Nom accession\", genre, espece FROM plant.vue_clone WHERE variete IN (SELECT id FROM plant.vue_variete WHERE id IN (SELECT id_variety FROM plant.variety_name WHERE nom """ +type_recherche + """%s)) AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"""
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByObtenteur(self,obtenteur,type_recherche,groups):
        query = """SELECT V.id, variete AS id_variete, numero AS \"Nom accession\", V.genre, V.espece, obtenteur FROM plant.vue_clone V
                            JOIN plant.vue_variete VV ON V.variete=VV.id
                            WHERE obtenteur"""+type_recherche+"""%s

        AND V.id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"""
        self.cur.execute(query, (obtenteur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByGenre(self, genre,type_recherche,groups):
        query = """SELECT DISTINCT id_clone AS id, variete AS id_variete, numero AS numero , V.genre, V.espece FROM plant.vue_lot L
                            JOIN plant.vue_clone C ON  L.id_clone = C.id
                            JOIN plant.vue_variete V ON C.variete = V.id
                            WHERE V.genre"""+type_recherche+""" %s
 AND L.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN plant.group_has_lot B ON A.id_group=B.id_group
 WHERE id_usergroup =ANY(%s))"""
        self.cur.execute(query, (genre,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByEspece(self, espece,type_recherche,groups):
        query = """SELECT DISTINCT id_clone AS id, variete AS id_variete, numero AS \"Nom accession\", V.genre, V.espece FROM plant.vue_lot L
                            JOIN plant.vue_clone C ON  L.id_clone = C.id
                            JOIN plant.vue_variete V ON C.variete = V.id
                    WHERE V.espece"""+type_recherche+""" %s
                    AND L.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup =ANY(%s))"""
        self.cur.execute(query, (espece,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByEditeur(self, editeur,type_recherche,groups):
        query = """SELECT V.id, variete AS id_variete, numero AS \"Nom accession\", V.genre, V.espece, editeur FROM plant.vue_clone V
                            JOIN plant.vue_variete VV ON V.variete=VV.id
                            WHERE editeur"""+type_recherche+"""%s

        AND V.id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"""
        self.cur.execute(query, (editeur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByCollection(self, collection, type_recherche,groups):
        query = "SELECT id, variete AS id_variete, numero AS \"Nom accession\", genre, espece  FROM plant.vue_clone WHERE id IN (SELECT id_clone FROM collection.collection_has_accession WHERE id_collection IN (SELECT id_collection FROM collection.collection WHERE name"+type_recherche + "%s)) AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"
        self.cur.execute(query,(collection,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByDateIntroduction(self, date, type_recherche,groups):
        query = """SELECT DISTINCT C.id_clone AS id,genre,espece,introduction_name AS \"Nom accession\",variete AS id_variete,introduction_date AS \"Date d'introduction\" FROM plant.vue_variete V
                           JOIN plant.accession C ON  V.id = C.id_variety
                           JOIN plant.lot L ON C.id = L.id_accession
                           JOIN plant.group_has_lot GL ON GL.id_lot= L.id_lot
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE C.introduction_date """+type_recherche+""" to_date('"""+date+"""', 'DD/MM/YYYY' ) AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(groups,))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByFournisseur(self, fournisseur, type_recherche,groups):
        query = """SELECT DISTINCT C.id_clone AS id, genre,espece,introduction_name AS \"Nom accession\",C.id_variety AS id_variete,P.lastname AS Fournisseur FROM plant.vue_variete V
                           JOIN plant.accession C ON  V.id = C.id_variety
                           JOIN address_book.address P ON C.provider=P.id_pepinieriste
                           JOIN plant.lot L ON C.id = L.id_accession
                           JOIN plant.group_has_lot GL ON GL.id_lot= L.id_lot
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE P.lastname """+type_recherche+""" %s AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(fournisseur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getIdCloneByNumero(self, numero):
        return self.sqlE("SELECT id AS id_clone FROM plant.accession WHERE introduction_name =%s ",(numero,))

    def getCloneByLot(self, id_lot):
        return self.sqlE("SELECT id AS id_clone, introduction_name AS numero,id_variety AS variete,comment AS remarque,introduction_clone,provider AS fournisseur,introduction_date AS date_intro,collection_date AS date_collecte,collection_site AS site_collecte, introduction_name AS nom FROM plant.accession WHERE id IN (SELECT clone FROM plant.lot WHERE id_lot=%s) ",(id_lot))


    def getCloneByNotation(self,valeur,type_notation_,type_recherche,groups):
        type_notation_valeur = self.getTypeNotation(type_notation_)[0]["type"]
        type_notation = self.getTypeNotation(type_notation_)[0]["id_type_notation"]
        if type_recherche != None:
            query = " SELECT DISTINCT id,variete AS id_variete, V.numero AS \"Nom accession\",genre,espece,ND.value  AS \" " + type_notation_  +"\" FROM plant.vue_clone V  JOIN plant.lot L  ON V.id = L.id_accession JOIN plant.group_has_lot GAL ON L.id_lot = GAL.id_lot JOIN plant.group_has_usergroup GAG ON GAG.id_group = GAL.id_group JOIN plant.group_has_notation GAN ON GAL.id_group = GAN.id_group  JOIN notation.notation_"+type_notation_valeur+" ND ON ND.id_notation = GAN.id_notation WHERE ND.value"+type_recherche +" %s AND ND.id_type_notation= %s AND GAG.id_usergroup =ANY(%s) "
            self.cur.execute(query,(valeur,type_notation,groups))
        else :
            query = "SELECT *  FROM plant.vue_clone WHERE id NOT IN (SELECT clone FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group IN (SELECT id_group FROM plant.group_has_notation WHERE id_notation IN (SELECT id_notation FROM notation.notation WHERE id_type_notation=%s)))) AND id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"
            self.cur.execute(query,(type_notation,groups))
        #return self.cur.fetchall()
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByLieu(self, lieu, type_lieu, type_recherche,groups):
        query = """SELECT V.id,variete AS id_variete, V.numero AS "Nom accession",genre,espece,LI.value AS \""""+type_lieu+"""\"
       FROM plant.vue_clone V
       JOIN plant.lot L ON V.id=L.id_accession
       JOIN plant.place E ON id_lot =E.id_lot
       JOIN location.place LI  ON E.id =LI.id_plant_place
       JOIN location.place_type TL ON  LI.id_place_type =TL.id
       WHERE LI.value """+type_recherche+""" %s AND  TL.value=%s
       AND V.id IN
       (SELECT id_accession FROM  plant.lot
       JOIN plant.group_has_lot A ON id_lot = A.id_lot
       JOIN plant.group_has_usergroup  B ON A.id_group=B.id_group
       WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(lieu,type_lieu,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneBySite(self, site, type_recherche,groups):
        query = """SELECT DISTINCT V.id,variete AS id_variete, V.numero AS "Nom accession",genre,espece,site  AS Site
                           FROM plant.vue_clone V
                           JOIN plant.lot L ON V.id=L.id_accession
                           JOIN plant.place E ON id_lot =E.id_lot
                           JOIN location.place LI  ON E.id =LI.id_plant_place
                           JOIN address_book.site S ON E.id_site = S.id_site
                            WHERE S.name """+type_recherche+""" %s AND V.id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot
       FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group
       WHERE id_usergroup = ANY(%s)))"""
        self.cur.execute(query,(site,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getCloneByNomAndTypeNom(self,nom,type_nom_,type_recherche,groups):
        type_nom = self.getTypeNom(type_nom_)[0]["id_type_nom"]
        query = """SELECT V.id, variete AS id_variete, V.numero AS "Nom accession", V.genre , V.espece, NV.value AS \" """ + type_nom_ + """\"
               FROM plant.vue_clone V
               LEFT JOIN plant.vue_variete VV ON V.variete = VV.id
               LEFT JOIN plant.variety_name NV ON NV.id_variety = V.variete
               WHERE NV.value """+ type_recherche+"""%s and id_variety_name_type=%s
               AND V.id IN (SELECT id_accession FROM  plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)))"""
        self.cur.execute(query,(nom,type_nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result
#
#//---------------------------------------Arbres
    def createArbre(self, id_clone, numero):
        return self.sqlI("INSERT INTO plant.tree (name, id_accession) VALUES (%s,%s) RETURNING id_lot",(numero, id_clone))

    def updateArbre(self, id_arbre, id_clone, numero):
        return self.sqlI("UPDATE plant.tree SET (name, id_accession) = (%s,%s) WHERE id_lot = %s RETURNING *;",(numero, id_clone, id_arbre,))

    def getArbre(self, id_arbre):
        return self.sqlE("SELECT id_lot, name AS numero_lot, id_accession AS clone,first_shoot_year AS annee_premiere_pousse FROM plant.tree WHERE id_lot=%s;",(id_arbre,))

#//---------------------------------------Graines

    def createGraines(self,id_clone, numero, equilibre, date_recolte, quantite, rang_evaluation):
        return self.sqlI("INSERT INTO plant.seed (name, clone, harvesting_date, quantity) VALUES (%s,%s,%s,%s) RETURNING id_lot",(numero, id_clone,  date_recolte, quantite))

    def updateGraines(self,id_graines, id_clone, numero, equilibre, date_recolte, quantite, rang_evaluation):
        return self.sqlI("UPDATE plant.seed SET (name, clone,  harvesting_date, quantity) = (%s,%s,%s,%s) WHERE id_lot=%s RETURNING id_lot", (numero, id_clone, date_recolte, quantite,id_graines,))

    def getGraines(self, id_graines):
        return self.sqlE("SELECT name AS numero_lot, id_accession AS clone, id_lot_source AS lot_parent,multiplication_date AS multiplication_date, harvesting_date AS date_recolte, quantity AS quantite FROM plant.seed WHERE id_lot=%s",(id_graines,))

#//--------------------------------------Requêtes sur les lots

    def getAllLot(self,groups):
        return self.sqlE("SELECT name AS numero_lot FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)) ORDER BY numero_lot ASC; ", (groups,))

    def getLotByGroupe(self,id_groupe):
        return self.sqlE("SELECT * FROM plant.lot WHERE id_lot IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group=%s)",(id_groupe,));

    def getLotById(self, id_lot):
        return self.sqlE("SELECT * FROM plant.vue_lot WHERE id = %s", (id_lot,))

    def getIdLotByNumeroArbre(self, numero):
        return self.sqlE("SELECT id_lot FROM plant.lot WHERE name=%s",(numero,))

    def getIdLotByNumeroGraines(self, numero):
        return self.sqlE("SELECT id_lot FROM plant.seed WHERE name=%s",(numero,))

    def getLotByNumero(self, numero, type_recherche,groups):
        query = """SELECT id, id_variete, numero_lot AS \"Nom lot\" , accession
        FROM plant.vue_lot
        WHERE numero_lot"""+type_recherche+ """%s
        AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s)) ORDER BY numero_lot ASC;"""
        self.cur.execute(query,(numero,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByNomClone(self, numero, type_recherche,groups):
        query = """SELECT id, id_variete, numero_lot AS \"Nom lot\" , accession
        FROM plant.vue_lot
        WHERE id_clone IN (SELECT id FROM plant.accession WHERE introduction_name"""+type_recherche+ """%s) AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(numero,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result


    def getLotByNomVariete(self, nom, type_recherche,groups): # lot by Nom
        query ="""SELECT DISTINCT id_lot AS id, id_variete, L.name AS "Nom lot", C.introduction_name AS accession
        FROM  plant.variety_name NV
        JOIN plant.variety V ON V.id_variete = NV.id_variety
        JOIN plant.accession C ON C.id_variety= V.id_variete
        JOIN plant.lot L ON L.id_accession = C.id
        WHERE NV.value""" + type_recherche + """%s
        AND id_lot IN
        (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group
        WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result


    def getLotByNumeroClone(self, numero, type_recherche,groups):
        query = """SELECT id, id_variete, numero_lot AS \"Nom lot\" , accession
        FROM plant.vue_lot
        WHERE accession"""+type_recherche+ """%s
         AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(numero,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByQuantite(self, quantite, type_recherche,groups):
        query = "SELECT id,type,numero_lot,accession,genre,espece FROM plant.vue_graines WHERE quantite "+type_recherche+ "%s AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"
        self.cur.execute(query,(quantite,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotBySite(self, site, type_recherche,groups):
        query = """SELECT DISTINCT V.id,id_variete,V.numero_lot AS \"Nom lot\", accession, S.name AS Site
                           FROM plant.vue_lot V
                           JOIN plant.place E ON  E.id_lot = V.id
                           JOIN address_book.site S ON E.id_site = S.id_site
                           JOIN plant.group_has_lot GL ON GL.id_lot= V.id
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE S.name """+type_recherche+""" %s AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(site,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByEquilibre(self, valeur, type_recherche,groups):
        query = "SELECT id,type,numero_lot,accession,genre,espece FROM plant.vue_graines WHERE equilibre "+type_recherche+"%s AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"
        self.cur.execute(query,(valeur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByLieu(self, lieu, type_lieu, type_recherche,groups):
        query = """ SELECT DISTINCT A.id , A.id_variete, C.value AS \""""+type_lieu+"""\",A.numero_lot AS \"Nom lot\", accession
        FROM plant.vue_lot A
        JOIN plant.place B ON A.id = B.id_lot
        JOIN location.place C ON B.id = C.id_plant_place
        JOIN location.place_type D ON D.id = C.id_place_type
        WHERE C.value """+type_recherche+""" %s AND D.value=%s
        AND A.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(lieu,type_lieu,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByRangEvaluation(self, rang, type_recherche,groups):
        query = "SELECT id,type,numero_lot,accession,genre,espece FROM plant.vue_graines WHERE  id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"
        self.cur.execute(query,(groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByAnneePremierePousse(self, annee, type_recherche,groups):
        query = """SELECT id,type,numero_lot,accession,genre,espece
        FROM plant.vue_arbre
        WHERE annee_premiere_pousse"""+type_recherche+"""%s
        AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(annee,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByClone(self, id_clone):
        return self.sqlE("SELECT * FROM plant.vue_lot WHERE id_clone=%s ",(id_clone,))

    def getArbreByClone(self, id_clone):
        return self.sqlE("SELECT id_lot AS id_lot, name AS numero_lot, id_accession AS clone, id_lot_source AS lot_parent, multiplication_date AS date_multiplication, first_shoot_year AS annee_premiere_pousse, rootstock AS porte_greffe  FROM plant.tree WHERE id_accession=%s ",(id_clone,))

    def getGrainesByClone(self, id_clone):
        return self.sqlE("SELECT id_lot AS id_lot, name AS numero_lot, id_accession AS clone, id_lot_source AS lot_parent,multiplication_date AS date_multiplication , harvesting_date AS date_recolte, quantity AS quantite FROM plant.seed WHERE id_accession=%s ",(id_clone,))

    def getLotByNotation(self, valeur, type_notation_, operateur,groups):
        type_notation_valeur = self.getTypeNotation(type_notation_)[0]["type"]
        type_notation = self.getTypeNotation(type_notation_)[0]["id_type_notation"]
        if operateur is not None:
            query = """SELECT DISTINCT id_variete,numero_lot AS \"Nom lot\", id, accession, ND.value  AS \" """ + type_notation_  +"""\"
            FROM plant.vue_lot VL
            JOIN plant.group_has_lot GL ON  VL.id = GL.id_lot
            JOIN plant.group_has_notation GN ON GN.id_group =GL.id_group
            JOIN notation.notation_"""+type_notation_valeur+""" ND  ON GN.id_notation = ND.id_notation
            WHERE value"""+operateur +""" %s AND ND.id_type_notation=%s
            AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
            self.cur.execute(query,(valeur, type_notation,groups))
        else :
            query = "SELECT * FROM plant.vue_lot WHERE id NOT IN (SELECT id_lot FROM plant.group_has_lot WHERE id_group IN(SELECT id_group FROM plant.group_has_notation WHERE id_notation IN (SELECT id_notation FROM notation.notation WHERE id_type_notation=%s))) AND id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"
            self.cur.execute(query,(type_notation,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result


    def getLotByNomAndTypeNom(self,nom,type_nom_,type_recherche,groups):
        type_nom = self.getTypeNom(type_nom_)[0]["id_type_nom"]
        query = """SELECT DISTINCT VL.id_variete,numero_lot AS \"Nom lot\", VL.id,id_clone, accession , NV.value AS \" """ + type_nom_ + """\"
        FROM plant.vue_lot VL
        JOIN plant.vue_clone VC ON  VL.id_clone = VC.id
        JOIN plant.vue_variete  VV ON VV.id = VC.variete
        JOIN plant.variety_name NV ON  VV.id = NV.id_variety
        WHERE NV.value """+ type_recherche+"""%s and NV.id_variety_name_type=%s
        AND VL.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(nom,type_nom,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result


    def getLotByCollection(self, collection, type_recherche,groups):
        query = """SELECT VL.id_variete, numero_lot AS \"Nom lot\", accession
                 FROM plant.vue_lot VL
                 JOIN plant.vue_clone VC ON  VL.id_clone = VC.id
                 JOIN collection.collection_has_accession CAC ON CAC.id_clone=VC.id
                 JOIN collection.collection CC ON CC.id_collection=CAC.id_collection
                 WHERE CC.name"""+type_recherche+ """%s AND VL.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query,(collection,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByDateIntroduction(self, date, type_recherche,groups):
        query = """SELECT DISTINCT V.id,id_variete,V.numero_lot AS \"Nom lot\", accession ,date_intro AS \"Date d'introduction\"
        FROM plant.vue_lot V
                           JOIN plant.accession C ON  V.id_clone = C.id
                           JOIN plant.lot L ON C.id = L.id_accession
                           JOIN plant.group_has_lot GL ON GL.id_lot= L.id_lot
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE C.introduction_date """+type_recherche+""" to_date('"""+date+"""', 'DD/MM/YYYY' ) AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(groups,))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByFournisseur(self, fournisseur, type_recherche,groups):
        query = """SELECT DISTINCT V.id,id_variete,V.numero_lot AS \"Nom lot\", accession ,P.lastname AS Fournisseur
        FROM plant.vue_lot V
                           JOIN plant.accession C ON  V.id_clone = C.id
                           JOIN address_book.address P ON C.provider = P.id_pepinieriste
                           JOIN plant.lot L ON C.id = L.id_accession
                           JOIN plant.group_has_lot GL ON GL.id_lot = L.id_lot
                           JOIN plant.group_has_usergroup gg ON GG.id_group = GL.id_group
                     WHERE P.lastname """+type_recherche+""" %s AND GG.id_usergroup = ANY(%s)"""
        self.cur.execute(query,(fournisseur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByObtenteur(self,obtenteur,type_recherche,groups):
        query = """SELECT DISTINCT V.id,id_variete,V.numero_lot AS \"Nom lot\", accession , Obtenteur
                           FROM plant.vue_lot V
                           JOIN plant.vue_clone VC ON VC.id = V.id_clone
                           JOIN plant.vue_variete VV ON VV.id = VC.variete
        WHERE VV.obtenteur"""+type_recherche+"""%s
        AND V.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query, (obtenteur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByGenre(self, genre,type_recherche,groups):
        query = """SELECT DISTINCT L.id,id_variete,L.numero_lot AS \"Nom lot\", accession ,V.genre
        FROM plant.vue_lot L
                            JOIN plant.vue_clone C   ON  L.id_clone = C.id
                            JOIN plant.vue_variete V  ON C.variete = V.id
                    WHERE V.genre"""+type_recherche+""" %s
                    AND L.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup =ANY(%s) )"""
        self.cur.execute(query, (genre,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByEspece(self, espece,type_recherche,groups):
        query = """SELECT DISTINCT L.id,id_variete,L.numero_lot AS \"Nom lot\", accession ,V.espece
        FROM plant.vue_lot L
                            JOIN plant.vue_clone C   ON  L.id_clone = C.id
                            JOIN plant.vue_variete V  ON C.variete = V.id
                    WHERE V.espece"""+type_recherche+""" %s
                    AND L.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup =ANY(%s) )"""
        self.cur.execute(query, (espece,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

    def getLotByEditeur(self, editeur,type_recherche,groups):
        query = """SELECT DISTINCT V.id,id_variete,V.numero_lot AS \"Nom lot\", accession , editeur FROM plant.vue_lot V
                           JOIN plant.vue_clone VC ON VC.id = V.id_clone
                           JOIN plant.vue_variete VV ON VV.id = VC.variete
        WHERE VV.editeur"""+type_recherche+"""%s
        AND V.id IN (SELECT id_lot FROM plant.group_has_usergroup A JOIN  plant.group_has_lot B ON A.id_group=B.id_group WHERE id_usergroup = ANY(%s))"""
        self.cur.execute(query, (editeur,groups))
        result = self.cur.fetchall()
        for i in result:
            idVar = i["id_variete"]
            i["nom usuel"]=self.getNomUsuelByIdVariete(idVar);
        return result

#//--------------------------------------Emplacements
    def getEmplacementByLot(self, id_lot):
        return self.sqlE("SELECT id AS id_emplacement, id_lot AS lot, id_site AS site, plantation_date AS date_plantation, removing_date AS date_arrachage FROM plant.place WHERE id_lot = %s", (id_lot,))

    def getEmplacement(self, id_emplacement):
        return self.sqlE("SELECT id AS id_emplacement, id_lot AS lot, id_site AS site, plantation_date  AS date_plantation, removing_date AS date_arrachage  FROM plant.place WHERE id = %s", (id_emplacement,))

    def createEmplacement(self, id_lot, site):
        return self.sqlI("INSERT INTO plant.place (id_site,id_lot) VALUES (%s,%s) RETURNING id;",(site,id_lot))

    def updateEmplacement(self, id_emplacement, site):
        return self.sqlI("UPDATE plant.place SET (id_site)=(%s) WHERE id=%s RETURNING id;",(site,id_emplacement))

#//--------------------------------------Lieux
    def createLieu(self, id_emplacement, type_lieu, lieu):
        return self.sqlI("INSERT INTO location.place (id_plant_place, id_place_type, value) VALUES (%s,%s,%s) RETURNING id_lieu",(id_emplacement, type_lieu, lieu))

    def updateLieu(self, id_lieu, type_lieu, valeur):
        return self.sqlI("UPDATE location.place SET (id_place_type,value)=(%s,%s) WHERE id_lieu=%s RETURNING *",(type_lieu,valeur,id_lieu))

    def deleteLieu(self, id_lieu):
        return self.sqlI("DELETE FROM location.place WHERE id_lieu=%s RETURNING *",(id_lieu,))

    def getLieu(self,id_lieu):
        return self.sqlE("SELECT id_lieu AS id_lieu, id_plant_place AS emplacement, id_place_type AS type_lieu, value AS valeur  FROM location.place WHERE id_lieu = %s",(id_lieu,))

    def getLieuxByEmplacement(self,id_emplacement):
        return self.sqlE("SELECT id_lieu AS id_lieu, id_plant_place AS emplacement, id_place_type AS type_lieu, value AS valeur FROM location.place WHERE id_plant_place = %s",(id_emplacement,))

    def deleteLieuByEmplacement(self, id_emplacement):
        return self.sqlI("DELETE FROM location.place WHERE id_plant_place = %s RETURNING id_lieu", (id_emplacement,))

    def getLieuByLot(self, id_lot):
        return self.sqlE(" SELECT * FROM plant.vue_lieu L LEFT JOIN plant.place E on L.emplacement=E.id WHERE id_lot = %s", (id_lot,))
    def getAllLieuByTypeLieu(self, type_lieu):
        return self.sqlE("SELECT distinct value AS valeur FROM location.place WHERE id_place_type IN (SELECT id FROM location.place_type WHERE value=%s)",(type_lieu,))

#//--------------------------------------Types notation

    def getAllTypeNotation(self):
        return self.sqlE("SELECT id_type_notation, name AS nom, type FROM notation.notation_type ORDER BY name ASC",())

    def createTypeNotation(self, nom, type_notation):
        return self.sqlI("INSERT INTO notation.notation_type (name, type) VALUES (%s,%s) RETURNING *",(nom, type_notation))

    def updateTypeNotation(self, nom,id_type_notation):
        return self.sqlI("UPDATE notation.notation_type SET (name) = (%s) WHERE id_type_notation=%s RETURNING *",(nom, id_type_notation))

    def getTypeByTypeNotation(self, type_notation):
        return self.sqlE("SELECT type FROM notation.notation_type WHERE name=%s",())

    def getTypeNotationById(self,id_type_notation):
        return self.sqlE("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE id_type_notation=%s",(id_type_notation,))

    def getTypeNotation(self,type_notation):
        return self.sqlE("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE name=%s",(type_notation,))

#//--------------------------------------Groupes
    def getNextIdGroupe(self):
        return self.sqlE("SELECT last_value FROM plant.plant_group_id_groupe_seq;",())

    def incrementGroup(self):
        return self.sqlE("SELECT nextval('plant.plant_group_id_groupe_seq');",())

    def createIdGroupe(self,nom, description):
        return self.sqlI("INSERT INTO plant.group (name, description) VALUES(%s, %s) returning *",(nom,description))

    def createGroupe(self,id_lot, description):
        nomlot = self.getLotById(id_lot)[0]["numero_lot"];
        id_groupe = self.createIdGroupe(nomlot,description)[0]["id_group"];
        return self.sqlI("INSERT INTO plant.group_has_lot (id_group,id_lot) VALUES (%s,%s) RETURNING *",(id_groupe,id_lot))

    def getGroupeByLot(self, numero_lot):
        return self.sqlE("SELECT id_lot AS lot,id_group AS id_group FROM plant.group_has_lot WHERE id_lot IN (SELECT id_lot FROM plant.lot WHERE name=%s)",(numero_lot,))

    def getGroupeByIdLot(self, id_lot):
        return self.sqlE("SELECT id_lot AS lot,id_group AS id_group FROM plant.group_has_lot WHERE id_lot IN (SELECT id_lot FROM plant.lot WHERE id_lot=%s)",(id_lot,))

    def getGroupeByClone(self, id_clone):
        return self.sqlE("SELECT id_group FROM plant.group_has_lot WHERE id_lot IN (SELECT id_lot FROM plant.lot WHERE id_accession=%s)",(id_clone,))

#//--------------------------------------Groupe_a_notation
    def setGroupeANotation(self, id_groupe, id_notation):
        return self.sqlI("INSERT INTO plant.group_has_notation (id_group, id_notation) VALUES (%s,%s) RETURNING *",(id_groupe, id_notation))

#//--------------------------------------Notations
    def createNotation(self,date,remarque,site,type_notation,valeur,observer):
        typeNotation = self.getTypeNotation(type_notation)[0];
        type_valeur = typeNotation["type"];
        type_notation = typeNotation["id_type_notation"]
        if (type_valeur =="integer"):
            return self.sqlI("INSERT INTO notation.notation_integer (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="texte"):
            return self.sqlI("INSERT INTO notation.notation_texte (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="double"):
            return self.sqlI("INSERT INTO notation.notation_double (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))
        if (type_valeur =="date"):
            return self.sqlI("INSERT INTO notation.notation_date (date, comment, id_site, observer,value,id_type_notation) VALUES (%s,%s,%s,%s,%s,%s) RETURNING *",(date,remarque,site,observer,valeur,type_notation))


    def updateNotation(self,id_notation,date,remarque,site,type_notation,valeur,observer):
        typeNotation = self.getTypeNotation(type_notation)[0];
        type_valeur = typeNotation["type"];
        type_notation = typeNotation["id_type_notation"]
        if (type_valeur =="integer"):
            return self.sqlI("UPDATE notation.notation_integer SET (date, comment, id_site, observer,value,id_type_notation) = (%s,%s,%s,%s,%s,%s) WHERE id_notation=%s RETURNING id_notation",(date,remarque,site,observer,valeur,type_notation,id_notation))
        if (type_valeur =="texte"):
            return self.sqlI("UPDATE notation.notation_texte SET (date, comment, id_site, observer,value,id_type_notation) = (%s,%s,%s,%s,%s,%s) WHERE id_notation=%s RETURNING id_notation",(date,remarque,site,observer,valeur,type_notation,id_notation))
        if (type_valeur =="double"):
            return self.sqlI("UPDATE notation.notation_double SET(date, comment, id_site, observer,value,id_type_notation) = (%s,%s,%s,%s,%s,%s) WHERE id_notation=%s RETURNING id_notation",(date,remarque,site,observer,valeur,type_notation,id_notation))
        if (type_valeur =="date"):
            return self.sqlI("UPDATE notation.notation_date SET (date, comment, id_site, observer,value,id_type_notation) = (%s,%s,%s,%s,%s,%s,%s) WHERE id_notation=%s RETURNING id_notation",(date,remarque,site,observer,valeur,type_notation,id_notation))

    def deleteNotation(self, id_notation):
        self.cur.execute("DELETE FROM plant.group_has_notation WHERE id_notation=%s",(id_notation,));
        return self.sqlI("DELETE FROM notation.notation WHERE id_notation=%s",(id_notation,))

    # A remplacer par PlantTools.getNotationIdsForLot et PlantTools.getNotations
    def getNotationsByLot(self, id_lot):
        self.cur.execute("""SELECT N.value AS valeur ,GL.id_lot AS lot, N.id_notation,TN.name as type_notation,N.date AS date_notation,S.name as nom_site,N.observer AS notateur,TN.type,N.comment AS remarque
                          FROM notation.notation_texte N
                             LEFT JOIN address_book.site S ON N.id_site=S.id_site
                             LEFT JOIN notation.notation_type TN ON N.id_type_notation = TN.id_type_notation
                             LEFT JOIN plant.group_has_notation GN ON GN.id_notation = N.id_notation
                             LEFT JOIN  plant.group_has_lot GL ON GL.id_group= GN.id_group
                            WHERE GL.id_lot=%s
UNION
SELECT CAST( N.value AS VARCHAR(10) )AS valeur,GL.id_lot AS lot, N.id_notation,TN.name as type_notation,N.date AS date_notation,S.name as nom_site,N.observer AS notateur,TN.type ,N.comment AS remarque
                          FROM notation.notation_integer N
                             LEFT JOIN address_book.site S ON N.id_site=S.id_site
                             LEFT JOIN notation.notation_type TN ON N.id_type_notation = TN.id_type_notation
                             LEFT JOIN plant.group_has_notation GN ON GN.id_notation = N.id_notation
                             LEFT JOIN  plant.group_has_lot GL ON GL.id_group= GN.id_group
                            WHERE GL.id_lot=%s
                            UNION
SELECT CAST( N.value AS VARCHAR(10) )AS valeur,GL.id_lot AS lot, N.id_notation,TN.name as type_notation,N.date AS date_notation,S.name as nom_site,N.observer AS notateur,TN.type ,N.comment AS remarque
                          FROM notation.notation_double N
                             LEFT JOIN address_book.site S ON N.id_site=S.id_site
                             LEFT JOIN notation.notation_type TN ON N.id_type_notation = TN.id_type_notation
                             LEFT JOIN plant.group_has_notation GN ON GN.id_notation = N.id_notation
                             LEFT JOIN  plant.group_has_lot GL ON GL.id_group= GN.id_group
                            WHERE GL.id_lot=%s
UNION
SELECT CAST( N.value AS VARCHAR(10) )AS valeur,GL.id_lot AS lot, N.id_notation,TN.name as type_notation,N.date AS date_notation,S.name as nom_site,N.observer AS notateur,TN.type,N.comment AS remarque
                          FROM notation.notation_date N
                             LEFT JOIN address_book.site S ON N.id_site=S.id_site
                             LEFT JOIN notation.notation_type TN ON N.id_type_notation = TN.id_type_notation
                             LEFT JOIN plant.group_has_notation GN ON GN.id_notation = N.id_notation
                             LEFT JOIN  plant.group_has_lot GL ON GL.id_group= GN.id_group
                            WHERE GL.id_lot=%s """,(id_lot,id_lot,id_lot,id_lot, ))
        rows = self.cur.fetchall()
        return rows

    # remplacer par PlantTools.getNotations
    def getNotationById(self, id_notation):
        self.cur.execute("""SELECT id_notation,notation_type.name as type_notation,date AS date_notation,address_book.site.name AS nom_site,observer AS notateur,notation_type.type FROM notation.notation
                            LEFT JOIN address_book.site ON notation.id_site=site.id_site
                            LEFT JOIN notation.notation_type ON notation.notation.id_type_notation = notation.notation_type.id_type_notation
                            WHERE id_notation = (%s)""",(id_notation,))
        rows = self.cur.fetchall()
        for row in rows:
            sql = "SELECT * FROM notation.notation_"+row['type']+" N ,notation.notation_type WHERE id_notation= %s AND notation.notation_type.id_type_notation=N.id_type_notation"
            self.cur.execute(sql,(row['id_notation'],))
            valeur = self.cur.fetchall()[0]
            champ = "value"
            row['valeur']= valeur[champ]
        return rows


    def getAllValeurByNotation(self,type_notation):
        self.cur.execute("SELECT id_type_notation, name AS nom, type FROM notation.notation_type WHERE name=%s;",(type_notation,))
        rows = self.cur.fetchall()
        type_notation = rows[0]["type"]
        id_type_notation = rows[0]["id_type_notation"]
        return self.sqlE("SELECT distinct value as valeur FROM notation.notation_"+type_notation+" WHERE id_type_notation=%s ORDER BY value ASC",(id_type_notation,))

#//--------------------------------------Multiplication
    def createMultiplication(self,grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale):
        return self.sqlI("INSERT INTO plant_cross.multiplication (grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale) VALUES (%s,%s,%s,%s,%s,%s,%s) RETURNING *;",(grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale))

    def updateMultiplication(self,id_multiplication,grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale):
        return self.sqlI("UPDATE plant_cross.multiplication SET (grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale) = (%s,%s,%s,%s,%s,%s,%s) WHERE id=%s RETURNING *;",(grp_ascendant,grp_descendant,conforme,multiplicateur,remarque,nb_porte_graines,quantite_initiale,id_multiplication))

    def getMultiplicationByLotDesc(self,id_lot):
        return self.sqlE("SELECT * FROM plant_cross.multiplication WHERE grp_descendant IN (SELECT id_group FROM plant.group_has_lot WHERE id_lot=%s)",(id_lot,))

    def getMultiplicationById(self,id_multiplication):
        return self.sqlE("SELECT * FROM plant_cross.multiplication WHERE id=%s",(id_multiplication,));

    def deleteMultiplicationByLotDesc(self, id_lot):
        return self.sqlI("DELETE FROM plant_cross.multiplication WHERE grp_descendant IN (SELECT id_group FROM plant.group_has_lot WHERE id_lot=%s) RETURNING *",(id_lot,))

#//--------------------------------------Compteurs
# Commenter le 13 juin 2019 (décision collégiale)
# TODO : Si pas de problème côté utilisateurs,
# supprimer la table plant.cpt_numero_clone de la database
    # def incrementCptRosa(self):
    #     return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_rosa)=(cpt_rosa+1) RETURNING *",())
    #
    # def incrementCptMalus(self):
    #     return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_malus)=(cpt_malus+1) RETURNING *",())
    #
    # def incrementCptDaucus(self):
    #     return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_daucus)=(cpt_daucus+1) RETURNING *",())
    #
    # def getNextValueCpt(self,genre):
    #     if (genre=="Rosa"):
    #         self.incrementCptRosa()
    #         return self.sqlE("SELECT char_rosa as char,cpt_rosa as cpt FROM plant.cpt_numero_clone",())
    #     if (genre =="Malus"):
    #         self.incrementCptMalus()
    #         return self.sqlE("SELECT char_malus as char,cpt_malus as cpt FROM plant.cpt_numero_clone",())
    #     if (genre=="Daucus"):
    #         self.incrementCptDaucus()
    #         return self.sqlE("SELECT char_daucus as char,cpt_daucus as cpt FROM plant.cpt_numero_clone",())
    #
    # def updateNumeroCloneValue(self,genre,numero):
    #     if (genre == "Rosa"):
    #         return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_rosa)=(%s) RETURNING *",(numero,))
    #     if (genre == "Daucus"):
    #         return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_daucus)=(%s) RETURNING *",(numero,))
    #     if (genre == "Malus"):
    #         return self.sqlI("UPDATE plant.cpt_numero_clone SET (cpt_malus)=(%s) RETURNING *",(numero,))
