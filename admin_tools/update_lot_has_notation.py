#!/usr/bin/env python3

import sys
import json
from tqdm import tqdm
sys.path.insert(0, "../lib/")

from elvis import PlantTools

pt = PlantTools()

nids = pt.getLotHasNotationValues()
cpt = 0
for i in tqdm(nids):
  if (pt.insertLotHasNotation(i)):
    cpt += 1

print("Inserted: {0}/{1}".format(cpt, len(nids)))
