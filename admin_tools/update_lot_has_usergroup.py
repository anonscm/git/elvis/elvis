#!/usr/bin/env python3

import sys
import json
from tqdm import tqdm
sys.path.insert(0, "../lib/")

from elvis import PlantTools

pt = PlantTools()

nids = pt.getLotHasUsergroupValues()
cpt = 0
for i in tqdm(nids):
  if (pt.insertLotHasUsergroup(i)):
    cpt += 1

print("Inserted: {0}/{1}".format(cpt, len(nids)))
